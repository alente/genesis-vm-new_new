<?php

namespace app\models;

//use app\components\behaviors\DateBehavior;
use Yii;
use app\components\behaviors\HandleBehavior;
/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $handle
 * @property string $image
 * @property string $description
 * @property string $text
 * @property integer $weight
 * @property integer $public
 */
class Stock extends CActiveRecord
{
  
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['description', 'text'], 'string'],
            [['public'], 'integer'],
            [['vin', 'model', 'complectation', 'color', 'suffix', 'year', 'location', 'status', 'price'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vin' => 'VIN',
            'model' => 'Модель',
            'complectation' => 'Комплектация',
            'color' => 'Цвет',
            'suffix' => 'Суффикс',
            'year' => 'Год',
            'location' => 'Салон',
            'status' => 'Статус',
            'price' => 'Цена',
            'public' => 'Публикация',
        ];
    }
	
    public function getColor() {
        $item = StockColor::find()->where(['code'=>$this->color])->one()->color;
        if ($item) return $item;
        return $this->color;
    }
	
    public function getCarImage() {
        $item = StockColor::find()->where(['code'=>$this->color])->one()->color_simple;
		if (!$item) $item = 'Gray';
		return '/images/stock/'.$this->model.'/simple/'.$item.'.jpg';
    }
	
    public function getComplName() {
        $item = StockComplectation::find()->where(['code'=>$this->complectation])->one()->name;
        if ($item) return $item;
        return $this->complectation;
    }
	
    public function getComplTech() {
        $item = StockComplectation::find()->where(['code'=>$this->complectation])->one()->complectation;
        if ($item) return $item;
        return false;
    }

}