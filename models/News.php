<?php

namespace app\models;

use app\components\behaviors\DateBehavior;
use Yii;
use app\components\behaviors\HandleBehavior;
/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $handle
 * @property string $image
 * @property string $description
 * @property string $text
 * @property integer $weight
 * @property integer $public
 */
class News extends CActiveRecord
{
        public $imagedeleting;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'text'], 'string'],
            [['weight', 'public', 'public_on_main'], 'integer'],
            [['title', 'handle', 'image_alt'], 'string', 'max' => 255],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'handle' => 'Алиас',
            'image' => 'Изображение',
            'image_preview' => 'Изображение для анонса',
            'image_alt' => 'Изображение альт',
            'description' => 'Описание',
            'text' => 'Содержание',
            'date' => 'Дата',
            'weight' => 'Порядок',
            'public' => 'Публикация',
            'public_on_main' => 'Выводить на главной',
            'imagedeleting' => 'Удалить изображение?',
        ];
    }

    public function behaviors() {
        return [
            'handleBehavior' => [
                'class' => HandleBehavior::className(),
            ],
            'dateBehavior' => [
                'class' => DateBehavior::className(),
                'dates' => ['date'],
            ],
        ];
    }
}