<?php

namespace app\models;

use app\components\behaviors\HandleBehavior;
use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sections".
 *
 * @property integer $id
 * @property integer $depth
 * @property integer $lft
 * @property integer $rgt
 * @property integer $tree
 * @property string $title
 * @property string $handle
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property integer $in_menu
 * @property integer $public
 */
class Sections extends CActiveRecord
{
    public $imagedeleting;
    
    public $tree_parent_id = false;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sections';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required', 'on' => 'default'],
            [['in_menu', 'public', 'in_side', 'in_footer', 'goto_child', 'tree_handle'], 'integer'],
            [['title', 'h1title', 'image', 'seo_title', 'seo_keywords', 'real_handle'], 'string', 'max' => 255],
            [['seo_description', 'handle'], 'string', 'max' => 500],
            [['tree_parent_id', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'depth' => 'Depth',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'tree' => 'Tree',
            'title' => 'Наименование',
            'h1title' => 'H1',
            'handle' => 'Генерируемый алиас',
            'real_handle' => 'Алиас',
            'tree_handle' => 'Самостоятельный алиас?',
            'seo_title' => 'Seo Title',
            'seo_keywords' => 'Seo Keywords',
            'seo_description' => 'Seo Description',
            'image' => 'Изображение',
            'imagedeleting' => 'Удалить изображение?',
            'in_menu' => 'Главное меню',
            'in_side' => 'Боковое меню',
            'in_footer' => 'Нижнее меню',
            'public' => 'Публикация',
            'tree_parent_id' => 'Родительский раздел',
            'description' => 'Описание',
            'type' => 'Тип отображения',
            'preview_type' => 'Вид блока анонса (только при отображении "сеткой" в родительском разделе)',
            'children' => 'Категория (Только для услуг)',
            'ask' => 'Выводить модуль "Задать вопрос"',
            'sections' => 'Выводить список услуг',
            'goto_child' => 'Редирект на первый дочерний элемент',
        ];
    }

    public function behaviors() {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::className(),
            ],
            'handleBehavior' => [
                'class' => HandleBehavior::className(),
                'attribute' => 'real_handle',
            ],
            'timeStamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ]
        ];
    }

    public function beforeSave($insert)
    {
        $res = parent::beforeSave($insert);

        if (($this->tree_handle != $this->getOldAttribute('tree_handle') || $this->real_handle != $this->getOldAttribute('real_handle') || $this->isNewRecord) && $this->handle != 'root') {
            if ($this->tree_handle == 1) {
                $this->handle = $this->real_handle;
            } else {
                $parent = $this->parents(1)->one();
                if ($parent && $parent->handle != '' && $parent->handle != 'root') {
                    $this->handle = $parent->handle . '/' . $this->real_handle;
                } else {
                    $this->handle = $this->real_handle;
                }
            }

            $this->changeChildHandle();
        }

        return $res;
    }

    private function changeChildHandle() {
        $children = $this->children(1)->all();
        foreach($children as $child) {
            if ($child->tree_handle != 1) {
                $child->handle = $this->handle . '/' . $child->real_handle;
                $child->save();

                $child->changeChildHandle();
            }
        }
    }

    public static function getDropDownList($type = false, $public = false, $defaultValue = false, $orderBy = false) {
        $result = array();
        $sections = Sections::find()->orderBy('lft')->all();
        foreach($sections as $section) {
            $result[$section->id] = str_repeat('–', $section->depth) . ' ' . $section->title . ' [' . $section->handle . ']';
        }
        return $result;
    }
    
    public function getBlocks() {
        return $this->hasMany(Blocks::className(), ['parent_id' => 'id'])->where(['public'=>1])->orderBy('weight');
    }
}
