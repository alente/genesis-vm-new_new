<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property integer $id
 * @property string $title
 * @property string $value
 * @property integer $weight
 */
class Contacts extends \yii\db\ActiveRecord
{
    public static $types = [
        1 => 'Текст',
        2 => 'HTML',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'string'],
            [['weight', 'type'], 'integer'],
            [['title', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'value' => 'Значение',
            'weight' => 'Порядок',
            'code' => 'Код',
            'type' => 'Тип',
        ];
    }

    public static $configs = [];
    public static function getParam($code) {
        if (!isset($configs[$code])) {
            $configs[$code] = '';
            $config = Contacts::find()->where(['code' => $code])->one();
            if ($config) $configs[$code] = $config->value;
        }
        
        return $configs[$code];
    }
}
