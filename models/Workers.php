<?php

namespace app\models;

use app\components\behaviors\DateBehavior;
use Yii;

/**
 * This is the model class for table "workers".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $button
 * @property string $link
 * @property integer $weight
 * @property integer $public
 */
class Workers extends CActiveRecord
{
    public static $types = [
        0 => 'Продажи',
        1 => 'Сервис',
    ];
    
    public static $handleTypes = [
        'sell' => 0,
        'service' => 1,
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'workers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'weight', 'public', 'type'], 'integer'],
            [['name', 'second_name', 'first_name', 'position', 'image', 'mail', 'phone', 'phone_link'], 'string', 'max' => 255],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'first_name' => 'Фамилия',
            'second_name' => 'Отчество',
            'position' => 'Должность',
            'mail' => 'E-mail',
            'phone' => 'Телефон',
            'phone_link' => 'Ссылка на телефоне',
            'image' => 'Фото',
            'weight' => 'Порядок',
            'public' => 'Публикация',
            'type' => 'Отдел',
            'date' => 'Дата',
        ];
    }

    public function behaviors() {
        return [
            'dateBehavior' => [
                'class' => DateBehavior::className(),
                'dates' => ['date'],
            ],
        ];
    }
}
