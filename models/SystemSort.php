<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "system_sort".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $model
 * @property string $column
 * @property integer $sort
 * @property integer $partial
 */
class SystemSort extends CActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_sort';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'sort', 'partial', 'pagesize'], 'integer'],
            [['model', 'column'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'model' => 'Model',
            'column' => 'Column',
            'sort' => 'Sort',
            'partial' => 'Partial',
            'pagesize' => 'Page Size',
        ];
    }

    public function behaviors() {
        return [];
    }
}