<?php

namespace app\models;

use app\components\behaviors\HandleBehavior;
use Yii;

/**
 * This is the model class for table "blocks".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $title
 * @property string $text
 * @property string $image
 * @property integer $weight
 * @property integer $public
 */
class Blocks extends \yii\db\ActiveRecord
{
    public static $types = [
        1 => 'Обычный текст',
        2 => 'Галерея',
    ];

    public static $typesTemplate = [
        1 => 'Text',
        2 => 'Gallery',
    ];

    public static $staticModel = [
        /*'Reviews',
        'Maps',
        'Faq',*/
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template_id', 'parent_id', 'weight', 'public', 'topmenu'], 'integer'],
            [['title', 'h2title', 'handle'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'template_id' => 'Тип страницы',
            'title' => 'Наименование',
            'h2title' => 'Выводимое наименование',
            'weight' => 'Порядок',
            'public' => 'Публикация',
            'topmenu' => 'Якорь?',
            'handle' => 'Алиас',
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['search'] = $scenarios['default'];
        return $scenarios;
    }

    public function behaviors() {
        return [
            'handleBehavior' => [
                'class' => HandleBehavior::className(),
            ],
        ];
    }

    public function template() {
        if ($this->template_id && !empty(self::$typesTemplate[$this->template_id])) {
            return self::$typesTemplate[$this->template_id];
        }
        return false;
    }

    public function getBlock() {
        $template = $this->template();
        if ($template) {
            $classname = '\\app\\models\\blocks\\Block'.$template;
            if (in_array($template, self::$staticModel)) {
                return new $classname;
            } else {
                return $this->hasOne($classname, ['parent_id' => 'id']);
            }
        }
        return false;
    }

    public function getBlockClass() {
        $template = $this->template();
        if ($template) {
            return '\\app\\models\\blocks\\Block'.$template;
        }
        return false;
    }
}
