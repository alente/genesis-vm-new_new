<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

$params = require(__DIR__ . '/params.php');
$db = array_merge(
	require(__DIR__ . '/db.php'),
	require(__DIR__ . '/db-local.php')
);

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
	    'pressReleaseLoader' => [
		    'class' => 'app\components\PressReleaseLoader',
		    'apiUrl' => 'https://press.genesis.ru/api/press/',
	    ],
	    'logger' => [
		    'class' => 'app\components\logger\Logger',
		    'log' => [
			    'press' => [
				    'type' => 'text',
			    ]
		    ],
	    ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];
