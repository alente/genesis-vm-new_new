<?php

return [
	'dealer' => [
		/**
		 * Название Вашей компании
		 */
		'name' => "Восток Моторс",
		/**
		 * Адрес Вашей компании
		 */
		'address' => 'г. Пермь, Шоссе Космонавтов, 316в',
		/**
		 * Телефон
		 */
		'phone' => '+7 (342) 205-54-99',
		/**
		 * ID города в CRM
		 */
		"city_id" => 1,
		/**
		 * Код дилера в CRM
		 */
		"code" => "C40AF01117",
		/**
		 * Координаты для Google карты
		 */
		"longitude" => "56.096574",
		"latitude" => "57.950178",
		/**
		 * Email дилера для отправки сообщения
		 */
		'email' => "info@hyundai-vm.ru",
		/**
		 * ID приложения Google Search
		 */
		'searchId' => '010400548130150922617:sz2vhytfpmc',
		/**
		 * Ключ API Google для поиска карт
		 */
		'apiKey' => 'AIzaSyCxu_QCKjOYRqnoONqc7X-FsicHN8Vicw4',
	],
];
