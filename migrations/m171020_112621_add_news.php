<?php

use yii\db\Schema;
use yii\db\Migration;

class m171020_112621_add_news extends Migration
{
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('news', [
            'id'                  => $this->primaryKey(),
            'name'                => $this->string(255),
            'active'              => $this->boolean(),
            'code'                => $this->string(255),
            'preview_description' => $this->string(255),
            'description'         => $this->text(),
            'preview_file'        => $this->string(255),
            'date'                => $this->date(),
            'detail_file'         => $this->string(255),
            'old_url'             => $this->string(255),
        ], $tableOptions);

    }

    public function down()
    {
        echo "m171020_112621_add_news cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
