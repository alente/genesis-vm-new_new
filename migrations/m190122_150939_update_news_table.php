<?php

use yii\db\Migration;

class m190122_150939_update_news_table extends Migration
{
    public function safeUp()
    {
	    $this->addColumn('news', 'press_id', 'integer'/*$this->integer()->after('id')*/);
    }

    public function safeDown()
    {
        echo "m190122_150939_update_news_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190122_150939_update_news_table cannot be reverted.\n";

        return false;
    }
    */
}
