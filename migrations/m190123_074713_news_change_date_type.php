<?php

use yii\db\Migration;

class m190123_074713_news_change_date_type extends Migration
{
    public function safeUp()
    {
	    $this->alterColumn('news', 'date', $this->dateTime());
    }

    public function safeDown()
    {
        echo "m190123_074713_news_change_date_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190123_074713_news_change_date_type cannot be reverted.\n";

        return false;
    }
    */
}
