<?php

namespace app\components;

use app\components\helpers\SystemTools;
use app\models\SystemSort;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use yii;
use yii\db\Query;

class AdminController extends CommonAdminController
{
    public $modelName='CActiveRecord';
    public $fullModelName='CActiveRecord';
    public $modelTitles = ['Сущность', 'Сущности', 'Сущностей'];
    public $pageSize = 20;
    public $images = [];
    /*public $images = [
        'image' => '@webroot/up/sections'
    ];*/
    public $children = [];
    /*public $children = [
        'blocks' => ['Блоки', 'Добавить блок'],
    ];*/
    public $parents = [];
    /*public $parents = [
        'section' => 'Назад к разделу',
    ];*/
    public $searchFields = [];
    /*
    public $searchFields = [
        'public' => ['type' => 'public'],
        'id' => ['type' => 'number'],
        'title' => ['type' => 'string'],
        'start_date' => ['type' => 'date'],
    ];
     */
    public $links = [];

    public function init(){
        parent::init();

        $this->fullModelName = '\\app\\models\\' . $this->modelName;
    }

    public function actionIndex($parent = false, $partial = false, $sort = false, $ps = false)
    {
        if (!empty($_GET['ps'])) $ps = $_GET['ps'];
        if ($sort !== false || $ps !== false) {
            $sSort = SystemSort::findOne(['user_id' => Yii::$app->user->id, 'model' => strtolower($this->modelName), 'partial' => $partial?1:0]);
            if (!$sSort) $sSort = new SystemSort();
            $sSort->user_id = Yii::$app->user->id;
            $sSort->model = strtolower($this->modelName);
            if ($sort !== false) {
                $sSort->column = preg_replace('/^\-/', '', $sort);
                $sSort->sort = preg_match('/^\-/', $sort) ? 2 : 1;
            } elseif($sSort->isNewRecord) {
                $sSort->column = 'id';
                $sSort->sort = 1;
            }
            if ($ps !== false) {
                if ($ps == 'all') {
                    $sSort->pagesize = 0;
                } else {
                    $sSort->pagesize = $ps;
                }
            } elseif($sSort->isNewRecord) {
                $sSort->pagesize = 20;
            }
            $sSort->partial = $partial?1:0;
            $sSort->save();
        }

        $model = $this->initModel();

        $dataProvider = $this->dataProviderModel(\Yii::$app->request->get(), $parent, $partial);

        if (!$partial) {
            return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider[0], 'item' => $dataProvider[1]]);
        } else {
            return preg_replace('/<h1>.*?<\/h1>/', '', $this->renderPartial('index', ['model' => $model, 'dataProvider' => $dataProvider[0], 'item' => $dataProvider[1]]));
        }
    }

    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }
    
    public function actionCreate($parent = false)
    {
        $model = $this->initModel();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (isset($_POST['cancel'])) return [];
            return ActiveForm::validate($model);
        }

        if (isset($_POST['cancel'])) {
            if ($parent !== false) {
                if ($this->parents) {
                    return $this->redirect(['admin/'.current(array_keys($this->parents)).'/update', 'id' => $parent]);
                } else {
                    return $this->redirect(['index', 'parent' => $parent]);
                }
            } else {
                $this->redirect('index');
            }
            \Yii::$app->end();
        }

        if ($parent !== false) {
            $model->parent_id = $parent;
        }

        if (isset($_POST[$this->modelName])) {
            $model->load(\Yii::$app->request->post());

            if (!isset($_POST[$this->modelName]['startParams']) || $_POST[$this->modelName]['startParams'] != 'Y') {
                foreach ($this->images as $imageVar => $imageWay) {
                    SystemTools::loadImage($model, $imageVar, $imageWay);
                }
                $this->beforeModelSave($model);

                if ($model->validate()) {
                    $this->saveModel($model);

                    foreach ($this->links as $link => $linkVars) {
                        if (isset($_POST[$this->modelName][$link])) {
                            $query = new Query;
                            $command = $query->createCommand();
                            $command->sql = 'DELETE FROM `' . $linkVars[0] . '` WHERE `' . $linkVars[1] . '`="' . $model->id . '";';
                            $command->execute();

                            $values = [];
                            if (is_array($_POST[$this->modelName][$link])) {
                                foreach ($_POST[$this->modelName][$link] as $val) {
                                    $values[] = '("' . $model->id . '", "' . $val . '")';
                                }
                            }

                            if (count($values)) {
                                $command->sql = 'INSERT INTO `' . $linkVars[0] . '` (`' . $linkVars[1] . '`, `' . $linkVars[2] . '`) VALUES ' . implode(', ', $values);
                                $command->execute();
                            }
                        }
                    }

                    if (isset($_POST['save'])) {
                        if ($parent !== false) {
                            if ($this->parents) {
                                return $this->redirect(['admin/' . current(array_keys($this->parents)) . '/update', 'id' => $parent]);
                            } else {
                                return $this->redirect(['index', 'parent' => $parent]);
                            }
                        } else {
                            return $this->redirect('index');
                        }
                    } else {
                        return $this->redirect(['update', 'id' => $model->id]);
                    }
                }
            }
        }

        $attrs = $model->attributes;
        if (array_key_exists ('public', $attrs)) $model->public = 1;
        if (array_key_exists ('in_menu', $attrs)) $model->in_menu = 0;
        if (array_key_exists ('in_side', $attrs)) $model->in_side = 0;
        if (array_key_exists ('in_footer', $attrs)) $model->in_footer = 0;
        if (array_key_exists ('in_row', $attrs)) $model->in_row = 2;
        if (array_key_exists ('list_type', $attrs)) $model->list_type = 1;
        if (array_key_exists ('weight', $attrs)) {
            $className = $this->fullModelName;
            $maxWeight = $className::find()->orderBy('weight DESC')->one();

            if ($maxWeight) {
                $model->weight = $maxWeight->weight + 10;
            } else {
                $model->weight = 10;
            }
        }
        if (array_key_exists ('gallery_in_row', $attrs)) $model->gallery_in_row = 1;

        $content = '<br>' . Yii::$app->controller->renderPartial('_form', array('model' => $model));

        return $this->render('create', ['model' => $model, 'content' => $content]);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id, $tab = '', $pback = false, $partial = false, $sort = false)
    {
        $model = $this->loadModel($id);
        $attrs = array_keys($model->getAttributes());

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (isset($_POST['cancel'])) return [];
            return ActiveForm::validate($model);
        }

        if (isset($_POST['cancel'])) {
            $params = [];
            if ($pback !== false)  $params['page'] = $pback;
            if ($this->parents/* && $partial !== false*/) {
                return $this->redirect(['admin/'.current(array_keys($this->parents)).'/update', 'id' => $model->parent_id, 'tab' => strtolower($this->modelName)] + $params);
            } else {
                return $this->redirect(['index'] + $params);
            }
            \Yii::$app->end();
        }

        if (isset($_POST[$this->modelName])) {
            $model->load(\Yii::$app->request->post());

            foreach ($this->images as $imageVar => $imageWay) {
                if (!empty($_POST[$imageVar.'_deleting'])) {
                    $model->$imageVar = '';
                } else {
                    SystemTools::loadImage($model, $imageVar, $imageWay);
                }
            }
            $this->beforeModelSave($model);

            if ($model->save()) {

                foreach ($this->links as $link => $linkVars) {
                    if (isset($_POST[$this->modelName][$link])) {
                        $query = new Query;
                        $command = $query->createCommand();
                        $command->sql = 'DELETE FROM `'.$linkVars[0].'` WHERE `'.$linkVars[1].'`="'.$model->id.'";';
                        $command->execute();

                        $values = [];
                        if (is_array($_POST[$this->modelName][$link])) {
                            foreach ($_POST[$this->modelName][$link] as $val) {
                                $values[] = '("' . $model->id . '", "' . $val . '")';
                            }
                        }

                        if(count($values)) {
                            $command->sql = 'INSERT INTO `'.$linkVars[0].'` (`'.$linkVars[1].'`, `'.$linkVars[2].'`) VALUES ' . implode(', ', $values);
                            $command->execute();
                        }
                    }
                }

                if (isset($_POST['save'])) {
                    $params = [];
                    if ($pback !== false)  $params['page'] = $pback;
                    if ($this->parents/* && $partial !== false*/) {
                        return $this->redirect(['admin/'.current(array_keys($this->parents)).'/update', 'id' => $model->parent_id, 'tab' => strtolower($this->modelName)] + $params);
                    } else {
                        return $this->redirect(['index'] + $params);
                    }
                } else {
                    $params = [];
                    if ($pback !== false) $params['pback'] = $pback;
                    if ($partial !== false) $params['partial'] = $partial;

                    return $this->redirect(['update', 'id' => $model->id] + $params);
                }
            }
        }

        $params = [];
        if ($this->pback) $params['pback'] = $this->pback;
        $children = [[
            'label' => $this->modelTitles[0],
            'active' => strtolower($tab)==$this->modelName||$tab=='',
            'link' => Yii::$app->urlManager->createUrl([Yii::$app->controller->id.'/update', 'id' => $id]+$params),
        ]];
        $content = '';
        foreach($this->children as $child => $childTitle) {
            $childElem = [
                'label' => $childTitle[0],
                'active' => strtolower($tab)==$child,
                'link' => Yii::$app->urlManager->createUrl([Yii::$app->controller->id.'/update', 'id' => $id, 'tab' => $child]+$params),
            ];
            if ($childElem['active']) {
                $content = '<br>' . Yii::$app->runAction('admin/'.$child.'', ['parent' => $model->id, 'partial' => true, 'sort' => $sort]);
            }
            $children[] = $childElem;
        }
        if ($content == '') {
            $content = '<br>' . Yii::$app->controller->renderPartial('_form', array('model' => $model));
            $this->beforeUpdate($model, $content);
        }

        return $this->render('update', ['model' => $model, 'tabs' => $children, 'content' => $content]);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id, $tab = '', $pback = false, $partial = false)
    {
        $parent = false;
        // we only allow deletion via POST request
        $model = $this->loadModel($id);

        if (isset($model->parent_id)) {
            $parent = $model->parent_id;
        }

        foreach ($this->images as $img => $way) {
            if ($model->$img != '' && file_exists($_SERVER['DOCUMENT_ROOT'] . $model->$img) && !is_dir($_SERVER['DOCUMENT_ROOT'] . $model->$img)) {
                unlink($_SERVER['DOCUMENT_ROOT'] . $model->$img);
            }
        }

        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $params = [];
            if ($pback !== false)  $params['page'] = $pback;
            if ($this->parents/* && $partial !== false*/) {
                return $this->redirect(['admin/'.current(array_keys($this->parents)).'/update', 'id' => $model->parent_id, 'tab' => strtolower($this->modelName)] + $params);
            } else {
                return $this->redirect(['index'] + $params);
            }
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $className = $this->fullModelName;

        $model = $className::findOne($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    public function initModel()
    {
        return new $this->fullModelName();
    }

    public function getRoot()
    {
        $className = $this->fullModelName;
        return $className::find()->where(['handle'=>'root'])->one();
    }

    public function dataProviderModel($params = false, $parent = false, $partial = false) {
        $className = $this->fullModelName;
        $model = $this->initModel();
        $model->scenario = 'search';

        if($parent) {
            $query = $className::find()->where(['parent_id'=>$parent]);
        } else {
            $query = $className::find();
        }

        $dpParams = [];
        $sSort = SystemSort::findOne(['user_id' => Yii::$app->user->id, 'model' => strtolower($this->modelName), 'partial' => $partial?1:0]);
        if ($sSort) {
            $dpParams['sort'] = ['defaultOrder'=> [$sSort->column => ($sSort->sort==1?SORT_ASC:SORT_DESC)]];
            if ($sSort->pagesize == 0) {
                $this->pageSize = 100000000;
            } else {
                $this->pageSize = $sSort->pagesize;
            }
        }

        if ($model->load($params)) {
            if ($model->validate()) {
                $this->searchModelChangeQuery($query, $model);
            }
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize,
            ],
        ]+$dpParams);

        return [$dataProvider, $model];
    }

    
    public function searchModelChangeQuery($query, $model) {
        foreach($model->fields() as $field) {
            if (trim($model->$field) != '') {
                if (isset($this->searchFields[$field])) {
                    switch($this->searchFields[$field]['type']) {
                        case 'public':
                        case 'number':
                            $query->andFilterWhere(['=', $field, $model->$field]);
                            break;
                        case 'numberModel':
                            $className = $this->searchFields[$field]['model'];
                            $fieldName = $this->searchFields[$field]['field'];
                            $item = $className::findOne([$fieldName => $model->$field]);
                            if($item) $query->andFilterWhere(['=', $field, $item->id]);
                            break;
                        case 'stringModel':
                            $className = $this->searchFields[$field]['model'];
                            $fieldName = $this->searchFields[$field]['field'];
                            $items = $className::find()->andFilterWhere(['like', $fieldName, $model->$field])->all();
                            $ids = [];
                            foreach ($items as $item) $ids[] = $item->id;
                            if (count($ids) > 0) $query->andFilterWhere(['in', $field, $ids]);
                            break;
                        case 'dateInterval':
                            $items = explode('-', $model->$field);
                            foreach ($items as $k => $item) {
                                $items[$k] = strtotime(trim($item));
                            }
                            if (count($items) == 1) {
                                $items[1] = $items[0];
                            }
                            if(count($items) > 1) {
                                $query->andFilterWhere(['>=', $field, $items[0]]);
                                $query->andFilterWhere(['<', $field, $items[1] + 86400]);
                            }
                            break;
                        case 'string':
                        default:
                            $query->andFilterWhere(['like', $field, $model->$field]);
                    }
                    
                } else {
                    $query->andFilterWhere(['like', $field, $model->$field]);
                }
            }
        }
    }

    public function beforeAction($action) {
        $res = parent::beforeAction($action);
        
        switch($action->id) {
            case 'index':
                if ($this->parent_id) {
                    $this->leftMenu['index'] = array(
                        'link' => \Yii::$app->urlManager->createUrl(['admin/'.$action->controller->mainmenuHandle.'/index', 'parent' => $this->parent_id]),
                        'title' => 'Управление',
                    );
                    $this->leftMenu['add'] = array(
                        'link' => \Yii::$app->urlManager->createUrl(['admin/' . $action->controller->mainmenuHandle . '/create', 'parent' => $this->parent_id]),
                        'title' => 'Добавить',
                    );
                } else {
                    $this->leftMenu['index'] = array(
                        'link' => \Yii::$app->urlManager->createUrl(['admin/'.$action->controller->mainmenuHandle.'/index']),
                        'title' => 'Управление',
                    );
                    $this->leftMenu['add'] = array(
                        'link' => \Yii::$app->urlManager->createUrl(['admin/' . $action->controller->mainmenuHandle . '/create']),
                        'title' => 'Добавить',
                    );
                }

                foreach($this->parents as $parent => $parentTitle) {
                    if ($this->parent_id) {
                        $this->leftMenu['parent' . $parent] = array(
                            'link' => \Yii::$app->urlManager->createUrl(['admin/' . $parent . '/update', 'id' => $this->parent_id]),
                            'title' => $parentTitle,
                        );
                    }
                }

                break;
            case 'update':
                foreach($this->children as $child => $childTitle) {
                    /*$this->leftMenu['child'.$child] = array(
                        'link' => \Yii::$app->urlManager->createUrl(['admin/'.$child.'/index', 'parent'=>$this->model_id]),
                        'title' => $childTitle[0],
                    );*/

                    $this->leftMenu['childadd'.$child] = array(
                        'link' => \Yii::$app->urlManager->createUrl(['admin/'.$child.'/create', 'parent'=>$this->model_id]),
                        'title' => $childTitle[1],
                    );
                }

                if ($this->children) {
                    $params = [];
                    if ($this->pback) $params['page'] = $this->pback;
                    $this->leftMenu['backtoindex'] = array(
                        'link' => \Yii::$app->urlManager->createUrl(['admin/' . $this->mainmenuHandle . '/index'] + $params),
                        'title' => 'Вернуться к списку ' . strtolower($this->modelTitles[2]),
                    );
                }

                break;
        }

        return $res;
    }

    public function beforeUpdate(&$model, &$tabs) {

    }

    public function beforeModelSave($model) {

    }

    public function afterAdmin($model) {

    }
    
    public function saveModel($model) {
        $model->save();
    }
}