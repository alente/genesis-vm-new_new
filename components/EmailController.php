<?php

namespace app\components;


class EmailController extends \yii\web\Controller {
    function ajaxForm($model, $ajaxForm, $successMessage, $viewName, $reactOnSuccess) {
        $cs = Yii::app()->clientScript;
        $cs->scriptMap=array(
            'jquery.js'=>false,
        );

        if(isset($_POST['ajax']) && $_POST['ajax']===$ajaxForm.'-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST[get_class($model)]) && isset($_POST['email']) && $_POST['email'] == 1) {
            $model->attributes = $_POST[get_class($model)];
            if ($model->validate()) {
                $this->$reactOnSuccess($model);
                echo json_encode(array('success'=>$successMessage));
                Yii::app()->end();
            }
        }

        $this->renderPartial($viewName, array('model'=>$model, 'actionId'=>$ajaxForm, 'formId'=>$ajaxForm.'-form'), false, !isset($_POST[get_class($model)]));
    }
}