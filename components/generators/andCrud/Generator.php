<?
namespace app\components\generators\andCrud;

use Yii;
use yii\gii\CodeFile;

class Generator extends \yii\gii\Generator
{
    public $tableName;
    public $fields;
    public $links;
    public $manyLinks;
    public $children;
    public $parents;
    public $modelLabels;
    // 0 - doc, 1 - label
    public $types = [
        'string_255' => ['string', 'String 255'],
        'string_500' => ['string', 'String 500'],
        'text' => ['string', 'Text'],
        'handle' => ['string', 'Handle'],
        'image' => ['string', 'Image'],
        'file' => ['string', 'File'],
        'date' => ['string', 'Date'],
        'date_only' => ['string', 'Date (Only)'],
        'integer' => ['integer', 'Integer'],
        'checkbox' => ['integer', 'Checkbox'],
        'table' => ['string', 'Table'],
    ];
    public $fieldWeight;
    public $fieldPublic;
    public $publicController;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['tableName', 'modelLabels'], 'required'],
            [['fields', 'links', 'manyLinks', 'parents', 'children', 'fieldWeight', 'fieldPublic', 'publicController'], 'safe'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'fieldWeight' => 'Добавить порядок',
            'fieldPublic' => 'Добавить публикацию',
            'publicController' => 'Генерация публичного контроллера',
            'tableName' => 'Наименование таблицы',
            'modelLabels' => 'Наименование элемента через запятую (Новость, Новости, Новостей)',
        ]);
    }

    public function hints()
    {
        return array_merge(parent::hints(), [
            'tableName' => 'Маленькими буквами. Если несколько слов, разделитель «_».',
            'modelLabels' => '',
        ]);
    }

    public function requiredTemplates()
    {
        return ['adminController.php', 'publicController.php', 'model.php'];
    }

    public function generate()
    {
        $filter = ['fields', 'links', 'manyLinks', 'children', 'parents'];
        foreach ($filter as $filterItem) {
            if (!is_array($this->{$filterItem})) $this->{$filterItem} = [];
            foreach ($this->{$filterItem} as $k => $field) {
                if ($field['name'] == '') {
                    unset($this->{$filterItem}[$k]);
                }
            }
            $this->{$filterItem} = array_values($this->{$filterItem});
        }
        
        $files = [];
        $files[] = new CodeFile(
            Yii::getAlias('@app/controllers/admin') . '/' . $this->getControllerName() . 'Controller.php',
            $this->render('adminController.php')
        );
        $files[] = new CodeFile(
            Yii::getAlias('@app/models') . '/' . $this->getModelName() . '.php',
            $this->render('model.php')
        );

        if ($this->publicController) {
            $files[] = new CodeFile(
                Yii::getAlias('@app/controllers') . '/' . $this->getControllerName() . 'Controller.php',
                $this->render('publicController.php')
            );
            $files[] = new CodeFile(
                Yii::getAlias('@app/views') . '/' . $this->getHandleName() . '/index.php',
                '<?'
            );
            $files[] = new CodeFile(
                Yii::getAlias('@app/views') . '/' . $this->getHandleName() . '/detail.php',
                '<?'
            );
        }

        $files[] = new CodeFile(
            Yii::getAlias('@app/views/admin') . '/' . $this->getHandleName() . '/update.php',
            '<?=$this->render("//admin/base/update", ["model" => $model, "tabs" => $tabs, "content" => $content])?>'
        );
        $files[] = new CodeFile(
            Yii::getAlias('@app/views/admin') . '/' . $this->getHandleName() . '/create.php',
            '<?=$this->render("//admin/base/create", ["content" => $content])?>'
        );
        $files[] = new CodeFile(
            Yii::getAlias('@app/views/admin') . '/' .  $this->getHandleName() . '/_form.php',
            $this->render('form.php')
        );
        $files[] = new CodeFile(
            Yii::getAlias('@app/views/admin') . '/' .  $this->getHandleName() . '/index.php',
            $this->render('index.php')
        );

        return $files;
    }

    public function getControllerName() {
        $nameParts = explode('_', $this->tableName);
        foreach ($nameParts as $k => $np) {
            $nameParts[$k] = strtolower($np);
            if ($k == 0) {
                $nameParts[$k] = ucfirst($np);
            }
        }

        return implode('', $nameParts);
    }

    public function getModelName() {
        $nameParts = explode('_', $this->tableName);
        foreach ($nameParts as $k => $np) {
            $nameParts[$k] = strtolower($np);
            $nameParts[$k] = ucfirst($np);
        }

        return implode('', $nameParts);
    }

    public function getHandleName() {
        $nameParts = explode('_', $this->tableName);
        foreach ($nameParts as $k => $np) {
            $nameParts[$k] = strtolower($np);
        }

        return implode('', $nameParts);
    }

    public function getModelLabels() {
        $nameParts = preg_split('/\s*,\s*/', $this->modelLabels);

        return "'" . implode("', '", $nameParts) . "'";
    }

    public function getName()
    {
        return 'Универсальный КРЮД под админку';
    }

    public function getDescription()
    {
        return '';
    }
}