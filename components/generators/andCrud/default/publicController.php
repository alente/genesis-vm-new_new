<?='<?'?>

namespace app\controllers;

use app\components\PublicController;
use app\models\<?=$generator->getModelName()?>;
use app\models\Settings;
use Yii;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class <?=$generator->getControllerName()?>Controller extends PublicController
{
    public function actionIndex($page = 1)
    {
        $this->tempInit('', '<?=$generator->getHandleName()?>');

        $criteria = <?=$generator->getModelName()?>::find()->published();
        $criteria->orderBy('t.date DESC, t.weight ASC');

        $countQuery = clone $criteria;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = Settings::getParam('<?=$generator->getHandleName()?>OnPage');
        $pages->route = '<?=$generator->getHandleName()?>/index';
        $pages->page = $page - 1;

        $items = $criteria->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('index', ['items' => $items, 'pages' => $pages]);
    }

    public function actionDetail($handle) {
        $item = <?=$generator->getModelName()?>::find()->where(['handle'=>$handle, 'public'=>1])->one();
        if (!$item) throw new NotFoundHttpException();

        $prev = <?=$generator->getModelName()?>::find()->published()->andWhere('id != '.$item->id)->andWhere('date > '.$item->getDateSrc('date') . ' OR date = '.$item->getDateSrc('date').' AND weight <= ' . $item->weight)->orderBy('t.date ASC, t.weight DESC')->one();
        $next = <?=$generator->getModelName()?>::find()->published()->andWhere('id != '.$item->id)->andWhere('date < '.$item->getDateSrc('date') . ' OR date = '.$item->getDateSrc('date').' AND weight >= ' . $item->weight)->orderBy('t.date DESC, t.weight ASC')->one();

        $this->tempInit('', '<?=$generator->getHandleName()?>');
        $this->addBreadCrumb('', $item->title);

        return $this->render('detail', ['item' => $item, 'prev' => $prev, 'next' => $next]);
    }
}
