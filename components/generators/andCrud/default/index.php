<?='<?'?>

use app\components\helpers\BImages;
use yii\helpers\Html;

$fields = [
    "id",
<?
if ($generator->fieldPublic) {
    echo "\t" . '[
        "attribute"  => "public",
        "content" => function ($data) {
            return $data->public>0?"+":"-";
        },
        "filter" => [1 => "Активные", 0 => "Неактивные"],
    ],'."\r\n";
}
foreach($generator->fields as $field) {
    if (!empty($field['show'])) {
        switch($field['type']) {
            case 'image':
                echo "\t" . '[
        "attribute"  => "'.$field['name'].'",
        "content" => function ($data) {
            return Html::img(BImages::doProp($data->'.$field['name'].', 100, 100));
        },
        "filter" => false,
    ],' . "\r\n";
                break;
            case 'checkbox':
                echo "\t" . '[
        "attribute"  => "'.$field['name'].'",
        "content" => function ($data) {
            return $data->'.$field['name'].'>0?"+":"-";
        },
        "filter" => [1 => "Вкл", 0 => "Выкл"],
    ],'."\r\n";
            default:
                echo "\t'".$field['name']."',\r\n";
        }
    }
}
if ($generator->fieldWeight) {
    echo "\t'weight',\r\n";
}
?>
];
<?='?>'?>

<?='<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>'?>