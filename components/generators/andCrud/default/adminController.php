<? echo "<?php"; ?>

namespace app\controllers\admin;

class <?=$generator->getControllerName()?>Controller extends \app\components\AdminController
{
    public $mainmenuHandle = '<?=$generator->getHandleName()?>';
    public $modelName='<?=$generator->getModelName()?>';
    public $modelTitles = [<?=$generator->getModelLabels()?>];
    public $images = [
<? foreach($generator->fields as $field) { ?><? if($field['type'] == 'image' || $field['type'] == 'file') { ?>
        '<?=$field['name']?>' => '@webroot/up/<?=$generator->getHandleName()?>',
<? } ?><? } ?>
    ];
    public $children = [
<? foreach($generator->children as $child) { ?>
        '<?=$child['name']?>' => ['<?=$child['label1']?>', '<?=$child['label2']?>'],
<? } ?>
    ];
    public $parents = [
<? foreach($generator->parents as $parent) { ?>
        '<?=$parent['name']?>' => '<?=$parent['label']?>',
<? } ?>
    ];

    public $links = [
<? foreach($generator->manyLinks as $mLink) { ?>
        '<?=$mLink['name']?>' => ['<?=$mLink['linktable']?>', '<?=$mLink['id_1']?>', '<?=$mLink['id_2']?>'],
<? } ?>
    ];
}