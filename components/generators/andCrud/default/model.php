<?='<?'?><?
    $commonRules = [
        'handle' => 'string_255',
        'image' => 'safe',
        'file' => 'safe',
        'date' => 'safe',
        'date_only' => 'safe',
        'checkbox' => 'integer',
    ];

    $required = [];
    $rules = [];
    $rulesSepareted = [];
    foreach($generator->fields as $field) {
        if (!empty($commonRules[$field['type']])) {
            $rules[$commonRules[$field['type']]][] = $field['name'];
        } else {
            $rules[$field['type']][] = $field['name'];
        }
        $rulesSepareted[$field['type']][] = $field['name'];

        if (!empty($field['required'])) {
            $required[] = $field['name'];
        }
    }
    if ($generator->fieldWeight) {
        $rules['integer'][] = 'weight';
    }
    if ($generator->fieldPublic) {
        $rules['integer'][] = 'public';
    }

    foreach($generator->links as $link) {
        $rules['integer'][] = $link['link'];
    }
?>

namespace app\models;

use Yii;
<? if(!empty($rulesSepareted['date']) || !empty($rulesSepareted['date_only'])) { ?>
use app\components\behaviors\DateBehavior;
<? } ?>
<? if(!empty($rulesSepareted['handle'])) { ?>
use app\components\behaviors\HandleBehavior;
<? } ?>
/**
 * This is the model class for table "<?=$generator->tableName?>".
 *
 * @property integer $id
<? foreach($generator->fields as $field) { ?>
 * @property <?=$generator->types[$field['type']][0]?> $<?=$field['name']?><?="\r\n"?>
<? } ?>
 * @property integer $weight
 * @property integer $public
 */
class <?=$generator->getModelName()?> extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '<?=$generator->tableName?>';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
<? if (count($required) > 0) { ?>
            [['<?=implode("', '", $required)?>'], 'required'],
<? } ?>
<? foreach($rules as $ruleType => $ruleFields) {
    switch($ruleType) {
        case 'string_255':
            echo "\t\t\t[['" . implode("', '", $ruleFields) . "'], 'string', 'max' => 255],\r\n";
            break;
        case 'string_500':
            echo "\t\t\t[['" . implode("', '", $ruleFields) . "'], 'string', 'max' => 500],\r\n";
            break;
        case 'text':
            echo "\t\t\t[['" . implode("', '", $ruleFields) . "'], 'string'],\r\n";
            break;
        case 'integer':
            echo "\t\t\t[['" . implode("', '", $ruleFields) . "'], 'integer'],\r\n";
            break;
        case 'safe':
            echo "\t\t\t[['" . implode("', '", $ruleFields) . "'], 'safe'],\r\n";
            break;
    }
} ?>
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
<? 
foreach($generator->fields as $field) {
    echo "\t\t\t'".$field['name']."' => '".$field['label']."',\r\n";
}
if ($generator->fieldWeight) {
    echo "\t\t\t'weight' => 'Порядок',\r\n";
}
if ($generator->fieldPublic) {
    echo "\t\t\t'public' => 'Публикация',\r\n";
}
foreach($generator->links as $link) {
    echo "\t\t\t'".$link['link']."' => '".$link['label']."',\r\n";
}
foreach($generator->manyLinks as $mLink) {
    echo "\t\t\t'".$mLink['name']."' => '".$mLink['label']."',\r\n";
}
?>
        ];
    }

    public function behaviors() {
        return [
<? if(!empty($rulesSepareted['handle'])) { ?>
            'handleBehavior' => [
                'class' => HandleBehavior::className(),
                'attribute' => '<?=current($rulesSepareted['handle'])?>',
            ],
<? } ?>
<? $dates = array_merge((!empty($rulesSepareted['date']))?$rulesSepareted['date']:[], (!empty($rulesSepareted['date_only']))?$rulesSepareted['date_only']:[]);
if(count($dates) > 0) { ?>
            'dateBehavior' => [
                'class' => DateBehavior::className(),
                'dates' => ['<?=implode("', '", $dates)?>'],
            ],
<? } ?>
        ];
    }

<? foreach($generator->links as $link) { ?>
    public function get<?=$link['name']?>() {
        return $this->hasOne(<?=$link['class']?>::className(), ['id' => '<?=$link['link']?>'])->where(['public'=>1]);
    }
<? } ?>

<? foreach($generator->manyLinks as $mLink) { ?>
    public function get<?=ucfirst($mLink['name'])?>() {
        return $this->hasMany(<?=$mLink['class']?>::className(), ['id' => '<?=$mLink['id_2']?>'])->viaTable('<?=$mLink['linktable']?>', ['<?=$mLink['id_1']?>' => 'id']);
    }
<? } ?>
}