<? echo '<?' . "\r\n";
foreach ($generator->links as $link) {
    echo "use app\\models\\".$link['class'].";\r\n";
}
foreach ($generator->manyLinks as $mLink) {
    echo "use app\\models\\".$mLink['class'].";\r\n";
}
echo '?>' . "\r\n"; ?>

<? echo '
<?
use app\components\helpers\AdminTools;
?>

<div class="form">
    <? $form = \yii\bootstrap\ActiveForm::begin([
        "layout" => "horizontal",
        "enableAjaxValidation" => true,
        "enableClientValidation" => false,
        "fieldConfig" => [
            "horizontalCssClasses" => [
                "label" => "col-sm-2",
                "offset" => "col-sm-offset-4",
                "wrapper" => "col-sm-8",
                "error" => "",
                "hint" => "",
            ],
        ],
    ]); ?>

    <p class="note"><span class="required">*</span> Обязательно для заполнения.</p>
';?>

<?
foreach ($generator->fields as $field) {
    if (!empty($field['edit'])) {
        switch ($field['type']) {
            case 'image':
                echo "\t" . '<?=AdminTools::generateImageInputs($model, $form, "'.$field['name'].'")?>' . "\r\n";
                break;
            case 'file':
                echo "\t" . '<?=AdminTools::generateFileInputs($model, $form, "'.$field['name'].'")?>' . "\r\n";
                break;
            case 'date':
                echo "\t" . '<?=AdminTools::generateDatepicker($model, $form, "'.$field['name'].'")?>' . "\r\n";
                break;
            case 'date_only':
                echo "\t" . '<?=AdminTools::generateOnlyDatepicker($model, $form, "'.$field['name'].'")?>' . "\r\n";
                break;
            case 'text':
                echo "\t" . '<?=AdminTools::generateRedactor($model, $form, "'.$field['name'].'")?>' . "\r\n";
                break;
            case 'string_500':
                echo "\t" . '<?=$form->field($model, "'.$field['name'].'")->textarea()?>' . "\r\n";
                break;
            case 'checkbox':
                echo "\t" . '<?=$form->field($model, "'.$field['name'].'")->checkbox([], false)?>' . "\r\n";
                break;
            case 'table':
                echo "\t" . '<?=AdminTools::generateTableDatas($model, $form, "'.$field['name'].'", 2, array(
                    "items" => [
                        "class 1" => ["field 11", "field 21"],
                        "class 2" => ["field 12", "field 22"],
                        "other" => ["", ""],
                    ],
                    "hidden" => [0],
                ))?>' . "\r\n";
                break;
            default:
                echo "\t" . '<?=$form->field($model, "'.$field['name'].'")?>' . "\r\n";
        }
    }
}
foreach ($generator->links as $link) {
    echo "\t" . '<?=AdminTools::generateSelect2($model, $form, "'.$link['link'].'", '.$link['class'].'::getDropDownList(false, true, "Не выбрано"))?>' . "\r\n";
}
foreach ($generator->manyLinks as $mLink) {
    echo "\t" . '<?=AdminTools::generateSelect2($model, $form, "'.$mLink['name'].'", '.$mLink['class'].'::getDropDownList(), true)?>' . "\r\n";
}
if ($generator->fieldWeight) {
    echo "\t" . '<?=$form->field($model, "weight")?>' . "\r\n";
}
if ($generator->fieldPublic) {
    echo "\t" . '<?=$form->field($model, "public")->checkbox([], false)?>' . "\r\n";
}
?>
<? echo '
    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=$form->errorSummary($model)?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=\yii\bootstrap\Html::submitButton("Сохранить", ["class" => "btn btn-primary", "name"=>"save"])?>
            <?=\yii\bootstrap\Html::submitButton("Применить", ["class" => "btn btn-primary", "name"=>"apply"])?>
            <?=\yii\bootstrap\Html::submitButton("Отмена", ["class" => "btn btn-default", "name"=>"cancel"])?>
        </div>
    </div>

    <? $form->end(); ?>

</div>
'; ?>