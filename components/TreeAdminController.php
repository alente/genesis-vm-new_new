<?php

namespace app\components;

use app\models\TreeForm;
use yii\base\Exception;

class TreeAdminController extends AdminController
{
    public function actionTree()
    {
        $model = $this->initModel();

        return $this->render('tree', ['model' => $model, 'tree' => $model->find()->orderBy('lft')->all()]);
    }

    public function actionTreeform($id)
    {
        $model = $this->loadModel($id);

        $treeForm = new TreeForm();
        $treeForm->load(\Yii::$app->request->post());
        if ($treeForm->validate()) {
            $parent = $this->loadModel($treeForm->target_id);

            try {
                switch ($treeForm->operation) {
                    case 1:
                        $model->insertBefore($parent);
                        break;
                    case 2:
                        $model->insertAfter($parent);
                        break;
                    case 3:
                        $model->prependTo($parent);
                        break;
                    case 4:
                        $model->appendTo($parent);
                        break;
                }
            } catch (Exception $e) {
                
            }

            if (isset($_POST['save'])) {
                return $this->redirect(['index']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            return $this->redirect(['update', 'id' => $model->id]);
        }
    }

    public function saveModel($model) {
        if(!($root = $this->loadModel($model->tree_parent_id))) $root = $this->getRoot();
        $model->appendTo($root);
    }

    public function beforeAction($action) {
        $res = parent::beforeAction($action);

        switch($action->id) {
            case 'tree':
                if ($this->parent_id) {
                    $this->leftMenu['index'] = array(
                        'link' => \Yii::$app->urlManager->createUrl(['admin/'.$action->controller->mainmenuHandle.'/index', 'parent' => $this->parent_id]),
                        'title' => 'Управление',
                    );
                    $this->leftMenu['add'] = array(
                        'link' => \Yii::$app->urlManager->createUrl(['admin/' . $action->controller->mainmenuHandle . '/create', 'parent' => $this->parent_id]),
                        'title' => 'Добавить',
                    );
                } else {
                    $this->leftMenu['index'] = array(
                        'link' => \Yii::$app->urlManager->createUrl(['admin/'.$action->controller->mainmenuHandle.'/index']),
                        'title' => 'Управление',
                    );
                    $this->leftMenu['add'] = array(
                        'link' => \Yii::$app->urlManager->createUrl(['admin/' . $action->controller->mainmenuHandle . '/create']),
                        'title' => 'Добавить',
                    );
                }
            case 'index':
                $this->leftMenu['tree'] = array(
                    'link' => \Yii::$app->urlManager->createUrl(['admin/'.$action->controller->mainmenuHandle.'/tree']),
                    'title' => 'Дерево',
                );
                break;
        }

        return $res;
    }
}