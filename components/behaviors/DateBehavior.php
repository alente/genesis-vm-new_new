<?
    namespace app\components\behaviors;

    use yii\base\Behavior;
    use yii\db\ActiveRecord;
    
    class DateBehavior extends Behavior
    {
        public $dates;
        public $dates_src;

        public function events()
        {
            return [
                ActiveRecord::EVENT_INIT => 'afterConstruct',
                ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
                ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
                ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
            ];
        }
    
        public function afterConstruct($event) {
            foreach($this->dates as $dateItem) {
                if ($this->owner->{$dateItem}) {
                    $ownerVal = $this->owner->{$dateItem};
                    $this->dates_src[$dateItem] = $ownerVal;
                    $this->owner->{$dateItem} = date('d.m.Y H:i', $ownerVal);
                }
            }
        }
    
        public function afterFind($event)
        {
            foreach($this->dates as $dateItem) {
                $this->dates_src[$dateItem] = $this->owner->{$dateItem};
                if ($this->owner->{$dateItem} == 0) {
                    $this->owner->{$dateItem} = '';
                } else {
                    $this->owner->{$dateItem} = date('d.m.Y H:i', $this->owner->{$dateItem});
                }
            }
        }
    
        public function beforeSave($event) {
            foreach($this->dates as $dateItem) {
                if ($this->owner->{$dateItem} != '') {
                    $t = explode(' ', $this->owner->{$dateItem});
                    $d = explode('.', $t[0]);
                    if (count($t) > 1) {
                        $t = explode(':', $t[1]);
                    } else {
                        $t = array(0, 0, 0);
                    }
                    $t[] = 0; $t[] = 0; $t[] = 0;

                    if (count($t) >= 3 && count($d) == 3)
                        $this->owner->{$dateItem} = mktime($t[0], $t[1], $t[2], $d[1], $d[0], $d[2]);
                    else
                        $this->owner->{$dateItem} = time();
                } else {
                    $this->owner->{$dateItem} = 0;
                }
            }
    
            return true;
        }
        
        public function convertDates() {
            foreach($this->dates as $dateItem) {
                $t = explode(' ', $this->owner->{$dateItem});
                $d = explode('.', $t[0]);
                if (count($t) > 1) {
                    $t = array_slice(explode(':', $t[1]) + [0, 0, 0], 0, 3);
                } else {
                    $t = [0, 0, 0];
                }
                if (count($t) == 3 && count($d) == 3)
                    $this->dates_src[$dateItem] = mktime($t[0], $t[1], $t[2], $d[1], $d[0], $d[2]);
                else
                    $this->dates_src[$dateItem] = 0;
            }
        }
    
        public function getDateSrc($dName) {
            return (!empty($this->dates_src[$dName]))?$this->dates_src[$dName]:0;
        }
    
        public function setDateSrc($dName, $time) {
            if (isset($this->dates_src[$dName])) {
                $this->dates_src[$dName] = $time;
                $this->owner->{$dName} = date('d.m.Y H:i', $time);
            }
        }
    }