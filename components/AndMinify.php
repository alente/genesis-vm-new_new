<?php

namespace app\components;

use Yii;

class AndMinify {
    public static function beginMinify($event) {
        //$outres = print_r(1, true);$outres=str_replace(">", "&gt;", $outres);$outres=str_replace("<", "&lt;", $outres);echo "<pre>".$outres."</pre>";
        //ob_start();
    }

    public static function doMinify($event) {
        $response = Yii::$app->getResponse();

        if (current(explode('/', $event->sender->requestedRoute)) != 'admin') {
            self::allBody($response->data);
            self::minifyJS($response->data);
            self::minifyCSS($response->data);
        }
    }

    public static $inlineCSS = true;
    public static $uploadExternalCSS = false;
    public static $uploadExternalJS = false;
    public static $excludes = [];
    public static $uplodaExternalDir = '/up/tmpsource';
    public static $adminPath = '/admin/';

    public static function allBody(&$content) {
        return;
        $content = preg_replace('/'."[\\s\n]*".'<\!\-\-.+?\-\-\!?>'."[\\s\n]*".'/s', '', $content);
        
        //validation
        $content = preg_replace('/cellpadding="[0-9]+"/', '', $content);
        $content = preg_replace('/cellspacing="[0-9]+"/', '', $content);
        $content = preg_replace('/border="[0-9]+"/', '', $content);
        $content = preg_replace('/size="0"/', '', $content);
        $content = preg_replace('/cols="0"/', '', $content);
        $content = preg_replace('/rows="0"/', '', $content);
        $content = preg_replace('/<select\s*SELECTED/', '<select ', $content);

        $styleArr = array('width' => 'width', 'height' => 'height');
        foreach ($styleArr as $sKey => $sVal) {
            if (preg_match_all('/<(?:div|td|table)[^>]*('.$sKey.'="(.*?)")[^>]*>/', $content, $m)) {
                foreach ($m[0] as $tagKey => $tagVal) {
                    $style = '';
                    $newTAG = $tagVal;
                    if (preg_match('/style="(.*?)"/', $tagVal, $m1)) {
                        $style = ' ' . $m1[1];
                        $newTAG = preg_replace(array('/\s+'.$m1[0].'/'), array(''), $newTAG);
                    }
                    $newTAG = preg_replace(array('/\s+'.$m[1][$tagKey].'/', '/>/'), array('', ' style="'.$sVal.': '.$m[2][$tagKey].';'.$style.'">'), $newTAG);

                    $content = str_replace($tagVal, $newTAG, $content);
                }
            }
        }

        if (preg_match_all('/<img[^>]*src\s*=\s*"(.*?)"[^>]*>/', $content, $m)) {
            foreach ($m[0] as $tagKey => $tagVal) {
                if (!preg_match('/alt/', $tagVal)) {
                    $alt = current(explode('.', current(array_reverse(explode('/', current(explode('?', $m[1][$tagKey])))))));

                    $newTAG = preg_replace('/\/?>/', ' alt="'.$alt.'"/>', $tagVal);
                    $content = str_replace($tagVal, $newTAG, $content);
                }
            }
        }

        if (preg_match_all('/<img[^>]*src\s*=\s*"(.*?)"[^>]*>/', $content, $m)) {
            foreach ($m[0] as $tagKey => $tagVal) {
                if (preg_match('/\s/', $m[1][$tagKey])) {
                    $src = preg_replace('/\s/', '%20', $m[1][$tagKey]);

                    $newTAG = preg_replace('/src\s*=\s*".*?"/', 'src="'.$src.'"', $tagVal);
                    $content = str_replace($tagVal, $newTAG, $content);
                }
            }
        }
    }

    public static function minifyJS(&$content) {
        if (!preg_match('/<body[^>]*>/', $content)) return;
        if (!preg_match('/^'.self::preg_text(self::$adminPath).'/', $_SERVER['REQUEST_URI'])) {
            $toEnd = '';
            $externals = array();
            if (self::$uploadExternalJS && file_exists($_SERVER['DOCUMENT_ROOT'].self::$uplodaExternalDir.'/js.json')) {
                $externals = unserialize(file_get_contents($_SERVER['DOCUMENT_ROOT'].self::$uplodaExternalDir.'/js.json'));
            }

            $fields = array(
                'head' => true, //to end
                'body' => true, // not to end
            );
            foreach($fields as $fKey => $fVal) {
                if (preg_match('/<'.$fKey.'>.*?<\/'.$fKey.'>/s', $content, $mcontent)) {
                    preg_match_all('/<script([^>]*)>(.*?)<\/script>/s', $mcontent[0], $m);
                    if (count($m[0]) > 0) {
                        foreach($m[2] as $scrKey => $scrBody) {
                            $toDelete = '';
                            $newToEnd = '';

                            if (!preg_match('/data-min=\"no\"/', $m[0][$scrKey]) &&
                                !preg_match('/kernel_main\.js/', $m[0][$scrKey])) {
                                if(strpos($m[1][$scrKey], 'src') === false) {
                                    // $content = str_replace($m[0][$scrKey], '', $content);
                                    $toDelete = $m[0][$scrKey];
                                    $newToEnd = '<script type="text/javascript">' . $scrBody . '</script>';
                                } else {
                                    preg_match('/src\s*=\s*[\"\']([^\"\']*)[\"\']/', $m[1][$scrKey], $m1);
                                    $scr = preg_replace('/^http\:\/\/'.self::preg_text(Yii::$app->params['domain']).'/', '', $m1[1]);
                                    $scr1 = realpath($scr);
                                    if ($scr1 != '') {
                                        $scr = str_replace($_SERVER['DOCUMENT_ROOT'], '', $scr1);
                                    }


                                    $toDelete = $m[0][$scrKey];

                                    if (!preg_match('/^http/', $scr) && !preg_match('/^\/\//', $scr)) {
                                        if (!in_array($scr, self::$excludes)) {
                                            $newToEnd = '<script type="text/javascript" src="/min/f=' . $scr . '&1"></script>';
                                        } else {
                                            $newToEnd = '<script type="text/javascript" src="' . $scr . '"></script>';
                                        }
                                    } else {
                                        if (self::$uploadExternalJS) {
                                            if (preg_match('/^\/\//', $scr)) $scr = 'http:' . $scr;
                                            $newSCR = md5($scr) . '.js';

                                            if (empty($externals[$newSCR]) || intval($externals[$newSCR]) + 86400 < time()) {
                                                $externals[$newSCR] = time();
                                                file_put_contents($_SERVER['DOCUMENT_ROOT'].self::$uplodaExternalDir.'/js/'.$newSCR, '/*'.$scr.'*/' . file_get_contents($scr));
                                            }
                                            $newToEnd = '<script type="text/javascript" src="/min/f='.self::$uplodaExternalDir.'/js/'.$newSCR.'&1"></script>';
                                        } else {
                                            $newToEnd = $m[0][$scrKey];
                                        }
                                    }
                                }
                            }

                            if ($fVal) {
                                /*if ($toDelete != '') {
                                    $content = str_replace($toDelete, '', $content);
                                }
                                $toEnd .= $newToEnd;*/

                                if ($toDelete != '') {
                                    $content = str_replace($toDelete, $newToEnd, $content);
                                }
                            } else {
                                // if ($toDelete != '' && $toDelete != $newToEnd) {
                                // $content = str_replace($toDelete, $newToEnd, $content);
                                // }
                            }
                        }
                    }
                }
            }

            if (self::$uploadExternalJS) {
                file_put_contents($_SERVER['DOCUMENT_ROOT'].self::$uplodaExternalDir.'/js.json', serialize($externals));
            }

            if ($toEnd != '') {
                $content = str_replace('<!--scriptAdding-->', $toEnd, $content);
            }
        }
    }

    public static function minifyCSS(&$content) {
        if (!preg_match('/<body[^>]*>/', $content)) return;
        if (!preg_match('/^'.self::preg_text(self::$adminPath).'/', $_SERVER['REQUEST_URI'])) {
            $externals = array();
            if (self::$uploadExternalCSS && file_exists($_SERVER['DOCUMENT_ROOT'].self::$uplodaExternalDir.'/css.json')) {
                $externals = unserialize(file_get_contents($_SERVER['DOCUMENT_ROOT'].self::$uplodaExternalDir.'/css.json'));
            }

            preg_match_all('/<link[^>]*href\s*=\s*[\'"]([^\'">]*)[\'"][^>]*>/', $content, $m);

            foreach ($m[0] as $mKey => $mVal) {
                $srcBuff = preg_replace('/^http\:\/\/'.self::preg_text(Yii::$app->params['domain']).'/', '', $m[1][$mKey]);
                $srcBuff1 = realpath($srcBuff);
                if ($srcBuff1 != '') {
                    $srcBuff = str_replace($_SERVER['DOCUMENT_ROOT'], '', $srcBuff1);
                }

                if (preg_match('/type=[\"\']text\/css[\"\']/', $mVal) || preg_match('/rel=[\"\']stylesheet[\"\']/', $mVal)) {
                    if (preg_match('/^http/', $srcBuff)) {
                        if (self::$uploadExternalCSS) {
                            if (preg_match('/^\/\//', $srcBuff)) $srcBuff = 'http:' . $srcBuff;
                            $newSCR = md5($srcBuff) . '.css';

                            if (empty($externals[$newSCR]) || intval($externals[$newSCR]) + 86400 < time()) {
                                $externals[$newSCR] = time();
                                file_put_contents($_SERVER['DOCUMENT_ROOT'].self::$uplodaExternalDir.'/css/'.$newSCR, file_get_contents($srcBuff));
                            }
                            $srcBuff = self::$uplodaExternalDir. '/css/' . $newSCR;
                        } else {
                            continue;
                        }
                    }

                    $src = preg_replace('/\?.*/', '', $srcBuff);
                    if (self::$inlineCSS) {
                        $content = str_replace($m[0][$mKey], '<!-- '.$src.' --><style type="text/css">'.file_get_contents('http://'.Yii::$app->params['domain'].'/min/f='.$src).'</style>', $content);
                    } else {
                        $content = str_replace($m[0][$mKey], '<link type="text/css" rel="stylesheet" href="/min/f='.$src.'&1">', $content);
                    }
                }
            }

            if (self::$uploadExternalCSS) {
                file_put_contents($_SERVER['DOCUMENT_ROOT'].self::$uplodaExternalDir.'/css.json', serialize($externals));
            }
        }
    }

    public static function preg_text($txt) {
        return str_replace(array('/', '.'), array('\/', '\.'), $txt);
    }
}