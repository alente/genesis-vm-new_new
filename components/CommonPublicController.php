<?php

namespace app\components;

use app\assets\AppAsset;
use app\models\Backgrounds;
//use app\models\Sections;

class CommonPublicController extends \yii\web\Controller {
    public $headerMenu = [];
    public $breadcrumbs = [];
    public $activeBreadcrumbs = [];
    public $seo = [
        'title' => '',
        'description' => '',
        'keywords' => '',
    ];
    public $og = [
        'title' => '',
        'description' => '',
        'image' => '',
    ];

    public $h1title = '';
    public $section;
    public $topSection;
    public $left_menu = [];
    public $footerMenu = [];
    public $topmenu = [];
    public $isMobile = false;

    public function init()
    {
        parent::init();

        $asset = new AppAsset();
        $asset->registerAssetFiles($this->view);

        $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], 'iPhone');
        $Android = stripos($_SERVER['HTTP_USER_AGENT'], 'Android');
        $Palmpre = stripos($_SERVER['HTTP_USER_AGENT'], 'webOS');
        $BlackBerry = stripos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry');
        $iPod = stripos($_SERVER['HTTP_USER_AGENT'], 'iPod');
        
        if ($iPhone || $Palmpre || $Android || $BlackBerry || $iPod)
            $this->isMobile = true;

        $this->layout = 'main';
        $this->breadcrumbs[] = [
            'url' => '/',
            'label' => 'Главная',
        ];
        $this->activeBreadcrumbs['/'] = 1;

        /*$sections = Sections::find()->where(['public'=>1, 'in_menu'=>1, 'depth'=>2])->orderBy('lft')->all();
        foreach ($sections as $section) {
            $this->headerMenu['top'.$section->id] = [
                'label' => $section->title,
                'url' => '/'.$section->handle,
                'active' => false,
                'section' => $section,
            ];
        }

        $this->topSection = Sections::find()->where(['depth'=>1])->one();

        $sections = Sections::find()->where(['public'=>1, 'in_footer'=>1, 'depth'=>2])->orderBy('lft')->all();
        foreach ($sections as $section) {
            $this->footerMenu['bottom'.$section->id] = [
                'label' => $section->title,
                'url' => '/'.$section->handle,
                'active' => false,
                'section' => $section,
            ];
        }*/
    }

    public function tempInit($pt, $handle, $last_section = false) {
        $handle = preg_replace('/\/$/', '', $handle);

        $this->seo['title'] = $pt;
        $this->h1title = $pt;
        //$this->section = Sections::find()->where(['handle' => $handle])->one();

        $left_menu = array();
        if ($this->section) {
            if (isset($this->headerMenu['top'.$this->section->id]))
                $this->headerMenu['top'.$this->section->id]['active'] = true;

            if (isset($this->footerMenu['bottom'.$this->section->id]))
                $this->footerMenu['bottom'.$this->section->id]['active'] = true;

            foreach ($this->section->parents()->all() as $parent) {
                if ($parent->handle != 'root') {
                    $this->breadcrumbs[] = [
                        'url' => '/'.$parent->handle,
                        'label' => $parent->title,
                    ];
                    $this->activeBreadcrumbs['/'.$parent->handle] = count($this->activeBreadcrumbs)+1;

                    if (isset($this->headerMenu['top'.$parent->id]))
                        $this->headerMenu['top'.$parent->id]['active'] = true;

                    if (isset($this->footerMenu['bottom'.$parent->id]))
                        $this->footerMenu['bottom'.$parent->id]['active'] = true;
                }
            }

            if ($last_section) {
                $this->breadcrumbs[] = [
                    'label' => $this->section->title,
                ];
            } else {
                $this->breadcrumbs[] = [
                    'url' => $last_section ? '' : '/'.$this->section->handle,
                    'label' => $this->section->title,
                ];
                $this->activeBreadcrumbs[$last_section ? '' : '/'.$this->section->handle] = count($this->activeBreadcrumbs)+1;
            }

            if ($this->section->children()->count() > 0 || $this->section->handle == 'root') {
                $left_menu = $this->section->children(1)->all();
            } elseif($this->section->depth == 3) {
                $left_menu = '';
            } elseif($this->section->depth == 4) {
                $left_menu = $this->section->parents(1)->one()->parents(1)->one()->children(1)->all();
            }

            $this->seo['title'] = !empty($this->section->seo_title) ? $this->section->seo_title : $this->seo['title'];
            $this->seo['keywords'] = !empty($this->section->seo_keywords) ? $this->section->seo_keywords : '';
            $this->seo['description'] = !empty($this->section->seo_description) ? $this->section->seo_description : '';

            if ($this->seo['title'] == '') $this->seo['title'] = $this->section->title;
            if ($this->h1title == '') $this->h1title = $this->section->h1title;
            if ($this->h1title == '') $this->h1title = $this->section->title;
        }
        $this->og['title'] = $this->h1title;
        $this->og['image'] = '/images/logo.jpg';
        $this->og['description'] = $this->seo['description'];

        foreach($left_menu as $lm) {
            if ($lm->public == 1 && $lm->in_side == 1) {
                $this->left_menu[] = array(
                    'handle' => $lm->handle,
                    'url' => '/' . $lm->handle,
                    'title' => $lm->title,
                    'active' => ($lm->handle==$handle),
                    'section' => $lm,
                );
            }
        }
    }

    public function addBreadCrumb($url, $label) {
        $bread = [];
        if ($url != '') {
            $bread['url'] = $url;
            $this->activeBreadcrumbs[$url] = count($this->activeBreadcrumbs)+1;
        }
        if ($label != '') $bread['label'] = $label;

        $this->breadcrumbs[] = $bread;
    }
}