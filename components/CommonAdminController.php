<?php

namespace app\components;

use yii\filters\AccessControl;

class CommonAdminController extends \yii\web\Controller
{
    public $layout='admin';
    public $leftMenu = [];
    public $parent_id;
    public $model_id;
    public $pback = false;
    public $partial = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($parent = false)
    {
        return $this->render('index');
    }

    public function beforeAction($action)
    {
        $this->parent_id = \Yii::$app->request->getQueryParam('parent');
        $this->model_id = \Yii::$app->request->getQueryParam('id');
        $this->pback = \Yii::$app->request->getQueryParam('pback');
        $this->partial = \Yii::$app->request->getQueryParam('partial');

        return parent::beforeAction($action);
    }
}