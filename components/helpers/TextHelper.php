<?php

namespace app\components\helpers;

use phpQuery;

class TextHelper {
    public static function translit($str)
    {
        $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
        $lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
        return str_replace($rus, $lat, $str);
    }
    
    public static function price($price) {
        return number_format($price, 0, '.', ' ');
    }

    public static function getMonth($m, $t = 0) {
        if ($t == 0) {
            $months = array(
                1 => 'января',
                'февраля',
                'марта',
                'апреля',
                'мая',
                'июня',
                'июля',
                'августа',
                'сентября',
                'октября',
                'ноября',
                'декабря',
            );
        } elseif($t == 1) {
            $months = array(
                1 => 'январь',
                'февраль',
                'март',
                'апрель',
                'май',
                'июнь',
                'июль',
                'август',
                'сентябрь',
                'октябрь',
                'ноябрь',
                'декабрь',
            );
        }
        return $months[intval($m)];
    }

    public static function getWeek($w) {
        $weekDays = array('Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс');
        return $weekDays[intval($w)];
    }

    public static function getFullWeek($w) {
        $weekDays = array('Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье');
        return $weekDays[intval($w)];
    }

    public static function getRusDate($date, $withTime = false) {
        return date('d', $date) . ' ' . self::getMonth(date('m', $date)) . ' ' . date('Y', $date) . ($withTime?' ' . date('H:i:s', $date):'');
    }

    public static function redactorText($text, $type = 1) {
        $pqText = phpQuery::newDocumentHTML($text);

        /*if (in_array($type, [1])) {
            $elements = $pqText->find('p, h1, h2, h3, h4, h5, h6');
            foreach ($elements as $elem) {
                $pq = pq($elem);
                if (!$pq->hasClass('auto-center')) {
                    $pq->addClass('auto-center');
                }
            }
        }

        if (in_array($type, [1])) {
            $elements = $pqText->find('blockquote');
            foreach ($elements as $elem) {
                $pq = pq($elem);
                $pq->wrap('<div class="grey-block text-align-left">');
                $pq->parent()->append('<p>' . $pq->html() . '</p>');
                $pq->remove();
            }
        }

        if (in_array($type, [2])) {
            $elements = $pqText->find('ul');
            foreach ($elements as $elem) {
                $pq = pq($elem);
                if (!$pq->hasClass('no-marker')) {
                    $pq->addClass('no-marker');
                }
            }
        }*/
        
        return $pqText->html();
    }

    public static function getNumEnding($number, $endingArray) {
        $number = $number % 100;
        if ($number >= 11 && $number <= 19) {
            $ending = $endingArray[2];
        } else {
            $i = $number % 10;
            switch ($i) {
                case (1): $ending = $endingArray[0]; break;
                case (2): case (3): case (4): $ending = $endingArray[1]; break;
                default: $ending=$endingArray[2];
            }
        }
        return $ending;
    }

    public static function getFirstString($text, $raz = ',', $todel = ['-', ' ']) {
        if (!is_array($todel)) $todel = [$todel];

        $text = str_replace($todel, array_fill(0, count($todel), ''), $text);
        return current(explode($raz, $text));
    }

    public static function filterExternalSource($text) {
        $regs = [
            '/<img[^>]*src\s*=\s*[\'\"]((?:https?\:)?\/\/([^\/]+)[^\'\"]+)[\'\"][^>]*>/s',
            '/<a[^>]*href\s*=\s*[\'\"]((?:https?\:)?\/\/([^\/]+)[^\'\"]+)[\'\"][^>]*>/s'
        ];
        foreach ($regs as $reg) {
            preg_match_all($reg, $text, $m);
            foreach ($m[1] as $imgKey => $imgSrc) {
                if ($m[2][$imgKey] != $_SERVER['HTTP_HOST']) {
                    $savedSrc = BImages::saveExternalPicture($imgSrc, '/up/external/'.md5($m[2][$imgKey]));
                    if ($savedSrc !== false) {
                        $text = str_replace($imgSrc, $savedSrc, $text);
                    }
                }
            }
        }
        return $text;
    }
    
    public static function tableDataToArray($tableData) {
        $res = [];
        if ($tableData != '') {
            $rows = explode('[;;]', $tableData);
            foreach ($rows as $row) {
                $resRow = [];
                $cols = explode('[::]', $row);
                foreach ($cols as $col) {
                    $resRow[] = $col;
                }
                $res[] = $resRow;
            }
        }
        return $res;
    }
}