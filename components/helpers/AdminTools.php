<?
    namespace app\components\helpers;

    use dosamigos\datetimepicker\DateTimePicker;
    use kartik\select2\Select2;
    use yii\helpers\Html;
    use yii\jui\DatePicker;
    use yii\redactor\widgets\Redactor;

    class AdminTools {
        public static function generateImageInputs($model, $form, $image_var){
            if ($model->$image_var) { ?>
                <div class="form-group" >
                    <label class="control-label col-sm-2" >Просмотр</label >

                    <div class="col-sm-8" >
                        <img style = "max-width: 800px;" src="<?=$model->$image_var?>" alt="" /><br />
                        <?=Html::checkbox($image_var.'_deleting', false, []);?> - удалить?
                    </div >
                </div>
            <?
                //echo $form->field($model, $image_var . 'deleting')->checkbox([], false);
            }
            echo $form->field($model, $image_var)->fileInput();
        }

        public static function generateFileInputs($model, $form, $file_var) {
            if ($model->$file_var) { $fileInfo = SystemTools::full_pathinfo($model->$file_var); ?>
                <div class="form-group">
                    <label class="control-label col-sm-2">Просмотр</label>

                    <div class="col-sm-8">
                        <a href="<?=$model->$file_var?>" target="_blank"><?=$fileInfo['basename']?></a><br />
                        <?=Html::checkbox($file_var.'_deleting', false, []);?> - удалить?
                    </div>
                </div>
                <?
            }
            echo $form->field($model, $file_var)->fileInput();
        }

        public static function generateSafeFilename($dir, $filename, $extension){
            $newFilename = $filename.'.'.$extension;
            $i = 0;
            if ($dir[strlen($dir)-1] !== DIRECTORY_SEPARATOR)
                $dir .= DIRECTORY_SEPARATOR;
            while(file_exists($dir.$newFilename)){
                $i++;
                $newFilename = $filename.'_'.$i.'.'.$extension;
            }
            return $filename.($i>0?'_'.$i:'');
        }

        public static function generateDatepicker($model, $form, $image_var, $onlyDate = false) {
            return $form->field($model, $image_var)->widget(DateTimePicker::className(),[
                'language' => 'ru',
                'clientOptions' => [
                    'autoclose' => true,                    
                    'format' => $onlyDate?'dd.mm.yyyy':'dd.mm.yyyy HH:ii',
                    'todayBtn' => true
                ],
            ]);
        }

        public static function generateOnlyDatepicker($model, $form, $image_var) {
            return $form->field($model, $image_var)->widget(DatePicker::className(), [
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
                'options' => [
                    'class' => 'form-control',
                ]
            ]);
        }

        public static function generateOnlyDatepickerWithBooking($model, $form, $image_var, $booking) {
            return $form->field($model, $image_var)->widget(DatePicker::className(), [
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
                'options' => [
                    'class' => 'form-control',
                ],
                'clientOptions' => [
                    'beforeShowDay' => new \yii\web\JsExpression('function(date) {
                            var bDates = '.json_encode($booking).';
                            var dTime = date.getTime() / 1000;
                            var nTime = new Date();
                            nTime = nTime / 1000;
                            
                            if (nTime > dTime) {
                                return [false, ""];
                            }
                            
                            if (dTime in bDates) {
                                if (bDates[dTime]) {
                                    return [true, "free"];
                                } else {
                                    return [false, "busy"];
                                }
                            }
                            
                            return [false, "unknown"];
                        }'),
                ],
            ]);
        }

        public static function generateRedactor($model, $form, $image_var) {
            return $form->field($model, $image_var)->widget(Redactor::className(),[
                'clientOptions' => [
                    'imageManagerJson' => ['/redactor/upload/image-json'],
                    'imageUpload' => ['/redactor/upload/image'],
                    'fileUpload' => ['/redactor/upload/file'],
                    'lang' => 'ru',
                    'plugins' => ['fontcolor','imagemanager', 'table', 'video'],
                ]
            ]);
        }

        public static function generateSelect2($model, $form, $variable, $data, $multiple = false, $placeholder = '') {
            return $form->field($model, $variable)->widget(Select2::classname(), [
                'data' => $data,
                'language' => 'ru',
                'options' => [
                    'placeholder' => $placeholder,
                    'multiple' => $multiple,
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
        }

        public static function generateTableDatas($model, $form, $variable, $cols = 2, $templates = []) {
            if (empty($templates['items'])) {
                $templates['items'] = [];
            }
            if (empty($templates['hidden'])) {
                $templates['hidden'] = [];
            }
            return $form->field($model, $variable)->textInput(['class' => 'tableData', 'data-cols' => $cols, 'data-templates' => json_encode($templates)]);
        }
    }