<?php

namespace app\components\helpers;

class Yandex {
    public static function payButton($booking, $target)
    {
        //return '<iframe frameborder="0" allowtransparency="true" scrolling="no" src="https://money.yandex.ru/embed/small.xml?account=' . \Yii::$app->params['yandexID'] . '&quickpay=small&any-card-payment-type=on&button-text=01&button-size=l&button-color=orange&targets='.urldecode($target).'&label=booking'.$booking->id.'&default-sum='.$booking->booking.'&successURL='.urlencode('http://'.\Yii::$app->params['domain'].'/yandexconfirm/success/'.$booking->id).'" width="242" height="54"></iframe>';
        return '<iframe frameborder="0" allowtransparency="true" scrolling="no" src="https://money.yandex.ru/embed/shop.xml?account=' . \Yii::$app->params['yandexID'] . '&quickpay=shop&payment-type-choice=on&mobile-payment-type-choice=on&writer=seller&targets='.urldecode($target).'&label=booking'.$booking->id.'&targets-hint=&default-sum='.$booking->booking.'&button-text=01&successURL='.urlencode('http://'.\Yii::$app->params['domain'].'/yandexconfirm/success/'.$booking->id).'" width="450" height="198"></iframe>';
    }
}