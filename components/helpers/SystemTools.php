<?
namespace app\components\helpers;

use yii\web\UploadedFile;

class SystemTools {
    public static function isCurPage($page){
        $curPage = current(explode('?', $_SERVER['REQUEST_URI']));
        return $page==$curPage;
    }
    
    public static function getUrl($add = [], $del = []) {
        list($url, $params) = explode('?', $_SERVER['REQUEST_URI']);
        if ($params != '') {
            $params = explode('&', $params);
        } else {
            $params = [];
        }
        
        $resparams = [];
        foreach($params as $param) {
            $param = explode('=', $param);
            if (!in_array($param[0], $del) && empty($add[$param[0]])) {
                $resparams[] = implode('=', $param);
            }
        }

        foreach($add as $addKey => $addItem) {
            $resparams[] = $addKey . '=' . $addItem;
        }

        if (count($resparams) > 0) {
            return $url . '?' . implode('&', $resparams);
        }
        return $url;
    }

    public static function full_pathinfo($path_file){
        $path_file = strtr($path_file,array('\\'=>'/'));

        $exp = explode('/', $path_file);
        $file = array_pop($exp);
        $dirname = implode('/', $exp);
        $fileexp = explode('.', $file);
        $extension = array_pop($fileexp);

        return array(
            'dirname' => $dirname,
            'basename' => $file,
            'extension' => $extension,
            'filename' => implode('.', $fileexp)
        );
    }

    public static function fileSize($path_file) {
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $path_file)) {
            $size = filesize($_SERVER['DOCUMENT_ROOT'] . $path_file);

            return self::formatSizeUnits($size);
        }

        return '';
    }

    public static function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' Gb';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' Mb';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' Kb';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public static function loadImage ($model, $img_var, $uploadDir) {
        $dir_name = \Yii::getAlias($uploadDir);
        $doc_root = \Yii::getAlias('@webroot');

        if (!file_exists($dir_name)) {
            mkdir($dir_name);
        }

        $buffImg = UploadedFile::getInstance($model, $img_var);
        if ($buffImg) {
            $extentionName = current(array_reverse(explode('.', $buffImg->name)));
            $rawFilename = TextHelper::translit($extentionName ? str_replace('.' . $extentionName, '', $buffImg->name) : $buffImg->name);
            $newRawFilename = AdminTools::generateSafeFilename($dir_name, $rawFilename, $extentionName);
            $newFilename = $newRawFilename . '.'.$extentionName;
            $buffImg->saveAs($dir_name . '/' . $newFilename);
            $model->$img_var = str_replace(array($doc_root, '\\'), array('', '/'), $dir_name . '/' . $newFilename);
        } else {
            $model->$img_var = $model->getOldAttribute($img_var);
        }
    }
}