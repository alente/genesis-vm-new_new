<?php

namespace app\components\helpers;

use yii\image\drivers\Image;

class BImages {
    const PROPORTIAL = 1;
    const CROP = 2;
    const PROPORTIALWITHWHITE = 3;

    public static $lastImageParam = false;

    public static function doProp($img, $w, $h, $q = 95) {
        return self::get_url($img, array('w' => $w, 'h' => $h, 't' => self::PROPORTIAL, 'q' => $q));
    }

    public static function doCrop($img, $w, $h, $q = 95) {
        return self::get_url($img, array('w' => $w, 'h' => $h, 't' => self::CROP, 'q' => $q));
    }

    public static function doPropW($img, $w, $h, $q = 95) {
        return self::get_url($img, array('w' => $w, 'h' => $h, 't' => self::PROPORTIALWITHWHITE, 'q' => $q));
    }

    public static function get_url($img_src, $params) {
        $nopic = '/images/common/logo_wite.png';
        Self::$lastImageParam = false;

        if ($img_src == '' || !file_exists($_SERVER['DOCUMENT_ROOT'] . $img_src) || is_dir($_SERVER['DOCUMENT_ROOT'] . $img_src)) $img_src = $nopic;

		if (self::is_image($_SERVER['DOCUMENT_ROOT'] . $img_src)) {
			try {
				$image = \Yii::$app->image->load($_SERVER['DOCUMENT_ROOT'] . $img_src);
			} catch (Exception $e) {
				$img_src = $nopic;
				$image = \Yii::$app->image->load($_SERVER['DOCUMENT_ROOT'] . $nopic);
			}
        } else {
			$img_src = $nopic;
			$image = \Yii::$app->image->load($_SERVER['DOCUMENT_ROOT'] . $nopic);
		}


        $defaultParams = array(
            'w' => $image->width,
            'h' => $image->height,
            't' => self::PROPORTIAL,
            'q' => 95,
        );

        $params = array_merge($defaultParams, $params);
        // $trueW = $params['w']<$image->width?$params['w']:$image->width;
        // $trueH = $params['h']<$image->height?$params['h']:$image->height;
        $trueW = $params['w'];
        $trueH = $params['h'];

        $pi = SystemTools::full_pathinfo($img_src);
        $tmpPath = '/up/resizetmp/' . $params['w'] . '_' . $params['h'] . '_' . $params['t'].'/'.md5(str_replace($_SERVER['DOCUMENT_ROOT'], '', $pi['dirname'])).'/'.TextHelper::translit($pi['filename']).'.'.$pi['extension'];
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $tmpPath) || filemtime($_SERVER['DOCUMENT_ROOT'] . $tmpPath) < filemtime($_SERVER['DOCUMENT_ROOT'] . $img_src)) {
            self::makeDir($tmpPath);
            switch ($params['t']) {
                case self::PROPORTIAL:
                    $image->resize($trueW, $trueH)->save($_SERVER['DOCUMENT_ROOT'] . $tmpPath, $params['q']);
                    break;
                case self::CROP:
                    $image->resize($trueW, $trueH, Image::CROP)->save($_SERVER['DOCUMENT_ROOT'] . $tmpPath, $params['q']);
                    break;
                case self::PROPORTIALWITHWHITE:
                    $image->resize($trueW, $trueH, Image::ADAPT)->background('#fff')->save($_SERVER['DOCUMENT_ROOT'] . $tmpPath, $params['q']);
                    break;
            }
        }
        $image = \Yii::$app->image->load($_SERVER['DOCUMENT_ROOT'] . $tmpPath);
        Self::$lastImageParam = $image;

        return $tmpPath;
    }

    /*const NONE    = 0x01;
    const WIDTH   = 0x02;
    const HEIGHT  = 0x03;
    const AUTO    = 0x04;
    const INVERSE = 0x05;
    const PRECISE = 0x06;
    const ADAPT   = 0x07;
    const CROP    = 0x08;*/

    private static function makeDir($path) {
        $pi = SystemTools::full_pathinfo($path);
        $pi['dirname'] = str_replace('\\', '/', $pi['dirname']);
        $allDirs = explode('/', $pi['dirname']);

        $way = $_SERVER['DOCUMENT_ROOT'];
        foreach($allDirs as $dir) {
            if (!(file_exists($way.'/'.$dir) && is_dir($way.'/'.$dir))) {
                mkdir($way.'/'.$dir);
                chmod($way.'/'.$dir, 0777);
            }
            $way = $way.'/'.$dir;
        }
    }

    public static function getUniqFileName($way) {
        $path = SystemTools::full_pathinfo($way);

        $i = 1;
        while (file_exists($_SERVER['DOCUMENT_ROOT'] . $way) && $i < 100) {
            $way = $path['dirname'] . '/' . $path['filename'] . '_' . $i . '.' . $path['extension'];
            $i++;
        }

        return $way;
    }

    public static function saveExternalPicture($way, $dir) {
        $whiteTypes = ['jpg', 'jpeg', 'png', 'gif'];
        $path = SystemTools::full_pathinfo(urldecode($way));
        if (in_array($path['extension'], $whiteTypes)) {
            $savePath = self::getUniqFileName($dir . '/' . TextHelper::translit($path['filename']) . '.' . $path['extension']);
            self::makeDir($savePath);
            $img = file_get_contents($way);
            if (!empty($img)) {
                file_put_contents($_SERVER['DOCUMENT_ROOT'] . $savePath, $img);
                return $savePath;
            }
        }
        return false;
    }
	
	public static function is_image($path)
	{
		$a = getimagesize($path);
		$image_type = $a[2];
		
		if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
		{
			return true;
		}
		return false;
	}
}