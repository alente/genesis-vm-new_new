<?
use app\components\helpers\BImages;
use app\components\helpers\TextHelper;
$this->title = $articles->title_seo;
$this->registerMetaTag(['name' => 'description', 'content' => $articles->description_seo]);
?>  

    <!--<link type="text/css" rel="stylesheet" href="/css/aos.css"/>
    <link type="text/css" rel="stylesheet" href="/css/vendor.css"/>-->
    <link type="text/css" rel="stylesheet" href="/css/app.css"/>
    <link type="text/css" rel="stylesheet" href="/styles/header/header.css">
    <div class="layout__content">
        <div class="layout__wrapper">
            <div class="news-cont">
                <div class="news-cont-in">
                    <h1><?=$articles->title?></h1>
                </div>

                <style>
                    .dot:before {
                        content: "·";
                        font-size:30px;
                        line-height:14px;
                        display:inline-block;
                        padding:0 15px;
                        vertical-align:middle;
                    }
                    body {
                    text-align: left;
                    }
                </style>
				<p><br></p>
			<p><img src="<?=$articles->image?>" alt="<?=$articles->image_alt?>" style="width: 100%;"></p>
             <?=TextHelper::redactorText($articles->text)?>   
                
            </div>
        </div>
    </div>

<div class="top-btn">
    Наверх
</div>
<div class="mobileDetector" id="mobileDetector"></div>
<script type="text/javascript" src="/js/vendor.js"></script>
<script type="text/javascript" src="/js/360view.js"></script>
<script type="text/javascript" src="/js/easings.js"></script>
<script type="text/javascript" src="/js/anim.js"></script>
<script type="text/javascript" src="/js/anim1.js"></script>
<script type="text/javascript" src="/js/aos.js"></script>
<script type="text/javascript" src="/js/aosSettings.js"></script>
<script type="text/javascript" src="/js/initAnim.js"></script>
<script type="text/javascript" src="/js/app.js?ver=1.2"></script>
<script type="text/javascript" src="/js/slick.min.js"></script>
<script type="text/javascript" src="/js/magnific-popup.min.js"></script>
<script type="text/javascript" src="/js/jquery.steps.min.js"></script>
<!-- Add scripts here if needed custom for a page as as a-->

<script src="/scripts/header/common.js"></script>
<script src="/scripts/header/App.js"></script>
<script src="/scripts/header/App.navigation.js"></script>
<script src="/scripts/header/App.navigation-mob.js"></script>