<?php
use app\components\helpers\BImages;
use app\components\helpers\TextHelper;
$this->title = 'Статьи Genesis';

?>    
    <!--<link type="text/css" rel="stylesheet" href="/styles/header/webfonts.css"> -->
      
    <link type="text/css" rel="stylesheet" href="/css/build.css"/>
    <link type="text/css" rel="stylesheet" href="/styles/header/header.css">
	
   <div class="layout__content">
   <?php if (!empty($articles[0]->handle)) {?>
        <div class="news_announcement-top">
            <div class="news_announcement__item al-right">
                <a class="news_announcement__item-half news-an-img" href="<?=Yii::$app->urlManager->createUrl(['articles/detail', 'handle' => $articles[0]->handle])?>"><img alt="Новости Genesis"  src="<?=BImages::doCrop((($articles[0]->image_preview!='')?$articles[0]->image_preview:$articles[0]->image), 675, 449)?>"></a>
                <div class="news_announcement__item-half news-an-text"><span class="news_announcement_date"><?=date('d.m.Y',$articles[0]->getDateSrc('date'))?></span>
                    <h2 class="news_announcement_title"><a class="news_announcement_title-link" href="<?=Yii::$app->urlManager->createUrl(['articles/detail', 'handle' => $articles[0]->handle])?>"><?=$articles[0]->title?></a></h2>
                    <p class="news_announcement_desc"><?=$articles[0]->description?></p>
                    <div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="<?=Yii::$app->urlManager->createUrl(['articles/detail', 'handle' => $articles[0]->handle])?>">читать далее</a></div>
                </div>
            </div>
        </div>
   <?php } ?>
        <div class="layout__wrapper news_announcement-content">
        <? foreach($articles as $k => $articlesItem) {
          if ($k == 0) continue; 
          if($k%2 == 0) {
          $side = "right";
          } else $side = "left";
        ?>
        
            <div class="news_announcement__item al-<?=$side?>">
                <a class="news_announcement__item-half news-an-img" href="<?=Yii::$app->urlManager->createUrl(['articles/detail', 'handle' => $articlesItem->handle])?>"><img alt="Новости Genesis"  src="<?=BImages::doCrop((($articlesItem->image_preview!='')?$articlesItem->image_preview:$articlesItem->image), 482, 321)?>"></a>
                <div class="news_announcement__item-half news-an-text"><span class="news_announcement_date"><?=date('d.m.Y',$articlesItem->getDateSrc('date'))?></span>
                    <h2 class="news_announcement_title"><a class="news_announcement_title-link" href="<?=Yii::$app->urlManager->createUrl(['articles/detail', 'handle' => $articlesItem->handle])?>"><?=$articlesItem->title?></a></h2>
                    <p class="news_announcement_desc"><?=$articlesItem->description?></p>
                    <div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="<?=Yii::$app->urlManager->createUrl(['articles/detail', 'handle' => $articlesItem->handle])?>">читать далее</a></div>
                </div>
            </div>
        <? } ?>    
        </div>
    </div>
<div class="top-btn">
    Наверх
</div>
<div class="mobileDetector" id="mobileDetector"></div>
<script src="/js/build.js" charset="utf-8"></script>