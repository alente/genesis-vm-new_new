<?php
use app\components\helpers\BImages;
use app\components\helpers\TextHelper;
use app\models\News;
use app\models\Settings;
/* @var $this yii\web\View */
$this->title = 'Карта сайта';
?>





<style>
	.layout__content--sitemap h2 {
		margin-bottom: 20px;
		font-size: 50px;
		letter-spacing: .06em;
		color: #000;
		text-align: center;
		font-family: GenesisSansHead,sans-serif;
		font-weight: 100;
	}
	.sitemap {
		text-align:left;
		padding: 50px 0;
	}
	.sitemap .sitemap--column {
		float:left;
		width:50%;
	}
	.sitemap .cf {
		clear:both;
	}
	.sitemap--list li {
		padding-top:5px;
		padding-bottom:5px;
	}
	.sitemap--list li a {
		font-family: GenesisSansHead,sans-serif;
		font-weight: 100;
	}
</style>

<div class="layout__content layout__content--sitemap">
	<div class="layout__box">
		<div class="section">
			<div class="layout__wrapper">
				<div class="page__heading">
					<div class="page-heading">
						<div class="page-heading__text" style="margin-top: 30px;">
							<h2>Карта сайта</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
<div class="section">
			<div class="layout__wrapper">
				<div class="page__content">
					<div class="sitemap">
						<hr>
						
						<div class="sitemap--column">
							<ul class="sitemap--list">
								<li><a href="/">Главная</a></li>
								<li><a href="/g70.html">GENESIS G70</a></li>
								<li><a href="/g70-design.html">GENESIS G70 - Дизайн</a></li>
								<li><a href="/g70-performance.html">GENESIS G70 - Производительность</a></li>
								<li><a href="/g70-safety.html">GENESIS G70 - Безопасность</a></li>
								<li><a href="/g70-technology.html">GENESIS G70 - Технологии</a></li>
								<li><a href="/g70-gallery.html">GENESIS G70 - Галерея</a></li>
								<li><a href="/g80.html">GENESIS G80</a></li>
								<li><a href="/g90.html">GENESIS G90</a></li>
							</ul>
						</div>
						<div class="sitemap--column">
							<ul class="sitemap--list">
								<li><a href="/genesis-finance.html">Genesis Finance</a></li>
								<li><a href="/trade-in.html">Trade-In</a></li>
								<li><a href="/brand.html">О бренде</a></li>
								<li><a href="/contacts">Контакты</a></li>
								<li><a href="/owners.html">Программы для владельцев</a></li>
								<li><a href="/test-drive">Запись на тест-драйв</a></li>
								<li><a href="/configurator">Конфигуратор</a></li>
								<li><a href="/news">Новости</a></li>
							</ul>
						</div>
						
						<div class="cf"></div>
						
						<hr>
						
						<ul class="sitemap--list">
						    <? foreach($news as $newsItem) { ?>	
								<li><a href="<?=Yii::$app->urlManager->createUrl(['news/detail', 'handle' => $newsItem->handle])?>"><?=$newsItem->title?></a></li>
							<? } ?>	
							
						</ul>
						
						<div class="cf"></div>
						
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>


