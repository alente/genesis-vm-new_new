<div class="news_announcement-top">
	<div class="news_announcement__item al-left"><a class="news_announcement__item-half news-an-img" href="news/eastern-economic-forum.html"><img src="images/econ-forum.jpg"></a>
		<div class="news_announcement__item-half news-an-text"><span class="news_announcement_date">19.09.2017</span>
			<h2 class="news_announcement_title"><a class="news_announcement_title-link" href="news/eastern-economic-forum.html">Премиальный бренд Genesis поддержал Восточный экономический форум</a></h2>
			<p class="news_announcement_desc">7 сентября 2017 года Genesis выступил официальным автомобильным партнером Восточного экономического форума, проходящего во Владивостоке 6-7 сентября. Для обеспечения транспортного обслуживания мероприятия компания предоставила организаторам форума 150 автомобилей Genesis G80.</p>
			<div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="news/eastern-economic-forum.html">читать далее</a></div>
		</div>
	</div>
</div>
<div class="layout__wrapper news_announcement-content">
	<div class="news_announcement__item al-right"><a class="news_announcement__item-half news-an-img" href="news/the-hollywood-reporter.html"><img src="images/news-10.jpg"></a>
		<div class="news_announcement__item-half news-an-text"><span class="news_announcement_date">26.06.2017</span>
			<h2 class="news_announcement_title"><a class="news_announcement_title-link" href="news/the-hollywood-reporter.html">Бренд GENESIS выступил официальным автомобильным партнером вечеринки White Party журнала The Hollywood Reporter</a></h2>
			<p class="news_announcement_desc">24 июня в рамках 39-го Московского Международного кинофестиваля журнал The Hollywood Reporter собрал звезд российского кино на вечеринке White Party и вручил молодым кинематографистам ежегодную премию «Аванс». Премиальный автомобильный бренд Genesis выступил официальным партнером данного мероприятия.</p>
			<div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="news/the-hollywood-reporter.html">читать далее</a></div>
		</div>
	</div>
	
	<div class="news_announcement__item al-left"><a class="news_announcement__item-half news-an-img" href="news/miss-russia-2017.html"><img src="images/miss-russia-2017-img-1.jpg"></a>
		<div class="news_announcement__item-half news-an-text"><span class="news_announcement_date">17.04.2017</span>
			<h2 class="news_announcement_title"><a class="news_announcement_title-link" href="news/miss-russia-2017.html">GENESIS И ОФИЦИАЛЬНЫЙ ДИЛЕР МАРКИ «АВИЛОН» ПОДДЕРЖАЛИ КОНКУРС «МИСС РОССИЯ 2017»</a></h2>
			<p class="news_announcement_desc">Genesis и официальный дилер марки «Авилон» выступили партнерами национального конкурса «Мисс Россия 2017», финал которого прошел 15 апреля 2017 года в концертном зале Барвиха Luxury Village.</p>
			<div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="news/miss-russia-2017.html">читать далее</a></div>
		</div>
	</div>
	<div class="news_announcement__item al-right"><a class="news_announcement__item-half news-an-img" href="news/top-safety-pick.html"><img src="images/top-safety-pick-news-1.jpg"></a>
		<div class="news_announcement__item-half news-an-text"><span class="news_announcement_date">12.12.2016</span>
			<h2 class="news_announcement_title"><a class="news_announcement_title-link" href="news/top-safety-pick.html">Genesis G90 получил награду Top Safety Pick+</a></h2>
			<p class="news_announcement_desc">Genesis G90 2017 модельного года получил престижную награду Top Safety Pick+ в тесте безопасности Американского страхового института дорожной безопасности (IIHS).</p>
			<div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="news/top-safety-pick.html">читать далее</a></div>
		</div>
	</div>
	<div class="news_announcement__item al-left"><a class="news_announcement__item-half news-an-img" href="news/context.html"><img src="images/context-news-1.jpg"></a>
		<div class="news_announcement__item-half news-an-text"><span class="news_announcement_date">21.11.2016</span>
			<h2 class="news_announcement_title"><a class="news_announcement_title-link" href="news/context.html">Бренд Genesis поддержал гала-закрытие IV международного фестиваля современной хореографии CONTEXT. ДИАНА ВИШНЁВА</a></h2>
			<p class="news_announcement_desc">19 ноября в Санкт-Петербурге состоялось торжественное закрытие IV Международного фестиваля современной хореографии CONTEXT.Диана Вишнёва, организованного выдающейся российской балериной Дианой Вишнёвой.</p>
			<div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="news/context.html">читать далее</a></div>
		</div>
	</div>
	<div class="news_announcement__item al-right"><a class="news_announcement__item-half news-an-img" href="news/press-release.html"><img src="images/news-8.jpg"></a>
		<div class="news_announcement__item-half news-an-text"><span class="news_announcement_date">15.11.2016</span>
			<h2 class="news_announcement_title"><a class="news_announcement_title-link" href="news/press-release.html">Genesis поддержал гала-открытие IV международного фестиваля современной хореографии CONTEXT. ДИАНА ВИШНЁВА</a></h2>
			<p class="news_announcement_desc">14 ноября в Музыкальном театре им. К.С. Станиславского и Вл. И. Немировича-Данченко состоялось гала-открытие международного фестиваля современной хореографии CONTEXT. ДИАНА ВИШНЁВА.</p>
			<div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="news/press-release.html">читать далее</a></div>
		</div>
	</div>
	<div class="news_announcement__item al-left"><a class="news_announcement__item-half news-an-img" href="news/vishneva.html"><img src="images/news-6.jpg"></a>
		<div class="news_announcement__item-half news-an-text"><span class="news_announcement_date">27.10.2016</span>
			<h2 class="news_announcement_title"><a class="news_announcement_title-link" href="news/vishneva.html">Международный фестиваль современной хореографии CONTEXT. ДИАНА ВИШНЕВА</a></h2>
			<p class="news_announcement_desc">С 14 по 19 ноября 2016 года сразу в двух городах — Москве и Санкт-Петербурге — состоится международный фестиваль современной хореографии CONTEXT. ДИАНА ВИШНЁВА.</p>
			<div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="news/vishneva.html">читать далее</a></div>
		</div>
	</div>
	<div class="news_announcement__item al-right"><a class="news_announcement__item-half news-an-img" href="news/award.html"><img src="images/award-news-1.JPG"></a>
		<div class="news_announcement__item-half news-an-text"><span class="news_announcement_date">20.10.2016</span>
			<h2 class="news_announcement_title"><a class="news_announcement_title-link" href="news/award.html">Genesis G90 стал обладателем престижной награды для автомобилей премиум-класса</a></h2>
			<p class="news_announcement_desc">Флагманская модель Genesis признана «Лучшим седаном» на выставке роскошных автомобилей EXCS Luxury Motor Show</p>
			<div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="news/award.html">читать далее</a></div>
		</div>
	</div>
	<div class="news_announcement__item al-left"><a class="news_announcement__item-half news-an-img" href="news/official-partner.html"><img src="images/official-partner-news-1.JPG"></a>
		<div class="news_announcement__item-half news-an-text"><span class="news_announcement_date">28.09.2016</span>
			<h2 class="news_announcement_title"><a class="news_announcement_title-link" href="news/official-partner.html">Новый премиальный автомобильный бренд Genesis – официальный партнер Международного инвестиционного форума «Сочи-2016»</a></h2>
			<p class="news_announcement_desc">Премиальный бренд Genesis выступит официальным автомобильным партнером XV Международного инвестиционного форума «Сочи-2016», который пройдет с 29 сентября по 2 октября 2016 года в Главном медиацентре Олимпийского парка.</p>
			<div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="news/official-partner.html">читать далее</a></div>
		</div>
	</div>
	<div class="news_announcement__item al-right"><a class="news_announcement__item-half news-an-img" href="news/g90-launch.html"><img src="images/news-5.jpg"></a>
		<div class="news_announcement__item-half news-an-text"><span class="news_announcement_date">22.09.2016</span>
			<h2 class="news_announcement_title"><a class="news_announcement_title-link" href="news/g90-launch.html">Премиальный бренд Genesis<br>выходит на российский рынок</a></h2>
			<p class="news_announcement_desc">22 сентября 2016 года в концертном зале «Барвиха Luxury Village» состоялась российская премьера премиального бренда Genesis.</p>
			<div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="news/g90-launch.html">читать далее</a></div>
		</div>
	</div>
	<div class="news_announcement__item al-left"><a class="news_announcement__item-half news-an-img" href="news/genesis-studio.html"><img src="images/genesis-studio-news-1.jpg"></a>
		<div class="news_announcement__item-half news-an-text"><span class="news_announcement_date">05.09.2016</span>
			<h2 class="news_announcement_title"><a class="news_announcement_title-link" href="news/genesis-studio.html">Компания Hyundai открывает в Ханаме первую в мире галерею Genesis Studio</a></h2>
			<p class="news_announcement_desc">В Корее откроется первое в мире пространство бренда Genesis</p>
			<div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="news/genesis-studio.html">читать далее</a></div>
		</div>
	</div>
</div>