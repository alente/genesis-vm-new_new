<?php
/* @var $this yii\web\View */
$this->title = 'Результаты поиска - Genesis';

?>
<div class="search-container">
    <div class="layout__wrapper">
        <h1>
            Результаты поиска
        </h1>
        <p>
            Результаты поиска по названию
        </p>
    </div>
    <div class="search-section">
        <div class="layout__wrapper">
            <form>
                <div class="grid-coll--md-8">
                    <input type="text" name="q"  value="<?=htmlspecialchars($query)?>" />
                </div>
                <div class="grid-coll--md-4">
                    <button type="submit" class="button">Найти</button>
                </div>
            </form>
        </div>
    </div>
    <div class="search-result-container">
        <div class="layout__wrapper">
	        <?if($error !== null):?>
                <p><?=$error?></p>
	        <?elseif(!$error && empty($result['items'])):?>
                <p>По Вашему запросу результаты не найдены.</p>
	        <?else:?>
                <ul>
                    <?foreach($result['items'] as $item):?>
                        <li>
                            <a href="<?=$item['formattedUrl']?>">
                                <figure>
                                    <figcaption>
                                        <h3><?=$item['htmlTitle']?></h3>
                                        <p><?=$item['htmlSnippet']?></p>
                                    </figcaption>
                                </figure>
                            </a>
                        </li>
	                <?endforeach?>
                </ul>
		
		        <?if($pages > 1):?>
                    <ul class="pagination">
                        <li class="pagination__prev"><a href="?q=<?=urlencode($query)?>&page=1"></a></li>
                        <?for ($i = 1; $i <= $pages; $i++):?>
                            <li<?if($i==$page):?> class="active"<?endif?>><a href="?q=<?=urlencode($query)?>&page=<?=$i?>"><?=$i?></a></li>
				        <?endfor?>
                        <li class="pagination__next"><a href="?q=<?=urlencode($query)?>&page=<?=$pages?>"></a></li>
                    </ul>
		        <?endif?>
	        <?endif?>
        </div>
    </div>
</div>