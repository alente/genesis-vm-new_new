<?php
use app\components\helpers\BImages;


?>
<div id="container" class="gatemain">
	<div class="inner-cont">
		<section class="cont-fullsize kv-area js-check-height" data-indicator-index="0">
			<div class="inner-cont-wrap">
				<div class="content">
					<!-- gate-swiper-wrap -->
					<div class="gate-swiper-wrap">
						<div class="swiper-container swiper-container-h">
							<!-- swiper-wrapper -->
							<div class="swiper-wrapper">
								<!-- section1 : key visual -->
								<div class="swiper-slide">
									
									<!-- kv slide -->
									<div class="swiper-container swiper-container-v">
										<div class="swiper-wrapper">
											<!-- kv01 -->
											<div class="swiper-slide">
												<div class="inner-cont-wrap">
													<div class="txt-holder">
														<div class="inner-txt">
															<h1 class="type-tit">
																<span>GENESIS</span>
															</h1>
															<div class="paragraph">
																<p>
																	Полноприводные седаны премиум-класса.
																</p>
															</div>
															
															<div class="gatemain-btn">
																<div class="gatemain-btn-wrap">
																	<a href="/configurator"><span>Конфигуратор</span></a>
																	<a href="/contacts"><span>Контакты</span></a>
																	<a href="/test-drive"><span>Тест-драйв</span></a>
																</div>
															</div>
														
														</div>
													</div>
													<div class="bg-holder">
														<img src="/images/gatemain/gatemain_kv.jpg" alt="" />
													</div>
													<span class="line"></span>
												</div>
											</div>
											<!-- //kv01 -->
										</div>
										<div class="swiper-pagination swiper-pagination-v"></div>
									</div>
									<!-- // kv slide -->
								
								</div>
								<!-- //section1 : key visual -->
								
								<!-- section2 : Models -->
								<div class="swiper-slide">
									<div class="intro-wrap">
										<ul class="model-list">
											<li>
												<a href="/g70.html">
													<strong>GENESIS G70</strong>
													<img src="/images/gatemain/genesis-g70-main.png" alt="G70">
												</a>
											</li>
											
											<li>
												<a href="/g80.html">
													<strong>GENESIS g80</strong>
													<img src="/images/gatemain/genesis-g80-main.png" alt="G80">
												</a>
											</li>
											<li>
												<a href="/g90.html">
													<strong>GENESIS g90</strong>
													<img src="/images/gatemain/genesis-g90-main.png" alt="G90">
												</a>
											</li>
											<?php /*<li>
												<a href="/g90.html#long">
													<strong>GENESIS G90 L</strong>
													<img src="/images/gatemain/genesis-g90l-main.png" alt="G90L">
												</a>
											</li>*/ ?>
										</ul>
									</div>
								</div>
								<!-- //section2 : Models -->
								
								<!-- section3 : contents -->
								<div class="swiper-slide">
									<div class="contents-wrap">
										
										<ul class="contents-tab">
											<li><a href="news.html">НОВОСТИ И МЕРОПРИЯТИЯ</a></li>
										</ul>
										
										<div class="contents-list">
											<!-- NEWS & EVENT contents -->
											<div class="tab01">
												<ul class="contents-area">
													<? foreach($news as $newsItem) { ?>		
													<li>
														<a href="<?=Yii::$app->urlManager->createUrl(['news/detail', 'handle' => $newsItem->handle])?>">
															<img src="<?=BImages::doCrop((($newsItem->image_preview!='')?$newsItem->image_preview:$newsItem->image), 300)?>" alt="<?=$newsItem->title?>">
															<div class="figcaption">
																<strong><?=$newsItem->title?></strong>
																<span class="date"><?=date('d.m.Y',$newsItem->getDateSrc('date'))?></span>
																<p><?=$newsItem->description?></p>
															</div>
														</a>
													</li>
													<? } ?>		
													
													<?php /*<li>
														<a href="news/g70-presented.html">
															<img src="/images/news/g70-presented-preview.jpg" alt="">
															<div class="figcaption">
																<strong>GENESIS ПРЕДСТАВИЛ НА РОССИЙСКОМ РЫНКЕ СПОРТИВНЫЙ СЕДАН G70</strong>
																<span class="date">12.04.2018</span>
																<p>Премиальный автомобильный бренд Genesis представил сегодня свою третью модель на российском рынке – элегантный спортивный седан G70.</p>
															</div>
														</a>
													</li>
													
													<li>
														<a href="news/app-hololens.html">
															<img src="/images/news/app-hololens-preview.jpg" alt="">
															<div class="figcaption">
																<strong>АВТОМОБИЛЬНЫЙ БРЕНД GENESIS ЗАПУСТИЛ ПРИЛОЖЕНИЕ  НА ПЛАТФОРМЕ MICROSOFT HOLOLENS</strong>
																<span class="date">02.04.2018</span>
																<p>Автомобильный бренд Genesis расширяет форматы общения с поклонниками марки и представляет приложение Genesis Presentation, доступное на базе Microsoft HoloLens.</p>
															</div>
														</a>
													</li>
													
													<li>
														<a href="news/essentia-concept.html">
															<img src="/images/news/essentia-concept-preview.jpg" alt="">
															<div class="figcaption">
																<strong>GENESIS ПРЕДСТАВИЛ КОНЦЕПТ-КАР ESSENTIA НА МЕЖДУНАРОДНОМ АВТОСАЛОНЕ В НЬЮ-ЙОРКЕ</strong>
																<span class="date">29.03.2018</span>
																<p>Вчера на Нью-Йоркском Международном Автосалоне бренд Genesis представил свой революционный концепт-кар Essentia – роскошный автомобиль с полностью электрической силовой установкой.</p>
															</div>
														</a>
													</li>
													
													<li>
														<a href="news/g70-premiere-new-york.html">
															<img src="/images/news/g70-premiere-new-york-preview.jpg" alt="">
															<div class="figcaption">
																<strong>ПРЕМЬЕРА СЕДАНА GENESIS G70 НА НЬЮ-ЙОРКСКОМ МЕЖДУНАРОДНОМ АВТОСАЛОНЕ</strong>
																<span class="date">29.03.2018</span>
																<p>На Нью-Йоркском Международном автосалоне NYIAS-2018 автомобильный бренд Genesis официально представил премиальный спортивный седан G70 2019 модельного года для рынка США</p>
															</div>
														</a>
													</li>
													
													<li>
														<a href="news/gran-pri-za-rulem.html">
															<img src="/images/news/gran-pri-za-rulem-preview.jpg" alt="">
															<div class="figcaption">
																<strong>БРЕНД GENESIS СТАЛ ПОБЕДИТЕЛЕМ КОНКУРСА ГРАН-ПРИ «ЗА РУЛЕМ» В НОМИНАЦИИ «ДЕБЮТ ГОДА»</strong>
																<span class="date">23.03.2018</span>
																<p>22 марта состоялось торжественное вручение наград Гран-при «За рулем» — ежегодной автомобильной премии в России, цель которой – определить лучшие автомобильные новинки 2017 года на российском рынке.</p>
															</div>
														</a>
													</li>
													
													
													<li>
														<a href="news/oscar-party-2018.html">
															<img src="/images/news/oscar-party-2018-preview.jpg" alt="">
															<div class="figcaption">
																<strong>БРЕНД GENESIS ДЕМОНСТРИРУЕТ ВЫСОКИЙ СТИЛЬ НА НЕДЕЛЕ ОСКАРА 2018, ПРЕДСТАВИВ 10 УНИКАЛЬНЫХ ПРЕМИАЛЬНЫХ СЕДАНОВ G90 СПЕЦИАЛЬНОЙ СЕРИИ</strong>
																<span class="date">1.03.2018</span>
																<p>На этой неделе, в преддверии юбилейной, 90-й церемонии вручения премии «Оскар» за заслуги в области кинематографа, премиальный бренд Genesis совместно со знаменитым журналом Vanity Fair впервые представит пять новых уникальных флагманских седанов G90 специальной серии.</p>
															</div>
														</a>
													</li>
													
													<li>
														<a href="news/if-design.html">
															<img src="/images/news/if-design-preview.jpg" alt="">
															<div class="figcaption">
																<strong>GENESIS G70 УДОСТОЕН НАГРАДЫ iF DESIGN 2018</strong>
																<span class="date">22.02.2018</span>
																<p>Элегантный спортивный седан Genesis G70 удостоен престижной награды iF Design 2018 в номинации «Дизайн транспортного средства».</p>
															</div>
														</a>
													</li>
													
													<li>
														<a href="news/invest-forum-partner.html">
															<img src="/images/news/invest-forum-partner.jpg" alt="">
															<div class="figcaption">
																<strong>БРЕНД GENESIS СТАЛ ОФИЦИАЛЬНЫМ АВТОМОБИЛЬНЫМ ПАРТНЕРОМ РОССИЙСКОГО ИНВЕСТИЦИОННОГО ФОРУМА</strong>
																<span class="date">15.02.2018</span>
																<p>Премиальный бренд Genesis в третий раз выступает официальным автомобильным партнером Российского инвестиционного форума 2018.</p>
															</div>
														</a>
													</li>
													
													<li>
														<a href="news/invest-forum.html">
															<img src="/images/news/invest-forum-preview.jpg" alt="">
															<div class="figcaption">
																<strong>АВТОМОБИЛЬНЫЙ БРЕНД GENESIS В ТРЕТИЙ РАЗ ПОДДЕРЖИТ РОССИЙСКИЙ ИНВЕСТИЦИОННЫЙ ФОРУМ</strong>
																<span class="date">6.02.2018</span>
																<p>На протяжении всех дней работы форума его спикеры и партнеры передвигаются на полноприводных седанах Genesis</strong> G80.</p>
															</div>
														</a>
													</li>
													
													<li>
														<a href="news/car5.html">
															<img src="/images/news/car5.jpg" alt="">
															<div class="figcaption">
																<strong>ПРЕМИАЛЬНЫЕ СЕДАНЫ GENESIS G80 ПОПОЛНЯТ ПАРК КАРШЕРИНГОВОЙ КОМПАНИИ CAR5</strong>
																<span class="date">5.12.2017</span>
																<p>Полноприводные седаны Genesis G80 пополнят парк каршеринговой компании CAR5. Первая партия составит 30 автомобилей.</p>
															</div>
														</a>
													</li>
													
													<li>
														<a href="news/special-terms.html">
															<img src="/images/news/special-terms.jpg" alt="">
															<div class="figcaption">
																<strong>СПЕЦИАЛЬНЫЕ УСЛОВИЯ КРЕДИТОВАНИЯ НА ПОКУПКУ СЕРТИФИЦИРОВАННЫХ АВТОМОБИЛЕЙ GENESIS С ПРОБЕГОМ</strong>
																<span class="date">4.12.2017</span>
																<p>Компания «Хендэ Мотор СНГ» информирует о новых выгодных условиях кредитования на покупку сертифицированных автомобилей Genesis с пробегом в рамках программы H-Promise.</p>
															</div>
														</a>
													</li>
													
													
													<li>
														<a href="news/choreography-close.html">
															<img src="/images/news/choreography-close-preview.jpg" alt="">
															<div class="figcaption">
																<strong>GENESIS ПОДДЕРЖАЛ  ЗАКРЫТИЕ V МЕЖДУНАРОДНОГО ФЕСТИВАЛЯСОВРЕМЕННОЙ ХОРЕОГРАФИИ CONTEXT. DIANA VISHNEVA</strong>
																<span class="date">20.11.2017</span>
																<p>19 ноября 2017 года на исторической сцене Мариинского театра состоялось гала-закрытие пятого международного фестиваля современной хореографии Context. Diana Vishneva. Официальным автомобильным партнером мероприятия выступил премиальный бренд Genesis.</p>
															</div>
														</a>
													</li>
													
													<li>
														<a href="news/ballet-nizhinsky.html">
															<img src="/images/news/ballet-preview.jpg" alt="">
															<div class="figcaption">
																<strong>ЭКСПОЗИЦИЯ GENESIS НА ПРЕМЬЕРЕ БАЛЕТА «НИЖИНСКИЙ» В РАМКАХ V МЕЖДУНАРОДНОГО ФЕСТИВАЛЯ СОВРЕМЕННОЙ ХОРЕОГРАФИИ CONTEXT. DIANA VISHNEVA</strong>
																<span class="date">15.11.2017</span>
																<p>14 ноября 2017 года в Театре им. Моссовета впервые в России прошел показ балета &laquo;Нижинский&raquo; в постановке Марко Гекке. Премьера состоялась в рамках пятого международного фестиваля современной хореографии Context. Diana Vishneva, который проходит при поддержке премиального автомобильного бренда Genesis.</p>
															</div>
														</a>
													</li>
													
													<li>
														<a href="news/dubai-2017.html">
															<img src="/images/news/dubai-2017-preview.jpg" alt="">
															<div class="figcaption">
																<strong>GENESIS НА МЕЖДУНАРОДНОМ АВТОСАЛОНЕ В ДУБАЕ-2017</strong>
																<span class="date">15.11.2017</span>
																<p>15 ноября 2017 года. Премиальный бренд&nbsp;Genesis, впервые участвующий в Дубайском международном автосалоне, представил сразу три модели для ближневосточного рынка -&nbsp; компактный седан&nbsp;G70, среднеразмерный седан G80 и флагманский премиальный седан&nbsp;G90.</p>
															</div>
														</a>
													</li>
													
													
													
													<li>
														<a href="news/contemporary-choreography-2.html">
															<img src="/images/news/choreography-2-1-preview.jpg" alt="">
															<div class="figcaption">
																<strong>Genesis поддержал открытие V международного фестиваля современной хореографии Context. Diana Vishneva</strong>
																<span class="date">14.11.2017</span>
																<p>13 ноября 2017 года в музыкальном театре им. К.С. Станиславского и Вл. И. Немировича-Данченко состоялось гала-открытие пятого международного фестиваля современной хореографии Context. Diana Vishneva.</p>
															</div>
														</a>
													</li>
													
													<li>
														<a href="news/energetic-week-2017.html">
															<img src="/images/news/choreography-preview.jpg" alt="">
															<div class="figcaption">
																<strong>Премиальный автомобильный бренд Genesis во второй раз поддержит международный фестиваль современной хореографии</strong>
																<span class="date">8.11.2017</span>
																<p>С 12 по 19 ноября 2017 года в Москве и Санкт-Петербурге состоится пятый международный фестиваль современной хореографии Context. Diana Vishneva.</p>
															</div>
														</a>
													</li>
													
													<li>
														<a href="news/energetic-week-2017.html">
															<img src="/images/news/energetic-week-2017-preview.jpg" alt="">
															<div class="figcaption">
																<strong>ПРЕМИАЛЬНЫЙ БРЕНД GENESIS – ОФИЦИАЛЬНЫЙ АВТОМОБИЛЬ ФОРУМА «РОССИЙСКАЯ ЭНЕРГЕТИЧЕСКАЯ НЕДЕЛЯ 2017»</strong>
																<span class="date">09.10.2017</span>
																<p>05 октября 2017 года. Премиальный автомобильный бренд Genesis выступает официальным автомобильным партнером Международного форума по энергоэффективности и развитию энергетики «Российская энергетическая неделя» (РЭН).</p>
															</div>
														</a>
													</li>
													
													<li>
														<a href="news/eastern-economic-forum.html">
															<img src="/images/news/econ-forum.jpg" alt="">
															<div class="figcaption">
																<strong>Премиальный бренд Genesis поддержал Восточный экономический форум.</strong>
																<span class="date">19.09.2017</span>
																<p>7 сентября 2017 года Genesis выступил официальным автомобильным партнером Восточного экономического форума, проходящего во Владивостоке 6-7 сентября. Для обеспечения транспортного обслуживания мероприятия компания предоставила организаторам форума 150 автомобилей Genesis G80.</p>
															</div>
														</a>
													</li>
													<li>
														<a href="news/the-hollywood-reporter.html">
															<img src="/images/news/news-10.jpg" alt="">
															<div class="figcaption">
																<strong>Бренд GENESIS выступил официальным автомобильным партнером вечеринки White Party журнала The Hollywood Reporter.</strong>
																<span class="date">26.06.2017</span>
																<p>24 июня в рамках 39-го Московского Международного кинофестиваля журнал The Hollywood Reporter собрал звезд российского кино на вечеринке White Party и вручил молодым кинематографистам ежегодную премию «Аванс». Премиальный автомобильный бренд Genesis выступил официальным партнером данного мероприятия.</p>
															</div>
														</a>
													</li>
													<li>
														<a href="news/miss-russia-2017.html">
															<img src="/images/news/miss-russia-2017-img-1.jpg" alt="">
															<div class="figcaption">
																<strong>GENESIS И ОФИЦИАЛЬНЫЙ ДИЛЕР МАРКИ «АВИЛОН» ПОДДЕРЖАЛИ КОНКУРС «МИСС РОССИЯ 2017»</strong>
																<span class="date">17.04.2017</span>
																<p>Genesis и официальный дилер марки «Авилон» выступили партнерами национального конкурса «Мисс Россия 2017», финал которого прошел 15 апреля 2017 года в концертном зале Барвиха Luxury Village.</p>
															</div>
														</a>
													</li>
													<li>
														<a href="news/top-safety-pick.html">
															<img src="/images/news/top-safety-pick-news-1.jpg" alt="">
															<div class="figcaption">
																<strong>Genesis G90 получил награду Top Safety Pick+.</strong>
																<span class="date">12.12.2016</span>
																<p>Genesis G90 2017 модельного года получил престижную награду Top Safety Pick+ в тесте безопасности Американского страхового института дорожной безопасности (IIHS).</p>
															</div>
														</a>
													</li>
													<li>
														<a href="news/context.html">
															<img src="/images/news/context-news-1.jpg" alt="">
															<div class="figcaption">
																<strong>Бренд Genesis поддержал гала-закрытие IV международного фестиваля современной хореографии CONTEXT. ДИАНА ВИШНЁВА</strong>
																<span class="date">21.11.2016</span>
																<p>19 ноября в Санкт-Петербурге состоялось торжественное закрытие IV Международного фестиваля современной хореографии CONTEXT.Диана Вишнёва, организованного выдающейся российской балериной Дианой Вишнёвой.</p>
															</div>
														</a>
													</li>
													<li>
														<a href="news/press-release.html">
															<img src="/images/news/news-8.jpg" alt="">
															<div class="figcaption">
																<strong>Genesis поддержал гала-открытие IV международного фестиваля современной хореографии CONTEXT. ДИАНА ВИШНЁВА</strong>
																<span class="date">15.11.2016</span>
																<p>14 ноября в Музыкальном театре им. К.С. Станиславского и Вл. И. Немировича-Данченко состоялось гала-открытие международного фестиваля современной хореографии CONTEXT. ДИАНА ВИШНЁВА</p>
															</div>
														</a>
													</li>
													<li>
														<a href="news/vishneva.html">
															<img src="/images/news/news-6.jpg" alt="">
															<div class="figcaption">
																<strong>Международный фестиваль современной хореографии CONTEXT. ДИАНА ВИШНЕВА</strong>
																<span class="date">27.10.2016</span>
																<p>С 14 по 19 ноября 2016 года сразу в двух городах — Москве и Санкт-Петербурге — состоится международный фестиваль современной хореографии CONTEXT. ДИАНА ВИШНЁВА</p>
															</div>
														</a>
													</li>
													<li>
														<a href="news/award.html">
															<img src="/images/news/award-news-1.jpg" alt="">
															<div class="figcaption">
																<strong>Genesis G90 стал обладателем престижной награды для автомобилей премиум-класса.</strong>
																<span class="date">20.10.2016</span>
																<p>Флагманская модель Genesis признана «Лучшим седаном» на выставке роскошных автомобилей EXCS Luxury Motor Show.</p>
															</div>
														</a>
													</li>
													<li>
														<a href="news/official-partner.html">
															<img src="/images/news/official-partner-news-1.jpg" alt="">
															<div class="figcaption">
																<strong>Новый премиальный автомобильный бренд Genesis – официальный партнер Международного инвестиционного форума «Сочи-2016».</strong>
																<span class="date">28.09.2016</span>
																<p>Премиальный бренд Genesis выступит официальным автомобильным партнером XV Международного инвестиционного форума «Сочи-2016», который пройдет с 29 сентября по 2 октября 2016 года в Главном медиацентре Олимпийского парка.</p>
															</div>
														</a>
													</li>
													<li>
														<a href="news/g90-launch.html">
															<img src="/images/news/news-5.jpg" alt="">
															<div class="figcaption">
																<strong>Премиальный бренд Genesis выходит на российский рынок.</strong>
																<span class="date">22.09.2016</span>
																<p>22 сентября 2016 года в концертном зале «Барвиха Luxury Village» состоялась российская премьера премиального бренда Genesis.</p>
															</div>
														</a>
													</li>
													<li>
														<a href="news/genesis-studio.html">
															<img src="/images/news/genesis-studio-news-1.jpg" alt="">
															<div class="figcaption">
																<strong>Компания Hyundai открывает в Ханаме первую в мире галерею Genesis Studio.</strong>
																<span class="date">05.09.2016</span>
																<p>В Корее откроется первое в мире пространство бренда Genesis</p>
															</div>
														</a>
													</li>*/ ?>
												</ul>
            
											</div>
											
										</div>
									
									
									</div>
								</div>
								<!-- //section3 : contents -->
							</div>
							<!-- //swiper-wrapper -->
							<!-- gatemain btn-wrap-->
							<div class="gate-swiper-btn">
								<div class="swiper-pagination swiper-pagination-h"></div>
								<div class="swiper-button-next">НОВОСТИ</div>
								<div class="swiper-button-prev">МОДЕЛИ</div>
								<ul class="">
									<li></li>
									<li></li>
								</ul>
							</div>
							<!-- //gatemain btn-wrap-->
						</div>
					</div>
					<!-- //gate-swiper-wrap -->
				</div>
			</div>
		</section>
	</div>
</div>
<style>
.mobile-fb-btn{
    display: none;
	bottom: 50px !important;
	z-index: 20;
}
.gatemain .cont-fullsize .txt-holder.active .gatemain-btn a:first-child{
	margin-top: 50px !important;
}
.gatemain .cont-fullsize .txt-holder{
	margin-top: 50px !important;
    top: 50% !important;
    -moz-transform: translateY(-50%);
	-webkit-transform: translateY(-50%);
	-o-transform: translateY(-50%);
	-ms-transform: translateY(-50%);
	transform: translateY(-50%);
    margin-bottom: 0;
}
</style>