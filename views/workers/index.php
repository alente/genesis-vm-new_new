<?
use app\components\helpers\BImages;
use app\components\helpers\TextHelper;
$this->title = 'Наша команда';
?>
    <link type="text/css" rel="stylesheet" href="css/build.css"/>
    <?/*<link type="text/css" rel="stylesheet" href="styles/header/header.css">*/?>
    <link type="text/css" rel="stylesheet" href="css/workers.css"/>
    
<div class="layout__content">
		<div id="workers" class="workers">
			<div class="section section--workers-x-breed">
				<div class="section__content">
					<div class="breed-screen">
						<div class="breed-screen-content">
							<div class="breed-screen-items">
								<div class="breed-screen-item breed-screen-item--main">
										<span class="breed-screen-heading">
											<strong class="breed-screen-header">НАША КОМАНДА!</strong>
											<?/*
                                                                                        <span class="breed-screen-description">
                                                                                            Наши сотрудники всегда готовы помочь&nbsp;Вам
                                                                                            принять правильное решение. Выберете&nbsp;своего
                                                                                            личного&nbsp;консультанта.
                                                                                        </span>
                                            */?>
										</span>
								</div>

								<div class="breed-screen-item breed-screen-item--cat">
									<a href="#workers-sales" class="breed-screen-link is-current">
											<span class="breed-screen-link-cont">
												<strong>Продажи</strong>
											</span>
									</a>
								</div>
								<div class="breed-screen-item breed-screen-item--cat">
									<a href="#workers-service" class="breed-screen-link">
											<span class="breed-screen-link-cont">
												<strong>Сервис</strong>
											</span>
									</a>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
      
      <? foreach($workers as $worker) { 
        if ($worker->type == 0) $sales[]=$worker; else $service[]=$worker; 
       } ?>
      
			<div class="section section--workers-x-cont">
        
				<div class="section__content workers-x-cont is-current" id="workers-sales">
					<div class="workers-list-wrapper">
						<ul class="workers-list">
            
							<? foreach($sales as $sale) {?>
							<li class="workers-item">
								<div class="workers-item-content">
									<div class="worker-avatar"><img src="<?=$sale->image;?>" class="worker-avatar__img js-ofit"></div>
									<div class="worker-name"><?=$sale->name;?><br><?=$sale->first_name;?></div>
									<div class="worker-post"><?=$sale->position;?></div>
									<div class="worker-contacts">
										<a href="mailto:<?=$sale->mail;?>" class="worker-mail"><?=$sale->mail;?></a>
										<a href="tel:<?=$sale->phone_link;?>" class="worker-phone"><?=$sale->phone;?></a>
									</div>
								</div>
							</li>
							<? } ?>
							<?/*
                                                            <li class="workers-item workers-item--feedback">
                                                                <div class="workers-item-content">

                                                                </div>
                                                            </li>
                            */?>
						</ul>
					</div>
				</div>
        
				<div class="section__content workers-x-cont" id="workers-service">
					<div class="workers-list-wrapper">
						<ul class="workers-list">
							<? foreach($service as $serv) {?>
							<li class="workers-item">
								<div class="workers-item-content">
									<div class="worker-avatar"><img src="<?=$serv->image;?>" class="worker-avatar__img js-ofit"></div>
									<div class="worker-name"><?=$serv->name;?><br><?=$serv->first_name;?></div>
									<div class="worker-post"><?=$serv->position;?></div>
									<div class="worker-contacts">
										<a href="mailto:<?=$serv->mail;?>" class="worker-mail"><?=$serv->mail;?></a>
										<a href="tel:<?=$serv->phone_link;?>" class="worker-phone"><?=$serv->phone;?></a>
									</div>
								</div>
							</li>
							<? } ?>

							<?/*
                                                            <li class="workers-item workers-item--feedback">
                                                                <div class="workers-item-content">

                                                                </div>
                                                            </li>
                            */?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="mobileDetector" id="mobileDetector"></div>
<script src="js/build.js" charset="utf-8"></script>

<!-- Add scripts here if needed custom for a page as-->
<svg hidden="hidden" style="display: none !important;" width="0" height="0">
	<symbol id="rouble-svg" viewBox="0 0 28.35 36.85">
		<title>Rouble</title>
		<path d="M12.14,21.1q.94,0,2,0a15.9,15.9,0,0,0,5.36-.86,9.43,9.43,0,0,0,4-2.57,8.32,8.32,0,0,0,2.24-6.17A8.23,8.23,0,0,0,25,8.12a7.57,7.57,0,0,0-1.82-2.53,9.17,9.17,0,0,0-3.4-2,15.57,15.57,0,0,0-5-.71q-2,0-3.73.17a29,29,0,0,0-3.12.47V25.15H2.67v2.31H7.94v6.46h2.41V27.46h9.56V25.15H10.36v-4.3A8.61,8.61,0,0,0,12.14,21.1ZM10.36,5.2A16.87,16.87,0,0,1,12.09,5q1.12-.11,2.61-.11a13.9,13.9,0,0,1,3.47.41,7.44,7.44,0,0,1,2.7,1.26A5.79,5.79,0,0,1,22.6,8.69a7.37,7.37,0,0,1,.61,3.15,6.71,6.71,0,0,1-2.33,5.51,10,10,0,0,1-6.54,1.91q-1.14,0-2.17-.09a6.68,6.68,0,0,1-1.82-.39Z"/>
	</symbol>
	<symbol id="arrow-svg" viewBox="0 0 40 15">
		<title>Arrow</title>
		<path d="M37.32,7h0L32.37,2.6v.9l3.48,3.1H2.34v1h33.5l-3.47,3.08a2.91,2.91,0,0,0,0,.86S37.3,7.18,37.3,7.18A.13.13,0,0,0,37.32,7Z"/>
	</symbol>
</svg>
<script>
	// workers scripting
	var	_mobileActivityClass = 'is-mobile-active',
		_mobilebp = '768px',
		_isMobileVW,
		_links = document.querySelectorAll('.breed-screen-link'),
		_xContentIds = [],
		_xContentClass = '.workers-x-cont',
		_xContentActivityClass = 'is-current',
		$workerSlick = $('#workers-sales .workers-list'),
		$workerSlick2 = $('#workers-service .workers-list');

	var slickTO, slickTO2;

	for (var i=0;i<_links.length;++i) {
		_links[i].addEventListener('click', toggleXcontent)
		_xContentIds.push(_links[i].getAttribute('href'))
	}

	window.addEventListener('resize', checkViewport)

	setTimeout(function(){
		toggleXcontent()
		checkViewport()
	}, 250)

	function toggleXcontent(ev) {
		var _newContentId;

		if (ev && ev.type !== 'DOMContentLoaded') {
			ev.preventDefault();
			_newContentId = this.getAttribute('href');
		} else {
			_newContentId = window.location.hash;
		}

		if (_newContentId === void 0 || _newContentId.length < 2) {
			return false
		} else {
			for (var i=0;i<_xContentIds.length;++i) {
				if (_xContentIds[i] === _newContentId) {
					var _$currentTarget = $(_newContentId), _$allTargets = $(_xContentClass);

					if (_$currentTarget.length) {
						if (!this.classList || (this.classList && !this.classList.contains(_xContentActivityClass))) {
							for (var i=0;i<_links.length;++i) {
								if (_links[i].getAttribute('href') === _newContentId) {
									_links[i].classList.add(_xContentActivityClass)
								} else {
									_links[i].classList.remove(_xContentActivityClass)
								}
							}
						}
						$('html, body').stop().animate({scrollTop: ($('.section--workers-x-cont').offset().top - $('.layout__header').height())}, function(){
							if (!_$currentTarget.hasClass(_xContentActivityClass)) {
								_$allTargets.filter('.'+_xContentActivityClass).fadeOut(250, function(){
									$(this).removeClass(_xContentActivityClass);

									$workerSlick.css('opacity', 0);
									$workerSlick2.css('opacity', 0);

									_$currentTarget.fadeIn(250, function(){
										$(this).addClass(_xContentActivityClass);

										setTimeout(function(){
											$workerSlick.css('opacity', 1);
											$workerSlick2.css('opacity', 1);
										}, 50);

										if($workerSlick.hasClass('slick-initialized'))
										{
											$workerSlick.slick('setPosition');
											$workerSlick2.slick('setPosition');
										}

									})
								})
							}
						})
					}
				}
			}
		}
	}

	function checkViewport() {
		if ((window.matchMedia && window.matchMedia('(max-width: '+_mobilebp+')').matches) || (window.innerWidth < 641)) {
			_isMobileVW = true;
		} else {
			_isMobileVW = false;
		}

		checkSlick();
		checkSlick2();

		return _isMobileVW;
	}

	checkSlick();
	checkSlick2();

	function checkSlick(){
		if (slickTO) clearTimeout(slickTO)

		slickTO = setTimeout(function(){
			if (_isMobileVW && !$workerSlick.hasClass('slick-initialized')) {
				$workerSlick.slick({
					centerMode: true,
					centerPadding: '160px',
					slidesToShow: 1,
					arrows: false,
					dots: true,
					responsive: [
						{
							breakpoint: 641,
							settings: {
								centerMode: false,
								arrows: true,
								centerPadding: '45px',
								slidesToShow: 1
							}
						}
					]
				})
			} else if (!_isMobileVW && $workerSlick.hasClass('slick-initialized')) {
				$workerSlick.slick('unslick')
			}
		}, 250)
	}



	function checkSlick2(){
		if (slickTO2) clearTimeout(slickTO2)

		slickTO2 = setTimeout(function(){
			if (_isMobileVW && !$workerSlick2.hasClass('slick-initialized')) {
				$workerSlick2.slick({
					centerMode: true,
					centerPadding: '160px',
					slidesToShow: 1,
					arrows: false,
					dots: true,
					responsive: [
						{
							breakpoint: 641,
							settings: {
								centerMode: false,
								arrows: true,
								centerPadding: '45px',
								slidesToShow: 1
							}
						}
					]
				})
			} else if (!_isMobileVW && $workerSlick2.hasClass('slick-initialized')) {
				$workerSlick2.slick('unslick')
			}
		}, 250)
	}

	// iedge object-fit fallback
	/*! npm.im/object-fit-images */
	var objectFitImages=function(){"use strict";function t(t){for(var e,r=getComputedStyle(t).fontFamily,i={};null!==(e=c.exec(r));)i[e[1]]=e[2];return i}function e(e,i){if(!e[n].parsingSrcset){var s=t(e);if(s["object-fit"]=s["object-fit"]||"fill",!e[n].s){if("fill"===s["object-fit"])return;if(!e[n].skipTest&&l&&!s["object-position"])return}var c=e[n].ios7src||e.currentSrc||e.src;if(i)c=i;else if(e.srcset&&!u&&window.picturefill){var o=window.picturefill._;e[n].parsingSrcset=!0,e[o.ns]&&e[o.ns].evaled||o.fillImg(e,{reselect:!0}),e[o.ns].curSrc||(e[o.ns].supported=!1,o.fillImg(e,{reselect:!0})),delete e[n].parsingSrcset,c=e[o.ns].curSrc||c}if(e[n].s)e[n].s=c,i&&(e[n].srcAttr=i);else{e[n]={s:c,srcAttr:i||f.call(e,"src"),srcsetAttr:e.srcset},e.src=n;try{e.srcset&&(e.srcset="",Object.defineProperty(e,"srcset",{value:e[n].srcsetAttr})),r(e)}catch(t){e[n].ios7src=c}}e.style.backgroundImage='url("'+c+'")',e.style.backgroundPosition=s["object-position"]||"center",e.style.backgroundRepeat="no-repeat",/scale-down/.test(s["object-fit"])?(e[n].i||(e[n].i=new Image,e[n].i.src=c),function t(){return e[n].i.naturalWidth?void(e[n].i.naturalWidth>e.width||e[n].i.naturalHeight>e.height?e.style.backgroundSize="contain":e.style.backgroundSize="auto"):void setTimeout(t,100)}()):e.style.backgroundSize=s["object-fit"].replace("none","auto").replace("fill","100% 100%")}}function r(t){var r={get:function(){return t[n].s},set:function(r){return delete t[n].i,e(t,r),r}};Object.defineProperty(t,"src",r),Object.defineProperty(t,"currentSrc",{get:r.get})}function i(){a||(HTMLImageElement.prototype.getAttribute=function(t){return!this[n]||"src"!==t&&"srcset"!==t?f.call(this,t):this[n][t+"Attr"]},HTMLImageElement.prototype.setAttribute=function(t,e){!this[n]||"src"!==t&&"srcset"!==t?g.call(this,t,e):this["src"===t?"src":t+"Attr"]=String(e)})}function s(t,r){var i=!A&&!t;if(r=r||{},t=t||"img",a&&!r.skipTest)return!1;"string"==typeof t?t=document.querySelectorAll("img"):"length"in t&&(t=[t]);for(var c=0;c<t.length;c++)t[c][n]=t[c][n]||r,e(t[c]);i&&(document.body.addEventListener("load",function(t){"IMG"===t.target.tagName&&s(t.target,{skipTest:r.skipTest})},!0),A=!0,t="img"),r.watchMQ&&window.addEventListener("resize",s.bind(null,t,{skipTest:r.skipTest}))}var n="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==",c=/(object-fit|object-position)\s*:\s*([-\w\s%]+)/g,o=new Image,l="object-fit"in o.style,a="object-position"in o.style,u="string"==typeof o.currentSrc,f=o.getAttribute,g=o.setAttribute,A=!1;return s.supportsObjectFit=l,s.supportsObjectPosition=a,i(),s}();

	objectFitImages('.js-ofit', {watchMQ: true});
</script>