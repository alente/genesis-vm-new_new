<?php
$this->title = 'Genesis';
?>  
    <link type="text/css" rel="stylesheet" href="css/build.css"/>
	<?/*
    <link type="text/css" rel="stylesheet" href="styles/header/header.css">
	*/?>

<div class="layout__content">
            <style type="text/css">
                /*! normalize.css v3.0.2 | MIT License | git.io/normalize */
                html, body {
                    font: 13px/1.5 GenesisSansText, sans-serif;
                    color: #333;
                    background:#efefed;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    -webkit-text-size-adjust:none;
                    /*min-width: 960px;*/
                }
                *, *:before, *:after {
                    -moz-box-sizing: border-box;
                    -webkit-box-sizing: border-box;
                    box-sizing: border-box;
                }
                h1,.h1 {
                    font-weight: bold;
                    font-size:1.8em;margin: 25px 0 15px;
                }
                h2,.h2 {
                    font-weight: bold;
                    font-size:1.5em;margin: 25px 0 15px;
                }
                a{
                    color:#9A5341;
                    text-decoration:none;
                }
                a:visited{}
                a:hover{
                    text-decoration:underline;
                }
                a:active{}
                a:focus{outline: none;}
                html:not(.desktop) a[href^=tel]{
                    text-decoration: underline;
                    color: inherit;
                }
                /* Form tags styles
-----------------------------------------------------------*/
                input[type=text],input[type=search],input[type=tel],input[type=email],input[type=password],
                textarea,
                select {
                    position: relative;
                    font: 12px GenesisSansText, sans-serif;
                    outline: none;
                    padding: 4px;
                    border: 1px solid #ccc;
                    color: black;
                    vertical-align: middle;
                    overflow: hidden;
                    background: #fff;
                    -ms-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    -webkit-box-sizing: border-box;
                    box-sizing: border-box; }
                textarea{
                    overflow: auto;
                }
                input::-webkit-input-placeholder,
                textarea::-webkit-input-placeholder,
                select::-webkit-input-placeholder {
                    color: #bcbcbc; }
                input:-ms-input-placeholder,
                textarea:-ms-input-placeholder,
                select:-ms-input-placeholder {
                    color: #bcbcbc;  }
                input:-moz-placeholder,
                textarea:-moz-placeholder,
                select:-moz-placeholder {
                    color: #bcbcbc;opacity: 1;  }
                input::-moz-placeholder,
                textarea::-moz-placeholder,
                select::-moz-placeholder {
                    color: #bcbcbc;opacity: 1;  }
                input.placeholder,
                textarea.placeholder,
                select.placeholder {
                    color: #bcbcbc; }
                input:focus:-moz-placeholder,
                textarea:focus:-moz-placeholder,
                select:focus:-moz-placeholder {
                    color: transparent !important; }
                input:focus::-moz-placeholder,
                textarea:focus::-moz-placeholder,
                select:focus::-moz-placeholder {
                    color: transparent !important; }
                input:focus:-ms-input-placeholder,
                textarea:focus:-ms-input-placeholder,
                select:focus:-ms-input-placeholder {
                    color: transparent !important; }
                input:focus::-webkit-input-placeholder,
                textarea:focus::-webkit-input-placeholder,
                select:focus::-webkit-input-placeholder {
                    color: transparent !important; }
                input.placeholder:focus,
                textarea.placeholder:focus,
                select.placeholder:focus {
                    color: transparent !important; }
                input[type=text]:focus,input[type=search]:focus,input[type=tel]:focus,input[type=email]:focus,input[type=password]:focus,
                textarea:focus,
                select:focus {
                    color: #606060;
                    box-shadow: 0 0 3px rgba(0, 0, 0, 0.2); }
                select {
                    padding: 0; }
                /*.button,
		button,*/
                input[type="submit"],
                input[type="reset"],
                input[type="button"] {
                    position: relative;
                    text-decoration: none;
                    outline: none;
                    padding: 4px 8px;
                    border: 1px solid #ccc;
                    font: 12px GenesisSansText, sans-serif;
                    display: inline-block; }
                .button:active,
                button:active,
                input[type="submit"]:active,
                input[type="reset"]:active,
                input[type="button"]:active {}
                /* Meta classes
-----------------------------------------------------------*/
                .reset{
                    margin: 0;
                    padding: 0;
                    list-style: none;
                }
                .hidden {
                    display: none; }
                .show {
                    display: block !important; }
                .visible {
                    visibility: visible !important; }
                .no-margin {
                    margin: 0 !important; }
                .no-padding {
                    padding: 0 !important; }
                .no-bg {
                    background: none !important; }
                .no-border {
                    border: none !important; }
                .bold {
                    font-weight: 700; }
                .italic {
                    font-style: italic; }
                .underline {
                    text-decoration: underline; }
                .uppercase {
                    text-transform: uppercase; }
                .lowercase {
                    text-transform: lowercase; }
                .disabled {
                    cursor: default !important;
                    opacity: .3;
                    filter: alpha(opacity=30); }
                .error {
                    color: red !important; }
                input.error {
                    outline: 1px solid red !important;
                    outline-offset: -1px; }
                .va-top {
                    vertical-align: top !important; }
                .red {
                    color: red; }
                .blue {
                    color: blue; }
                .yellow {
                    color: yellow; }
                .green {
                    color: green; }
                .gray {
                    color: gray; }
                .orange {
                    color: orange; }
                .f-left, .float-left, .left {
                    position: relative;
                    float: left; }
                .f-right, .float-right, .right {
                    position: relative;
                    float: right; }
                .f-center, .float-center {
                    position: relative;
                    float: none;
                    margin: 0 auto;
                    padding: 0 10px;
                    /*width: 980px; */
                }
                .t-center, .text-center {
                    text-align: center; }
                .t-left, .text-left {
                    text-align: left; }
                .t-right, .text-right {
                    text-align: right; }
                .t-justify, .text-justify {
                    text-align: justify; }
                .col {
                    position: relative;
                    margin: 0 0 15px;
                    padding: 0;zoom: 1; }
                .pic-default{
                    background-repeat:no-repeat;
                    background-position:center center;
                    background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAjAgMAAACbV6A9AAAADFBMVEUAAAAAAAAAAAAAAAA16TeWAAAABHRSTlMNAAUJRVLypgAAAMxJREFUGNNdz7HNwjAQBeD3x/oLCkTFCB4hDRWCBdK8JIgULpFowgYWO9wcMIKX8AgUdAxAQThfGl7jrzi9O4NkD/jp+TIAC2MEqsIWU0TZAzqMIywPjDOXiDMrpJkOP3meyqv9UaFbg7Fm+zKuOcCSOW7OhcIkHAu7pR0F6f/tVOTBXwYmbWhyCj5qbxDAb3VbkC2yVrggTWVtLwnOCEnwI5ISd79H1LpdI9fy4/rQ8g9HO75bgFPaihxqJeODMRcG93Ys7KZxI28r4Qc6l0eAD7DptgAAAABJRU5ErkJggg==');
                    min-height: 35px;
                    min-width: 40px;
                }
                .col:after, .f-row:after, .clearfix:after {
                    content: ".";
                    display: block;
                    height: 0;
                    line-height: 0;
                    clear: both;
                    visibility: hidden;
                    font-size: 0;
                    zoom: 1;}
                /* Grid system
-----------------------------------------------------------*/
                .f-row{
                    margin: 0 -10px 15px;
                    padding: 0;zoom: 1;
                }
                .f-row-item{
                    float: left;
                    margin-bottom: 15px;
                    padding: 0 10px;
                }
                .w-10 {
                    width: 10%; }
                .w-15 {
                    width: 15%; }
                .w-20 {
                    width: 20%; }
                .w-25 {
                    width: 25%; }
                .w-30 {
                    width: 30%; }
                .w-33 {
                    width: 33.3333%; }
                .w-35 {
                    width: 35%; }
                .w-40 {
                    width: 40%; }
                .w-45 {
                    width: 45%; }
                .w-50 {
                    width: 50%; }
                .w-60 {
                    width: 60%; }
                .w-66 {
                    width: 66.6667%; }
                .w-70 {
                    width: 70%; }
                .w-75 {
                    width: 75%; }
                .w-80 {
                    width: 80%; }
                .w-85 {
                    width: 85%; }
                .w-90 {
                    width: 90%; }
                .full-width, .w-100 {
                    width: 100%; }
                /* Wrapper
-----------------------------------------------------------*/
                #wrapper,#header,#content,#footer,.pie{position:relative}
                #wrapper{
                    min-height:100%;
                    height: auto !important;
                    height:100%;
                    margin:0 auto;
                }
                /* Header
-----------------------------------------------------------*/
                #header{
                    background: #fff;
                }
                .header-center{
                    position: relative;
                    width: 940px;
                    margin: 0 auto;
                    padding: 6px 0 3px;
                }
                /* Container
-----------------------------------------------------------*/
                #content{
                    padding: 0;
                    zoom: 1;
                }
                #content:after{content: ".";display: block;height: 0;clear: both;visibility: hidden;}
                .columns{
                    position: relative;
                    width: 940px;
                    margin: 0 auto;
                }
                .center{
                    position: relative;
                    width: 940px;
                    margin: 0 auto;
                    padding-top: 50px;
                }
                .columns-main{
                    float:left;
                    width:100%;
                    position: relative;
                }
                .columns-main-content{
                    margin: 0 260px;
                    position: relative;
                }
                /* Sidebar
-----------------------------------------------------------*/
                .columns-left{
                    width:250px;
                    margin-left:-100%;
                    float:left;
                    background:#808080;
                    position: relative;
                }
                .columns-right{
                    width:250px;
                    margin-left: -254px;
                    float:right;
                    background:#808040;
                    position: relative;
                }
                /* Footer
-----------------------------------------------------------*/
                #footer{
                    margin:0 auto 0;
                    clear:both;
                }
                .footer-center{
                    position: relative;
                    width: 940px;
                    margin: 0 auto;
                    padding: 34px 0 0;
                }
                /* Style guide
-----------------------------------------------------------*/
                .pic.f-left{
                    margin: 4px 15px 10px 0
                }
                .pic.f-right{
                    margin: 4px 0 10px 15px
                }
                .table{
                    width: 100%;
                    max-width: 100%;
                    margin-bottom: 20px
                }
                .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{
                    padding: 8px;
                    line-height: 1.42857143;
                    vertical-align: top;
                    border-top: 1px solid #ddd;
                    text-align: left
                }
                .table > thead > tr > th{
                    vertical-align: bottom;
                    border-bottom: 2px solid #ddd;
                    text-align: left
                }
                .table > caption + thead > tr:first-child > td, .table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table > thead:first-child > tr:first-child > th{
                    border-top: 0
                }
                .fotorama, .fotorama *{
                    -webkit-box-sizing: content-box;
                    -moz-box-sizing: content-box;
                    box-sizing: content-box
                }
                .note{
                    border-left: 5px solid #ccc;
                    margin-right: 100px;
                    padding: 5px 0 5px 15px
                }
                .link-underline-dashed{
                    text-decoration: none;
                    cursor: pointer;
                    border-bottom: 1px dashed #000
                }
                .link-underline-dashed:hover{
                    text-decoration: none;
                    border-bottom-color: transparent
                }
                .hint{
                    display: inline-block;
                    position: relative;
                    text-decoration: none;
                    border-bottom: 1px dotted #a1a092;
                    line-height: 13px;
                    cursor: pointer
                }
                abbr{
                    font-weight: 700;
                    font-style: normal
                }
                .hint-body{
                    background: none repeat scroll 0 0 #FFF;
                    border-radius: 6px;
                    bottom: 100%;
                    box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.3);
                    color: #000;
                    display: block;
                    font-size: 12px;
                    left: 50%;
                    line-height: 14px;
                    margin-bottom: 16px;
                    margin-left: -71px;
                    padding: 11px 11px 8px;
                    position: absolute;
                    width: 142px;
                    z-index: 1;
                    font-weight: 400;
                    cursor: default;
                    visibility: hidden;
                    opacity: 0;
                    -webkit-transition: all .2s ease-out;
                    -moz-transition: all .2s ease-out;
                    -ms-transition: all .2s ease-out;
                    -o-transition: all .2s ease-out;
                    transition: all .2s ease-out
                }
                .hint-body:after{
                    position: absolute;
                    width: 0;
                    height: 0;
                    border-top: 6px solid #fff;
                    border-left: 6px solid transparent;
                    border-right: 6px solid transparent;
                    border-bottom: 6px solid transparent;
                    top: 100%;
                    left: 50%;
                    content: "";
                    margin-left: -6px;
                    margin-top: 0
                }
                .hint-body--w{
                    margin-left: -142px;
                    width: 284px
                }
                .hint:hover .hint-body{
                    visibility: visible;
                    opacity: 1;
                    -webkit-transition: all .2s ease-out;
                    -moz-transition: all .2s ease-out;
                    -ms-transition: all .2s ease-out;
                    -o-transition: all .2s ease-out;
                    transition: all .2s ease-out;
                    margin-bottom: 6px
                }
                .hint-body-head{
                    font-weight: 700;
                    font-size: 14px;
                    line-height: 16px;
                    margin-bottom: 7px;
                    display: block
                }
                .hint-body-p{
                    display: block
                }
                .blockquote{
                    padding: 5px 15px;
                    margin: 15px 0;
                    border-left: 5px solid #eee
                }
                .blockquote .small, .blockquote footer, .blockquote small{
                    display: block;
                    font-size: 80%;
                    line-height: 1.42857143;
                    color: #777
                }
                .blockquote p{
                    margin: 0 0 10px
                }
                .blockquote p:after{
                    content: "»"
                }
                .blockquote p:before{
                    content: "«"
                }
                .incut{
                    background: none repeat scroll 0 0 #fefefe;
                    font-size: 1.5em;
                    line-height: 1.2;
                    margin: 20px 0;
                    padding: 15px 20px;
                    border: 1px solid #ccc
                }
                .tabs{
                    margin: 15px 0
                }
                .tabs-menu{
                    position: relative
                }
                .tabs-menu-item{
                    border: 1px solid #ccc;
                    border-bottom: 0;
                    position: relative;
                    float: left;
                    padding: 5px 15px;
                    -webkit-border-radius: 4px 4px 0 0;
                    -moz-border-radius: 4px 4px 0 0;
                    border-radius: 4px 4px 0 0;
                    margin-right: 5px;
                    cursor: pointer;
                    background: #fafafa
                }
                .tabs-content{
                    border: 1px solid #ccc;
                    padding: 15px;
                    -webkit-border-radius: 0 4px 4px 4px;
                    -moz-border-radius: 0 4px 4px;
                    border-radius: 0 4px 4px 4px;
                    background: #fff;
                }
                .tabs-content > div{
                    display: none
                }
                .tabs-content > div.active{
                    display: block
                }
                .tabs-menu-item.active{
                    background: none repeat scroll 0 0 #fff;
                    margin-bottom: 0;
                    margin-top: -6px;
                    padding: 8px 20px;
                    top: 1px;
                    cursor: default;
                }
                .guide-menu{
                    position: fixed;
                    left: 0;
                    background: #fff
                }
                .guide-menu-item-link{
                    color: #666;
                    text-decoration: none;
                    -webkit-transition: all .2s ease-out;
                    -moz-transition: all .2s ease-out;
                    -ms-transition: all .2s ease-out;
                    -o-transition: all .2s ease-out;
                    transition: all .2s ease-out;
                    display: inline-block;
                    background: none repeat scroll 0 0 #eee;
                    padding: 2px 6px
                }
                .guide-menu-item-link:hover{
                    text-decoration: none;
                    color: #000;
                    -webkit-transition: all .2s ease-out;
                    -moz-transition: all .2s ease-out;
                    -ms-transition: all .2s ease-out;
                    -o-transition: all .2s ease-out;
                    transition: all .2s ease-out;
                    margin-left: 5px;
                    margin-right: -5px
                }
                .guide-menu-item{
                    padding: 4px 0 4px 3px;
                    width: 200px;
                    line-height: 16px;
                    border-bottom: 1px solid #eee
                }
                .guide-menu-item:last-child{
                    border-bottom: 0
                }
                @media only screen and (max-width: 1400px), only screen and (max-device-width: 1400px){
                    .guide-menu{
                        position: static;
                        top: 0;
                        left: 0;
                        margin: 15px 0
                    }

                    .guide-menu-item{
                        padding: 0;
                        display: inline-block;
                        border: 0;
                        margin-right: 15px;
                        margin-bottom: 5px;
                        width: auto
                    }

                    .guide-menu-item-link{
                        display: inline
                    }

                    .guide-menu-item-link:hover{
                        margin: 0
                    }
                }
                .guide-menu-item:first-child{
                    border-top: 0
                }
                .feedback{
                    margin: 20px 0;
                }
                .feedback-item{
                    margin-bottom: 15px;
                }
                .feedback-item-left{
                    position: relative;
                    float: left;
                    width: 145px;
                    display: block;
                    font-size: 14px;
                    font-weight: bold;
                    padding-top: 6px;
                    text-align: right;
                }
                .feedback-item-right{
                    margin-left: 166px;
                }
                input.input-text,.input-text{
                    padding: 9px;
                    border: 1px solid #ccc;
                    -webkit-border-radius: 4px 4px 4px 4px;
                    -moz-border-radius: 4px 4px 4px 4px;
                    border-radius: 4px 4px 4px 4px;
                    width: 100%;
                    -webkit-box-shadow: 1px 1px 2px 0 rgba(0, 0, 0, 0.1) inset;
                    -moz-box-shadow: 1px 1px 2px 0 rgba(0, 0, 0, 0.1) inset;
                    box-shadow: 1px 1px 2px 0 rgba(0, 0, 0, 0.1) inset;
                    background: #fff;
                    font-size: 14px;
                }
                input.input-text.input-text--error,.input-text.input-text--error{
                    border-color: red;
                }
                textarea.input-text{
                    resize: none;
                    height: auto;
                }
                /*stylized-switch*/
                .switch{
                    position: relative;
                    display: inline-block;
                    height: 15px;
                    width: 15px;
                    vertical-align: middle;
                    cursor: pointer;
                    top: -1px;
                    margin-right: 3px;
                }
                .switch input{
                    vertical-align: middle;
                    margin: 0;
                    position: absolute;
                    top: 0;
                    left: 0;
                    height: 15px;
                    width: 15px;
                }
                .switch input:not(:checked),.switch input:checked{
                    opacity: 0;
                    z-index: 1;
                    cursor: pointer;
                }
                /*not checked radio*/
                .switch input[type=radio]:not(:checked)+i,.switch input[type=radio]:checked+i{
                    position: absolute;
                    top: 0;
                    left: 0;
                    right: 0;
                    bottom: 0;
                    background: #fff;
                    border: 1px solid #ccc;
                    -webkit-border-radius: 30px 30px 30px 30px;
                    -moz-border-radius: 30px 30px 30px 30px;
                    border-radius: 30px 30px 30px 30px;
                }
                /*not checked checkbox*/
                .switch input[type=checkbox]:not(:checked)+i,.switch input[type=checkbox]:checked+i{
                    position: absolute;
                    top: 0;
                    left: 0;
                    right: 0;
                    bottom: 0;
                    background: #fff;
                    -webkit-border-radius: 3px 3px 3px 3px;
                    -moz-border-radius: 3px 3px 3px 3px;
                    border-radius: 3px 3px 3px 3px;
                    border: 1px solid #ccc;
                }
                /*checked radio*/
                .switch input[type=radio]:checked+i{
                    background: #ccc;
                    -webkit-box-shadow: 0 0 0 3px #fff inset;
                    -moz-box-shadow:0 0 0 3px #fff inset;
                    box-shadow: 0 0 0 3px #fff inset;
                }
                /*checked checkbox*/
                .switch input[type=checkbox]:checked+i{
                    background: #ccc;
                    -webkit-box-shadow: 0 0 0 2px #fff inset;
                    -moz-box-shadow:0 0 0 2px #fff inset;
                    box-shadow: 0 0 0 2px #fff inset;
                }
                /*hover radio*/
                .switch input[type=radio]:checked+i:hover,label:hover .switch input[type=radio]:checked+i{
                    opacity: 0.85;
                }
                .switch  input[type=radio]+i:hover, label:hover .switch  input[type=radio]+i{
                    opacity: 0.85;
                }
                /*hover checkbox*/
                .switch input[type=checkbox]:checked+i:hover,label:hover .switch input[type=checkbox]:checked+i{
                    opacity: 0.85;
                }
                .switch  input[type=checkbox]+i:hover, label:hover .switch  input[type=checkbox]+i{
                    opacity: 0.85;
                }
                /*end stylized-switch*/
                .up-btn{
                    position: fixed;
                    z-index: 1;
                    right: 15px;
                    bottom:15px;
                    cursor: pointer;
                    background: #eee;
                    -webkit-border-radius: 4px 4px 4px 4px;
                    -moz-border-radius: 4px 4px 4px 4px;
                    border-radius: 4px 4px 4px 4px;
                    -webkit-box-shadow: 0px 0px 6px 0px rgba(0, 0, 0, 0.3);
                    -moz-box-shadow: 0px 0px 6px 0px rgba(0, 0, 0, 0.3);
                    box-shadow: 0px 0px 6px 0px rgba(0, 0, 0, 0.3);
                    width: 50px;
                    height: 50px;
                    line-height: 45px;
                    text-align: center;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .up-btn:hover{
                    -webkit-box-shadow: 0px 0px 6px 0px rgba(0, 0, 0, 0.6);
                    -moz-box-shadow: 0px 0px 6px 0px rgba(0, 0, 0, 0.6);
                    box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.4);
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .up-btn:after{
                    content: "▲";
                    font-size: 32px;
                    display: inline-block;
                    vertical-align: middle;
                    color: #ccc;
                }
                /* Page styles
==============================================================================*/
                .breadcrumbs{}
                .header-center-right{
                    position: relative;
                    float: right;
                    padding-top: 2px;
                    margin-right: 30px;
                }
                .header-center-right-btn{
                    display: inline-block;
                    text-decoration: none;
                    color: #9A5341;
                    font-size: 13px;
                    font-family: 'GenesisSansHead', GenesisSansText, sans-serif;
                    height: 30px;
                    overflow: hidden;
                    margin-left: 23px;
                    opacity: 1;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                }
                .header-center-right-btn:hover{
                    /* color: #88c3e8;*/
                    text-decoration: none;
                    opacity: 0.5;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                }
                .header-center-right-btn.my-hyundai {
                    background: #9A5341;
                    color: #fff;
                    line-height: 27px;
                    padding: 0 20px;
                    margin-left: 0;
                    margin-right: 30px;
                    -webkit-perspective: 1000;
                    transform: skewX(-15deg);
                }
                .header-center-right-btn.my-hyundai > span {
                    display: block;
                    transform: skewX(15deg);
                }

                .header-center-right-btn i{
                    margin-right: 4px;
                }
                .user-bar{
                    z-index:102;
                    width: 940px;
                    position: absolute;
                    top: 100%;
                    margin-top: 15px;
                    background: #636669;
                    left: 50%;
                    margin-left: -470px;
                    height: 46px;
                }
                .user-bar-logo{
                    position: relative;
                    float: left;
                    width: 221px;
                    margin: 13px 30px;
                }
                .user-bar-logo img{
                    max-width: 100%;
                    vertical-align: middle;
                }
                .user-bar-btn-search{
                    position: relative;
                    float: right;
                    cursor: pointer;
                    margin-left: 18px;
                    margin-right: 30px;
                    margin-top: 15px;
                    display: inline-block;
                    height: 15px;
                    width: 15px;
                    overflow: hidden;
                    opacity: 1;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                }
                .user-bar-btn-search .user-bar-btn-search-item{
                    display: block;
                }
                .user-bar-btn-search:hover .user-bar-btn-search-item:first-child,.user-bar-btn-search.active .user-bar-btn-search-item:first-child{
                    opacity: 0.5;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                }
                .user-bar-search{
                    position: absolute;
                    top: 100%;
                    left: 0;
                    right: 0;
                    padding: 0;
                    background: #f1f1f1;
                    overflow: hidden;
                    height: 0;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .user-bar-search.active{
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                    height: auto;
                    padding: 10px;
                    z-index: 999;
                }
                input.user-bar-search-input{
                    border: 0;
                    text-transform: uppercase;
                    color: #606060;
                    font-size: 11px;
                    font-family:'GenesisSansHead',GenesisSansText,sans-serif;
                    width: 100%;
                    text-align: center;
                    background: none;
                }
                input.user-bar-search-input:focus{
                    box-shadow: none;
                }
                .user-bar-nav{
                    position: relative;
                    float: right;
                }
                .user-bar-nav-item{
                    position: relative;
                    float: left;
                }
                .user-bar-nav-item-link{
                    display: block;
                    padding: 14px 13px;
                    text-transform: uppercase;
                    color: #fff;
                    font-size: 11px;
                    text-decoration: none;
                    font-weight: 600;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                    font-family: "ModernH",sans-serif;
                    height: 46px;
                }
                .user-bar-nav-item-link:hover{
                    text-decoration: none;
                    color: #a4a4a4;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .user-bar-nav-item-link.active{
                    cursor: default;
                    color: #a4a4a4;
                }
                .main-slideshow{
                    position: relative;
                    width: 100%;
                    margin-bottom: 3px;
                    clear: both;
                    height: 520px;
                    background: #eee;
                    overflow: hidden;
                    text-align: center;
                }
                .main-slideshow-item{
                    width: 100%;
                    height: 520px;
                    position: relative;
                }
                .main-slideshow-item-content{
                    position: absolute;
                    top: 0;
                    left: 50%;
                    width: 2500px;
                    bottom: 0;
                    margin-left: -1250px;
                    text-align: center;
                }
                .main-slideshow-pager{
                    z-index:101;
                    position: absolute;
                    bottom: 40px;
                    left: 0;
                    right: 0;
                    text-align: center;
                    height: 0;
                }
                .main-slideshow-pager div{
                    display: inline-block;
                    vertical-align: middle;
                    cursor: pointer;
                    -webkit-border-radius: 30px 30px 30px 30px;
                    -moz-border-radius: 30px 30px 30px 30px;
                    border-radius: 30px 30px 30px 30px;
                    background: #fff;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                    margin:0 5px;
                    width: 12px;
                    height: 12px;
                    position: relative;
                }
                .main-slideshow-pager .cycle-pager-active{
                    background: #9A5341;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .main-slideshow-prev{
                    position: absolute;
                    top: 50%;
                    left: -80px;
                    cursor: pointer;
                    z-index:101;
                    overflow: hidden;
                    width:27px;
                    height: 45px;
                    margin-top: -22px;
                    -webkit-transition: all 0.6s ease-out;
                    -moz-transition: all 0.6s ease-out;
                    -ms-transition: all 0.6s ease-out;
                    -o-transition: all 0.6s ease-out;
                    transition: all 0.6s ease-out;
                }
                .main-slideshow:hover .main-slideshow-prev{
                    -webkit-transition: all 0.6s ease-out;
                    -moz-transition: all 0.6s ease-out;
                    -ms-transition: all 0.6s ease-out;
                    -o-transition: all 0.6s ease-out;
                    transition: all 0.6s ease-out;
                    left: 35px;
                }
                .main-slideshow-next{
                    position: absolute;
                    top: 50%;
                    right: -80px;
                    cursor: pointer;
                    z-index:101;
                    overflow: hidden;
                    width:27px;
                    height: 45px;
                    margin-top: -22px;
                    -webkit-transition: all 0.6s ease-out;
                    -moz-transition: all 0.6s ease-out;
                    -ms-transition: all 0.6s ease-out;
                    -o-transition: all 0.6s ease-out;
                    transition: all 0.6s ease-out;
                }
                .main-slideshow:hover .main-slideshow-next{
                    -webkit-transition: all 0.6s ease-out;
                    -moz-transition: all 0.6s ease-out;
                    -ms-transition: all 0.6s ease-out;
                    -o-transition: all 0.6s ease-out;
                    transition: all 0.6s ease-out;
                    right: 35px;
                }
                .main-slideshow-prev-ico{
                    vertical-align: middle;
                    display: inline-block;
                    background: url("../img_new/slider_arrows.png") no-repeat 0 0;
                    width:27px;
                    height: 45px;
                }
                .main-slideshow-prev-ico-h{
                    vertical-align: middle;
                    display: inline-block;
                    background: url("../img_new/slider_arrows.png") no-repeat -29px 0;
                    width:27px;
                    height: 45px;
                }
                .main-slideshow-next-ico{
                    vertical-align: middle;
                    display: inline-block;
                    background: url("../img_new/slider_arrows.png") no-repeat -56px 0;
                    width:27px;
                    height: 45px;
                }
                .main-slideshow-next-ico-h{
                    vertical-align: middle;
                    display: inline-block;
                    background: url("../img_new/slider_arrows.png") no-repeat -83px 0;
                    width:27px;
                    height: 45px;
                }
                .main-slideshow-prev i,.main-slideshow-next i{
                    display: block;
                }
                .main-slideshow-prev i:first-child,.main-slideshow-next i:first-child{
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                    position: absolute;
                    top: 0;
                    left: 0;
                    opacity: 0;
                }
                .main-slideshow-prev:hover i:first-child,.main-slideshow-next:hover i:first-child{
                    opacity: 1;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                }
                .main-slideshow-loader{
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    margin-left: -38px;
                    margin-top: -38px;
                }
                .banners{
                    margin-bottom: 0;
                }
                .banners-item{
                    position: relative;
                    float: left;
                    display: block;
                    overflow: hidden;
                    width: 25%;
                    height: 230px;
                    background: #eee;
                    background-size: cover;
                    background-position: 50% 50%;
                    background-repeat: no-repeat;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                }
                .banners-item--pic1{
                    background-image: url('../img_new/promo1.jpg');
                }
                .banners-item--pic2{
                    background-image: url('../img_new/promo2.jpg');
                }
                .banners-item--pic3{
                    background-image: url('../img_new/promo3.jpg');
                    background-position: 50% 50%;
                }
                .banners-item--pic4{
                    background-image: url('../img_new/promo4.jpg');
                }
                .news{
                    background: #f7f7f7;
                    padding-bottom: 15px;
                }
                .news-center{
                    width: 940px;
                    margin: 0 auto;
                    padding: 49px 0 0;
                }
                .news-center-head{
                    margin-bottom: 28px;
                    font-family:'ModernH',sans-serif;
                    font-weight: 300;
                    color: #888888;
                    font-size: 42px;
                    line-height: 1;
                }
                .news-center-head *{
                    margin: 0;
                    font-weight: 300;
                    font-size: 42px;
                    line-height: 1;
                }
                .news-center-body{
                    margin-bottom: 15px;
                }
                .news-center-body-item{
                    padding-right: 0;
                    color: #555555;
                    font-size: 17px;
                    line-height: 22px;
                    display: block;
                    text-decoration: none;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out 0s;
                    margin-right: 40px;
                }
                .news-center-body-item:hover{
                    color: #a6a6a6;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                    text-decoration: none;
                }
                .news-center-body-item-date{
                    color: #9A5341;
                    font-size: 13px;
                    font-family:'GenesisSansHead',sans-serif;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                    margin-bottom: 12px;
                }
                .news-center-body-item:hover .news-center-body-item-date{
                    color: #88c3e8;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .news-line{
                    overflow: hidden;
                    margin: 25px 0 14px;
                    clear: both;
                    height: 1px;
                    background: #e8e8e8;
                }
                .news-center-rss{
                    position: relative;
                    float: right;
                    padding-right: 71px;
                    text-align: right;
                }
                .btn{
                    position: relative;
                    cursor: pointer;
                    display: inline-block;
                    padding: 15px 28px;
                    text-transform: uppercase;
                    text-decoration: none;
                    border: 2px solid #9A5341;
                    background: #9A5341;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                    color: #fff;
                    font-size: 12px;
                    font-family:'GenesisSansHead',sans-serif;
                    line-height: 16px;
                    vertical-align: middle;
                }
                .btn:hover{
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                    background: transparent;
                    color: #9A5341;
                    text-decoration: none;
                }
                .news-center-rss-text{
                    font-family:'ModernH',sans-serif;
                    font-weight: 300;
                    color: #888888;
                    font-size: 26px;
                    margin-right: 33px;
                }
                .all-link{
                    cursor: pointer;
                    color: #9A5341;
                    font-size: 12px;
                    font-family:'GenesisSansHead',sans-serif;
                    text-transform: uppercase;
                    display: inline-block;
                    overflow: hidden;
                    position: relative;
                    height: 18px;
                    line-height: 19px;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                    opacity:1;
                }
                .all-link-item{
                    display: block;
                    height: 18px;
                }
                .all-link-item:first-child{
                    /*     margin-top: -18px; */
                    position: absolute;
                    top: 0;
                    left: 0;
                    opacity: 0;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                    color: #9A5341;
                }
                .all-link i{
                    vertical-align: baseline;
                    margin-left: 6px;
                }
                .all-link:hover{
                    text-decoration: none;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                    opacity:0.5;
                    color: #9A5341;
                }
                .all-link--mod:hover{
                    opacity:1;
                    -webkit-transition: color 0.2s ease-out;
                    -moz-transition: color 0.2s ease-out;
                    -ms-transition: color 0.2s ease-out;
                    -o-transition: color 0.2s ease-out;
                    transition: color 0.2s ease-out;
                    color: #fff;
                }
                .all-link:hover .all-link-item:first-child{
                    display: block;
                    opacity: 1;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                }
                .news-center-left{
                    float: left;
                    margin-top: 16px;
                }
                .events{
                    position: relative;
                    background: url("../img_new/events_bgr.jpg") no-repeat 50% 0;
                    background-size: auto 100%;
                    min-height: 600px;
                }
                .events-center{
                    width: 940px;
                    margin:0 auto;
                    padding: 44px 0 36px;
                }
                .events-center-head{
                    color: #fff;
                    font-family: 'ModernH',sans-serif;
                    font-size: 42px;
                    font-weight: 300;
                    line-height: 1;
                    margin-bottom: 44px;
                }
                .events-center-head *{
                    font-size: 42px;
                    font-weight: 300;
                    line-height: 1;
                    margin: 0;
                }
                .events-center-body{
                    border: 1px solid #fff;
                    position: relative;
                    padding: 38px 30px 38px;
                    min-height: 305px;
                    margin-bottom: 31px;
                }
                .datepicker{
                    position: absolute;
                    top: 40px;
                    left: 47px;
                    width: 322px;
                    text-align: center;
                }
                .datepicker-head{
                    margin-bottom: 27px;
                    text-transform: uppercase;
                    color: #fff;
                    font-size: 12px;
                    font-family:'GenesisSansHead',sans-serif;
                    text-align: center;
                }
                .datepicker-head-left{
                    display: inline-block;
                    vertical-align: middle;
                    overflow: hidden;
                    width: 12px;
                    height: 20px;
                    cursor: pointer;
                    position: relative;
                }
                .datepicker-head-right{
                    display: inline-block;
                    vertical-align: middle;
                    overflow: hidden;
                    width: 12px;
                    height: 20px;
                    cursor: pointer;
                }
                .datepicker-head-left .datepicker-head-left-item{
                    display: block;
                }
                .datepicker-head-left-item:first-child{
                    position: absolute;
                    top: 0;
                    left: 0;
                    opacity: 0;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                }
                .datepicker-head-left:hover .datepicker-head-left-item{
                    opacity:1;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                }
                .datepicker-head-text {
                    display: inline-block;
                    margin: 0;
                    padding: 0;
                    vertical-align: middle;
                    width: 166px;
                }
                .datepicker-body{
                    width: 100%;
                    text-align: center;
                    color: #fff;
                    table-layout: fixed;
                }
                .datepicker-body th{
                    text-align: center;
                    color: #fff;
                    padding: 5px 5px 11px;
                    vertical-align: middle;
                    font-size: 14px;
                    font-weight:normal;
                }
                .datepicker-body td{
                    text-align: center;
                    color: #fff;
                    padding: 3px 5px 4px;
                    vertical-align: middle;
                    font-size: 14px;
                    font-family:'GenesisSansHead',sans-serif;
                }
                .datepicker-body-item{
                    font-size: 14px;
                    font-family:'GenesisSansHead',sans-serif;
                    color: #fff;
                    display: inline-block;
                    border: 1px solid transparent;
                    -webkit-border-radius: 50px 50px 50px 50px;
                    -moz-border-radius: 50px 50px 50px 50px;
                    border-radius: 50px 50px 50px 50px;
                    width: 36px;
                    height: 36px;
                    text-align: center;
                    line-height: 34px;
                    cursor: default;
                }
                .changeday {
                    cursor: pointer !important;
                }
                .datepicker-body-item--prev{
                    border-color: #fff;
                    cursor: pointer;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .datepicker-body-item--prev:hover{
                    border-color: #9A5341;
                    background: #9A5341;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .datepicker-body-item--current-day{
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                    border-color: #fff;
                    background: #fff;
                    color: #9A5341;
                    cursor: default !important;
                }
                .datepicker-body-item--active{
                    border-color: #9A5341;
                    background: #9A5341;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                    color: #fff;
                    cursor: default;
                }
                .datepicker-body-item--next{
                    border-color: #9A5341;
                    cursor: pointer;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .datepicker-body-item--next:hover{
                    border-color: #9A5341;
                    background: #9A5341;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .events-center-body-line{
                    position: absolute;
                    top: 44px;
                    left: 418px;
                    width: 1px;
                    background: #fff;
                    opacity: 0.2;
                    bottom: 44px;
                }
                .events-center-body-content{
                    position: relative;
                    height: 302px;
                    overflow: auto;
                }
                .events-center-body-content-wrap{
                    margin-left: 438px;
                    color: #fff;
                }

                .events-center-foot{
                    text-align: right;
                }
                .all-link--mod:hover{
                    color: #fff;
                }
                .all-link--mod .all-link-item:first-child{
                    color: #fff;
                }
                .technologies{
                    position: relative;
                    height: 500px;
                    overflow: hidden;
                }
                .technologies-item{
                    width: 100%;
                    height: 500px;
                    position: relative;
                }
                .technologies-item-content{
                    position: absolute;
                    top: 0;
                    bottom: 0;
                    width: 2500px;
                    left:50%;
                    margin-left: -1250px;
                    text-align: center;
                }
                .technologies-item-content-center{
                    position: absolute;
                    width: 940px;
                    left: 50%;
                    margin-left: -470px;
                    top: 0;
                }
                .technologies-item-content-center-left{
                    position: relative;
                    float: left;
                    width: 421px;
                    text-align: center;
                    padding-top: 118px;
                }
                .btn-play{
                    display: inline-block;
                    position: relative;
                    overflow: hidden;
                    vertical-align: middle;
                    width: 124px;
                    height: 124px;
                    text-align: center;
                    margin-top: 70px;
                }
                .btn-play .btn-play-item{
                    display: block;
                    opacity: 0.7;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .btn-play .btn-play-item:first-child{

                    /*
    -webkit-transition: all 0.2s ease-out;
    -moz-transition: all 0.2s ease-out;
    -ms-transition: all 0.2s ease-out;
    -o-transition: all 0.2s ease-out;
    transition: all 0.2s ease-out;
*/
                }
                .btn-play:hover .btn-play-item{
                    opacity: 1;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .technologies-item-content-center-left-video{
                    background: #000;
                    height: 260px;
                    width: 421px;
                }
                .technologies-item-content-center-right{
                    position: relative;
                    float: right;
                    width: 470px;
                    padding-top: 237px;
                    text-align: left;
                }
                .technologies-item-content-center-right-tabs{

                }
                .technologies-item-content-center-right-tabs-menu{
                    margin-bottom: 68px;
                }
                .technologies-item-content-center-right-tabs-menu-item{
                    position: relative;
                    text-align: center;
                    cursor: pointer;
                    margin-right: 1px;
                    background: #fff;
                    padding: 9px 29px;
                    font-family:'ModernH',sans-serif;
                    font-weight: 500;
                    color: #888888;
                    font-size: 18px;
                    float: left;
                    min-width: 155px;
                    -webkit-transition: color 0.2s ease-out;
                    -moz-transition: color 0.2s ease-out;
                    -ms-transition: color 0.2s ease-out;
                    -o-transition: color 0.2s ease-out;
                    transition: color 0.2s ease-out;
                }
                .technologies-item-content-center-right-tabs-menu-item:after{
                    content: '';
                    position: absolute;
                    top: 100%;
                    width: 0;
                    height: 0;
                    border-top: 5px solid transparent;
                    border-bottom: 5px solid transparent;
                    border-left:7px solid transparent;
                    border-right:7px solid transparent;
                    left: 50%;
                    margin-left: -7px;
                    margin-top: -6px;
                    -webkit-transition: margin-top 0.2s ease-out;
                    -moz-transition: margin-top 0.2s ease-out;
                    -ms-transition: margin-top 0.2s ease-out;
                    -o-transition: margin-top 0.2s ease-out;
                    transition: margin-top 0.2s ease-out;
                }
                .technologies-item-content-center-right-tabs-menu-item.active{
                    color: #fff;
                    background: #9A5341;
                    cursor: default;
                }
                .technologies-item-content-center-right-tabs-menu-item.active:after{
                    margin-top: 0px;
                    border-top-color: #9A5341;
                }
                .news-center-head--mb{
                    margin-bottom: 34px;
                }
                .technologies-item-content-center-right-tabs-menu-item:hover{
                    color: #555555;
                }
                .technologies-item-content-center-right-tabs-menu-item.active:hover{
                    color: #fff;
                }
                .technologies-item-content-center-right-tabs-content{
                    margin-bottom: 15px;
                }
                .technologies-item-content-center-right-tabs-content-item{
                    color: #888888;
                    font-size: 16px;
                    line-height: 24px;
                }
                .technologies-item-content-center-right-tabs-content-item.active{
                    display: block;
                }
                .technologies-item-content-center-right-tabs-content-item-head{
                    font-weight:bold;
                    margin-bottom: 16px;
                    color: #333333;
                }
                .technologies-item-content-center-right-tabs-content-item p{
                    margin-bottom: 27px;
                    max-height: 144px;
                    overflow: hidden;
                }
                .technologies-prev{
                    cursor: pointer;
                    height: 42px;
                    left: 35px;
                    margin-top: -22px;
                    overflow: hidden;
                    position: absolute;
                    top: 50%;
                    /*     transition: all 0.6s ease-out 0s; */
                    width: 23px;
                    z-index: 101;
                }
                .technologies-next{
                    cursor: pointer;
                    height: 42px;
                    right: 35px;
                    margin-top: -22px;
                    overflow: hidden;
                    position: absolute;
                    top: 50%;
                    /*     transition: all 0.6s ease-out 0s; */
                    width: 23px;
                    z-index: 101;
                }
                .technologies-prev i,.technologies-next i{
                    display: block;
                }
                .technologies-prev i:first-child, .technologies-next i:first-child{
                    position: absolute;
                    top: 0;
                    left: 0;
                    opacity: 0;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                    /*
    -webkit-transition: all 0.2s ease-out;
    -moz-transition: all 0.2s ease-out;
    -ms-transition: all 0.2s ease-out;
    -o-transition: all 0.2s ease-out;
    transition: all 0.2s ease-out;
*/
                }
                .technologies-prev:hover i:first-child, .technologies-next:hover i:first-child{
                    opacity: 1;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                }
                .site-nav{
                    margin-bottom: 15px;
                    background: #9A5341;
                    position: relative;
                    clear: both;
                    overflow: hidden;
                }
                .site-nav-center{
                    max-width: 1197px;
                    margin: 0 auto;
                    padding:20px 0;
                }
                .site-nav-center-item{
                    position: relative;
                    float: left;
                    width: 25%;
                    border-right: 1px solid #9A5341;
                    height:70px;
                    text-align: center;
                }
                .site-nav-center-item:nth-child(4n){
                    border-right: 0;
                }
                .site-nav-center-item-link{
                    display: block;
                    text-align: center;
                    text-decoration: none;
                    height: 110px;
                    color: #fff;
                    margin-top: -20px;
                    font-size: 14px;
                    font-weight:bold;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                    margin-left: -1px;
                    margin-right: -1px;
                    overflow: hidden;
                    padding: 0 15px;
                }
                .site-nav-center-item-link:hover{
                    color: #fff;
                    background: #9A5341;
                    text-decoration: none;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .site-nav-center-item-link-item{
                    height: 110px;
                    position: relative;
                    /*
    -webkit-transition: all 0.2s ease-out;
    -moz-transition: all 0.2s ease-out;
    -ms-transition: all 0.2s ease-out;
    -o-transition: all 0.2s ease-out;
    transition: all 0.2s ease-out;
*/
                    padding-top: 23px;
                }
                .site-nav-center-item-link-item{
                    padding-top: 23px;
                }
                .site-nav-center-item-link:hover .site-nav-center-item-link-item{
                    top: 0;
                    /*
    -webkit-transition: all 0.2s ease-out;
    -moz-transition: all 0.2s ease-out;
    -ms-transition: all 0.2s ease-out;
    -o-transition: all 0.2s ease-out;
    transition: all 0.2s ease-out;
*/
                }
                .site-nav-center-item-link-item-pic{
                    height: 46px;
                }
                .footer-center-nav-head{
                    text-transform: uppercase;
                    font-family:'ModernH',sans-serif;
                    font-weight: 600;
                    color: #555555;
                    font-size: 14px;
                    margin-bottom: 14px;
                }
                .footer-center-nav-head a{
                    color: #555555;
                }
                .footer-center-nav-head a:hover{
                    text-decoration: none;
                    color: #9A5341;
                }
                .footer-center-nav{
                    padding:0 26px;
                    margin-bottom: 10px;
                }
                .f-row-item-list{
                    margin: 0;
                    padding: 0;
                    list-style: none;
                }
                .f-row-item-list-item{
                    margin-bottom: 9px;
                }
                .f-row-item-list-item-link{
                    font-size: 14px;
                    color: #888888;
                }
                .f-row-item-list-item-link:hover{
                    text-decoration: underline;
                    color: #888888;
                }
                .footer-center-line{
                    clear: both;
                    margin: 10px 0 14px;
                    height: 1px;
                    overflow: hidden;
                    background: #e9e9e9;
                }
                .footer-center-call{
                    padding-left: 24px;
                    margin-bottom: 28px;
                    margin-top: 14px;

                }
                .lang .ico-arr-b-h{
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                    opacity: 0.5;
                }
                .lang:hover .ico-arr-b-h{
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                    opacity: 1;
                }
                .footer-center-call-right-btn{
                    color: #888888;
                    display: inline-block;
                    font-family: 'GenesisSansHead',GenesisSansText,sans-serif;
                    font-size: 11px;
                    height: 32px;
                    line-height: 18px;
                    margin-right: 45px;
                    overflow: hidden;
                    text-decoration: none;
                    cursor: pointer;
                    text-transform: uppercase;
                    vertical-align: middle;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .footer-center-call-right-btn:hover{
                    color: #9A5341;
                    text-decoration: none;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .footer-center-call-right-btn-item{
                    display: block;
                    /*
    -webkit-transition: all 0.2s ease-out;
    -moz-transition: all 0.2s ease-out;
    -ms-transition: all 0.2s ease-out;
    -o-transition: all 0.2s ease-out;
    transition: all 0.2s ease-out;
*/
                    height:32px;
                }
                .footer-center-call-right-btn-item:first-child{
                }
                .footer-center-call-right-btn-item i{
                    margin-right: 5px;
                }
                .footer-center-call-right-btn:hover .footer-center-call-right-btn-item:first-child{
                    margin-top: 0;
                    /*
    -webkit-transition: all 0.2s ease-out;
    -moz-transition: all 0.2s ease-out;
    -ms-transition: all 0.2s ease-out;
    -o-transition: all 0.2s ease-out;
    transition: all 0.2s ease-out;
*/
                }
                .footer-center-call-right{
                    position: relative;
                    float: right;
                }
                .footer-center-call-text{
                    margin-right: 63px;
                    font-weight: 500;
                    font-family:'ModernH',sans-serif;
                    color: #555555;
                    font-size: 14px;
                }
                .footer-center-call-text-phone{
                    font-size: 22px;
                }
                .footer-center-call-right-btn:last-child{
                    margin-right: 0;
                }
                .footer-center-banner{
                    margin: 22px 0;
                }
                .footer-center-banner img{
                    max-width: 100%;
                    vertical-align: middle;
                }
                .footer-center-copy{
                    font-size: 11px;
                    color: #b6b6b6;
                    margin: 22px 25px 25px;
                    line-height: 18px;
                    text-align: justify;
                }
                .footer-center-foot{
                    padding: 5px 25px 8px;
                }
                .footer-center-foot-nav{
                    position: relative;
                    float: right;
                    color: #a6a6a6;
                    font-size: 11px;
                }
                .footer-center-foot-nav-item{
                    color: #a6a6a6;
                    /*
    -webkit-transition: all 0.2s ease-out;
    -moz-transition: all 0.2s ease-out;
    -ms-transition: all 0.2s ease-out;
    -o-transition: all 0.2s ease-out;
    transition: all 0.2s ease-out;
*/
                }
                .footer-center-foot-nav-item:hover{
                    color: #555555;
                    text-decoration: none;
                    /*
    -webkit-transition: all 0.2s ease-out;
    -moz-transition: all 0.2s ease-out;
    -ms-transition: all 0.2s ease-out;
    -o-transition: all 0.2s ease-out;
    transition: all 0.2s ease-out;
*/
                }
                .footer-center-foot-nav-sep{
                    margin: 0 3px;
                }
                .ico-arr-b{
                    /*background: url("images/icons/dropdown_icon.png") no-repeat 0 0;*/
                    display: inline-block;
                    width: 7px;
                    height: 5px;
                    vertical-align: middle;
                    margin-left: 0;
                }
                .ico-arr-b-h{
                    /*background: url("images/icons/dropdown_icon.png") no-repeat 100% 0;*/
                    display: inline-block;
                    width: 7px;
                    height: 5px;
                    vertical-align: middle;
                    margin-left: 0;
                }
                .lang{
                    position: relative;
                    display: inline-block;
                    height: 18px;
                    line-height: 16px;
                    cursor: pointer;
                    text-decoration: none;
                    color: #a6a6a6;
                    font-weight:bold;
                    font-size: 13px;
                    overflow: hidden;
                }
                .lang:hover{
                    text-decoration: none;
                    color: #555555;
                }
                .lang-item{
                    display: block;
                    height: 18px;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .lang-item:first-child{
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .lang:hover .lang-item:first-child{
                    margin-top: 0;
                    -webkit-transition: all 0.2s ease-out;
                    -moz-transition: all 0.2s ease-out;
                    -ms-transition: all 0.2s ease-out;
                    -o-transition: all 0.2s ease-out;
                    transition: all 0.2s ease-out;
                }
                .lang-item i{
                    margin-right: 7px;
                }
                .socialIconsHeader {
                    /* display: inline-block; */
                }
                .socialIconsHeader a {
                    float: right;
                    margin-left: 5px;
                    position: relative;
                }
                .socialIconsHeader a span:first-child{
                    position: absolute;
                    top: 0;
                    left: 0;
                    opacity: 0;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                }
                .socialIconsHeader a:hover span:first-child{
                    opacity: 1;
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                }
                .socialIconsHeader a span {
                    display: inline-block;
                    width: 24px;
                    height: 24px;
                    background: url("../img_new/sprites24.png");
                    -webkit-transition: 0.2s ease 0s;
                    -moz-transition: 0.2s ease 0s;
                    -o-transition: 0.2s ease 0s;
                    transition: 0.2s ease 0s;
                }
                .socialIconsHeader a span.yt {
                    background-position: -96px 0px;
                }
                .socialIconsHeader a span.ig {
                    background-position: -72px 0px;
                }
                .socialIconsHeader a span.tw {
                    background-position: -48px 0px;
                }
                .socialIconsHeader a span.vk {
                    background-position: -24px 0px;
                }
                .footer-center-foot-left-social{
                    float: left;
                    margin-top: -6px;
                }
                .footer-center-foot-left{
                    float: left;
                    margin-right: 51px;
                }
                .socialIconsHeader a span:hover{
                    background-position:0 24px;
                }
                .socialIconsHeader a span.yt:hover {
                    background-position:-96px 24px;
                }
                .socialIconsHeader a span.ig:hover {
                    background-position:-72px 24px;
                }
                .socialIconsHeader a span.tw:hover {
                    background-position:-48px 24px;
                }
                .socialIconsHeader a span.vk:hover {
                    background-position:-24px 24px;
                }
                .banners-item:hover{
                    -webkit-transition: opacity 0.2s ease-out;
                    -moz-transition: opacity 0.2s ease-out;
                    -ms-transition: opacity 0.2s ease-out;
                    -o-transition: opacity 0.2s ease-out;
                    transition: opacity 0.2s ease-out;
                    opacity: 0.5;
                }







                /* Icons
==============================================================================*/
                .i-0014-play-btn-copy-2, .i-0015-play-btn, .i-0012-icon-2, .i-0013-icon-1, .i-0011-icon-3,
                .i-0008-mail-icon, .i-0009-phone-icon, .i-0010-icon-4, .i-0001-test-drive-icon, .i-0028-test-drive-icon-h,
                .i-0029-auto-icon-h, .i-0030-auto-icon, .i-0005-slider-arrow-left-light-h, .i-0006-slider-arrow-right-light-h, .i-0016-slider-arrow-left-light,
                .i-0017-slider-arrow-right-light, .i-0007-flag-icon, .i-0000-call-icon-h, .i-0018-clock-icon-h, .i-0019-clock-icon,
                .i-0031-call-icon, .i-0002-mail-icon-h, .i-0003-mail-icon, .i-0026-search-icon-h, .i-0027-search-icon,
                .i-0004-slider-arrow-left-h, .i-0021-slider-arrow-right-h, .i-0022-slider-arrow-right, .i-0023-slider-arrow-left, .i-0020-arrow-icon-white-h,
                .i-0024-arrow-icon-h, .i-0025-arrow-icon
                { display: inline-block; background: url('../img_new/sprite.png') no-repeat;vertical-align: middle; }
                .i-0014-play-btn-copy-2 { background-position: -2px -0px; width: 124px; height: 124px; }
                .i-0015-play-btn { background-position: -2px -126px; width: 124px; height: 124px; }
                .i-0012-icon-2 { background-position: -2px -252px; width: 40px; height: 40px; }
                .i-0013-icon-1 { background-position: -44px -252px; width: 40px; height: 34px; }
                .i-0011-icon-3 { background-position: -44px -288px; width: 39px; height: 39px; }
                .i-0008-mail-icon { background-position: -86px -252px; width: 32px; height: 32px; }
                .i-0009-phone-icon { background-position: -86px -286px; width: 32px; height: 32px; }
                .i-0010-icon-4 { background-position: -2px -294px; width: 31px; height: 39px; }
                .i-0001-test-drive-icon { background-position: -85px -320px; width: 30px; height: 30px; }
                .i-0028-test-drive-icon-h { background-position: -35px -329px; width: 30px; height: 30px; }
                .i-0029-auto-icon-h { background-position: -2px -335px; width: 30px; height: 30px; }
                .i-0030-auto-icon { background-position: -67px -352px; width: 30px; height: 30px; }
                .i-0005-slider-arrow-left-light-h { background-position: -99px -352px; width: 23px; height: 42px; }
                .i-0006-slider-arrow-right-light-h { background-position: -34px -361px; width: 23px; height: 42px; }
                .i-0016-slider-arrow-left-light { background-position: -2px -367px; width: 23px; height: 42px; }
                .i-0017-slider-arrow-right-light { background-position: -59px -384px; width: 23px; height: 42px; }
                .i-0007-flag-icon { background-position: -84px -396px; width: 22px; height: 18px; }
                .i-0000-call-icon-h { background-position: -27px -405px; width: 19px; height: 19px; }
                .i-0018-clock-icon-h { background-position: -2px -411px; width: 19px; height: 19px; }
                .i-0019-clock-icon { background-position: -84px -416px; width: 19px; height: 19px; }
                .i-0031-call-icon { background-position: -105px -416px; width: 19px; height: 19px; }
                .i-0002-mail-icon-h { background-position: -67px -329px; width: 16px; height: 12px; }
                .i-0003-mail-icon { background-position: -108px -396px; width: 16px; height: 12px; }
                .i-0026-search-icon-h { background-position: -23px -426px; width: 15px; height: 15px; }
                .i-0027-search-icon { background-position: -40px -426px; width: 15px; height: 15px; }
                .i-0004-slider-arrow-left-h { background-position: -57px -428px; width: 12px; height: 20px; }
                .i-0021-slider-arrow-right-h { background-position: -2px -432px; width: 12px; height: 20px; }
                .i-0022-slider-arrow-right { background-position: -71px -437px; width: 12px; height: 20px; }
                .i-0023-slider-arrow-left { background-position: -85px -437px; width: 12px; height: 20px; }
                .i-0020-arrow-icon-white-h { background-position: -35px -294px; width: 6px; height: 9px; }
                .i-0024-arrow-icon-h { background-position: -35px -305px; width: 6px; height: 9px; }
                .i-0025-arrow-icon { background-position: -35px -316px; width: 6px; height: 9px; }
                /* 13th of may, 2016 */
                .i-spec-offers-icon {
                    background: url('/media/img_new/i-spec-offers-icon.png') no-repeat center top;
                    display: inline-block;
                    vertical-align: middle;
                    height: 30px;
                    width: 30px;
                }
                /* Screens
==============================================================================*/
                @media only screen  and (max-width: 1780px){
                    .banners-item{
                        height: 184px;
                    }
                }
                @media only screen  and (max-width: 1400px){
                    .banners-item{
                        height: 138px;
                    }
                }
                @media only screen  and (max-width: 1300px){
                    .banners-item{
                        height: 138px;
                    }
                }

                @media only screen  and (max-width: 1180px){
                    .banners-item{
                        height: 230px;
                        width: 50%;
                    }
                }
                @media only screen  and (max-width: 1024px){
                    .banners-item{
                        height: 230px;
                        width: 50%;
                    }
                }

                /* Retina
==============================================================================*/
                @media only screen and (-Webkit-min-device-pixel-ratio: 1.5),
                only screen and (-moz-min-device-pixel-ratio: 1.5),
                only screen and (-o-min-device-pixel-ratio: 3/2),
                only screen and (min-device-pixel-ratio: 1.5) {}


                /* popup menu */


                .gnb_layer .showroom .list > ul {
                    background: url(/media/img/vehicle-listing-bg.png);
                    background-position: 0px 45px; }

                .gnb_layer .showroom .list li {
                    width: 142px;
                    height: 112px;
                    list-style: none outside none;
                }

                .gnb_layer .showroom .list li img {
                    height: auto;
                    width: 176px;
                    margin-left: -20px;
                    margin-top: -6px;
                }


                .showroom .tab {
                    display: none;
                }

                #showroomAll li a span {
                    color: #888;
                    font-weight: bold;
                    -webkit-transition: 0.2s ease 0s;
                    -moz-transition: 0.2s ease 0s;
                    -o-transition: 0.2s ease 0s;
                    transition: 0.2s ease 0s;
                }

                #showroomAll li a:hover span {
                    color: #000;
                }

                #gnbShowroom {
                    padding-bottom: 0px !important;
                }

                /* Стырено из большого старого файла стилей */

                /* gnb layer */
                .gnb_layer {visibility:hidden;height:0;display:block;position:absolute;left:0px;top:46px;z-index:100;width:940px;overflow:hidden; box-shadow: 0px 3px 25px rgba(0,0,0,.5); background:#fff;}
                .gnb_layer .layer_body {position:relative;width:940px;padding:10px 13px 10px 3px;/*background:url('../img/bg_gnb_layer.png') repeat-y;*/ background: #FFF;}
                .gnb_layer .layer_body .shopping {width:905px; padding:10px 0; margin:0 0 0 30px; display:inline-block; background:url(/media/img/bg_gnb_shopping.gif) repeat-x; }

                .gnb_layer .layer_foot {width:956px;height:16px;/*background:url('../img/bg_gnb_layer.png') no-repeat right bottom;*/ background: #FFF;}

                .gnb_layer .btn_close {display:none;position:absolute;z-index:101;right:19px;top:6px;width:25px;height:25px;text-indent:-1000px;overflow:hidden;background:url('/media/img/btn_close_gnb.png') no-repeat center center;}
                .gnb_layer .showroom {padding-bottom:20px;}
                .gnb_layer .showroom .tab {position:relative;height:40px;padding:0 20px 0;margin-bottom:11px;border-bottom:2px solid #b5bcc3;}
                .gnb_layer .showroom .tab ul {position:relative;float:left;height:42px;}
                .gnb_layer .showroom .tab ul li {float:left;padding-right:1px;text-transform:uppercase;background:url('/media/img/tab_showroom_bar.gif') no-repeat right 14px;}
                .gnb_layer .showroom .tab ul li a {display:block;height:30px; padding:13px 20px 0 20px;overflow:hidden;text-decoration:none;color: #333;font-size: 14px;font-family:'GenesisSansText'; text-align: center;}
                .gnb_layer .showroom .tab ul li a:hover {text-decoration:none;}
                .gnb_layer .showroom .tab ul li a.on {position:relative;height:29px;padding-top:12px;color: #004282;margin:0 -1px 0;border:solid #b5bcc3;border-width:1px 1px 0;background:#fff;}
                .gnb_layer .showroom .tab .btn_finder {display:block;position:absolute;right:60px;top:0;height:27px;padding:12px 30px 0 77px;text-transform:uppercase;text-decoration: none;font-size: 14px;font-family:'GenesisSansText';color: #575a5d;overflow:hidden;background:url('/media/img/txt_car_finder.gif') no-repeat 30px 10px; border:solid #fff; border-width:1px 1px 0}
                .gnb_layer .showroom .tab .btn_finder.on {height:29px;border-color:#b9c0c7;background-color:#fff;color:#004282;}
                .gnb_layer .showroom .list {width:900px;margin:0 auto;/*padding:9px 0 0;*/padding:0;overflow:hidden;}
                .gnb_layer .showroom .list ul {visibility:hidden;height:0;display:block;padding:0 0 2px 30px;overflow:hidden;}
                .gnb_layer .showroom .list li {float:left;overflow:hidden;}
                .gnb_layer .showroom .list li.right {float:right;padding-right:30px;}
                .gnb_layer .showroom .list li a {display:block;text-indent:0;text-decoration:none;}
                .gnb_layer .showroom .list li a:hover {text-decoration:none;}
                .gnb_layer .showroom .list li .model {display:block;font-size: 12px; line-height: 14px; padding-left: 10px; }
                .gnb_layer .showroom .list li .price {display:block;margin-top:-4px;padding-left:10px;color:#858c93;font-family:'Georgia';font-style:italic;font-size:11px;}
                .gnb_layer .showroom .list li .price em {font-size:14px;}
                .gnb_layer .showroom .list ul.middle {padding:0 0 5px 30px;}
                .gnb_layer .showroom .list ul.middle li {width:170px;height:121px;}
                .gnb_layer .showroom .list ul.middle li img {width:160px;height:72px;}
                .gnb_layer .showroom .list ul.large {padding:0 0 10px 30px;overflow:hidden;}
                .gnb_layer .showroom .list ul.large li {width:210px;height:155px;}
                .gnb_layer .showroom .list ul.large li img {width:210px;height:95px;}
                .gnb_layer .showroom .list li .fueleconomy2 {display:block;margin-top:-4px;padding-left:10px;color:#858c93;font-family:'Georgia';font-style:italic;font-size:11px;}
                .gnb_layer .showroom .list li .fueleconomy2 em {font-size:14px;}
                .gnb_layer .showroom .banners {width:900px;height:110px;margin:0 auto;padding:20px 0 0;border-top:1px dotted #cecece;overflow:hidden;}
                .gnb_layer .showroom .banners ul {height:110px;margin:0 -10px;overflow:hidden;}
                .gnb_layer .showroom .banners ul li {float: left;width:210px;height:108px;padding:0 10px 25px 10px;}
                .gnb_layer .showroom .banners ul li img {border:1px solid #ccc;}
                .gnb_layer .showroom .banners ul a {display:inline;text-indent:0;}
                .gnb_layer .showroom .carfinder {width:910px;margin:0 auto;overflow:hidden;}
                .gnb_layer .carfinder {visibility:hidden;height:0;display:block;}
                .gnb_layer .showroom .carfinder .list ul {display:block;margin-bottom:-17px;}
                .gnb_layer .carfinder .btn_reset {float:right;margin:0 13px 1px 0;padding:0 0 0 14px;color:#666;text-decoration:none;text-transform:capitalize;background:url(/wcm/images/common/btn/btn_reset.gif) no-repeat 0 center;}
                .gnb_layer .carfinder .find_options {position:relative;clear:both;height:142px;margin-bottom:19px;border:1px solid #d6d6d6;overflow:hidden;background:#f0f0f0 url(/media/img/bg_gnb_carfinder.gif) repeat-x;}
                .gnb_layer .carfinder .find_options form {width:910px;margin-right:-2px;}
                .gnb_layer .carfinder .option {position:relative;float:left;overflow:hidden;width:129px;height:142px;border-right:1px solid #d6d6d6;}
                .gnb_layer .carfinder .option2 {position:relative;height:70px;border-bottom:1px solid #dfdfdf;}
                .gnb_layer .carfinder .option3 {position:relative;height:70px;border-top:1px solid #fbfbfb;}
                .gnb_layer .carfinder .option .option2 .slidebar,
                .gnb_layer .carfinder .option .option3 .slidebar {bottom:12px;border:0;height:21px;}
                .gnb_layer .carfinder .option .option2 .name,
                .gnb_layer .carfinder .option .option3 .name {padding-top:6px;line-height:11px;}
                .gnb_layer .carfinder .name {display:block;height:21px;padding:3px 0 0 7px;color:#666;font-weight:bold;/* text-transform:capitalize; */}
                .gnb_layer .carfinder .name.line2 { line-height:13px; }
                .gnb_layer .carfinder .option .slidebar {position:absolute;left:0;bottom:0;width:100%;height:33px;border-top:1px solid #e5e5e5;}
                .gnb_layer .carfinder .category ul.case1 li {height:17px;padding:3px 0 0 10px;clear:both;color:#666;font-size:11px;font-weight:bold;}
                .gnb_layer .carfinder .category ul.case1 li .check {display:inline-block;padding:0 0 0 25px;cursor:pointer;background:url(/media/img/bg_check_function2.gif) no-repeat 0 2px;}
                .gnb_layer .carfinder .category ul.case1 li.select .check {background-position:0 -82px;color:#10286c;}

                .gnb_layer .carfinder a.jqTransformCheckbox { width:20px;height:13px;margin-top:-3px;padding:7px 0 0;background: url("/media/img/bg_check_function2.gif") no-repeat 0 4px;}
                .gnb_layer .carfinder a.jqTransformCheckbox.jqTransformChecked {background-position:0 -80px !important;}
                .gnb_layer .carfinder span.jqTransformCheckboxWrapper {margin:0 5px 0 10px;}
                .gnb_layer .carfinder .option .slider {width:110px;margin:14px auto 0;}
                .gnb_layer .carfinder .ui-slider {position: relative;background:url(/wcm/images/common/function/jslider_finder.round.png) right -20px;}
                .gnb_layer .carfinder .price .ui-slider {position: relative;background:url(/wcm/images/common/function/jslider_finder.round.png);}
                .gnb_layer .carfinder .ui-slider .ui-slider-handle {position:absolute;z-index:2;}
                .gnb_layer .carfinder .ui-slider .ui-slider-range {position: absolute;z-index: 1;}
                .gnb_layer .carfinder .ui-slider-horizontal {height:11px;}
                .gnb_layer .carfinder .ui-slider-horizontal .ui-slider-range {height:100%;top:0;}
                .gnb_layer .carfinder .ui-slider-horizontal .ui-slider-range-min {left:0;background:url(/wcm/images/common/function/jslider_finder.round.png) 0 0;}
                .gnb_layer .carfinder .price .ui-slider-horizontal .ui-slider-range-min {left:0;background:url(/wcm/images/common/function/jslider_finder.round.png) right -20px;}
                .gnb_layer .carfinder .ui-slider-horizontal .ui-slider-handle {top:-6px;margin-left:-10px;width:19px;height:19px;}
                .gnb_layer .carfinder .ui-state-default {width:19px;height:19px;background:url(/wcm/images/common/function/jslider_finder.round.png) no-repeat -6px -38px; border:none;}
                .gnb_layer .carfinder .ui-widget-header {background:url(/wcm/images/common/function/jslider_finder.round.png) 0 0;}
                .gnb_layer .carfinder .price .ui-widget-header {background:url(/wcm/images/common/function/jslider_finder.round.png) right -20px;}
                .gnb_layer .carfinder .find_options ul li span.unable {cursor:text;}
                .gnb_layer .carfinder .category ul.case2 {overflow:hidden;padding:2px 0 0 6px;}
                .gnb_layer .carfinder .category ul.case2 li {float:left;width:59px;height:57px;}
                .gnb_layer .carfinder .category ul.case2 li span {display:block;cursor:pointer;text-indent:-1000px;overflow:hidden;width:57px;height:53px;background:url(/wcm/images/ru/function/carfinder_icon_category.gif) no-repeat;}
                .gnb_layer .carfinder .category ul.case2 li.cars span {background-position:-134px -128px;}
                .gnb_layer .carfinder .category ul.case2 li.cars span.select {background-position:-269px -128px;}
                .gnb_layer .carfinder .category ul.case2 li.cars span.unable {background-position:1px -128px;}
                .gnb_layer .carfinder .category ul.case2 li.suv span {background-position:-193px -128px;}
                .gnb_layer .carfinder .category ul.case2 li.suv span.select {background-position:-328px -128px;}
                .gnb_layer .carfinder .category ul.case2 li.suv span.unable {background-position:-58px -128px;}
                .gnb_layer .carfinder .category ul.case2 li.commercial span {background-position:-134px -185px;}
                .gnb_layer .carfinder .category ul.case2 li.commercial span.select {background-position:-269px -185px;}
                .gnb_layer .carfinder .category ul.case2 li.commercial span.unable {background-position:1px -185px;}
                .gnb_layer .carfinder .category ul.case2 li.etc span {background-position:-193px -185px;}
                .gnb_layer .carfinder .category ul.case2 li.etc span.select {background-position:-328px -185px;}
                .gnb_layer .carfinder .category ul.case2 li.etc span.unable {background-position:-58px -185px;}
                .gnb_layer .carfinder .category ul.case3 {padding:1px 0 0 5px;}
                .gnb_layer .carfinder .category ul.case3 li {height:35px;padding:1px;}
                .gnb_layer .carfinder .category ul.case3 li span {display:block;cursor:pointer;text-indent:-1000px;overflow:hidden;width:117px;height:36px;background:url(/wcm/images/ru/function/carfinder_icon_category.gif) no-repeat;}
                .gnb_layer .carfinder .category ul.case3 li.cars span {background-position:-134px 0;}
                .gnb_layer .carfinder .category ul.case3 li.cars span.select {background-position:-269px 0;}
                .gnb_layer .carfinder .category ul.case3 li.cars span.unable {background-position:1px 0;}
                .gnb_layer .carfinder .category ul.case3 li.suv span {background-position:-134px -37px;}
                .gnb_layer .carfinder .category ul.case3 li.suv span.select {background-position:-269px -37px;}
                .gnb_layer .carfinder .category ul.case3 li.suv span.unable {background-position:1px -37px;}
                .gnb_layer .carfinder .category ul.case3 li.commercial span {background-position:-134px -74px;}
                .gnb_layer .carfinder .category ul.case3 li.commercial span.select {background-position:-269px -74px;}
                .gnb_layer .carfinder .category ul.case3 li.commercial span.unable {background-position:1px -74px;}
                .gnb_layer .carfinder .category ul.case4 {padding:1px 0 0 5px;}
                .gnb_layer .carfinder .category ul.case4 li {height:54px;padding:1px;}
                .gnb_layer .carfinder .category ul.case4 li span {display:block;cursor:pointer;text-indent:-1000px;overflow:hidden;width:117px;height:54px;background:url(/wcm/images/ru/function/carfinder_icon_category.gif) no-repeat;}
                .gnb_layer .carfinder .category ul.case4 li.cars span {background-position:-134px -256px;}
                .gnb_layer .carfinder .category ul.case4 li.cars span.select {background-position:-269px -256px;}
                .gnb_layer .carfinder .category ul.case4 li.cars span.unable {background-position:1px -256px;}
                .gnb_layer .carfinder .category ul.case4 li.suv span {background-position:-134px -312px;}
                .gnb_layer .carfinder .category ul.case4 li.suv span.select {background-position:-269px -312px;}
                .gnb_layer .carfinder .category ul.case4 li.suv span.unable {background-position:1px -312px;}
                .gnb_layer .carfinder .price .range {height:24px;padding-top:2px;text-align:center;}
                .gnb_layer .carfinder .price .range span {color:#696969;font-weight:bold;font-size:14px;vertical-align:middle;}
                .gnb_layer .carfinder .price .range strong {color:#2c407b;font-size:20px;vertical-align:middle;}
                .gnb_layer .carfinder .price .image {width:121px;height:47px;margin:0 auto;overflow:hidden;background:url(/wcm/images/common/function/carfinder_img_price.png) no-repeat;}
                .gnb_layer .carfinder .price .image .img_gauge {height:47px;background:url(/wcm/images/common/function/carfinder_img_price.png) no-repeat 0 -47px;}
                .gnb_layer .carfinder .price .image .img_gauge div {height:47px;background:url(/wcm/images/common/function/carfinder_img_price.png) no-repeat;}
                .gnb_layer .carfinder .fueleconomy .image {float:left;width:53px;padding:5px 0 0 11px;}
                .gnb_layer .carfinder .fueleconomy .range {float:left;padding:29px 0 0;}
                .gnb_layer .carfinder .fueleconomy .range .values {width:55px;color:#2c407b;font-size:20px;font-weight:bold;background:transparent;}
                .gnb_layer .carfinder .fueleconomy .range span {font-size:10px;}
                .gnb_layer .carfinder .fueleconomy .range span {display:inline-block;margin-top:-4px;padding-left:3px;color:#383837;font-weight:bold;}
                .gnb_layer .carfinder .seats .case1 .gauge {width:102px;height:60px;padding:14px 0 0 13px;background:url(/wcm/images/common/function/carfinder_img_seats.png) no-repeat right 5px;}
                .gnb_layer .carfinder .seats .case1 .gauge .values {width:60px;color:#2c407b;font-size:20px;line-height:24px;font-weight:bold;border:none;background:transparent;}
                .gnb_layer .carfinder .seats ul {padding:2px 0 0 6px;}
                .gnb_layer .carfinder .seats ul li span {display:block;cursor:pointer;text-indent:-1000px;overflow:hidden;background:url(/wcm/images/ru/function/carfinder_icon_seats.gif) no-repeat;}
                .gnb_layer .carfinder .seats ul.case2 li {height:37px;}
                .gnb_layer .carfinder .seats ul.case2 li span {width:117px;height:36px;}
                .gnb_layer .carfinder .seats ul.case2 li.small span {background-position:-134px -127px;}
                .gnb_layer .carfinder .seats ul.case2 li.small span.select {background-position:-264px -127px;}
                .gnb_layer .carfinder .seats ul.case2 li.small span.unable {background-position:1px -127px;}
                .gnb_layer .carfinder .seats ul.case2 li.middle span {background-position:-134px -164px;}
                .gnb_layer .carfinder .seats ul.case2 li.middle span.select {background-position:-264px -164px;}
                .gnb_layer .carfinder .seats ul.case2 li.middle span.unable {background-position:1px -164px;}
                .gnb_layer .carfinder .seats ul.case2 li.large span {background-position:-134px -201px;}
                .gnb_layer .carfinder .seats ul.case2 li.large span.select {background-position:-264px -201px;}
                .gnb_layer .carfinder .seats ul.case2 li.large span.unable {background-position:1px -201px;}
                .gnb_layer .carfinder .seats ul.case3 li {height:56px;}
                .gnb_layer .carfinder .seats ul.case3 li span {width:117px;height:54px;}
                .gnb_layer .carfinder .seats ul.case3 li.small span {background-position:-135px 0;}
                .gnb_layer .carfinder .seats ul.case3 li.small span.select {background-position:-265px 0;}
                .gnb_layer .carfinder .seats ul.case3 li.small span.unable {background-position:0 0;}
                .gnb_layer .carfinder .seats ul.case3 li.middle span {background-position:-135px -56px;}
                .gnb_layer .carfinder .seats ul.case3 li.middle span.select {background-position:-265px -56px;}
                .gnb_layer .carfinder .seats ul.case3 li.middle span.unable {background-position:1px -56px;}
                .gnb_layer .carfinder .configuration ul {overflow:hidden;height:38px;padding:0 0 0 6px;margin-top:-2px;}
                .gnb_layer .carfinder .configuration ul li span {display:block;cursor:pointer;text-indent:-1000px;overflow:hidden;background:url(/wcm/images/ru/gnb/carfinder_icon_configuration.png) no-repeat;}
                .gnb_layer .carfinder .configuration ul li {float:left;width:40px;height:36px;}
                .gnb_layer .carfinder .configuration ul li span {width:38px;height:36px;}
                .gnb_layer .carfinder .configuration ul li.fwd span {background-position:0 -36px;}
                .gnb_layer .carfinder .configuration ul li.fwd span.select {background-position:0 -74px;}
                .gnb_layer .carfinder .configuration ul li.fwd span.unable {background-position:0 1px;}
                .gnb_layer .carfinder .configuration ul li.rwd span {background-position:-40px -36px;}
                .gnb_layer .carfinder .configuration ul li.rwd span.select {background-position:-40px -74px;}
                .gnb_layer .carfinder .configuration ul li.rwd span.unable {background-position:-40px 1px;}
                .gnb_layer .carfinder .configuration ul li.awd span {background-position:-80px -36px;}
                .gnb_layer .carfinder .configuration ul li.awd span.select {background-position:-80px -74px;}
                .gnb_layer .carfinder .configuration ul li.awd span.unable {background-position:-80px 1px;}
                .gnb_layer .carfinder .fueltype ul li span {display:block;cursor:pointer;text-indent:-1000px;overflow:hidden;background:url(/wcm/images/ru/gnb/carfinder_icon_fueltype.png) no-repeat;}
                .gnb_layer .carfinder .fueltype ul.case1 {height:36px;padding:0 0 0 6px;overflow:hidden;}
                .gnb_layer .carfinder .fueltype ul.case1 li {float:left;width:61px;height:36px;}
                .gnb_layer .carfinder .fueltype ul.case1 li span {width:56px;height:36px;}
                .gnb_layer .carfinder .fueltype ul.case1 li.gasoline span {background-position:0 0;}
                .gnb_layer .carfinder .fueltype ul.case1 li.gasoline span.select {background-position:0 -76px;}
                .gnb_layer .carfinder .fueltype ul.case1 li.gasoline span.unable {background-position:0 -38px;}
                .gnb_layer .carfinder .fueltype ul.case1 li.diesel span {background-position:-58px 0;}
                .gnb_layer .carfinder .fueltype ul.case1 li.diesel span.select {background-position:-58px -76px;}
                .gnb_layer .carfinder .fueltype ul.case1 li.diesel span.unable {background-position:-58px -38px;}
                .gnb_layer .carfinder .fueltype ul.case1 li.hybrid span {background-position:-116px 0;}
                .gnb_layer .carfinder .fueltype ul.case1 li.hybrid span.select {background-position:-116px -76px;}
                .gnb_layer .carfinder .fueltype ul.case1 li.hybrid span.unable {background-position:-116px -38px;}
                .gnb_layer .carfinder .fueltype ul.case2 {overflow:hidden;padding:0 0 0 6px;}
                .gnb_layer .carfinder .fueltype ul.case2 li {float:left;width:40px;height:36px;}
                .gnb_layer .carfinder .fueltype ul.case2 li span {width:36px;height:36px;}
                .gnb_layer .carfinder .fueltype ul.case2 li.gasoline span {background-position:-174px 0;}
                .gnb_layer .carfinder .fueltype ul.case2 li.gasoline span.select {background-position:-174px -76px;}
                .gnb_layer .carfinder .fueltype ul.case2 li.gasoline span.unable {background-position:-174px -38px;}
                .gnb_layer .carfinder .fueltype ul.case2 li.diesel span {background-position:-213px 0;}
                .gnb_layer .carfinder .fueltype ul.case2 li.diesel span.select {background-position:-213px -76px;}
                .gnb_layer .carfinder .fueltype ul.case2 li.diesel span.unable {background-position:-213px -38px;}
                .gnb_layer .carfinder .fueltype ul.case2 li.hybrid span {background-position:-252px 0;}
                .gnb_layer .carfinder .fueltype ul.case2 li.hybrid span.select {background-position:-252px -76px;}
                .gnb_layer .carfinder .fueltype ul.case2 li.hybrid span.unable {background-position:-252px -38px;}

                .gnb_layer .carfinder .transmission ul li span {display:block;cursor:pointer;text-indent:-1000px;overflow:hidden;background:url(/wcm/images/ru/gnb/carfinder_icon_fueltype.png) no-repeat;}
                .gnb_layer .carfinder .transmission ul.case1 {height:36px;padding:4px 0 0 7px;overflow:hidden;}
                .gnb_layer .carfinder .transmission ul.case1 li {float:left;width:58px;height:36px;}
                .gnb_layer .carfinder .transmission ul.case1 li span {width:56px;height:36px;}
                .gnb_layer .carfinder .transmission ul.case1 li.manual span {background-position:-291px 0;}
                .gnb_layer .carfinder .transmission ul.case1 li.manual span.select {background-position:-291px -76px;}
                .gnb_layer .carfinder .transmission ul.case1 li.manual span.unable {background-position:-291px -38px;}
                .gnb_layer .carfinder .transmission ul.case1 li.automatic span {background-position:-350px 0;}
                .gnb_layer .carfinder .transmission ul.case1 li.automatic span.select {background-position:-350px -76px;}
                .gnb_layer .carfinder .transmission ul.case1 li.automatic span.unable {background-position:-350px -38px;}

                .gnb_layer .carfinder .torque .range,
                .gnb_layer .carfinder .speed .range,
                .gnb_layer .carfinder .power .range,
                .gnb_layer .carfinder .displacement .range {height:24px;margin-top:4px;padding:0 7px;}
                .gnb_layer .carfinder .torque .range strong,
                .gnb_layer .carfinder .speed .range strong,
                .gnb_layer .carfinder .power .range strong,
                .gnb_layer .carfinder .displacement .range strong {color:#2c407b;font-size:20px;}
                .gnb_layer .carfinder .torque .range span,
                .gnb_layer .carfinder .speed .range span,
                .gnb_layer .carfinder .power .range span,
                .gnb_layer .carfinder .displacement .range span {color:#768089;font-weight:bold;}
                .gnb_layer .services {width:940px;}
                .gnb_layer .services ul { width:930px; display:inline-block; margin:0 0 0 5px; padding:10px 0; }
                .gnb_layer .services li {float:left;background: #fff; list-style: none outside none;
                }
                .gnb_layer .services ul.length5 li { width:19.8%; height: 136px;}
                .gnb_layer .services ul.length6 li { width:16.66%;  height: 136px; }
                .gnb_layer .services li strong {height:32px; display: block; font-weight: normal;text-transform:uppercase; font-family:'GenesisSansText'; font-size: 14px; color: #333; line-height: 16px;text-align: center;}
                .gnb_layer .services li span {display: block;}
                .gnb_layer .services li span.txt {display: none; padding:0 10px 0 20px; color: #666; margin-top:-2px; line-height: 14px; font-size: 12px; }
                .gnb_layer .services li span.icon {text-align: center; padding-top:7px; }
                .gnb_layer .services li a:hover {cursor:pointer;}
                .gnb_layer .services li a:hover span.icon {display: none;}
                .gnb_layer .services li a:hover span.txt {display: block;}
                .gnb_layer .services li span.txt img {margin-top: 10px;}
                .gnb_layer .services li:first-child a,
                .gnb_layer .services li.HyundaiService {border: none;}
                .gnb_layer .services li a {display:block; padding:18px 0 0 0; height:128px; border-left: 1px solid #e8eaec; overflow:hidden; text-decoration:none; }
                .gnb_layer .services li a:hover {position:relative;z-index:100;background:url(/wcm/images/ru/common/bg_over.gif) repeat-x #e8e5e0; border-color:#e8e5e0;}
                .gnb_layer .shopping .tools {float:left;padding:0 0 0 0;}
                .gnb_layer .shopping .tools strong.title {display:block; padding-top:10px; height:32px; line-height:31px; font-size:14px;font-weight:normal; color:#575a5d;text-transform:uppercase; font-family:'ModernHBold';}
                .gnb_layer .shopping .tools ul {float:left;padding:19px 45px 0 0;overflow:hidden;}
                .gnb_layer .shopping .tools ul:first-child { padding-left:0; }
                .gnb_layer .shopping .tools ul:last-child { padding-right:0; }
                .gnb_layer .shopping .tools li {white-space: nowrap;margin-bottom:1px;padding:0 5px 7px 7px;line-height:13px;background:url('/media/img/bul_dot.gif') no-repeat 0 5px;}
                .gnb_layer .shopping .tools li a {text-indent:0;overflow:visible;color:#575a5d;}
                .gnb_layer .shopping .etc {float:right;padding: 0 0 0 0; display:inline-block;}
                .gnb_layer .shopping .etc li {float:left;width:125px; list-style: none outside none;}
                .gnb_layer .shopping .etc li strong {height:31px; display:block; font-weight: normal;text-transform:uppercase;padding-bottom:10px; font-family:'GenesisSansText'; font-size: 14px; color: #333; line-height: 16px;text-align: center;letter-spacing: -0.5px;}
                .gnb_layer .shopping .etc li span {display: block;}
                .gnb_layer .shopping .etc li a {display:block;text-indent:0;height:149px;padding:18px 0 0 0;overflow:hidden;text-decoration:none; background-color:#fff;border-left:1px solid #e8eaec;}
                .gnb_layer .shopping .etc li a:hover {position:relative;z-index:100; background:url(/wcm/images/ru/common/bg_over.gif) repeat-x #e8e5e0; border-color:#e8e5e0;}
                .gnb_layer .shopping .etc li span {display: block;}
                .gnb_layer .shopping .etc li span.txt {display: none; padding: 5px 0 0 10px; color: #afafaf; line-height: 14px;}
                .gnb_layer .shopping .etc li span.txt img {margin-top: 10px;}
                .gnb_layer .shopping .etc li span.icon {text-align: center;}
                .gnb_layer .shopping .etc li a:hover {cursor:pointer;}
                .gnb_layer .shopping .etc li a:hover span.icon {display: none;}
                .gnb_layer .shopping .etc li a:hover span.txt {display: block;}
                .light .header, .dark {height:60px;}

                .menuPrice {
                    font-size: 10px !important;
                    padding-left: 10px;
                }

                .scroll_me a{
                    width:50px;
                    height: 50px;
                    background: url("/media/img/to_top.png");
                    position: fixed;
                    bottom: 50px;
                    right: 21px;
                    z-index: 999;
                    cursor: pointer;
                    display: none;
                    opacity: 0.7;
                }
                .scroll_me a:hover{
                    opacity:1;
                }

                .btn_area1>a>img{
                    max-width: 220px;
                }
                .btn_area1>a:hover{
                    opacity:.8;
                }

                /* zing forms */

                .zing_form_wrp, #brochure_go_check_email {
                    background: #64c0e3 none repeat scroll 0 0;
                    border: 4px solid #caeaff;
                    box-shadow: 0 0 15px #777;
                    color: #333;
                    display: none;
                    height: 400px;
                    left: 50%;
                    margin: 0 0 0 -192px;
                    opacity: 0.95;
                    position: absolute;
                    width: 385px;
                    z-index: 999;
                }
                #brochure_go_check_email {
                    height: 150px;
                    padding: 15px 25px;
                    text-align: center;
                    line-height: 11px;
                }
                #brochure_email {
                    font-size: 120%;
                }
                .zing_form label.error {
                    background: #6bc3e3 none repeat scroll 0 0;
                    border: 2px solid #ccebfe;
                    box-shadow: 0 0 5px #0b698b;
                    left: 100%;
                    margin-left: -18px;
                    padding: 2px 5px;
                    position: absolute;
                    width: 225px;
                }
                .zing_form label.error::before {
                    border-color: rgba(204, 235, 254, 0) #ccebfe rgba(204, 235, 254, 0) rgba(204, 235, 254, 0);
                    border-width: 8px;
                    margin-top: -8px;
                }
                .zing_form label.error::after, .zing_form label.error::before {
                    border: medium solid transparent;
                    content: " ";
                    height: 0;
                    pointer-events: none;
                    position: absolute;
                    right: 100%;
                    top: 50%;
                    width: 0;
                }
                .zing_form label.error::after {
                    border-color: rgba(107, 195, 227, 0) #6bc3e3 rgba(107, 195, 227, 0) rgba(107, 195, 227, 0);
                    border-width: 5px;
                    margin-top: -5px;
                }
                .zing_form label.error::after, .zing_form label.error::before {
                    border: medium solid transparent;
                    content: " ";
                    height: 0;
                    pointer-events: none;
                    position: absolute;
                    right: 100%;
                    top: 50%;
                    width: 0;
                }
                .zing_form_content {
                    padding: 15px 25px;
                }
                .zing_form_content * {
                    opacity: 1;
                }
                .zing_form p, #brochure_go_check_email p {
                    border: medium none !important;
                    color: #fff !important;
                    font-size: 14px !important;
                    position: relative;
                    /*     padding: 0 0 10px !important; */
                    margin-bottom: 10px !important;
                }
                .zing_lbl {
                    cursor: pointer;
                    display: inline-block;
                    line-height: 14px;
                    vertical-align: middle;
                    width: 110px;
                }
                .zing_lbl.req::after, .zing_star::after {
                    color: #fff;
                    content: "*";
                    font-size: 20px;
                    margin: 5px 0 0 5px;
                    position: absolute;
                    text-shadow: 0 0 1px #000;
                }
                .zing_star {
                    display: inline-block;
                    width: 25px;
                }
                .zing_txt {
                    background-color: #eaf8ff;
                    color: #333;
                    font-size: 14px;
                    padding: 5px;
                    width: 214px;
                }
                .zing_txt.error {
                    background-color: #ffeaea;
                    outline: none !important;
                }
                .zing_rules_agree_cb.error{
                    outline: none !important;
                }
                .zing_txt.valid {
                    background-color: #eaffea;
                }
                .zing_rules_wrp {
                    margin-top: 5px;
                }
                .zing_rules_agree_cb {
                    cursor: pointer;
                    float: left;
                    height: 14px;
                    margin-right: 3px;
                    width: 14px;
                }
                .zing_rules_agree_txt {
                    cursor: pointer;
                    display: block;
                    line-height: 16px;
                    margin-left: 20px;
                }
                .zing_rules_wrp a {
                    border-bottom: 1px dotted #000;
                }
                #zing_rules_wrp a:hover {
                    border-bottom: medium none;
                    text-decoration: none;
                }
                .zing_close {
                    color: #caeaff !important;
                    font-size: 32px;
                    height: 24px;
                    line-height: 24px;
                    overflow: hidden;
                    position: absolute;
                    right: 0;
                    text-align: center;
                    top: 0;
                    width: 24px;
                }
                .zing_close:hover {
                    color: #fff !important;
                }
                .zing_pers_rules_wrp {
                    background: #64c0e3 none repeat scroll 0 0;
                    bottom: 105px;
                    color: #fff;
                    display: none;
                    font-size: 12px;
                    left: 0;
                    overflow: auto;
                    padding: 25px;
                    position: absolute;
                    right: 0;
                    top: 0;
                }
                .zing_ok, #brochure_go_check_email input {
                    background-color: #caeaff;
                    color: #333;
                    cursor: pointer;
                    display: block;
                    font-size: 14px;
                    height: 28px;
                    line-height: 20px;
                    margin-top: 5px;
                    outline: medium none;
                    width: 100%;
                }
                .zing_ok:hover, #brochure_go_check_email input:hover {
                    background-color: #def2ff;
                }
                .zing_ok:active, #brochure_go_check_email input:active {
                    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3) inset;
                    line-height: 32px;
                }

                #ui-datepicker-div {
                    background: #ebf8fe none repeat scroll 0 0;
                    border: 5px solid #ccebfe;
                    box-shadow: 0 5px 15px #999;
                    display: none;
                    text-align: center;
                    width: 214px;
                }
                #ui-datepicker-div a, #ui-datepicker-div a:hover {
                    display: block;
                    font-size: 14px;
                    line-height: 20px;
                    text-decoration: none;
                }
                #ui-datepicker-div a:hover {
                    background-color: #64c0e3;
                    color: #fff;
                }
                .ui-datepicker-current-day {
                    background-color: #64c0e3;
                }
                .ui-datepicker-current-day a {
                    color: #fff;
                    display: block;
                }
                .ui-datepicker-header {
                    margin: 5px 0;
                }
                .ui-datepicker-prev {
                    float: left;
                }
                .ui-datepicker-next {
                    float: right;
                }
                .ui-datepicker-prev, .ui-datepicker-next {
                    color: #25aae1;
                    cursor: pointer;
                    display: none !important;
                    font-size: 18px;
                    line-height: 24px;
                    width: 22px;
                }
                #ui-datepicker-div select {
                    border: 1px solid #6bc3e3;
                    cursor: pointer;
                    font-size: 14px;
                    line-height: 16px;
                    padding: 2px 7px;
                }
                .dealer-icons {
                    margin: 0 0 15px;
                    overflow: hidden;
                    text-align: center;
                    width: 200px;
                }
                .dealer-icons-test-drive {
                    margin: 0;
                }
                a.zingaya_button1370324828422 {
                    display: block;
                    float: none !important;
                    margin: 0 auto 10px !important;
                }
                .simplemodal-container {
                    line-height: 19px;
                }
                .zing_form_toggler, .newsletter_form_toggler {
                    cursor: pointer;
                }
                .newsletter_form .btn {
                    background-color: #caeaff;
                    color: #333;
                    cursor: pointer;
                    display: block;
                    font-size: 14px;
                    height: 28px;
                    line-height: 20px;
                    margin-top: 5px;
                    outline: medium none;
                    width: 100%;
                }
                .newsletter_form label.error {
                    display:none;
                }
                .newsletter_form .zing_pers_rules_wrp {
                    bottom:125px;
                }
                .newsletter_form .message {
                    font-weight:700;
                    display:none;
                    padding:100px 0!important;
                    text-align:center;
                }
                .zing_form label.error::after, .zing_form label.error::before {
                    border: medium solid transparent;
                    content: " ";
                    height: 0;
                    pointer-events: none;
                    position: absolute;
                    right: 100%;
                    top: 50%;
                    width: 0;
                }
                .zing_form label.error::before {
                    border-color: rgba(204, 235, 254, 0) #ccebfe rgba(204, 235, 254, 0) rgba(204, 235, 254, 0);
                    border-width: 8px;
                    margin-top: -8px;
                }
                .zing_form label.error::after {
                    border-color: rgba(107, 195, 227, 0) #6bc3e3 rgba(107, 195, 227, 0) rgba(107, 195, 227, 0);
                    border-width: 5px;
                    margin-top: -5px;
                }
                .zing_form label.error {
                    background: #6bc3e3 none repeat scroll 0 0;
                    border: 2px solid #ccebfe;
                    box-shadow: 0 0 5px #0b698b;
                    left: 108%;
                    margin-left: -18px;
                    padding: 2px 5px;
                    position: absolute;
                    width: 225px;
                    top: 0;
                    color: #fff !important;
                }

                /* // ZING */
                /* hide Google Conversion Frame (conversion.js) */
                iframe[height="13"] {display: none;}
                /*
**  video popup styles;
**	now we can remove these styles on all other pages :)
 */
                .videoPopup {display: none;top: 0px;position: fixed;width: 100%;height: 100%;z-index: 1100 !important;background-color: rgba(0, 0, 0, 0.8);}
                .videoPopup .closeButton {color: white;font-size: 60px;font-weight: 100;opacity: 0.8;position: absolute;right: 14%;text-decoration: none;top: 10px;}
                .videoPopup .closeButton:hover {opacity: 1;}
                .videoPopup .videoContainer {max-width: 800px;margin: 0 auto;}
                .videoPopup .videoContainer iframe,
                .videoPopup .videoContainer object,
                .videoPopup .videoContainer embed {max-width: 100%;}
                .videoPopup .videoContainer iframe {position: relative;top: 50px;height: 560px;}
                /* // video popup */

                .p_div {
                    color: #999;
                    font-size: 10px;
                    cursor: pointer;
                    float: left;
                    margin-left: 400px;
                }

                .p_div span {
                    display: none;
                }

                .p_div > span:first-of-type {
                    display: inline;
                }

                .p_div.active span {
                    display: inline !important;
                }

                .technologies-controls{
                    position: absolute;
                    z-index:101;
                    top: 47px;
                    left: 50%;
                }

                .mt14 {
                    margin-top: 14px !important;
                    margin-bottom: 3px;
                }

                .news-center-table{
                    display: table;
                    width: 100%;
                }
                .news-center-table-td{
                    display: table-cell;
                    vertical-align: middle;
                }
                .news-center-table-td--w{
                    width: 224px;
                }
                .news-center-table-td--w1{
                    width: 482px;
                }
                .news-center-table .all-link{
                    vertical-align: middle;
                }

                .sale {
                    font-size:10px;
                    display:inline-block;
                    padding:0 3px;
                    color:#FFF!important;
                    background:#C7141C;
                }

                .b-news-view a {
                    text-decoration: underline !important;
                }

                .b-news-view a:hover {
                    text-decoration: none !important;
                }
                @font-face{
                    font-family: 'Rouble';
                    src: url('fonts/rouble.eot');
                    src: url('fonts/rouble.eot?#iefix') format('embedded-opentype'),
                    url('fonts/rouble.ttf') format('truetype');
                    font-style: normal;
                }
                .rouble {
                    font-family: "Rouble" !important;
                    font-weight: 400;
                }

                .favourite-block {
                    position: relative;
                    height: 46px;
                    background: url('/media/img_new/favourite-block-bg.jpg') repeat-x center center;
                }
                .favourite-block .favourite-block-container {
                    color: #fff;
                    font-family: ModernH;
                    font-size: 16px;
                    line-height: 46px;
                    text-align: center;
                }
                .favourite-block .favourite-block__txt,
                .favourite-block .favourite-block__img {
                    display: inline-block;
                }
                .favourite-block .favourite-block__img {
                    padding: 0 0 0 15px;
                    vertical-align: middle;
                }



                .car-year-logo {
                    position: absolute;

                    left: 290px;
                    top: 104px;
                    width: 145px;
                    height: 51px;
                    background: url('/media/img_new/car_year_logo.png') no-repeat center center;
                }

                #leftlink {
                    width: 250px;
                    height: 215px;
                    position: absolute;
                    left: 50%;
                    margin-left: -510px;
                    top: 135px;
                    cursor: pointer;
                    z-index: 100;
                }

                #rightlink {
                    width: 179px;
                    height: 50px;
                    position: absolute;
                    left: 50%;
                    top: 264px;
                    margin-left: 224px;
                    background: url(/media/img_new/banner-button.png) no-repeat center top;
                    cursor: pointer;
                    z-index: 100;
                }

                #rightlink:hover {
                    background-position: center bottom;
                }


                .detailedSpecLink {
                    text-align: center;
                    margin: 40px auto;
                }
                .detailedSpecLink > a {
                    display: inline-block;
                    padding: 10px 25px;
                    background-color: #9A5341!important;
                    color: #fff;
                    text-align: center;
                    text-decoration: none!important;
                    font-size: 18px;
                    font-weight: bold;
                    transition: all 0.2s ease-out;
                }
                .detailedSpecLink > a:hover {
                    background-color: #1fa1f5!important;
                }

                .i-0030-auto-icon.service {
                    background: url('/media/img_new/instruments_1.png') no-repeat center center;
                }

                .i-0030-auto-icon:hover.service {
                    background: url('/media/img_new/instruments_2.png') no-repeat center center;
                }
            </style>

            <style type="text/css">
                /*

		DEALERS TEMPLATE STYLES

		*/
                .dealer__container {
                    background: #fff none repeat scroll 0 0;
                    overflow: hidden;
                    padding: 0;
                    position: relative;
                    width: 100%;
                    z-index: 50;
                }

                .dealer__content_wrap {
                    margin: 72px auto 0;
                    position: relative;
                }
                .dealer__title_area {
                    margin: 0 auto;
                    padding: 20px 0 25px;
                    position: relative;
                    width: 940px;
                }
                .dealer__title_area .title {
                    float: left;
                    font-family: "ModernHEcoLight";
                    font-size: 45px;
                    line-height: 0.9;
                }
                .dealer__maps {
                    margin: 0 auto;
                }
                .dealer__map_container {
                    float: none;
                    width: 100%;
                }
                #map_canvas, #map_canvas2 {
                    width: 100%;
                    height: 420px;
                }
                @media screen and (max-width: 1024px) {
                    #map_canvas {
                        height: 380px;
                    }
                }
                /*nano*/
                /** initial setup **/
                .nano {
                    position : relative;
                    width    : 100%;
                    height   : 233px;
                    overflow : hidden;
                }
                .nano > .nano-content {
                    position      : absolute;
                    overflow      : scroll;
                    overflow-x    : hidden;
                    top           : 0;
                    right         : 0;
                    bottom        : 0;
                    left          : 0;
                    margin-top    : 15px;
                }
                .nano > .nano-content:focus {
                    outline: thin dotted;
                }
                .nano > .nano-content::-webkit-scrollbar {
                    display: none;
                }
                .has-scrollbar > .nano-content::-webkit-scrollbar {
                    display: block;
                }
                .nano > .nano-pane {
                    background : #9A5341;
                    position   : absolute;
                    bottom     : 0;
                    visibility : hidden\9; /* Target only IE7 and IE8 with this hack */
                    opacity    : .01;
                    -webkit-transition    : .2s;
                    -moz-transition       : .2s;
                    -o-transition         : .2s;
                    transition            : .2s;
                    right: 26px;
                    top: 15px;
                    width: 1px;
                }
                .nano > .nano-pane > .nano-slider {
                    background: #9A5341 none repeat scroll 0 0;
                    border-radius: 2px;
                    margin: 0 -3px;
                    position: relative;
                    width: 7px;
                }
                .nano:hover > .nano-pane, .nano-pane.active, .nano-pane.flashed {
                    visibility : visible\9; /* Target only IE7 and IE8 with this hack */
                    opacity    : 0.99;
                }
                .dealer__location-dropdown {
                    position: absolute;
                    height: 45px;
                    width: 270px;
                    border: 1px solid #cfcfcf;
                    color: #404040;
                    cursor: pointer;
                    font-size: 14px;
                    overflow: hidden;
                    right: 0;
                    z-index: 100;
                    background: #fff;
                }
                .dropdown-list {
                    padding: 0 20px;
                    margin: 0px !important;
                }
                .dropdown-first {
                    padding: 0 20px;
                    position: relative;
                    z-index: 110;
                }
                .dropdown-item {
                    display: none;
                    color: #303030;
                    cursor: pointer;
                    display: block;
                    font-family: "GenesisSansText";
                    font-size: 15px;
                    padding: 6px 0;
                    width: 230px;
                }
                .dropdown-item.active {
                    color: #9A5341;
                    display: none;
                }
                /*.dropdown-item.first {*/
                /*background-color: #fff;*/
                /*background-image: url("images/icons/dropdown-icon.png");*/
                /*background-position: 420px -7px;*/
                /*background-repeat: no-repeat;*/
                /*border-bottom: 1px solid #fff;*/
                /*margin: 7px 0 0;*/
                /*padding-left: 25px;*/
                /*width: 100%;*/
                /*z-index: 120;*/
                /*box-sizing: content-box;*/
                /*}*/
                /*.dropdown-item.first-active {*/
                /*background-position: 218px -52px;*/
                /*border-bottom: 1px solid #ebebeb;*/
                /*color: #ababab;*/
                /*padding-bottom: 14px;*/
                /*padding-left: 0;*/
                /*width: 230px;*/
                /*}*/
                /*.dropdown-item.first::before {*/
                /*background: transparent url("images/icons/cd-icon-location.png") no-repeat scroll center center;*/
                /*content: "";*/
                /*display: inline-block;*/
                /*height: 18px;*/
                /*left: 14px;*/
                /*position: absolute;*/
                /*width: 25px;*/
                /*}*/
                .dropdown-item.divider {
                    color: #ebebeb;
                    cursor: default;
                    font-weight: bolder;
                }
                .dropdown-item.divider:hover {
                    color: #ebebeb;
                }
                .dropdown-item.first-active:before{
                    display: none;
                }
                .dropdown-item.first:hover {
                    color: #ababab;
                }
                .dropdown-item:hover {
                    color: #9A5341;
                }
                /*      infowindow     */
                .infoWindow-close {
                    background: transparent url("/media/img/map-icon-close.png") no-repeat scroll center center;
                    right: 20px !important;
                    top: 20px !important;
                }
                .hideThis {
                    display: none;
                }
                .gm-style-parent {
                    background: #fff;
                    width: auto !important;
                    height: auto !important;
                }
                .gm-style-parent:after {
                    right: 100%;
                    top: 50%;
                    border: solid transparent;
                    content: " ";
                    height: 0;
                    width: 0;
                    position: absolute;
                    pointer-events: none;
                    border-color: rgba(255, 255, 255, 0);
                    border-right-color: #fff;
                    border-width: 8px;
                    margin-top: -8px;
                }
                .gm-style-child-1 {
                    border: none !important;
                    border-radius: none !important;
                }
                .gm-style-child-2 {
                    background: none !important;
                    box-shadow: none !important;
                    border-radius: unset !important;
                }
                .gm-style-child-4 {
                    border-radius: unset !important;
                }
                .gm-style-child-trin {
                    box-shadow: none !important;
                }

                /*remove these importants - cssstyleru conflicting*/

                .dealer-info {
                    background-color: none !important;
                    border: none !important;
                    margin: 0 !important;
                    padding: 0 !important;
                }
                .dealer-test-drive {
                    background: #9A5341 url("/media/img/map-icon-arrow.png") no-repeat scroll 150px center;
                    box-sizing: border-box;
                    width: 210px;
                    height: 45px;
                    font-family: GenesisSansText, sans-serif;
                    font-size: 14px;
                    font-weight: bold;
                    text-align: center;
                    line-height: 45px;
                    text-transform: uppercase;
                    background-color: #9A5341;
                    color: #fff;
                    padding: 0 16px 0 0;
                }
                .dealer-test-drive_parent-link:hover {
                    text-decoration: none;
                }
                .dealer-name {
                    font-size: 20px;
                    font-family: "GenesisSansText";
                    color: #303030;
                    font-weight: bold;
                    text-align: left;
                    line-height: 1.1;
                    font-weight: normal !important;
                    border-bottom: 1px solid #ebebeb;
                    margin-bottom: 25px !important;
                    padding-bottom: 15px !important;
                }
                .pop_text {
                    font-family: GenesisSansText, sans-serif;
                    font-size: 14px;
                    text-align: left;
                    margin-bottom: 15px !important;
                }
                .pop_text > span {
                    display: block;
                    margin-bottom: 5px;
                }
                .pop_text .dealer-site a {
                    color: #9A5341;
                    white-space: nowrap;
                }

                .gm-style .gm-style-iw {
                    overflow: hidden !important;
                    width: 210px !important;
                    height: auto !important;
                    padding: 25px !important;
                    position: relative !important;
                    box-sizing: content-box;
                }
                .gm-style-iw .call-to-dealer {
                    display: none;
                }
                /*--------------listing-------------*/
                .dealer__map_results {
                    width: 940px;
                    height: auto;
                    margin: 0 auto;
                    padding-top: 25px;
                }
                .dealer__map_results__city {
                    font-size: 30px;
                    font-family: GenesisSansText, "Helvetica Neue", Helvetica, sans-serif;
                    color: #9A5341;
                    text-transform: uppercase;
                    padding: 0;
                    margin: 0 0 15px;
                }
                .dealer-info-box {
                    margin: 10px 0;
                }
                .dealer-info-box .dealer-test-drive_parent-link {
                    display: none;
                }

                /*remove these importants - cssstyleru conflict*/

                .dealer-info-box .dealer-info {
                    display: inline-block;
                    height: 185px;
                    margin: 10px 10px 10px 0 !important;
                    vertical-align: top;
                    width: 300px;
                }
                .pop_body {
                    height: 100%;
                }
                .dealer-info-box .dealer-name {
                    border: medium none;
                    margin: 0 !important;
                    padding-bottom: 15px !important;
                }
                .dealer-info-box .pop_text > span {
                    font-size: 16px;
                }
                .dealer-info-box .pop_text > span.dealer-address {
                    color: #303030;
                    line-height: 1.3;
                    max-width: 270px;
                    min-height: 45px;
                    text-decoration: underline dashed #808080;
                }
                .dealer-info-box .pop_text > span.dealer-address:hover{
                    text-decoration: none;
                }
                .dealer-info-box .pop_text > span.dealer-phone {
                    color: #404040;
                    font-size: 15px;
                    font-weight: bold;
                }
                .dealer-info-box-devider {
                    background-color: #e6e6e6;
                    height: 1px;
                    margin: 35px 0;
                    width: 100%;
                }
                .dealer-info-box .call-to-dealer a.zingaya_button1370324828422 {
                    background-image: url("/media/img/call-to-dealer-icon-off.png");
                    background-color: #fff;
                    background-repeat: no-repeat;
                    background-position: 20px center;
                    display: inline-block;
                    float: none;
                    height: 42px;
                    margin: 0 !important;
                    width: 150px;
                    box-sizing: border-box;
                    border: 2px solid #9A5341;
                    font-size: 11px;
                    font-family:"GenesisSansHead", Gadget, GenesisSansText, sans-serif;
                    font-weight: 900;
                    text-align: center;
                    padding: 12px 0 0 32px;
                    color: #9A5341;
                    text-transform: uppercase;
                    transition: background 100ms, color 100ms;
                }
                .dealer-info-box .call-to-dealer a.zingaya_button1370324828422:hover {
                    background-image: url("/media/img/call-to-dealer-icon-on.png");
                    background-color: #9A5341;
                    background-repeat: no-repeat;
                    background-position: 20px center;
                    color: #fff;
                    text-decoration: none;
                }
                .nano > .nano-content:focus {
                    outline: none;
                }
                .fleft {
                    float: left;
                }
                .fright {
                    float: right;
                }
                .skyblue{
                    color: #9A5341 !important;
                }
                .offers__listing {
                    display: block;
                    margin: 30px 0 35px;
                    padding-bottom: 15px;
                    border-bottom: 1px solid #e6e6e6;
                }
                .offers__listing__item-skyblue {
                    padding: 0 0 15px 35px;
                    list-style: none;
                    position: relative;
                }
                .offers__listing__item-skyblue::before {
                    display: block;
                    position: absolute;
                    content: '';
                    background-color: #9A5341;
                    border-radius: 5px;
                    top: 10px;
                    left: 0;
                    margin: 0;
                    width:10px;
                    height:10px;
                }
                .dashedskyblue::after {
                    background: transparent;
                    border-bottom: 1px dashed #9A5341;
                    bottom: -2px;
                    content: "";
                    display: block;
                    height: 8px;
                    left: 0;
                    margin: 0;
                    padding: 0;
                    position: absolute;
                    width: 100%;
                    -webkit-transition: border 200ms ease 0s;
                    -moz-transition: border 200ms ease 0s;
                    -o-transition: border 200ms ease 0s;
                    -ms-transition: border 200ms ease 0s;
                    transition: border 200ms ease 0s;
                }
                .dashedskyblue:hover::after {
                    border-bottom: 1px dashed transparent;
                }
                .underlined::after {
                    background: transparent none repeat scroll 0px 0px;
                    border-top: 1px solid #9A5341;
                    bottom: -3px;
                    content: "";
                    display: block;
                    height: 1px;
                    left: 0px;
                    margin: 0px;
                    padding: 0px;
                    position: absolute;
                    width: 100%;
                    -webkit-transition: border 200ms ease 0s;
                    -moz-transition: border 200ms ease 0s;
                    -o-transition: border 200ms ease 0s;
                    -ms-transition: border 200ms ease 0s;
                    transition: border 200ms ease 0s;
                }
                .underlined:hover::after {
                    border-top: 1px solid transparent;
                }
                .offers__content-togglers {
                    margin: 45px 0 50px;
                }
                .offers__content-togglers .offers__content-toggler-item {
                    border: 2px solid #9A5341;
                    box-sizing: border-box;
                    width: 260px;
                    line-height: 51px;
                    font-size: 14px;
                    font-family: GenesisSansText,sans-serif;
                    font-weight: bold;
                    text-align: center;
                    text-transform: uppercase;
                    color: #9A5341;
                    cursor: pointer;
                    background: #fff;
                    position: relative;
                    -o-transition: background 200ms, color 200ms;
                    -webkit-transition: background 200ms, color 200ms;
                    -moz-transition: background 200ms, color 200ms;
                    -ms-transition: background 200ms, color 200ms;
                    transition: background 200ms, color 200ms;
                }
                .offers__content-togglers .offers__content-toggler-item.underlined.current::after {
                    width: 0 !important;
                    height: 0 !important;
                    bottom: 0 !important;
                    padding: 0 !important;
                    margin: 0 0 0 -5px;
                    content: "";
                    display: block;
                    left: 50% !important;
                    top: 52px;
                    border-color: #9A5341 transparent transparent;
                    background: transparent;
                    border-style: solid;
                    border-width: 10px;
                    position: absolute;
                    transition: border 0ms !important;
                }
                .offers__content-togglers .offers__content-toggler-item.underlined.current:hover::after {
                    width: 0 !important;
                    height: 0 !important;
                    bottom: 0 !important;
                    padding: 0 !important;
                    margin: 0 0 0 -5px;
                    left: 50% !important;
                    top: 52px;
                    border-color: #9A5341 transparent transparent;
                    border-width: 10px;
                }
                .offers__content-togglers .offers__content-toggler-item.current{
                    color: #fff;
                    background: #9A5341;
                    cursor: default;
                    -moz-transition: background 200ms;
                    -o-transition: background 200ms;
                    -webkit-transition: background 200ms;
                    -moz-transition: background 200ms;
                    transition: background 200ms;
                }
                .offers__content-togglers .offers__content-toggler-item:hover {

                }
                /* configuring underline to each element*/
                .offers__content-togglers .offers__content-toggler-item.underlined:first-child::after {
                    left: 95px;
                    width: 26%;
                }
                .offers__content-togglers .offers__content-toggler-item.underlined::after {
                    left: 27px;
                    width: 79%;
                    bottom: 14px;
                    border-top: 1px solid transparent;
                    transition: border-color 200ms !important;
                }
                .offers__content-togglers .offers__content-toggler-item:hover.underlined::after {
                    border-top: 1px solid #9A5341;
                }
                .offers__head-image {
                    display: block;
                    left: 50%;
                    margin-left: -950px;
                    position: relative;
                    width: 1900px;
                }
                /*  BASE  */

                .offers__main {
                    width: 100%;

                }
                .offers__container {
                    width: 940px;
                    margin: 0 auto;
                }
                .hpromise-content-hide {
                    display: none;
                }
                .hpromise-content-hide.current {
                    display: block;
                }
                .offers__breadcrumbs {
                    margin: 30px auto 0;
                }
                .offers__breadcrumbs ul li {
                    display: inline-block;
                    color: #a5a5a5;
                    font-size: 13px;
                    font-family: "GenesisSansText", sans-serif;
                    text-align: left;
                    z-index: 25;
                }
                .offers__breadcrumbs ul li a{
                    color: #a5a5a5;
                }
                .breadcrumbs-fromtop {
                    margin-top: 100px;
                    margin-bottom: 10px;
                }
                .offers__content-title {
                    font-family: "ModernHEcoLight";
                    color: #303030;
                    font-size: 45px;
                    margin: 0 auto 20px;
                    text-align: left;
                    font-weight: 200;
                }

                h2.offers__content-title {
                    font-family: "ModernHLight";
                    font-size: 30px;
                    margin: 0 auto 20px;
                }

                /*
		text
		*/

                .offers__text {
                    font-size: 16px;
                    font-family: "GenesisSansText", sans-serif;
                    color: rgb(64,64,64);
                    line-height: 1.625;

                }
                .offers__text sup{
                    font-weight: bolder;
                    font-size: 0.7em;
                }

                /* style banners */

                .offers__prefooter-banner {
                    width: 100%;
                    margin: 0 auto;
                    background: #fff;
                }
                .offers__prefooter-banner__subscribe {
                    width: 940px;
                    margin: 0 auto;
                    padding: 20px 0;
                }
                .offers__prefooter-banner__subscribe .subscribe-q {
                    color: rgb(136,136,136);
                    float: left;
                    font-family: "ModernHLight";
                    font-size: 27px;
                    line-height: 48px;
                    text-align: left;
                }
                .offers__prefooter-banner__subscribe .subscribe-btn {
                    float: right;
                    width: 185px;
                    height: 48px;
                    font-size: 12px;
                    font-family: GenesisSansText,sans-serif;
                    font-weight: 900;
                    color: rgb(255, 255, 255);
                    text-transform: uppercase;
                    line-height: 50px;
                    text-align: center;
                    background: #9A5341;
                    transition: background 200ms;
                }
                .offers__prefooter-banner__subscribe .subscribe-btn:hover {
                    text-decoration: none;
                    background: #1fa1f5;
                }
                .offers__prefooter-banner__biglinks {
                    width: 1200px;
                    margin: 0 auto;
                }
                .offers__prefooter-banner__biglinks .biglinks__unit {
                    float: left;
                    height: 265px;
                    width: 400px;
                    position: relative;
                }
                .offers__prefooter-banner__biglinks .biglinks__unit__wrap-link {
                    display: block;
                }
                .offers__prefooter-banner__biglinks .biglinks__unit .biglinks__unit__title {
                    position: absolute;
                    top: 25px;
                    left: 30px;
                    font-size: 26px;
                    font-family: "GenesisSansText";
                    color: #fff;
                    line-height: 1.08;
                }
                .offers__prefooter-banner__biglinks .biglinks__unit .biglinks__unit__link {
                    position: absolute;
                    bottom: 25px;
                    left: 30px;
                    font-size: 12px;
                    font-family: GenesisSansText, sans-serif;
                    font-weight: 900;
                    text-transform: uppercase;
                    color: #fff;
                    line-height: 28px;
                    background: rgba(0,0,0,0) url("/media/img/icon-arrow-offers-banners-link.png") no-repeat center right;
                    padding-right: 38px;
                }

                /*
		1. OFFERS DEVIDER STYLES
		*/
                .offers-divider {
                    background: #f5f5f5;
                }
                .offers_divider__content {
                }

                .offers_divider__content__title {
                    color: #9A5341;
                    font-family: "GenesisSansText";
                    font-size: 45px;
                    margin: 80px auto 25px;
                    text-align: left;
                }
                .offers_divider__content__offers-grid-section {
                    width: 100%;
                    margin-bottom: 25px;
                }
                .insection-offer-unit {
                    position: relative;
                    float: left;
                    margin-bottom: 10px;
                    width: 465px;
                    height: 275px;
                    background: #ddd;
                }
                .insection-offer-unit:nth-child(odd) {
                    margin-right: 10px;
                }
                .insection-offer-unit:nth-child(even) {
                }
                .insection-offer-unit .insection-offer-unit__back-img {
                    position: relative;
                    width: 100%;
                    height: 100%;
                    opacity: 0.7;
                }
                .insection-offer-unit .insection-offer-unit__info {
                    position: absolute;
                    left: 35px;
                    bottom: 10px;
                }
                .insection-offer-unit .insection-offer-unit__info span.name,
                .insection-offer-unit .insection-offer-unit__info span.addinfo {
                    color: #fff;
                    display: block;
                }
                .insection-offer-unit .insection-offer-unit__info span.name {
                    font-size: 28px;
                    font-family: "GenesisSansText";
                    margin-bottom: 10px;
                }
                .insection-offer-unit .insection-offer-unit__info span.addinfo {
                    font-size: 15px;
                    font-family: "GenesisSansText", sans-serif;
                }
                .insection-offer-unit .insection-offer-unit__link {
                    display: block;
                    background: #000;
                    height: 275px;
                }
                .insection-offer-unit .insection-offer-unit__back-img {
                    position: relative;
                    width: 100%;
                    height: 100%;
                    opacity: 0.7;
                    transition: opacity 200ms;
                }
                .insection-offer-unit:hover .insection-offer-unit__back-img {
                    opacity: 1;
                }
                .hpromise-main {
                    background: #fff none repeat scroll 0 0;
                    overflow: hidden;
                }
                .hpromise-main .offers__prefooter-banner {
                    background: #f5f5f5;
                }
                .offers__content-hide {
                }
                .hpromise-content__terms {
                    margin-bottom: 45px;
                }
                .v-listing {
                    width: 100%;
                    margin: 25px 0;
                    padding: 0 0 25px;
                    border-bottom: 1px solid #e6e6e6;
                }
                .v-listing__col {
                    width: 50%;
                    float: left;
                }
                .v-listing .offers__text.v-listing__numeric {
                    display: inline-block;
                    margin: 0 0 25px;
                    padding: 0 0 0 50px;
                    position: relative;
                    width: 80%;
                }
                .v-listing .offers__text.v-listing__numeric::before {
                    background-color: #9A5341;
                    border-radius: 50%;
                    color: #ffffff;
                    content: "1";
                    display: block;
                    font-family: GenesisSansText,sans-serif;
                    font-size: 20px;
                    font-weight: bold;
                    left: 0;
                    line-height: 1.8;
                    margin: 0;
                    padding: 0 13px 0 12px;
                    position: absolute;
                    top: 0;
                }
                .v-listing .v-listing__col:nth-child(1) .offers__text.v-listing__numeric:nth-child(2)::before {content: "2"}
                .v-listing .v-listing__col:nth-child(1) .offers__text.v-listing__numeric:nth-child(3)::before {content: "3"}
                .v-listing .v-listing__col:nth-child(2) .offers__text.v-listing__numeric:nth-child(1)::before {content: "4"}
                .v-listing .v-listing__col:nth-child(2) .offers__text.v-listing__numeric:nth-child(2)::before {content: "5"}
                .v-listing .v-listing__col:nth-child(2) .offers__text.v-listing__numeric:nth-child(3)::before {content: "6"}

                .hpromise-content__sale {
                    overflow: hidden;
                    margin-bottom: 45px;
                }
                .v-listing.cars-list {
                    padding-bottom: 0;
                    margin: 45px 0;
                    width: 100%;
                    overflow: hidden;
                    border-top: 1px solid #e6e6e6;
                    border-bottom: 0;
                }
                .cars-list .cars-list__row {
                    border-bottom: 1px solid #e6e6e6;
                    padding: 35px 0 40px;
                    width: 100%;
                }
                .cars-list__row-item {
                    box-sizing: border-box;
                    float: left;
                    padding-left: 50px;
                    width: 470px;
                }
                .cars-list__row-item:first-child {
                    padding-left: 0;
                    width: 470px;
                }
                .cars-list__row-item__title {
                    color: #303030;
                    font-family: GenesisSansText;
                    font-size: 26px;
                    margin: 0;
                    text-align: left;
                }
                .cars-list__row-item__sales_wrap {
                    width: 420px;
                    margin-top: 10px;
                }
                .cars-list__row-item__sales__label {
                    display: block;
                    font-family: GenesisSansText,sans-serif;
                    font-weight: 500;
                    font-size: 17px;
                    /* 	color: rgb(180,180,180); */
                    color: #555;
                    margin: 25px 0 10px;
                }
                .cars-list__row-item__sales .offer-price-count {
                    margin: 0;
                    color: #0d75ba;
                    font-size: 22px;
                }
                .cars-list__row-item__sales .offer-price-count::after {
                    /* 	content: "<span class='rouble'>р</span>"; */
                    /* 	background: rgba(0, 0, 0, 0) url("../img/rub.png") no-repeat scroll 2px 3px / 14px auto; */
                    z-index: 1;
                }
                .cars-list__row-item__img {
                    max-width: 260px;
                }
                .cars-list__row-item__buttons {
                    margin-top: 25px;
                }
                .cars-list__row-item__buttons a.offer-actions__button.config{
                    background-color: transparent;
                    color: #9A5341;
                    width: 180px;
                    padding: 0 0 0 50px;
                    height: 55px;
                    background-size: 45px;
                    background-position: 5px 5px;
                    font-size: 11px;
                    font-weight: 600;
                }
                .cars-list__row-item__buttons a.offer-actions__button.config:hover{
                    background-position: 5px -169px;
                    /* 	color: #fff; */
                    /* 	text-decoration: none; */
                }
                .cars-list__row-item__buttons .cars-list__row-item__buttons__car-about {
                    font-size: 16px;
                    font-family: "GenesisSansText", GenesisSansText, sans-serif;
                    color: #9A5341;
                    text-decoration: none;
                    padding-right: 15px;
                    margin-left: 120px;
                    background: rgba(0, 0, 0, 0) url("../img/offers/hpromise/hp_elements/hp_icon_link.png") no-repeat scroll right 6px;
                    position: relative;
                    top: 15px;
                }
                .cars-list__row-item__buttons .cars-list__row-item__buttons__car-about:hover {
                    text-decoration: none;
                }
                .cars-list__row-item__buttons .cars-list__row-item__buttons__car-about.underlined::after {
                    width: 90%;
                }
                .hpromise-content__sale__footnote .offers__text {
                    color: #959595;
                    font-size: 15px;
                }
                .hpromise-content__sale__footnote .offers__text:first-child {
                    margin-bottom: 30px;
                }
                .hpromise-content__docs {
                    overflow: hidden;
                }
                .docs__listing .offers__listing__item-skyblue{
                    padding-bottom: 25px;
                }
                .hpromise-oldcar-features {
                    margin: 50px 0;
                    padding-bottom: 50px;
                    border-bottom: 1px solid #e6e6e6;
                }
                .hpromise-oldcar-features__item {
                    width: 134px;
                    float: left;
                }
                .hpromise-oldcar-features__item > * {
                    margin: 0 auto;
                    display: block;
                }
                .offers__text.hpromise-oldcar-features__item__text {
                    text-align: center;
                    font-size: 15px;
                    color: rgb(75,75,75);
                    font-family: GenesisSansText, sans-serif;
                    font-weight: bold;
                    line-height: 1.1;
                }
                .hpromise-oldcar-features__item__img {
                    margin-bottom: 25px;
                }

                .hpromise-content__map {
                }
                .hpromise-toggler-area {
                    position: relative;
                }
                /*REWRITE Z-INDEX TO HIDE DROPDOWN UNDER TOPMENU*/
                .hpromise-content__map .dealer__location-dropdown {
                    z-index: 55;
                    position: relative;
                }
                .hpromise-content__map__head {
                    position: relative;
                    width: 900px;
                    margin: 0 auto;
                }
                .hpromise-content__map .toggles-dropdown-wrap {
                    position: absolute;
                    right: 0;
                    top: 0;
                }
                .hpromise-content__map .offers__content-title {
                    margin-bottom: 30px;
                }
                .hpromise-content__map .hpromise-toggler__map {
                    margin: 0 0 0 12px;
                }
                .hpromise-content__map .hpromise-toggler__map .hpromise-toggler__map-item {
                    width: 96px;
                    height: 46px;
                    line-height: 46px;
                    font-size: 11px;
                    font-weight: 900;
                    border: 2px solid #9A5341;
                    box-sizing: border-box;
                    font-family: GenesisSansText,sans-serif;
                    text-align: center;
                    text-transform: uppercase;
                    color: #9A5341;
                    cursor: pointer;
                    background-color: transparent;
                    position: relative;
                    transition: background 200ms ease 0s, color 200ms ease 0s;
                }
                .hpromise-content__map .hpromise-toggler__map .hpromise-toggler__map-item.current {
                    background-color: #9A5341;
                    color: #fff;
                    cursor: default;
                }
                /* CONFIGURING TOGGLERS UNDERLINE ON HOVER*/
                /* REWRITING '.UNDERLINED' CLASS */
                .hpromise-content__map .hpromise-toggler__map .hpromise-toggler__map-item.underlined.current::after {
                    border-top: 1px solid transparent;
                    transition: border 200ms;
                }
                .hpromise-content__map .hpromise-toggler__map .hpromise-toggler__map-item.underlined.current:hover::after {
                    border-top: 1px solid transparent;
                    transition: border 200ms;
                }
                .hpromise-content__map .hpromise-toggler__map .hpromise-toggler__map-item.underlined:first-child::after {
                    left: 25px;
                    width: 46%;
                }
                .hpromise-content__map .hpromise-toggler__map .hpromise-toggler__map-item.underlined::after {
                    left: 20px;
                    width: 58%;
                    bottom: 10px;
                    border-top: 1px solid transparent;
                }
                .hpromise-content__map .hpromise-toggler__map .hpromise-toggler__map-item.underlined:hover.underlined::after {
                    border-top: 1px solid #9A5341;
                }
                .dealer__map_container {
                    display: none;
                    margin-bottom: 35px;
                    position: relative;
                    width: 100%;
                }
                .dealer__map_container .dealer__map_results.hpromise-dealers-tab__list {
                    display: none;
                    height: 100%;
                    left: 0;
                    margin: 0 auto;
                    position: relative;
                    right: 0;
                    top: 0;
                    width: 940px;
                }
                .dealer__map_container .dealer__map_results.hpromise-dealers-tab__list.current {
                    display: block;
                }
                .dealer__map_container .hpromise-dealers-tab__map {
                    display: none;
                }
                .dealer__map_container .hpromise-dealers-tab__map.current {
                    display: block;
                }
                .dealer-info-hpromise {
                    border-top: 1px solid #e6e6e6;
                    padding: 25px 0;
                }
                .dealer-info-hpromise:last-child {
                    border-bottom: 1px solid #e6e6e6;
                }
                .dealer-info-hpromise__leftblock {
                    width: 100%;
                    overflow: hidden;
                }
                .dealer-info-hpromise .dealer-info-hpromise__leftblock > .dealer-name{
                    padding-bottom: 8px !important;
                    width: 100%;
                }
                .dealer-info-hpromise .dealer-info-hpromise__leftblock > .dealer-address {
                    color: #303030;
                    display: block;
                    max-width: 470px;
                    text-decoration: underline dashed #808080;
                }
                .dealer-info-hpromise .dealer-info-hpromise__leftblock > .dealer-address:hover{
                    text-decoration: none;
                }
                .dealer-info-hpromise .dealer-phone {
                    background: transparent url("../img/offers/hpromise/hp_elements/hp_icon-phone.png") no-repeat scroll left 6px;
                    color: #404040;
                    display: block;
                    padding: 0 0 0 20px;
                    width: 170px;
                }
                .dealer-info-hpromise .dealer-site {
                    display: block;
                    /*     max-width: 240px; */
                    width: 250px;
                    padding-right: 40px;
                    word-break: keep-all;
                }
                .dealer-info-hpromise .dealer-site a{
                    color: #9A5341;
                    text-decoration: none;
                }
                .dealer-info-hpromise .dealer-site:hover a{
                    text-decoration: underline;
                }
                .add-toggler-after-content{
                    font-size: 14px;
                    font-family: GenesisSansText, sans-serif;
                    font-weight: 900;
                    text-decoration: none;
                    color: #9A5341 !important;
                    display: inline-block;
                    margin-bottom: 35px;
                    position: relative;
                    text-decoration: none !important;
                }
                .add-toggler-after-content:hover::after {
                    border-top: 1px dashed #fff;
                }
                .add-toggler-after-content.current {
                    display: none;
                }
                .add-toggler-after-content::before {
                    background: transparent url("/media/img/icon-arrow-offers-banners-link-bluevar.png") no-repeat scroll center center;
                    content: "";
                    display: block;
                    height: 28px;
                    margin: 0;
                    padding: 0;
                    position: absolute;
                    right: -40px;
                    top: -5px;
                    width: 28px;
                }
                .add-toggler-after-content::after {
                    background: transparent none repeat scroll 0 0;
                    border-top: 1px dashed #9A5341;
                    bottom: -3px;
                    content: "";
                    display: block;
                    height: 1px;
                    left: 0;
                    margin: 0;
                    padding: 0;
                    position: absolute;
                    width: 100%;
                    transition: border 200ms;
                }
                .add-toggler-after-content__wrap {
                    width: 940px;
                    margin: 0 auto;
                }


                .offer-actions__button {
                    background-color: #fff;
                    background-image: url("/media/img/buttons-sprite.png");
                    background-repeat: no-repeat;
                    border: 2px solid #9A5341;
                    box-sizing: border-box;
                    display: inline-block;
                    font-family: GenesisSansText,AppleGothic,sans-serif,Helvetica;
                    font-size: 14px !important;
                    font-weight: bold;
                    height: 58px;
                    line-height: 52px;
                    padding: 0 0 0 68px;
                    text-transform: uppercase;
                    transition: background-color 100ms ease 0s, color 100ms ease 0s !important;
                    text-decoration: none !important;
                    width: 180px;
                }

                .offer-actions__button:hover {
                    background-color: #9A5341 !important;
                    color: #fff !important;
                }

                .offer-actions__button.config {
                    background-position: 0 0;
                    height: 58px !important;
                }

                .offer-actions__button.config:hover {
                    background-position: 0 -224px;
                }

                .offer-actions__button:first-child {
                    margin-right: 10px;
                }

                .offer-actions__button.testdrive {
                    background-position: 0 -172px;
                }

                .offer-actions__button.testdrive:hover {
                    background-position: 0 -396px;
                }

                .grey {
                    color: rgb(136, 136, 136);
                }

                .tradein-banner {
                    padding-left: 3px;
                }
            </style>

            <style type="text/css">
                .test-drive-main {
                    padding-top: 50px;
                    background: #202020; /* url('/media/img/test_drive_imgs/ui/test_drive_bgr.jpg') no-repeat center top; */
                }
                .map-overlayed {
                    display: none;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    background: rgba(0,0,0,0.8);
                    position: fixed;
                    z-index: 201;
                }
                .map_canvas_wrap {
                    position: fixed;
                    opacity: 0;
                    margin: 0;
                    top: 50%;
                    left: -940px;
                    width: 940px;
                    height: 550px;
                    z-index: 200;
                    transition: opacity 200ms;
                }
                .map_canvas_wrap.vis {
                    left: 50%;
                    opacity: 1;
                    margin: -275px 0 0 -470px;
                }
                #map_canvas {
                    height: 550px;
                    z-index: 202;
                }
                .closeMap {
                    background: #9a9a9a none repeat scroll 0 0;
                    box-sizing: border-box;
                    color: #fff;
                    cursor: pointer;
                    font-size: 32px;
                    height: 48px;
                    padding: 0 0 0 15px;
                    position: absolute;
                    right: 0;
                    top: 0;
                    width: 48px;
                    z-index: 203;
                    transition: background 200ms;
                    -webkit-transition: background 200ms;
                }
                .closeMap:hover {
                    background: #9A5341;
                }
                /*

		PREFOOTER BANNERS BACKGROUND CHANGE TEMP

		*/
                .test-drive-main .offers__prefooter-banner {
                    background: #202020 url('/media/img/test_drive_imgs/ui/test_drive_bgr.jpg') no-repeat center top;
                }
                .test-drive-content {
                    padding-top: 0px;
                }
                .test-drive-content .offers__content-title.skyblue{
                    font-size: 35px;
                    margin-bottom: 25px;
                    font-size: 100;
                    text-align: center;
                }

                @media screen and (max-width: 850px){
                    .offers__content-title.skyblue{
                        margin-top: 15px;
                    }
                }
                /* TEST DRIVE GRID */
                .info-gather__body {
                    border-top: 1px solid #353535;
                    border-bottom: 1px solid #353535;
                    padding: 25px 0 15px;
                    margin: 0 0 30px;
                }
                .info-gather__col {
                    width: 290px;
                    margin-right: 35px;
                }
                .info-gather__col:last-child {
                    margin-right: 0;
                }
                .info-gather__col__title {
                    color: rgb(255, 255, 255);
                    font-family: "GenesisSansText";
                    font-size: 20px;
                    margin-bottom: 20px;
                    font-weight: 100;
                }

                /*DROPS*/
                .carlistdropdown, .dealerlistdropdown, .dealercitylistdropdown {
                    position: relative;
                    height: 45px;
                    overflow: visible;
                    max-height: 290px;
                }
                .drop-it-down {
                    position: relative;
                    background: transparent;
                    border-color: #505050;
                    width: 290px;
                    text-decoration: none;
                    box-sizing: border-box;
                    transition: border-color 200ms, height 200ms;
                    /*background: rgba(32,32,32,0.98);*/
                }
                .is-dropped {
                    height: 290px;
                    background: rgba(32,32,32,0.98);
                    border-color: #fff  !important;
                    z-index: 30;
                }
                .dealercitylistdropdown {
                    z-index: 50;
                }
                .dealerlistdropdown {
                    z-index: 1;
                    margin: 17px 0 0;
                }
                .dealer__location-dropdown.drop-it-down.isDealerselected {
                    border-bottom-color: transparent;
                }
                .drop-it-down:hover,
                .drop-it-down:hover .dealer__location-dropdown.drop-it-down.isDealerselected.is-dropped {
                    border-color: #fff !important;
                }
                .drop-it-down:focus {
                    outline: none;
                }
                .info-gather__col .dealer__location-dropdown {
                    z-index: 50;
                }
                .drop-it-down .dropdown-item {
                    color: #fff;
                    cursor: pointer !important;
                    transition: color 200ms;
                    user-select: none;
                    -moz-user-select: none;
                    -webkit-user-select: none;
                }
                .drop-it-down .dropdown-item.active {
                    color: #9A5341;
                }
                .drop-it-down .dropdown-item:hover {
                    text-decoration: underline;
                }

                .dropdown-item.first {
                    /*background: rgba(0, 0, 0, 0) url("images/icons/dropdown-icon.png") no-repeat scroll 420px -1px;*/
                    color: #fff;
                    width: 100%;
                    padding-left: 20px;
                    padding-right: 20px;
                    padding-top: 12px;
                    margin-top: 0;
                    padding-bottom: 13px;
                    border-color: #505050;
                    box-sizing: content-box;
                    z-index: 120;
                }
                .dropdown-list.first {
                    padding: 0;
                    margin: 0 !important;
                }
                .is-dropped.drop-it-down .dropdown-item.first {
                    color: #757575;
                    /*background: rgba(0, 0, 0, 0) url("images/icons/dropdown-icon.png") no-repeat scroll 258px -46px;*/
                }
                .drop-it-down .dropdown-item.first::before {
                    content: none;
                }
                #dropdown-dealerlisting.drop-it-down .dropdown-list.first .dropdown-item.first {
                    color: #757575;
                }
                #dropdown-dealerlisting.drop-it-down .dropdown-list.first .dropdown-item.first:hover {
                    color: #ababab;
                }
                #dropdown-dealerlisting.drop-it-down.isDealerselected .dropdown-list.first .dropdown-item.first {
                    color: #fff;
                }
                .drop-it-down .dropdown-item.divider {
                    color: #757575;
                }
                .drop-it-down .dropdown-item.divider:hover,
                .drop-it-down .dropdown-item.first:hover  {
                    text-decoration: none;
                }
                .selected-dealer__wrap {
                    left: 0;
                    top: 0;
                    background: rgba(32,32,32,0.98);
                    position: relative;
                    margin: -1px 0 17px;
                    padding: 0;
                    overflow: hidden;
                    border: 1px solid #505050;
                    border-top-color: #202020;
                    z-index: 15;
                    opacity: 0;
                    transition: opacity 200ms;
                }
                .selected-dealer__wrap.active{
                    padding: 0 20px 35px;
                    opacity: 1;
                    z-index: 0;
                }
                .selected-dealer__wrap #selected-dealer {
                    border-top: 1px solid #505050;
                    padding: 0;
                    font-size: 14px;
                }
                .selected-dealer__wrap.active #selected-dealer {
                    border-top: 0 none;
                    padding-top: 20px;
                    font-size: 14px;
                }
                #selected-dealer > span,
                #selected-dealer > span > a {
                    display: block;
                    font-size: 12px;
                    font-family: "GenesisSansText";
                    color: rgb(117, 117, 117);
                    line-height: 15px;
                    margin-bottom: 6px;
                    font-size: 14px;
                }
                #selected-dealer > span > a {
                    display: block;
                    font-size: 14px;
                    font-family: "GenesisSansText";
                    color: rgb(117, 117, 117);
                    line-height: 15px;
                    margin-bottom: 0px;
                    font-size: 14px;
                }
                #selected-dealer > a {
                    display: block;
                    font-family: "GenesisSansText";
                    color: #9A5341;
                    line-height: 15px;
                    margin-bottom: 0px;
                    font-size: 14px;
                    font-weight: 500;
                }
                #selected-dealer > a:hover {
                    display: block;
                    font-size: 14px;
                    font-family: "GenesisSansText";
                    color: white;
                    line-height: 15px;
                    margin-bottom: 0px;
                    font-size: 14px !important;
                }


                /*
		next styles
		*/
                .show-map {
                    /*display: none;*/
                    font-size: 14px;
                    margin-left: 21px;
                    position: relative;
                    text-decoration: none !important;
                }
                a.show-map{
                    display: none;
                }
                .car-about {
                    /*background: transparent url("../img/offers/hpromise/hp_elements/hp_icon_link.png") no-repeat scroll 140px 6px;*/
                    color: #9A5341;
                    font-family: "GenesisSansText",GenesisSansText,sans-serif;
                    font-size: 16px;
                    margin-top: 15px;
                    padding-right: 15px;
                    position: relative;
                    text-decoration: none !important;
                }
                .car-about.underlined::after {
                    width: 90%;
                }
                .currentCarImg__wrap {
                    height: 197px;
                    margin-top: 15px;
                    width: 290px;
                }
                .currentCarImg__wrap > .currentCarImg {
                    left: -50%;
                    margin-left: 111px;
                    margin-top: -75px;
                    position: relative;
                    top: 50%;
                }

                /*	PERSONAL BLOCK	*/
                .selectMr__wrap {
                    height: 45px;
                    width: 290px;
                    margin-bottom: 17px;
                    /*color: #9A5341;*/
                    /*background-color: #9A5341;*/
					position: relative;
                }
                /* ie8 */
                .selectMr__option {
                    vertical-align: top;
                    margin: 0 3px 0 0;
                    width: 17px;
                    height: 17px;
                }
                .selectMr__option + label {
                    cursor: pointer;
                }

                /* modern */
                .selectMr__option:not(checked) {
                    position: absolute;
                    opacity: 0;
                }
                .selectMr__option:not(checked) + label {
                    color: #fff;
                    display: inline-block;
                    font-family: "GenesisSansText";
                    font-size: 15px;
                    line-height: 45px;
                    padding: 0 0 0 45px;
                    position: relative;
                }
                .selectMr__wrap > label:last-of-type {
                    float: right;
                    padding-right: 10px;
                }
                .selectMr__option:not(checked) + label::before {
                    background: transparent none repeat scroll 0 0;
                    border: 1px solid #505050;
                    border-radius: 18px;
                    box-sizing: border-box;
                    content: "";
                    height: 35px;
                    left: 0;
                    position: absolute;
                    top: 5px;
                    transition: border-color 200ms ease 0s;
                    width: 35px;
                }
                .selectMr__option:not(checked) + label::after {
                    background: #9A5341 none repeat scroll 0 0;
                    border-radius: 5px;
                    content: "";
                    height: 9px;
                    left: 13px;
                    opacity: 0;
                    position: absolute;
                    top: 18px;
                    transition: opacity 200ms ease 0s;
                    width: 9px;
                }
                .selectMr__option:checked + label::after {
                    opacity: 1;
                }
                .selectMr__option:hover + label::before {
                    border: 1px solid #fff;
                    background: #202020;
                }
                .controls__wrap {
                    margin-bottom: 17px;
                    position: relative;
                }
                .controls__wrap input {
                    padding-left: 20px;
                    box-sizing: border-box;
                    height: 45px;
                    width: 290px;
                    color: #fff;
                    border: 1px solid #505050;
                    font-family: "GenesisSansText";
                    font-size: 15px;
                    background: transparent;
                    position: relative;
                    -ms-transition: background-color 200ms, border-color 200ms, color 200ms;
                    -webkit-transition: background-color 200ms, border-color 200ms, color 200ms;
                    -moz-transition: background-color 200ms, border-color 200ms, color 200ms;
                    -o-transition: background-color 200ms, border-color 200ms, color 200ms;
                    transition: background-color 200ms, border-color 200ms, color 200ms;
                }
                .controls__wrap input.inactive {
                    color: #757575;
                }
                /* I KNOW THAT FF DOESNT SOPPORT THIS PLACEHOLDER'S TRANSITIONS.
		   I TRIED.. HOPE IT WILL HELPS IN THE FUTURE.
		*/
                .controls__wrap input::-webkit-input-placeholder {color:#757575; opacity: 1; -webkit-transition: color 200ms ease 0s; -moz-transition: color 200ms ease 0s; -o-transition: color 200ms ease 0s; -ms-transition: color 200ms ease 0s; transition: color 200ms ease 0s;}
                .controls__wrap input::-moz-placeholder          {color:#757575; opacity: 1; -webkit-transition: color 200ms ease 0s; -moz-transition: color 200ms ease 0s; -o-transition: color 200ms ease 0s; -ms-transition: color 200ms ease 0s; transition: color 200ms ease 0s;}/* Firefox 19+ */
                .controls__wrap input:-moz-placeholder           {color:#757575; opacity: 1; -webkit-transition: color 200ms ease 0s; -moz-transition: color 200ms ease 0s; -o-transition: color 200ms ease 0s; -ms-transition: color 200ms ease 0s; transition: color 200ms ease 0s;}/* Firefox 18- */
                .controls__wrap input:-ms-input-placeholder      {color:#757575; opacity: 1; -webkit-transition: color 200ms ease 0s; -moz-transition: color 200ms ease 0s; -o-transition: color 200ms ease 0s; -ms-transition: color 200ms ease 0s; transition: color 200ms ease 0s;}

                .controls__wrap input:hover::-webkit-input-placeholder {color: #ababab; opacity: 1;}
                .controls__wrap input:hover::-moz-placeholder          {color: #ababab; opacity: 1;}/* Firefox 19+ */
                .controls__wrap input:hover:-moz-placeholder           {color: #ababab; opacity: 1;}/* Firefox 18- */
                .controls__wrap input:hover:-ms-input-placeholder      {color: #ababab; opacity: 1;}

                .controls__wrap input:focus::-webkit-input-placeholder {color: #fff; opacity: 1;}
                .controls__wrap input:focus::-moz-placeholder          {color: #fff; opacity: 1;}/* Firefox 19+ */
                .controls__wrap input:focus:-moz-placeholder           {color: #fff; opacity: 1;}/* Firefox 18- */
                .controls__wrap input:focus:-ms-input-placeholder      {color: #fff; opacity: 1;}

                .controls__wrap input:hover, .controls__wrap input:focus {
                    border: 1px solid #fff;
                    background: #202020;
                    outline: none;
                }
                .controls__wrap input:focus {
                    color: #fff;
                }
                /*---------------------------------------------------------------------*/
                /* INPUT ERRORS */
                .controls__wrap.incorrect input {
                    border: 1px solid #9A5341;
                    /*background: transparent url("images/icons/form-incorrect-ico.png") no-repeat scroll 259px 12px;*/
                }
                .controls__wrap.incorrect input::-webkit-input-placeholder{ color: #9A5341 }
                .controls__wrap.incorrect input::-moz-placeholder{ color: #9A5341 }
                .controls__wrap.incorrect input:-moz-placeholder{ color: #9A5341 }
                .controls__wrap.incorrect input:-ms-input-placeholder{ color: #9A5341 }



                /*.redPlace::-webkit-input-placeholder{*/
                /*color: #9A5341;*/
                /*}*/
                /*.controls__wrap.incorrect::before {*/
                /*background-color: #9A5341;*/
                /*bottom: 55px;*/
                /*box-sizing: content-box;*/
                /*color: #fff;*/
                /*content: "Пожалуйста, введите \a корректные данные.";*/
                /*font-family: "ModernHBold";*/
                /*font-size: 14px;*/
                /*margin-right: -76px;*/
                /*opacity: 0;*/
                /*padding: 10px 20px;*/
                /*position: absolute;*/
                /*z-index: 800;*/
                /*right: 15px;*/
                /*text-align: center;*/
                /*transform: translateY(12px);*/
                /*transition: opacity 200ms ease 0s, transform 200ms ease 0s;*/
                /*white-space: pre;*/
                /*width: 140px;*/
                /*}*/
                /*.controls__wrap.incorrect::after {*/
                /*border-color: #9A5341 transparent transparent;*/
                /*border-style: solid;*/
                /*border-width: 6px;*/
                /*bottom: 45px;*/
                /*content: "";*/
                /*display: block;*/
                /*position: absolute;*/
                /*right: 15px;*/
                /*transform: translateY(12px);*/
                /*opacity: 0;*/
                /*transition: opacity 200ms, transform 200ms;*/
                /*}*/
                .controls__wrap.incorrect:hover::before {
                    opacity: 1;
                    transform: translateY(0);
                }
                .controls__wrap.incorrect:hover::after {
                    opacity: 1;
                    transform: translateY(0);
                }
                /* DEALER ERROR */
                .dealerlistdropdown.incorrect {
                    z-index: 5;
                }
                .controls__wrap.incorrect input {
                    border: 1px solid #9A5341;
                }
                .dealerlistdropdown.incorrect .drop-it-down {
                    border-color: #9A5341;
                    background: rgba(32, 32, 32, 0.98) url("images/icons/form-incorrect-ico.png") no-repeat scroll 88% 12px
                }
                .incorrect:after {
                    border-color: #9A5341;
                    background: rgba(32, 32, 32, 0.98) url("images/icons/form-incorrect-ico.png") no-repeat scroll 88% 12px
                }
                .dealerlistdropdown.incorrect:hover .drop-it-down {
                    border-color: #9A5341;
                    background: rgba(32, 32, 32, 0.98) url("images/icons/form-incorrect-ico.png") no-repeat scroll 88% -69px
                }
                .dealerlistdropdown.incorrect::before {
                    background-color: #9A5341;
                    bottom: 55px;
                    box-sizing: content-box;
                    color: #fff;
                    content: "Пожалуйста, введите \a корректные данные.";
                    font-family: GenesisSansText !important;
                    font-size: 14px;
                    margin-right: -76px;
                    opacity: 0;
                    padding: 10px 20px;
                    position: absolute;
                    z-index: 800;
                    right: 15px;
                    text-align: center;
                    transform: translateY(12px);
                    transition: opacity 200ms ease 0s, transform 200ms ease 0s;
                    white-space: pre;
                    width: 140px;

                    display: none;
                }
                .dealerlistdropdown.incorrect::after {
                    border-color: #9A5341 transparent transparent;
                    border-style: solid;
                    border-width: 6px;
                    bottom: 43px;
                    /*content: url("images/icons/form-incorrect-ico.png");*/
                    display: block;
                    opacity: 0;
                    position: absolute;
                    right: 21px;
                    transform: translateY(12px);
                    transition: opacity 200ms ease 0s, transform 200ms ease 0s;

                    /*border-color: #9A5341;*/
                    /*background: rgba(32, 32, 32, 0.98) url("images/icons/form-incorrect-ico.png") no-repeat scroll 220px 12px*/
                }
                .dealerlistdropdown.incorrect:hover::before {
                    opacity: 1;
                    transform: translateY(0);
                }
                .dealerlistdropdown.incorrect:hover::after {
                    opacity: 1;
                    transform: translateY(0);
                }
                /* AGREE ERROR */
                .legal-info.incorrect {
                    z-index: 50;
                }
                .legal-info.incorrect .drop-it-down {
                    border-color: #9A5341;
                }
                .legal-info.incorrect::before {
                    opacity: 1;
                    transform: translateY(0);
                }
                .legal-info.incorrect::after {
                    opacity: 1;
                    transform: translateY(0);
                }
                .legal-info::before {
                    background-color: #9A5341;
                    bottom: 62px;
                    color: #fff;
                    content: "Пожалуйста, \a отметьте пункт.";
                    font-family: GenesisSansText !important;
                    font-size: 14px;
                    left: -86px;
                    padding: 20px 15px;
                    position: absolute;
                    text-align: center;
                    opacity: 0;
                    transform: translateY(12px);
                    white-space: pre;
                    width: 180px;
                    transition: opacity 200ms ease 0s, transform 200ms ease 0s;
                    box-sizing: content-box;
                }
                .legal-info::after {
                    border-color: #9A5341 transparent transparent;
                    border-style: solid;
                    border-width: 6px;
                    bottom: 50px;
                    content: "";
                    display: block;
                    left: 15px;
                    opacity: 0;
                    position: absolute;
                    transform: translateY(12px);
                    transition: opacity 200ms ease 0s, transform 200ms ease 0s;
                }
                .legal-info.incorrect .agree_rules:not(checked) + label::before{
                    border-color: #9A5341;
                }
                .legal-info.incorrect:hover .agree_rules:not(checked) + label::before{
                    /*border-color: #9A5341;*/
                }
                /*---------------------------------------------------------------------*/

                .show-legal {
                    bottom: 8px;
                    display: block;
                    font-size: 14px;
                    left: 57px;
                    position: relative;
                    text-decoration: none !important;
                    width: 124px;
                    white-space: nowrap;
                }
                .legal-info {
                    position: relative;
                    margin-bottom: 25px;
                }
                .info-gather__body-footer {
                    padding-bottom: 50px;
                }

                .agree_rules:not(checked) {
                    position: absolute;
                    opacity: 0;
                }
                .agree_rules:not(checked) + label {
                    color: #fff;
                    display: inline-block;
                    font-family: GenesisSansText !important;
                    font-size: 14px;
                    font-weight: normal;
                    line-height: 30px;
                    margin-right: 50px;
                    padding: 0 0 0 58px;
                    position: relative;
                    cursor: pointer;
                }
                .agree_rules:not(checked) + label::before {
                    background: transparent;
                    border: 1px solid #505050;
                    box-sizing: border-box;
                    content: "";
                    height: 40px;
                    left: 0;
                    position: absolute;
                    top: 5px;
                    transition: border-color 200ms ease 0s;
                    width: 40px;
                }
                .agree_rules:not(checked) + label::after {
                    background: transparent url("images/icons/checked.png") no-repeat scroll 15px 18px;
                    content: "";
                    height: 40px;
                    left: 0;
                    opacity: 0;
                    position: absolute;
                    top: 0;
                    transition: opacity 200ms ease 0s;
                    width: 40px;
                }
                .agree_rules:checked + label::after {
                    opacity: 1;
                }
                .agree_rules:hover + label::before {
                    border: 1px solid #fff;
                    background: #202020;
                }
                .controls__wrap {
                    margin-bottom: 17px;
                }
                .send-req {
                    background: #fff;
                    color: #9A5341;
                    cursor: pointer;
                    font-family: "GenesisSansText";
                    font-size: 12px;
                    font-weight: 900;
                    padding: 18px 0 14px;
                    text-align: center;
                    text-transform: uppercase;
                    transition: background 200ms ease 0s;
                    width: 290px;
                }
                .send-req:hover {
                    background: #9A5341;
                    color: #fff;
                }
                /*
		POP UP
		*/
                .gm-style .gm-style-iw__testdrive {
                    width: 250px !important;
                    padding: 30px !important;
                    box-sizing: border-box;
                }
                .pop_text__test-drive > span {
                    color: #959595;
                }
                /*
		MAP and LEGAL POPUP
		*/
                .test-drive-popup {
                    background: #fff;
                    position: absolute;
                    width: 940px;
                    height: 550px;
                    top: 50%;
                    left: 0;
                    right: 0;
                    margin: -275px auto 0;
                    z-index: 150;
                    box-sizing: border-box;
                }
                .test-drive-popup__map #map_canvas {
                    height: 550px;
                }
                .test-drive-popup__legal {
                    padding: 45px 55px;
                }
                .test-drive-popup__legal h2 {
                    font-family: "ModernHBold";
                    font-size: 30px;
                    line-height: 1;
                    margin-bottom: 25px;
                }
                .test-drive-popup__legal .offers__text{
                    font-size: 13px;
                    font-family: "GenesisSansText", sans-serif;
                    width: 380px;
                    color: #505050;
                    line-height: 1;
                    line-height: 1.385;
                    text-align: left;
                }
                .test-drive-popup__legal .offers__text strong{
                    font-weight: 800;
                }
                .test-drive-popup__legal .offers__text:last-child{
                    margin-left: 70px;
                }
                .closeIt {
                    background: #9a9a9a none repeat scroll 0 0;
                    box-sizing: border-box;
                    color: #fff;
                    cursor: pointer;
                    font-size: 32px;
                    height: 48px;
                    padding: 0 0 0 15px;
                    position: absolute;
                    right: 0;
                    top: 0;
                    width: 48px;
                    z-index: 155;
                    transition: background 200ms;
                    -webkit-transition: background 200ms;
                }
                .closeIt:hover {
                    background: #9A5341;
                }
                #test-drive-response {
                    background: url('images/icons/check.jpg') no-repeat center 45px #fff;
                    bottom: 0;
                    box-sizing: border-box;
                    height: 260px;
                    left: 0;
                    margin: auto;
                    padding: 80px 0 40px;
                    position: absolute;
                    right: 0;
                    top: 0;
                    width: 465px;
                    z-index: 150;
                }
                #test-drive-response h2 {
                    font-family: "ModernH";
                    font-size: 25px;
                    font-weight: bold;
                    line-height: 40px;
                    margin-bottom: 5px;
                    text-align: center;
                }
                #test-drive-response .text {
                    color: #7e7e7e;
                    line-height: 24px;
                    text-align: center;
                    display: none;
                }
                .overlayed {
                    display: none;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    background: rgba(0,0,0,0.8);
                    position: fixed;
                    z-index: 200;
                }
                /*
		.overlayed.map-wrap {
			left: -999999px;
			display: block;
		}
		*/

                .layout-black {
                    background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
                    border: 0 none;
                    display: none;
                    height: 100%;
                    left: 0;
                    position: fixed;
                    right: 0;
                    top: 0;
                    width: 100%;
                    z-index: 1000;
                }
                .progress-ring-wrap {
                    left: 50%;
                    margin-left: -100px;
                    margin-top: -100px;
                    opacity: 1;
                    top: 50%;
                    z-index: 10000;
                }
                .legal-info::before {
                    padding: 0 !important;
                }
                .progress-ring {
                    animation: 1.5s linear 0s normal none infinite running spin360;
                }
                @keyframes spin360 {
                    0% {
                        transform: rotate(0deg);
                    }
                    100% {
                        transform: rotate(360deg);
                    }
                }
                @keyframes spin360 {
                    0% {
                        transform: rotate(0deg);
                    }
                    100% {
                        transform: rotate(360deg);
                    }
                }
                @keyframes spin360 {
                    0% {
                        transform: rotate(0deg);
                    }
                    100% {
                        transform: rotate(360deg);
                    }
                }
                /*
		.info-gather__col__car {
			display: none;
		}
*/
                .dealer__location-dropdown,
                    /*.selectMr__wrap,*/
                .controls__wrap > input {
                    width: 100%;
                }
                .info-gather__col {
                    width:48% !important;
                }
                .dropdown-item,
                .first {
                    width: 100%;
                }
                /**{
                    font-family: GenesisSansText ;
                }*/
                .legal-info {
                    float:left;
                }
                .test_drive_car {
                    display: block;
                    margin: 20px auto;
                    width:50%;
                }
                .test-drive-main {
                    background-image: url("images/test_drive_back.jpg");
                    background-size: cover;
                }
                @media screen and (max-width: 573px){
                    .test-drive-main {
                        padding-top: 50px;
                    }
                }
                @media only screen and (min-width : 574px) and (max-width : 769px) {
                    .test-drive-main {
                        padding-top: 50px;
                    }
                }
                @media screen and (min-width: 1270px){
                    .test-drive-content {
                        padding-top: 0px !important;
                    }
                }

                .button--link-inverted {
                    border: none !important;
                }
                h2.skyblue {
                    margin: 0 !important;
                }

                .info-gather__col__personal.fleft {
                    display: block;
                    float:none;
                    margin: 0 auto 30px auto;
                    width: 70%;
                }

                .info-gather__body-footer .fleft {
                    display: block;
                    float:none;
                    margin: 0 auto 30px auto;
                    width: 48%;
					text-align: left;
                }

                @media screen and (max-width: 980px) {
                    .info-gather__col__dealer {
                        margin: 0 auto;
                        float: none;
                        width: 100%;
                    }
                    .info-gather__col__personal {
                        float: none;
                        margin: 0 auto;
                        width: 100%;
                    }
                    .info-gather__col:last-child {
                        margin: 0 auto;
                    }
                    .offers__container {
                        width: 100%;
                        margin: 0 auto;
                    }
                    .info-gather__col {
                        width:80% !important;
                    }
                    .offers__content-title {
                        text-align: center;
                        float:none;
                    }
                    .legal-info {
                        display: block;
                        float:none;
                        margin: 0 auto 30px auto;
                    }
                    .fright, .fleft {
                        /*float: left;*/
                        /*clear: left;*/
                        display: block;
                        float:none;
                        margin: 0 auto 30px auto;
                        width: 70%;
                    }
                    .info-gather__body-footer .fleft {
                        display: block;
                        float:none;
                        margin: 0 auto 30px auto;
                        width: 70%;
                    }
                    .footer-link__item {
                        width: 100%;
                        text-align: center;
                    }
                }
                .error_select {
                    width: 20px;
                    height:20px;
                    position: absolute;
                    /*background-image: url("images/icons/form-incorrect-ico.png");*/
                    background-repeat: no-repeat;
                    z-index: 55;
                    top:13px;
                    right:45px;
                }
                .error_select:hover {
                    width: 20px;
                    height:20px;
                    position: absolute;
                    /*background-image: url("images/icons/form-incorrect-ico.png");*/
                    background-repeat: no-repeat;
                    background-position: left bottom;
                    z-index: 55;
                    top:13px;
                    right:45px;
                }
                .arrow_select {
                    width: 20px;
                    height:20px;
                    position: absolute;
                    background-image: url("images/icons/dropdown-icon.png");
                    background-repeat: no-repeat;
                    background-position-y: -13px;
                    z-index: 56;
                    top:13px;
                    right:15px;
                }
                /*.arrow_select:after {*/
                /*content:'';*/
                /*width: 20px;*/
                /*height:20px;*/
                /*background-color: red;*/
                /*position: absolute;*/
                /*background-image: url("images/icons/dropdown-icon.png");*/
                /*background-repeat: no-repeat;*/
                /*background-position-y: -13px;*/
                /*z-index: 56;*/
                /*top:13px;*/
                /*right:15px;*/
                /*}*/
                /*.incorrect:after {*/
                /*content:'';*/
                /*width: 15px;*/
                /*height:15px;*/
                /*background-color: green;*/
                /*position: relative;*/
                /*top:15px;*/
                /*right:15px;*/
                /*}*/

                .info-gather__col__dealer {
                    display: none !important;
                }
                /*
		.info-gather__col__car {
			display: none !important;
		}
*/          a.genesis-btn.toggleForm { display: none !important}
            .phone { padding-top: 20px; }
            </style>

            <script type="text/html" id="dealerListTemp">
                <li class="dropdown-item" data-id="<%= id %>"><%= name %></li>
            </script>

            <script type="text/html" id="dealerTemplateInfo">
                <span class="dealer-address"><%= address %></span>
                <span class="dealer-phone"><%= phone %></span>
                <span class="dealer-site"><%= site %></span>
            </script>

            <script type="text/javascript">
				var currentModel = '';
            </script>

            <script type="text/javascript">
				var currentDealerName = '';
            </script>
            <script type="text/javascript">
				var currentDealerId = '';
            </script>
            <script type="text/javascript">
				var currentDealerCity = '';
            </script>

        <!-- configuring main part -->
        <div class="offers__main test-drive-main ">
            <div class="offers__container test-drive-content">
                <h1 class="offers__content-title skyblue" style="font-family: 'GenesisSansTextKROTFRegular'">ЗАПИСЬ НА ТЕСТ-ДРАЙВ</h1>
                <img src="images/test_drive_car.png" class="test_drive_car" alt="">
                <div class="info-gather__body clearfix">
                    <div class="info-gather__col info-gather__col__car fleft">

                    </div>
                    <div class="info-gather__col info-gather__col__dealer fleft">
                        <h2 class="info-gather__col__title">Выбор дилера</h2>
                        <div class="dealercitylistdropdown">
                            <!--<div class="error_select" alt=""></div>-->
                            <div id="dropdown-dealercitylisting" class="dealer__location-dropdown drop-it-down" tabindex="0">
                                <i class="arrow_select" alt=""></i>
                                <ul class="dropdown-list first"><li  data-value="" class="dropdown-item first">&nbsp;</li></ul>
                                <div class="nano" id="citiesNano">
                                    <ul class="dropdown-list nano-content" id="cities">
                                        <!-- CITIES -->
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- try to insert dealers after selecting sity -->
                        <div class="dealerlistdropdown">
                            <div id="dropdown-dealerlisting" class="dealer__location-dropdown drop-it-down" tabindex="0">
                                <i class="arrow_select" alt=""></i>
                                <ul class="dropdown-list first"><li  data-value="" class="dropdown-item first">Выберите дилера</li></ul>
                                <div class="nano" id="dealersNano">
                                    <ul class="dropdown-list nano-content" id="dealers">
                                        <!-- DEALERS -->
                                    </ul>
                                </div>
                            </div>
                        </div><!-- //dealers -->
                        <div class="selected-dealer__wrap">
                            <div id="selected-dealer"></div>
                        </div>
                        <a class="show-map skyblue dashedskyblue" href="#">Показать на карте</a>
                    </div>
                    <div class="info-gather__col info-gather__col__personal fleft">
                        <h2 class="info-gather__col__title">Автомобиль</h2>
                        <!-- cars -->
                        <div class="carlistdropdown">
                            <div id="dropdown-carlisting" class="dealer__location-dropdown drop-it-down" tabindex="0">
                                <i class="arrow_select" alt=""></i>
                                <ul class="dropdown-list first">
                                    <li  data-value="" class="dropdown-item first first-active">Выберите автомобиль</li>
                                </ul>
                                <div class="nano nanocar">
                                    <ul class="dropdown-list">
                                        <li data-model="G70" model="G70" class="dropdown-item">Genesis G70</li>
                                        <li data-model="G80" model="G80" class="dropdown-item">Genesis G80</li>
                                        <li data-model="G90" model="G90" class="dropdown-item">Genesis G90</li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                        <h2 class="info-gather__col__title">Ваши данные</h2>

                        <div class="selectMr__wrap">
                            <input type="radio" class="selectMr__option" name="mrOrms" value="Mr." id="Mr." checked="true"><label for="Mr.">Уважаемый</label>
                            <input type="radio" class="selectMr__option" name="mrOrms" value="Ms." id="Ms."><label for="Ms.">Уважаемая</label>
                        </div>

                        <div class="controls__wrap redPlace" data-content="Пожалуйста, &#xa; введите Имя.">
                            <input type="text" name="name" id="name" placeholder="Имя">
                        </div>
                        <div class="controls__wrap redPlace" data-content="Пожалуйста, &#xa; введите Фамилию.">
                            <input type="text" name="surname" id="surname" placeholder="Фамилия">
                        </div>
                        <div class="controls__wrap redPlace" data-content="Пожалуйста, &#xa; введите телефон">
                            <input type="text" name="phone" id="phone" maxlength="16" placeholder="+7 (___)___-__-__">
                        </div>
                        <div class="controls__wrap redPlace" data-content="Введен &#xa; некорректный E-mail.">
                            <input type="email" name="email" id="email" placeholder="E-mail"><div id="err" style="display:none">Введен некорректный E-mail</div>
                        </div>
                    </div>
                </div><!-- //info-gather__body -->
                <div class="info-gather__body-footer clearfix">
                    <div class="legal-info fleft">
                        <input type="checkbox" class="agree_rules" id="agree_rules"><label for="agree_rules">Я согласен на обработку данных</label>
                        <a href="#" class="show-legal skyblue dashedskyblue">Смотреть правила</a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="send-req fleft" style="text-align: center;">Отправить заявку</div>
                </div>

                <noindex>
                    <div class="overlayed legal-wrap">
                        <div class="test-drive-popup test-drive-popup__legal">
                            <div class="closeIt">&times;</div>
                            <h2 class="skyblue">Правила обработки<br>персональных данных</h2>
                            <div class="clearfix legal_info_">
                                <p class="offers__text fleft">
                                    Настоящим Я, в соответствии с требованиями Федерального закона от 27.07.09 №152-ФЗ «О персональных данных» даю свое согласие лично, своей волей и в своем интересе
                                    на обработку (сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование, распространение, передачу (включая трансграничную передачу), обезличивание, блокирование и уничтожение) моих персональных данных, в том числе с использованием средств автоматизации.
                                    <br><br>
                                    Такое согласие мною даётся в отношении следующих персональных данных: фамилии, имя, отчество; контактный адрес электронной почты (e-mail); контактный телефон;
                                    для определения потребностей в производственной мощности, мониторинга исполнения сервисными центрами гарантийной политики; ведения истории обращения в сервисные центры; проведения дилерами, дистрибьюторами, контрагентами маркетинговых исследований в области продаж, сервиса и послепродажного обслуживания;
                                    для рекламных, исследовательских, информационных,
                                </p>
                                <p class="offers__text fleft">
                                    а также иных целей, в том числе, путем осуществления
                                    со мной прямых контактов по различным средствам связи.
                                    <br><br>
                                    Согласие дается Hyundai Motor Company (Хёндэ Мотор Компани, 231 Янгджи-Донг, Сеочо-Гу, Сеул, 137-938, Республика Корея), ООО «Хендэ Мотор СНГ» (г. Москва, ул.Тестовская, д.10), ООО «Хендэ Мотор МануфактурингPус» (197706, Санкт-Петербург, г. Сестрорецк, Левашовское ш.,
                                    д. 20, литер А).
                                    <br><br>
                                    Я даю свое согласие передавать мои персональные данные для обработки исследовательским агентствам: ООО «Международный институт маркетинговых и социальных исследований «ГФК-Русь» (г. Москва, 9-я Парковая улица,
                                    д. 48, корп. 4), ЗАО «Бизнес Аналитика МК» (г. Москва,
                                    ул. Новослободская, д. 31, стр. 2); ЗАО «АвтоАссистанс»
                                    (г. Москва, 2-й Южнопортовый проезд, д.18 корп. 2),
                                    ООО «Ипсос» (г. Москва, Гамсоновскийпереулок, д. 5),
                                    а также любым другим третьим лицам, для целей, указанных в настоящем согласии.
                                </p>
                            </div>
                            <!--<div class="closeIt">&times;</div>-->
                        </div>
                    </div>
                </noindex>

                <!-- req response -->

                <div class="overlayed response-wrap">
                    <div id="test-drive-response">
                        <h2></h2>
                        <p class="text">В ближайшее время с вами свяжется менеджер<br/>для уточнения информации</p>
                        <div class="closeIt">&times;</div>
                    </div>
                </div>

            </div><!-- offers__main test-drive-main-->


        </div><!-- offers__container test-drive-content -->

    </div>
<div class="top-btn">
    Наверх
</div>
<div class="mobileDetector" id="mobileDetector"></div>
<script src="/js/build.js" charset="utf-8"></script>
<!-- Add scripts here if needed custom for a page as as as-->
<script type="text/javascript" src="/js/dealers.js"></script>
<script type="text/javascript" src="/js/test-drive-redesign.js"></script>
<script type="text/javascript" src="/js/underscore.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.min.js"></script> 
<script src="/scripts/header-inner/common.js"></script>
<script src="/scripts/header-inner/App.js"></script>
<script src="/scripts/header-inner/App.navigation.js"></script>
<script src="/scripts/header-inner/App.navigation-mob.js"></script>