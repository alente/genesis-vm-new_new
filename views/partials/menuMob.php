<!-- Navigation Mobile -->
<div class="common-menu2">
	<div class="inner-wrap clearfix">
		<div style="position: fixed; z-index: 2; top: 3px!important; right: 80px!important;  color: #ccc;  font-size: 12px;  text-align: center; font-family: 'GenesisSansHead';" itemscope="" itemtype="http://schema.org/Organization">
			<a href="tel:+73422055499" itemprop="telephone">+7 (342) 205-54-99</a>
			<span itemprop="name" style="display: none;">Genesis-центр</span>
			<div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress"  style="color: #fff;">
				«ВОСТОК МОТОРС»<br><span itemprop="addressLocality"  style="color: #fff;">г. Пермь</span>, <span itemprop="streetAddress" style="color: #fff;">ш. Космонавтов, 316В</span>
			</div>
		</div>
		<strong class="logo">
			<a href="/"><span><img src="/images/common/logo_header.png" alt="Genesis"/></span></a>
		</strong>
		<div class="right-menus">
			<a href="#" class="btn-open-menu2">
				<span>menu</span>
			</a>
   
			<nav class="gnb-menu">
				<div class="gnb-background"></div>
				<div class="cont-top">
					<div class="input-wrap">
                        <form action="/search" id="gnbSearchForm" name="gnbSearchForm" method="get">
                            <label class="acc-tag" for="sch-input">Поиск</label>
                            <input type="text" name="q" id="sch-input" placeholder="ПОИСК" maxlength="16">
                            <img src="/images/common/bg_search-new.png" alt=""/>
                            <a href="javascript:void(0);" class="btn-search" onclick="document.forms[ 'gnbSearchForm' ].submit();"><span></span>Поиск</a>
                        </form>
						
					</div>
				</div>
    
				<div class="cont-btns">
					<div class="cont-mid">
						<ul class="nav-high-priority">
							<li class="hoverable brand">
								<a href="javascript:void(0);">
									Бренд
									<span class="arr-con"></span>
								</a>
								<div class="sub-menu">
									<ul class="depth-2">
										<li><a href="/brand.html">О бренде</a></li>
										<li><a href="/news">Новости и мероприятия</a></li>
									</ul>
								</div>
							</li>
							<li class="hoverable models">
								<a href="javascript:void(0);">
									Модели
									<span class="arr-con"></span>
								</a>
								<div class="sub-menu">
									<ul class="depth-2">
										<li>
											<a href="/g70.html">
												<strong>g70</strong>
												<img src="/images/gatemain/genesis-g70-main.png" alt="g70" />
											</a>
										</li>
										<li>
											<a href="/g80.html">
												<strong>g80</strong>
												<img src="/images/gatemain/genesis-g80-main.png" alt="g80" />
											</a>
										</li>
										<li>
											<a href="/g90.html">
												<strong>g90</strong>
												<img src="/images/gatemain/genesis-g90-main.png" alt="g90" />
											</a>
										</li>
										<?php /*<li>
											<a href="/g90l.html">
												<strong>g90 L</strong>
												<img src="/images/gatemain/genesis-g90l-main.png" alt="g90L" />
											</a>
										</li>*/ ?>
									</ul>
								</div>
							</li>
							<li class="hoverable shopping">
								<a href="javascript:void(0);">
									Покупка
									<span class="arr-con"></span>
								</a>
								<div class="sub-menu">
									<ul class="depth-2">
										<li><a href="/configurator">Конфигуратор</a></li>
										<li><a href="/configurator?credit">Кредитный калькулятор</a></li>
										<li><a href="/genesis-finance.html">Genesis Finance</a></li>
										<li><a href="/test-drive">Запись на тест-драйв</a></li>
										<li><a href="/stock">Автомобили в наличии</a></li>
										<li><a href="/workers">Наша команда</a></li>
										<li><a href="/contacts">Контакты</a></li>
									</ul>
								</div>
							</li>
							<li class="hoverable owners">
								<a href="javascript:void(0);">
									Владельцам
									<span class="arr-con"></span>
								</a>
								<div class="sub-menu">
									<ul class="depth-2">
										<li><a href="/warranty.html">Гарантия</a></li>
										<li><a href="/owners.html">Программы для владельцев</a></li>
									</ul>
								</div>
							</li>
						
						</ul>
						
						<ul class="nav-low-priority">
							<li class="hoverable">
								<a href="javascript:void(0);">
									Быстрые ссылки
									<span class="arr-con"></span>
								</a>
								<div class="sub-menu">
									<ul class="depth-2">
										<li><a href="/configurator">Конфигуратор</a></li>
										<li><a href="/test-drive">Тест-драйв</a></li>
										<li><a href="/contacts">Контакты</a></li>
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</div>
</div>