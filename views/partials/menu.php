<span class="header-background"></span>
<!-- Navigation Desktop -->
<div class="common-menu2">
    <div class="inner-wrap clearfix">
        <strong class="logo">
            <a href="/"><img src="/images/common/logo_header.png" alt="Genesis"/></a>
        </strong>
        <nav class="gnb-menu">
            <div class="cont-mid">
			<style>
				@media (max-width: 767px) {
				  .hidden-xs {
					display: none !important;
				  }
				}
				@media (min-width: 768px) and (max-width: 991px) {
				  .hidden-sm {
					display: none !important;
				  }
				}
			</style>
			<div class="hidden-xs hidden-sm" style="position:fixed;z-index:2;top:2px!important;right:50px!important;text-align:right;color:#ccc;font-size:12px;width:auto!important" itemscope="" itemtype="http://schema.org/Organization">
					<span itemprop="name" style="display: none;">Genesis-центр</span>
					<div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress"  style="color: #fff;">
						«ВОСТОК МОТОРС» — <span itemprop="addressLocality" style="color: #fff;">г. Пермь</span>, <span itemprop="streetAddress"  style="color: #fff;">ш. Космонавтов, 316В</span>
					</div>
				</div>
				
			<?php /*<div itemscope="" itemtype="http://schema.org/Organization" style="position:fixed;z-index:2;top:2px!important;right:50px!important;text-align:right;color:#ccc;font-size:12px;width:600px!important">
					<a href="tel:+73422055499"><span style="color:#ccc" itemprop="telephone" class="callibri_phone">+7 (342) 205-54-99</span></a> «ВОСТОК МОТОРС» — <span itemprop="addressLocality" style="color:#ccc;">Г. ПЕРМЬ</span>, <span itemprop="streetAddress" style="color:#ccc;">Ш. КОСМОНАВТОВ, 316В</span>
				</div>*/ ?>
                <span class="gnb-background"></span>
                <ul class="nav-high-priority">
                    <li class="hoverable brand">
                        <a href="javascript:void(0);">Бренд</a>
                        <div class="sub-menu">
                            <ul class="depth-2">
                                <li><a href="/brand.html">О бренде</a></li>
                                <li><a href="/news">Новости и мероприятия</a></li>
                            </ul>
                        </div>
                    </li>
                    
                    <li class="hoverable models">
                        <a href="javascript:void(0);">Модели</a>
                        <div class="sub-menu">
                            <ul class="depth-2">
								<li>
									<a href="/g70.html">
										<strong>g70</strong>
										<span class="figure"><img src="/images/desktop/common/genesis-g70-main.png" alt="g70" /></span>
									</a>
								</li>
                                <li>
                                    <a href="/g80.html">
                                        <strong>g80</strong>
										<span class="figure"><img src="/images/gatemain/genesis-g80-main.png" alt="g80" /></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/g90.html">
                                        <strong>g90</strong>
										<span class="figure"><img src="/images/gatemain/genesis-g90-main.png" alt="g90" /></span>
                                    </a>
                                </li>
                               <?php /* <li>
                                    <a href="/g90l.html">
                                        <strong>g90 L</strong>
										<span class="figure"><img src="/images/gatemain/genesis-g90l-main.png" alt="g90L" /></span>
                                    </a>
                                </li>*/ ?>
                            </ul>
                        </div>
                    </li>
                    <li class="hoverable shopping">
                        <a href="javascript:void(0);">Покупка</a>
                        <div class="sub-menu">
                            <ul class="depth-2">
                                <li><a href="/configurator">Конфигуратор</a></li>
                                <li><a href="/configurator?credit">Кредитный калькулятор</a></li>
                                <li><a href="/genesis-finance.html">Genesis Finance</a></li>
                                <li><a href="/workers">Наша команда</a></li>
                            </ul>
							<ul class="depth-2">
								<li><a href="/test-drive">Запись на тест-драйв</a></li>
								<li><a href="/contacts">Контакты</a></li>
								<li><a href="/trade-in.html">Trade-In</a></li>
								<li><a href="/stock">Автомобили в наличии</a></li>
							</ul>
                        </div>
                    </li>
                    <li class="hoverable membership">
                        <a href="javascript:void(0);">Владельцам</a>
                        <div class="sub-menu">
                            <ul class="depth-2">
                                <li><a href="/warranty.html">Гарантия и руководства</a></li>
                                <li><a href="/owners.html">Программы для владельцев</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="right-menus">
		<div class="dealercontact" style="margin-right: 10px;margin-top: 10px;">
		        <a href="tel:+73422055499" class="phone callibri_phone header-phone" itemprop="telephone" style="display: block;font-size: 22px !important;color: #fff !important; font-family: GenesisSansHead,sans-serif; font-weight: 100;position: relative; z-index: 1000000;">+7 (342) 205-54-99</a>
		        <a href="#" class="genesis-btn toggleForm" style="border: 1px #999 solid; font-size: 16px !important;margin-top: 5px;text-align: center;padding: 5px; position: relative; z-index: 1000000; color: #fff !important; display: block;">Заказать звонок</a> 
	        </div>
			
            <form action="/search" id="gnbSearchForm" name="gnbSearchForm" method="get">
                <fieldset>
                    <div class="input-wrap">
                        <label class="acc-tag" for="sch-input">Поиск</label>
                        <input type="text" name="q" id="sch-input" placeholder="ПОИСК" maxlength="16" value="">
                        <img src="/images/common/ico_search_w.png" alt="">
                        <a href="javascript:void(0);" class="btn-search" onclick="document.forms[ 'gnbSearchForm' ].submit();">
                            <span>Поиск</span>
                        </a>
                    </div>
                </fieldset>
            </form>
            
            <div class="quicklink-btns">
                <a href="javascript:void(0);">
                    <span>Быстрые ссылки</span>
                    <span class="arr"><img src="/images/common/arr_updown04.png" alt="Быстрые ссылки" /></span>
                </a>
                <ul>
                    <li><a href="/configurator">Конфигуратор</a></li>
                    <li><a href="/test-drive">Тест-драйв</a></li>
                    <li><a href="/contacts">Контакты</a></li>
                
                </ul>
            </div>
        
        </div>
    </div>
</div>