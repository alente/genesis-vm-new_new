<!-- Форма заказать звонок -->

<link type="text/css" rel="stylesheet" href="/styles/forma-call.css">

<div class="callback">
    <form class="callback__content CKiForm">
        <div class="callback__close"></div>
        <div class="info-gather__col info-gather__col__personal fleft">
            <h2 class="info-gather__col__title"></h2>
            <div class="selectMr__wrap">
              <input type="radio" class="selectMr__option" name="mrOrms" value="Mr." id="Mr." checked="true"><label for="Mr.">Уважаемый</label>
              <input type="radio" class="selectMr__option" name="mrOrms" value="Ms." id="Ms."><label for="Ms.">Уважаемая</label>
            </div>
            <div class="controls__wrap redPlace" data-content="Пожалуйста,введите Имя.">
                <input name="name_call" type="text" id="name_call" placeholder="Имя">
            </div>
            <div class="controls__wrap redPlace" data-content="Пожалуйста,введите Фамилию.">
                <input name="surname_call" type="text" id="surname_call" placeholder="Фамилия">
            </div>
            <div id="case1" class="controls__wrap redPlace" data-content="Пожалуйста,введите телефон">
                <input name="phone_call" type="tel" id="phone_call" maxlength="16" placeholder="+7 (___)___-__-__" class="">
            </div>
            <div id="case2" class="controls__wrap redPlace" data-content="Пожалуйста,введите email" style="display:none;">
                <input name="email_call" type="email" id="email_call" placeholder="Email" class="">
            </div>
            <div id="case3" class="controls__wrap redPlace" data-content="">
                <input name="vin" type="hidden" id="vin" value="" />
            </div>
        </div>
        <div class="info-gather__body-footer clearfix">
            <div class="legal-info stock fleft">
                <input type="checkbox" name="agreed" class="agree_rules" id="agree_rules"><label for="agree_rules">Я согласен на обработку данных</label>
                <a href="#" class="show-legal skyblue dashedskyblue rules-opener">Смотреть правила</a>
            </div>
            <div class="clearfix"></div>
            <button data-id="" class="send-req fleft CKFormTrigger">Отправить заявку</button>
        </div>
    </form>
</div>

<noindex>
    <div class="overlayed legal-wrap">
        <div class="test-drive-popup test-drive-popup__legal">
            <div class="closeIt">×</div>
            <h2 class="skyblue">Правила обработки<br>персональных данных</h2>
            <div class="clearfix legal_info_">
                <p class="offers__text fleft">
                    Настоящим Я, в соответствии с требованиями Федерального закона от 27.07.09 №152-ФЗ «О персональных данных» даю свое согласие лично, своей волей и в своем интересе
                    на обработку (сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование, распространение, передачу (включая трансграничную передачу), обезличивание, блокирование и уничтожение) моих персональных данных, в том числе с использованием средств автоматизации.
                    <br><br>
                    Такое согласие мною даётся в отношении следующих персональных данных: фамилии, имя, отчество; контактный адрес электронной почты (e-mail); контактный телефон;
                    для определения потребностей в производственной мощности, мониторинга исполнения сервисными центрами гарантийной политики; ведения истории обращения в сервисные центры; проведения дилерами, дистрибьюторами, контрагентами маркетинговых исследований в области продаж, сервиса и послепродажного обслуживания;
                    для рекламных, исследовательских, информационных,
                </p>
                <p class="offers__text fleft">
                    а также иных целей, в том числе, путем осуществления
                    со мной прямых контактов по различным средствам связи.
                    <br><br>
                    Согласие дается Hyundai Motor Company (Хёндэ Мотор Компани, 231 Янгджи-Донг, Сеочо-Гу, Сеул, 137-938, Республика Корея), ООО «Хендэ Мотор СНГ» (г. Москва, ул.Тестовская, д.10), ООО «Хендэ Мотор МануфактурингPус» (197706, Санкт-Петербург, г. Сестрорецк, Левашовское ш.,
                    д. 20, литер А).
                    <br><br>
                    Я даю свое согласие передавать мои персональные данные для обработки исследовательским агентствам: ООО «Международный институт маркетинговых и социальных исследований «ГФК-Русь» (г. Москва, 9-я Парковая улица,
                    д. 48, корп. 4), ЗАО «Бизнес Аналитика МК» (г. Москва,
                    ул. Новослободская, д. 31, стр. 2); ЗАО «АвтоАссистанс»
                    (г. Москва, 2-й Южнопортовый проезд, д.18 корп. 2),
                    ООО «Ипсос» (г. Москва, Гамсоновскийпереулок, д. 5),
                    а также любым другим третьим лицам, для целей, указанных в настоящем согласии.
                </p>
            </div>
            <!--<div class="closeIt">&times;</div>-->
        </div>
    </div>
</noindex>

<div class="success-msg">
    <div class="success-msg__close"></div>
    <h3>Заявка отправлена</h3>
    <p>Наши менеджеры свяжутся с вами в ближайшее время</p>
</div>


<!-- /Форма заказать звонок -->

<footer>
	<div class="inner-wrap">
		<div class="footer-share">
			<h3>GENESIS В СОЦИАЛЬНЫХ СЕТЯХ</h3>
			<ul>
				<li><a href="https://www.facebook.com/genesisrussia/" target="_blank"><i class="icon icon-facebook"></i></a></li>
				<li><a href="https://www.youtube.com/channel/UCVuL0DbEUznYLbGWxxsUlBA" target="_blank"><i class="icon icon-youtube"></i></a></li>
				<li><a href="https://www.instagram.com/genesis.perm/" target="_blank"><i class="icon icon-instagram"></i></a></li>
			</ul>
		</div>
		
		
		<div class="footer-top">
			<ul>
				<li><a href="/configurator" target="_self">Конфигуратор</a></li>
				<li><a href="/test-drive" target="_self">Тест-драйв</a></li>
				<li><a href="/contacts" target="_self">Контакты</a></li>
				<li><a href="/warranty.html" target="_self">Гарантия и руководства</a></li>
				<li><a href="/files/privacy.pdf" target="_blank">Конфиденциальность</a></li>
			</ul>
		</div>
		<div class="footer-bot">
			<div class="bot-content clearfix">
				<p class="txt-ww">
					<a href="https://www.genesis.com/worldwide/en/select-a-local-site.html" target="_blank" title="WORLDWIDE">
						WORLDWIDE
					</a>
					<a href="http://media.genesis.com/" target="_blank">
						MEDIA CENTER
					</a>
					<a href="/sitemap" target="_blank">
						КАРТА САЙТА
					</a>
				</p>
				</p>
				<p class="addres" style="margin-top: 0;">«ВОСТОК МОТОРС» г. Пермь <br><a href="tel:+73422055499">+7 (342) 205-54-99</a></p>
				<p class="copy" style="margin-top: 0;">© <?php echo date('Y') ?>, Хендэ Мотор СНГ. Все права защищены.</p>
			</div>
		</div>
		<style>
		
			footer .bot-content .txt-ww{
				width: 33.33333%;
				text-align: left;
			}
			footer .bot-content .addres{
				width: 33.3333%;
				float: left;
				padding: 15px;
				color: black;
				font-size: 18px;
				text-align: center;
			}
			footer .bot-content .addres a{
				color: #a36b4f;
				vertical-align: text-top;
				font-size: 14px;
			}
    @media (max-width: 1024px){
      footer .bot-content .addres{
      color: white;
    }
		@media (max-width: 767px){
			footer .bot-content .txt-ww{
				width: 100%;
				text-align: left;
			}
			footer .bot-content .txt-ww a{
				/*width: 33.33333%;*/
				text-align: left;
			}
		footer .bot-content .addres{
				width: 100%;
				float: none;
				padding: 15px;
				color: white;
				font-size: 18px;
				text-align: center;
        background-color: #303030;
			}
		}
		</style>
		<div class="footer__disclaimer">
			Вся представленная на сайте информация, касающаяся автомобилей и сервисного обслуживания, носит информационный характер и не является публичной офертой, определяемой положениями ст. 437 (2) ГК РФ. Все цены указанные на данном сайте носят информационный характер и являются максимально рекомендуемыми розничными ценами по расчетам дистрибьютора (ООО «Хендэ Мотор СНГ»). Для получения подробной информации просьба обращаться к ближайшему официальному дилеру ООО «Хендэ Мотор СНГ». Опубликованная на данном сайте информация может быть изменена в любое время без предварительного уведомления.
		</div>
	</div>
</footer>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47307309 = new Ya.Metrika({
                    id:47307309,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47307309" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter41418819 = new Ya.Metrika({
                    id:41418819,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WQ9SQMC');</script>
<!-- End Google Tag Manager -->

<script src="//cdn.callibri.ru/callibri.js" type="text/javascript" charset="utf-8"></script>

<script async src="//callkeeper.ru/w/?85ee78c8"></script>


<!-- StreamWood code -->
<link href="https://clients.streamwood.ru/StreamWood/sw.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://clients.streamwood.ru/StreamWood/sw.js" charset="utf-8"></script>
<script type="text/javascript">
  swQ(document).ready(function(){
    swQ().SW({
      swKey: '0734b7bca2320baabfd629d393e3a13d',
      swDomainKey: 'c2cf6949c591231c4afafa9691b8a649'
    });
    swQ('body').SW('load');
  });
</script>
<!-- /StreamWood code -->
