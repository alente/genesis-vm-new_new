<?
$this->title = "Comfort - Genesis ".$this->params['modelName'];

$mobilePrefix = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?'m-':null;
?>
<?if(!$mobilePrefix):?>
<div id="container" class="Eq900">
	<nav class="sec-indicator">
		<div class="nav-inner">
			<div class="gnb-area">
				<ul>
					<li><a href="javascript:void(0);" class="cur"><span>COMFORT</span></a></li>
					<li><a href="javascript:void(0);"><span>Real metal <br>interior material</span></a></li>
					<li><a href="javascript:void(0);"><span>Modern Ergo Seats</span></a></li>
					<li><a href="javascript:void(0);"><span>12.3″ HD <br>panoramic display</span></a></li>
					<!-- <li><a href="javascript:void(0);"><span>Smart posture <br>caring system</span></a></li> -->
					<li><a href="javascript:void(0);"><span>Head-up display</span></a></li>
					<li><a href="javascript:void(0);"><span>First class VIP seat</span></a></li>
					<li><a href="javascript:void(0);"><span>Rear seat <br>dual monitor</span></a></li>
					<li><a href="javascript:void(0);"><span>Semi-aniline <br>leather seats</span></a></li>
					<li><a href="javascript:void(0);"><span>One-touch posture <br>control system</span></a></li>
					<li><a href="javascript:void(0);"><span>Power leg support</span></a></li>
				</ul>
				<span class="line"></span>
			</div>
		</div>
	</nav>
	<div class="inner-container">
		<div class="btn-down-wrap visible">
			<a class="btn-back on up" href="<?=Yii::$app->homeUrl?>/<?=$this->params['model']?>#design">BACK <i></i></a>
			<a class="btn-down" href="#"><i></i> CLICK FOR MORE</a>
		</div>
		<!-- comfort -->
		<section class="section module-skin1 title-type1 color-type2">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">КОМФОРТ</h2>
					<p class="desc">КИЛОМЕТРЫ<br> УДОВОЛЬСТВИЯ</p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-comfort_intro.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- // comfort -->
		<!-- real metal -->
		<section class="section module-skin2 title-type1 color-type2 zoom-type1">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">КЛАССИКА И МЕТАЛЛ</h2>
					<p class="desc">Акустическая система Lexicon с 17 динамиками,<br> сабвуфером и линзой обеспечивает глубокий и богатый звук.<br> Решётки из нержавеющей стали помогают добиться объёмного<br> пространственного звучания и визуально дополняют композиции. </p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-real-metal_bg.jpg" alt="">
					</figure>
					<figure class="feature on">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-real-metal_detail.jpg" alt="">
					</figure>
				</div>
				<button class="btn-zoom" type="button">
					<span>상세보기</span>
				</button>
				<button class="btn-zoom-close" type="button">
					<span>상세보기 close</span>
				</button>
			</article>
		</section>
		<!-- // real metal -->
		<!-- modern ergo -->
		<section class="section module-skin2 title-type4 color-type2">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">ЭРГОНОМИЧНЫЕ СИДЕНЬЯ</h2>
					<p class="desc">Современные эргономичные передние и задние сиденья,<br> сертифицированные AGR (Aktion Gesunder Rucken, Германия),<br> регулируются в 22 и 14 направлениях соответственно<br> (включая наклон плечевой зоны, передний валик и подголовник) <br> и обеспечивают высочайший уровень комфорта для пассажира и водителя. </p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-modern-ergo_bg.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- // modern ergo -->
		<!-- resolution display -->
		<section class="section module-skin2 title-type4 color-type2 fade-type">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">ШИРОКОФОРМАТ&shy;НЫЙ TFT-ДИСПЛЕЙ 12.3″</h2>
					<p class="desc">12-дюймовый широкоформатный дисплей с высоким разрешением<br> обеспечивает исключительное качество изображения. Навигационная<br> и мультимедийная информация отображается в полноэкранном формате<br> или в формате разделённого экрана. </p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-resolution-display_bg.jpg" alt="">
					</figure>
					<figure class="feature on">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-resolution-display_bg_on.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- // resolution display -->
		<!-- smart -->
		<!-- <section class="section module-skin2 title-type5 color-type2">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">Smart posture caring system</h2>
					<p class="desc">When you enter your height, weight, and seated height on the cluster screen, the system automatically adjusts the seat, steering-wheel, outside mirrors, and head-up display to the optimal position for your body.</p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-smart_bg.jpg" alt="">
					</figure>
				</div>
			</article>
		</section> -->
		<!-- // smart -->
		<!-- haed up -->
		<section class="section layer-type module-skin4 opacity-on">
			<article class="feature">
				<ul class="layer-box cell-1">
					<li>
						<figure>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-headup_content.jpg" alt=""></span>
							<span class="on"><img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-headup_content_on.jpg" alt=""></span>
						</figure>
						<div class="brand-header">
							<h2 class="title">Проекция на лобовое стекло</h2>
							<p class="desc">Данные задних и боковых ультразвуковых датчиков, информация системы интеллектуального круиз-контроля, навигационная и другая информация проецируется на лобовое стекло.</p>
						</div>
					</li>
				</ul>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-smart_content.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- // haed up -->
		<!-- vip sit -->
		<section class="section module-skin2 title-type4 color-type2">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">ПУТЕШЕСТВИЕ ПЕРВЫМ КЛАССОМ</h2>
					<p class="desc">Эргономика салона первого класса в седане G90. <br> Сиденья предлагают исключительный уровень комфорта<br> как для пассажира, так и для водителя.</p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-vip-sit_bg.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- // vip sit -->
		<!-- back seat system -->
		<section class="section layer-type module-skin4 opacity-on">
			<article class="feature">
				<ul class="layer-box cell-3">
					<li>
						<figure>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-back-seat-system_content01.jpg" alt=""></span>
							<span class="on"><img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-back-seat-system_content01_on.jpg" alt=""></span>
						</figure>
						<div class="brand-header">
							<h2 class="title">Система мультимедиа для задних сидений</h2>
							<p class="desc">Просмотр и управление медиаконтентом может осуществляться с задних сидений на больших, размером 9,2 дюйма, мониторах с широким углом обзора.</p>
						</div>
					</li>
					<li>
						<figure>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-back-seat-system_content02.jpg" alt=""></span>
							<span class="on"><img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-back-seat-system_content02_on.jpg" alt=""></span>
						</figure>
						<div class="brand-header">
							<h2 class="title">Беспроводная зарядка для смартфона</h2>
							<p class="desc">Смартфоны* можно легко заряжать, положив их на специальную панель центральной консоли.</p>
							<p class="refer-text">*только для смартфонов с возможностью беспроводной зарядки.</p>
						</div>
					</li>
					<li>
						<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-back-seat-system_content03.jpg" alt=""></span>
						<div class="brand-header">
							<h2 class="title">Память положений сидений</h2>
							<p class="desc">Память положения сидений, которой оборудовано каждое сиденье, позволяет запоминать настройки кресла и восстанавливать предпочтительное положение простым нажатием кнопки.</p>
						</div>
					</li>
				</ul>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-vip-sit_bg.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- // back seat system -->
		<!-- back seat -->
		<section class="section module-skin2 title-type5 color-type2">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">СИДЕНИЯ ИЗ ПОЛУАНИЛИ&shy;НОВОЙ КОЖИ</h2>
					<p class="desc">В отделке салона используются только эксклюзивные материалы, точно подобранные друг к другу в едином стилистическом решении. Сиденья выполнены из полуанилиновой кожи тончайшей выделки, обладающей высоким тактильным эффектом.</p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-back-seat_bg.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- // back seat -->
		<!-- one touch -->
		<section class="section layer-type module-skin4">
			<article class="feature">
				<ul class="layer-box cell-2">
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-one-touch_content01.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">Управление одним касанием</h2>
							<p class="desc">Одним нажатием кнопки вы можете выбрать режимы расслабления, ТВ, чтения или возврат к базовым настройкам, которые обеспечат оптимальную позу в зависимости от выбранного положения. При выборе режима ТВ или чтения положение сиденья оптимально регулируется для просмотра фильмов и передач, или чтения книги. Режим отдыха обеспечивает идеальную атмосферу для расслабления, а режим возврата перемещает ваше кресло в базовое положение для удобного выхода из автомобиля или посадки в него.</p>
						</div>
					</li>
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-one-touch_content02.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">Лампы приглушённого освещения и газетные ниши</h2>
							<p class="desc">Светодиодные лампы с регулировкой цвета и яркости, встроенные в обшивку салона, помогают поддерживать атмосферу спокойствия и комфорта. А в удобных нишах под ними можно хранить журналы и газеты.</p>
						</div>
					</li>
				</ul>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-back-seat_bg.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- // one touch -->
		<!-- leg support -->
		<section class="section module-skin2 title-type4 color-type2">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">ПОДСТАВКА ДЛЯ НОГ</h2>
					<p class="desc">Увеличенное пространство для пассажиров задних сидений<br> позволяет создать эксклюзивные условия для отдыха.<br> Чтобы расслабиться и в полной мере насладиться комфортом,<br> здесь предусмотрена подставка для ног.</p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-leg-support_bg.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- // leg support -->
		<!-- banner -->
		<section class="eq-banner">
			<a href="<?=Yii::$app->homeUrl?>/<?=$this->params['model']?>/safety">
					<span class="description">
						<span>NEXT</span>
						<strong>БЕЗОПАСНОСТЬ</strong>

						<i>SCROLL</i>
					</span>
				<img src="<?=Yii::$app->homeUrl?>/images/desktop/comfort/eq900-comfort_banner.jpg" alt="">
			</a>
		</section>
		<!-- // banner -->
	</div>
</div>
<?else:?>
	<div id="container">

		<div class="inner-container">
			<!-- <a class="btn-back" href="#" title="">BACK <i></i></a> -->
			<!-- kv -->
			<section class="section m-module-skin1 m-title-type2 m-color-type2">
				<article class="feature">
					<div class="brand-header">
						<h2 class="title">КОМФОРТ</h2>
						<p class="desc">КИЛОМЕТРЫ<br> УДОВОЛЬСТВИЯ</p>
					</div>
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-comfort_intro.jpg" alt="">
						</figure>
					</div>
				</article>
			</section>
			<!-- // kv -->

			<!-- real metal -->
			<section class="section m-module-skin2 m-text-type2 m-color-type1 m-zoom-type1">
				<article class="feature">
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-real-metal_bg.jpg" alt="">
						</figure>
						<figure class="feature on">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-real-metal_detail.jpg" alt="">
						</figure>
					</div>
					<div class="brand-header">
						<h2 class="title">КЛАССИКА И МЕТАЛЛ</h2>
						<p class="desc">Акустическая система Lexicon с 17 динамиками,<br> сабвуфером и линзой обеспечивает глубокий и богатый звук.<br> Решётки из нержавеющей стали помогают добиться <br> объёмного пространственного звучания и визуально дополняют композиции. </p>
					</div>
					<button class="btn-zoom" type="button">
						<span>상세보기</span>
					</button>
					<button class="btn-zoom-close" type="button"><img src="<?=Yii::$app->homeUrl?>/images/mobile/btn_close.png" alt=""></button>
				</article>
			</section>
			<!-- // real metal -->

			<!-- modern ergo -->
			<section class="section m-module-skin2 m-text-type2 m-color-type1">
				<article class="feature">
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-modern-ergo_bg.jpg" alt="">
						</figure>
					</div>
					<div class="brand-header">
						<h2 class="title">ЭРГОНОМИЧНЫЕ СИДЕНЬЯ</h2>
						<p class="desc">Современные эргономичные передние и задние сиденья, сертифицированные AGR (Aktion Gesunder Rucken, Германия), регулируются в 22 и 14 направлениях соответственно (включая наклон плечевой зоны, передний валик и подголовник) и обеспечивают высочайший уровень комфорта для пассажира и водителя. </p>
					</div>
				</article>
			</section>
			<!-- // modern ergo -->

			<!-- resolution display -->
			<section class="section m-module-skin2 m-text-type2 m-color-type1 m-opacity-on">
				<article class="feature">
					<div class="brand-content">
						<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-resolution-display_bg.jpg" alt=""></figure>
						<figure class="feature on"><img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-resolution-display_bg_on.jpg" alt=""></figure>
					</div>
					<div class="brand-header">
						<h2 class="title">ШИРОКОФОРМАТ&shy;НЫЙ TFT-ДИСПЛЕЙ 12.3″</h2>
						<p class="desc">12-дюймовый широкоформатный дисплей с высоким разрешением<br> обеспечивает исключительное качество изображения. Навигационная<br> и мультимедийная информация отображается в полноэкранном формате<br> или в формате разделённого экрана. </p>
					</div>
				</article>
			</section>
			<!-- // resolution display -->

			<!-- smart -->
			<!-- <section class="section m-module-skin2 m-text-type1 m-color-type2">
				<article class="feature">
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-smart_bg.jpg" alt="">
						</figure>
					</div>
					<div class="brand-header">
						<h2 class="title">Smart posture caring system</h2>
						<p class="desc">When you enter your height, weight, and seated height on the cluster screen, the system automatically adjusts the seat, steering-wheel, outside mirrors, and head-up display to the optimal position for your body.</p>
					</div>
				</article>
			</section> -->
			<!-- // smart -->

			<!-- head up -->
			<section class="section m-module-skin4 m-color-type1 m-opacity-on">
				<article class="feature">
					<ul>
						<li>
							<figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-headup_content.jpg" alt=""></figure>
							<figure class="on"><img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-headup_content_on.jpg" alt=""></figure>
							<div class="brand-header">
								<h2 class="title">Проекция на лобовое стекло</h2>
								<p class="desc">Данные задних и боковых ультразвуковых датчиков, информация системы интеллектуального круиз-контроля, навигационная и другая информация проецируется на лобовое стекло.</p>
							</div>
						</li>
					</ul>
				</article>
			</section>
			<!-- // head up -->

			<!-- vip sit -->
			<section class="section m-module-skin2 m-text-type2 m-color-type1">
				<article class="feature">
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-vip-sit_bg.jpg" alt="">
						</figure>
					</div>
					<div class="brand-header">
						<h2 class="title">ПУТЕШЕСТВИЕ ПЕРВЫМ КЛАССОМ</h2>
						<p class="desc">Эргономика салона первого класса в седане G90. Сиденья предлагают исключительный уровень комфорта как для пассажира, так и для водителя.</p>
					</div>
				</article>
			</section>
			<!-- // vip sit -->

			<!-- back seat system -->
			<section class="section m-module-skin4 m-color-type1 m-opacity-on">
				<article class="feature">
					<ul>
						<li>
							<figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-back-seat-system_content01.jpg" alt=""></figure>
							<figure class="on"><img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-back-seat-system_content01_on.jpg" alt=""></figure>
							<div class="brand-header">
								<h2 class="title">Система мультимедиа для задних сидений</h2>
								<p class="desc">Просмотр и управление медиаконтентом может осуществляться с задних сидений на больших, размером 9,2 дюйма, мониторах с широким углом обзора.</p>
							</div>
						</li>
						<li>
							<figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-back-seat-system_content02.jpg" alt=""></figure>
							<figure class="on"><img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-back-seat-system_content02_on.jpg" alt=""></figure>
							<div class="brand-header">
								<h2 class="title">Беспроводная зарядка для смартфона</h2>
								<p class="desc">Смартфоны* можно легко заряжать, положив их на специальную панель центральной консоли.</p>
								<p class="refer-text">*только для смартфонов с возможностью беспроводной зарядки.</p>
							</div>
						</li>
						<li>
							<figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-back-seat-system_content03.jpg" alt=""></figure>
							<div class="brand-header">
								<h2 class="title">Память положений сидений</h2>
								<p class="desc">Память положения сидений, которой оборудовано каждое сиденье, позволяет запоминать настройки кресла и восстанавливать предпочтительное положение простым нажатием кнопки.</p>
							</div>
						</li>
					</ul>
				</article>
			</section>
			<!-- // back seat system -->

			<!-- back seat -->
			<section class="section m-module-skin2 m-text-type2 m-color-type1">
				<article class="feature">
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-back-seat_bg.jpg" alt="">
						</figure>
					</div>
					<div class="brand-header">
						<h2 class="title">СИДЕНИЯ ИЗ ПОЛУАНИЛИНОВОЙ КОЖИ</h2>
						<p class="desc">В отделке салона используются только эксклюзивные материалы, точно подобранные друг к другу в едином стилистическом решении. Сиденья выполнены из полуанилиновой кожи тончайшей выделки, обладающей высоким тактильным эффектом.</p>
					</div>
				</article>
			</section>
			<!-- // back seat -->

			<!-- one touch -->
			<section class="section m-module-skin4 m-color-type1">
				<article class="feature">
					<ul>
						<li>
							<figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-one-touch_content01.jpg" alt=""></figure>
							<div class="brand-header">
								<h2 class="title">Управление одним касанием</h2>
								<p class="desc">Одним нажатием кнопки вы можете выбрать режимы расслабления, ТВ, чтения или возврат к базовым настройкам, которые обеспечат оптимальную позу в зависимости от выбранного положения. При выборе режима ТВ или чтения положение сиденья оптимально регулируется для просмотра фильмов и передач, или чтения книги. Режим отдыха обеспечивает идеальную атмосферу для расслабления, а режим возврата перемещает ваше кресло в базовое положение для удобного выхода из автомобиля или посадки в него.</p>
							</div>
						</li>
						<li>
							<figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-one-touch_content02.jpg" alt=""></figure>
							<div class="brand-header">
								<h2 class="title">Лампы приглушённого освещения и газетные ниши</h2>
								<p class="desc">Светодиодные лампы с регулировкой цвета и яркости, встроенные в обшивку салона, помогают поддерживать атмосферу спокойствия и комфорта. А в удобных нишах под ними можно хранить журналы и газеты.</p>
							</div>
						</li>
					</ul>
				</article>
			</section>
			<!-- // one touch -->

			<!-- leg support -->
			<section class="section m-module-skin2 m-text-type2 m-color-type1">
				<article class="feature">
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/eq900-leg-support_bg.jpg" alt="">
						</figure>
					</div>
					<div class="brand-header">
						<h2 class="title">ПОДСТАВКА ДЛЯ НОГ</h2>
						<p class="desc">Увеличенное пространство для пассажиров задних сидений позволяет создать эксклюзивные условия для отдыха. Чтобы расслабиться и в полной мере насладиться комфортом, здесь предусмотрена подставка для ног.</p>
					</div>
				</article>
			</section>
			<!-- // leg support -->

			<!-- banner -->
			<section class="eq-banner">
				<a href="./g90-safety-mobile.html">
					<span class="description">
						<span>NEXT</span>
						<strong>БЕЗОПАСНОСТЬ</strong>
					</span>
					<img src="<?=Yii::$app->homeUrl?>/images/mobile/comfort/img_banner.jpg" alt="">
				</a>
			</section>
			<!-- // banner -->

		</div>

	</div>
<?endif?>

<?php echo $this->render('/partials/footer'); ?>
<?php echo $this->render('/model/scripts'); ?>

<?if(!$mobilePrefix):?>
<script>
	;
	(function(window, $, undefined) {
		$(function() {
			App.brand.init();
			App.brand.section.init('.Eq900 .section');
			App.brand.navigator.init({
				section: '.section > article'
			});
		});
	}(window, jQuery));

</script>
<?endif?>