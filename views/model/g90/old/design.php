<?
$this->title = "Design - Genesis ".$this->params['modelName'];

$mobilePrefix = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?'m-':null;
?>
<?if(!$mobilePrefix):?><div id="container" class="Eq900">
	<nav class="sec-indicator">
		<div class="nav-inner">
			<div class="gnb-area">
				<ul>
					<li><a href="#" class="cur"><span>DESIGN</span></a></li>
					<li><a href="#"><span>Crest grille</span></a></li>
					<li><a href="#"><span>Adaptive full <br>LED headlamps</span></a></li>
					<li><a href="#"><span>Prime Nappa <br>leather seats</span></a></li>
					<li><a href="#"><span>Prime Nappa leather <br>interior material</span></a></li>
					<li><a href="#"><span>Center console and <br>transmission knob</span></a></li>
					<li><a href="#"><span>Ambient mood lamps</span></a></li>
					<li><a href="#"><span>Limousine B pillar</span></a></li>
					<li><a href="#"><span>Limousine <br>sputtering wheels</span></a></li>
					<li><a href="#"><span>Outside mirror <br>chrome molding</span></a></li>
				</ul>
				<span class="line"></span>
			</div>
		</div>
	</nav>
	<div class="inner-container">
		<div class="btn-down-wrap visible">
			<a class="btn-back on up" href="<?=Yii::$app->homeUrl?>/<?=$this->params['model']?>#design">BACK <i></i></a>
			<a class="btn-down" href="#"><i></i> CLICK FOR MORE</a>
		</div>
		<!-- Design kv -->
		<section class="section module-skin1 title-type1 color-type2">
			<article class="feature">
				<div class="brand-header" data-hello="text">
					<h2 class="title">ДИЗАЙН</h2>
					<p class="desc">АТЛЕТИЧНАЯ<br/>ЭЛЕГАНТНОСТЬ</p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature01.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- // Design kv -->
		<!-- Crest -->
		<section class="section module-skin2 title-type4 color-type2">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">РАДИАТОРНАЯ РЕШЕТКА</h2>
					<p class="desc">Широкая шестиугольная решетка радиатора придает автомобилю особый шарм.</p>
					<!-- <a href="#" class="desc2">
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_design_circle.jpg" alt="" /></span>
							<strong>GENESIS EQ900<em>INSIDER'S STORY EPISODE 1. ORIGIN</em></strong>
						</a> -->
				</div>
				<div class="brand-content">
					<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature02.jpg" alt=""></figure>
				</div>
			</article>
		</section>
		<!-- // Crest -->
		<!-- Crest layer -->
		<section class="section layer-type module-skin4">
			<article class="feature">
				<ul class="layer-box cell-3">
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature03.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">Полностью светодиодные адаптивные фары</h2>
							<p class="desc">Стильные фары с 3-мерным источником света гармонично вписываются в роскошный дизайн передней части G90.</p>
						</div>
					</li>
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature04.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">Задние комбинированные фонари</h2>
							<p class="desc">Изогнутый дизайн фар гармонично сочетается с линиями экстерьера, придавая законченность образу G90.</p>
							<!-- <a href="#" class="desc2">
										<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_design_circle02.jpg" alt="" /></span>
										<strong>GENESIS EQ900<em>INTERIOR #01</em></strong>
									</a> -->
						</div>
					</li>
				</ul>
				<div class="brand-content">
					<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature02.jpg" alt=""></figure>
				</div>
			</article>
		</section>
		<!-- // Crest layer -->
		<!-- prime -->
		<section class="section module-skin2 title-type1 color-type2">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">ТЕРРИТОРИЯ УДОВОЛЬСТВИЯ</h2>
					<p class="desc">Интерьер Genesis G90 выполнен с изыском. Подбор материалов действительно впечатляет:<br> алюминий, натуральное дерево, шерсть и полуанилиновая кожа тончайшей выделки,<br> идеально ровная прострочка которой выполнена вручную.</p>
				</div>
				<div class="brand-content">
					<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature06.jpg" alt=""></figure>
				</div>
			</article>
		</section>
		<!-- // prime -->
		<!-- prime layer -->
		<section class="section module-skin4">
			<article class="feature">
				<ul class="layer-box cell-2">
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature07.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">Лучшие материалы</h2>
							<p class="desc">Богатство текстуры обивки G90 способно привести в восторг с первого прикосновения. Секрет в премиальной коже Наппа от итальянского ателье Pasubio и уникальной технологии обработки швов, созданной совместно с австрийской швейной фирмой.</p>
						</div>
					</li>
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature08.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">Отделка натуральным деревом</h2>
							<p class="desc">В Genesis с особым пристрастием подходят к выбору материалов отделки и месту их происхождения. Декоративные сорта шпона поставляются из Германии и Италии. Технологическая цепочка выстроена таким образом, что обработка исходного материала производится в Италии, а покраска шпона — в Германии. Это позволяет сохранить оригинальный цвет и текстуру дерева.</p>
						</div>
						<!-- <a href="#" class="desc2">
										<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_design_circle03.jpg" alt="" /></span>
										<strong>GENESIS EQ900<em>INTERIOR #01</em></strong>
									</a> -->
					</li>
				</ul>
				<div class="brand-content">
					<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature02.jpg" alt=""></figure>
				</div>
			</article>
		</section>
		<!-- // prime layer -->
		<!-- center fesia -->
		<section class="section module-skin2 title-type4 color-type2">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">ЦЕНТРАЛЬНАЯ КОНСОЛЬ</h2>
					<p class="desc">Детально продуманная функциональность обрамлена элегантным дизайном.<br> Элементы управления и переключатели изготовлены из алюминия,<br> приятны на ощупь и расположены именно там, где ими удобнее всего пользоваться.</p>
					<!-- <p class="refer-text">* Terrestrial DMB supports resolution of 320X240. If the DMB service provider converts to a higher resolution (1,280X720) <br>or the broadcasting policy changes, terrestrial DMB may not be received.</p> -->
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature09.jpg" alt="">
						<span class=""><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature09.jpg" alt=""></span>
					</figure>
				</div>
			</article>
		</section>
		<!-- // center fesia -->
		<!-- center fesia layer : cell-1 -->
		<section id="g90a" class="section layer-type module-skin4 opacity-on">
			<article class="feature">
				<ul class="layer-box cell-1">
					<li>
						<figure>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature14.jpg" alt=""></span>
							<span class="on"><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature14_on.jpg" alt=""></span>
						</figure>
						<div class="brand-header">
							<h2 class="title">Лампы приглушённого освещения</h2>
							<p class="desc">Светодиодные лампы, встроенные в обшивку салона, помогают поддерживать атмосферу спокойствия и комфорта. Вы можете выбрать любой из 7 цветов, а также регулировать яркость подсветки, чтобы настроить интерьер под своё настроение.</p>
						</div>
					</li>
				</ul>
				<div class="brand-content">
					<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature09.jpg" alt=""></figure>
				</div>
			</article>
		</section>
		<!-- // center fesia layer : cell-1 -->
		<!-- limousine -->
		<section id="g90l" class="section module-skin2 title-type1 color-type2 zoom-type2">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">Genesis G90 L</h2>
					<p class="desc">Дополнительные 290 мм увеличенной колёсной базы<br> создают еще более комфортное пространство<br> для пассажиров заднего ряда сидений G90 L.</p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature11.jpg" alt="">
					</figure>
					<figure class="feature on">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature11_pop.jpg" alt="">
					</figure>
				</div>
				<button class="btn-zoom" type="button">
					<span>상세보기</span>
				</button>
				<button class="btn-zoom-close" type="button">
					<span>상세보기 close</span>
				</button>
			</article>
		</section>
		<!-- // limousine -->
		<!-- wheel -->
		<section class="section module-skin2 title-type4 color-type1">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">ЭКСКЛЮЗИВНЫЕ<br> 19-ДЮЙМОВЫЕ ДИСКИ</h2>
					<p class="desc">Лаконично дополняют роскошный образ седана<br> представительского класса, придавая ему особый шарм.</p>
				</div>
				<div class="brand-content">
					<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature12.jpg" alt=""></figure> -->
				</div>
			</article>
		</section>
		<!-- // wheel -->
		<!-- wheel layer -->
		<section class="section layer-type module-skin4">
			<article class="feature">
				<ul class="layer-box cell-1">
					<li>
						<figure><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature13.jpg" alt=""></figure>
						<div class="brand-header">
							<h2 class="title">Боковые зеркала<br> с повторителями</h2>
							<p class="desc">Повторители поворотов элегантно вписаны в дизайн боковых зеркал. Комбинация материалов в оформлении зеркал создаёт мягкий акцент на эксклюзивность в общем облике седана.</p>
						</div>
					</li>
				</ul>
				<div class="brand-content">
					<figure class="feature"><img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_feature12.jpg" alt=""></figure>
				</div>
			</article>
		</section>
		<!-- // wheel layer -->
		<!-- banner -->
		<section class="eq-banner">
			<a href="<?=Yii::$app->homeUrl?>/<?=$this->params['model']?>/comfort">
					<span class="description">
						<span>NEXT</span>
						<strong>comfort</strong>

						<i>SCROLL</i>
					</span>
				<img src="<?=Yii::$app->homeUrl?>/images/desktop/design/img_banner.jpg" alt="">
			</a>
		</section>
		<!-- // banner -->
	</div>
</div>
<?else:?>
    <div id="container">
        <div class="inner-container">
            <!-- <a class="btn-back" href="#" title="">BACK <i></i></a> -->
            <!-- kv type -->
            <section class="section m-module-skin1 m-title-type1 m-color-type2">
                <article class="feature">
                    <div class="brand-content">
                        <figure class="feature">
                            <img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature_kv.jpg" alt="">
                        </figure>
                    </div>
                    <div class="brand-header">
                        <h2 class="title">ДИЗАЙН</h2>
                        <p class="desc">АТЛЕТИЧНАЯ<br/>ЭЛЕГАНТНОСТЬ</div>
                </article>
            </section>
            <!-- // kv type -->
            <div>
                <!-- crest -->
                <section class="section m-module-skin2 m-text-type1 m-color-type2">
                    <article class="feature">
                        <div class="brand-content">
                            <figure class="feature">
                                <img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature02.jpg" alt="">
                            </figure>
                        </div>
                        <div class="brand-header">
                            <h2 class="title">РАДИАТОРНАЯ РЕШЕТКА</h2>
                            <p class="desc">Широкая шестиугольная решетка радиатора придает автомобилю особый шарм.</p>
                            <!-- <a href="#" class="desc2">
									<span><img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_design_circle.jpg" alt="" /></span>
									<strong>GENESIS EQ900<em>INSIDER'S STORY EPISODE 1. ORIGIN</em></strong>
								</a> -->
                        </div>
                    </article>
                </section>
            </div>
            <!-- crest sub content -->
            <section class="section m-module-skin4 m-color-type1">
                <article class="feature">
                    <ul>
                        <li>
                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature03.jpg" alt=""></figure>
                            <div class="brand-header">
                                <h2 class="title">Полностью светодиодные адаптивные фары</h2>
                                <p class="desc">Стильные фары с 3-мерным источником света гармонично вписываются в роскошный дизайн передней части G90.</p>
                            </div>
                        </li>
                        <li>
                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature04.jpg" alt=""></figure>
                            <div class="brand-header">
                                <h2 class="title">Задние комбинированные фонари</h2>
                                <p class="desc">Изогнутый дизайн фар гармонично сочетается с линиями экстерьера, придавая законченность образу G90.</p>
                                <!-- <a href="#" class="desc2">
										<span><img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_design_circle02.jpg" alt="" /></span>
										<strong>GENESIS EQ900<em>INTERIOR #01</em></strong>
									</a> -->
                            </div>
                        </li>
                    </ul>
                </article>
            </section>
            <!-- // crest -->
            <!-- prime -->
            <section class="section m-module-skin2 m-text-type2 m-color-type1">
                <article class="feature">
                    <div class="brand-content">
                        <figure class="feature">
                            <img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature06.jpg" alt="">
                        </figure>
                    </div>
                    <div class="brand-header">
                        <h2 class="title">ТЕРРИТОРИЯ УДОВОЛЬСТВИЯ</h2>
                        <p class="desc">Интерьер Genesis G90 выполнен с изыском. Подбор материалов действительно впечатляет:<br> алюминий, натуральное дерево, шерсть и полуанилиновая кожа тончайшей выделки,<br> идеально ровная прострочка которой выполнена вручную.</p>
                    </div>
                </article>
            </section>
            <!-- prime sub content -->
            <section class="section m-module-skin4 m-color-type1">
                <article class="feature">
                    <ul>
                        <li>
                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature07.jpg" alt=""></figure>
                            <div class="brand-header">
                                <h2 class="title">Лучшие материалы</h2>
                                <p class="desc">Богатство текстуры обивки G90 способно привести в восторг с первого прикосновения. Секрет в премиальной коже Наппа от итальянского ателье Pasubio и уникальной технологии обработки швов, созданной совместно с австрийской швейной фирмой.</p>
                            </div>
                        </li>
                        <li>
                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature08.jpg" alt=""></figure>
                            <div class="brand-header">
                                <h2 class="title">Отделка натуральным деревом</h2>
                                <p class="desc">В Genesis с особым пристрастием подходят к выбору материалов отделки и месту их происхождения. Декоративные сорта шпона поставляются из Германии и Италии. Технологическая цепочка выстроена таким образом, что обработка исходного материала производится в Италии, а покраска шпона — в Германии. Это позволяет сохранить оригинальный цвет и текстуру дерева.</p>
                                <!-- <a href="#" class="desc2">
										<span><img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_design_circle03.jpg" alt="" /></span>
										<strong>GENESIS EQ900<em>INTERIOR #01</em></strong>
									</a> -->
                            </div>
                        </li>
                    </ul>
                </article>
            </section>
            <!-- // prime -->
            <!-- center fesia -->
            <section class="section m-module-skin2 m-text-type1 m-color-type2">
                <article class="feature">
                    <div class="brand-content">
                        <figure class="feature">
                            <img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature09.jpg" alt="">
                        </figure>
                    </div>
                    <div class="brand-header">
                        <h2 class="title">ЦЕНТРАЛЬНАЯ КОНСОЛЬ</h2>
                        <p class="desc">Детально продуманная функциональность обрамлена элегантным дизайном.<br> Элементы управления и переключатели изготовлены из алюминия,<br> приятны на ощупь и расположены именно там, где ими удобнее всего пользоваться.</p>
                        <!-- <p class="refer-text">* Terrestrial DMB supports resolution of 320X240. If the DMB service provider converts to a higher resolution (1,280X720) <br>or the broadcasting policy changes, terrestrial DMB may not be received.</p> -->
                    </div>
                </article>
            </section>
            <section class="section m-module-skin4 m-color-type1 m-opacity-on">
                <article class="feature">
                    <ul>
                        <li>
                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature10.jpg" alt=""></figure>
                            <figure class="on"><img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature10_on.jpg" alt=""></figure>
                            <div class="brand-header">
                                <h2 class="title">Лампы приглушённого освещения</h2>
                                <p class="desc">Светодиодные лампы, встроенные в обшивку салона, помогают поддерживать атмосферу спокойствия и комфорта. Вы можете выбрать любой из 7 цветов, а также регулировать яркость подсветки, чтобы настроить интерьер под своё настроение.</p>
                            </div>
                        </li>
                    </ul>
                </article>
            </section>
            <!-- // center fesia -->
            <section class="section m-module-skin2 m-text-type1 m-color-type2 m-zoom-type2">
                <article class="feature">
                    <div class="brand-content">
                        <figure class="feature">
                            <img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature11.jpg" alt="">
                        </figure>
                        <figure class="feature on">
                            <img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature11_detail.jpg" alt="">
                        </figure>
                    </div>
                    <div class="brand-header">
                        <h2 class="title">Genesis G90 L</h2>
                        <p class="desc">Дополнительные 290 мм увеличенной колёсной базы<br> создают еще более комфортное пространство<br> для пассажиров заднего ряда сидений G90 L.</p>
                    </div>
                    <button class="btn-zoom" type="button">
                        <span>상세보기</span>
                    </button>
                    <button class="btn-zoom-close" type="button"><img src="<?=Yii::$app->homeUrl?>/images/mobile/btn_close.png" alt=""></button>
                </article>
            </section>
            <section class="section m-module-skin4 m-color-type1">
                <article class="feature">
                    <ul>
                        <li>
                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature12.jpg" alt=""></figure>
                            <div class="brand-header">
                                <h2 class="title">ЭКСКЛЮЗИВНЫЕ<br> 19-ДЮЙМОВЫЕ ДИСКИ</h2>
                                <p class="desc">Лаконично дополняют роскошный образ седана<br> представительского класса, придавая ему особый шарм.</p>
                            </div>
                        </li>
                        <li>
                            <figure><img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_feature13.jpg" alt=""></figure>
                            <div class="brand-header">
                                <h2 class="title">Боковые зеркала<br> с повторителями</h2>
                                <p class="desc">Повторители поворотов элегантно вписаны в дизайн боковых зеркал. Комбинация материалов в оформлении зеркал создаёт мягкий акцент на эксклюзивность в общем облике седана.</p>
                            </div>
                        </li>
                    </ul>
                </article>
            </section>
            <!-- banner -->
            <section class="eq-banner">
                <a href="g90-comfort-mobile.html">
					<span class="description">
						<span>NEXT</span>
						<strong>COMFORT</strong>
					</span>
                    <img src="<?=Yii::$app->homeUrl?>/images/mobile/design/img_banner.jpg" alt="">
                </a>
            </section>
            <!-- // banner -->
        </div>
    </div>
<?endif?>

<?php echo $this->render('/partials/footer'); ?>
<?php echo $this->render('/model/scripts'); ?>

<?if($mobilePrefix):?>
    <script>
		;
		(function(window, $) {
			$(function() {

				$('.btn-zoom').on('click', function() {
					$('.m-zoom-type1, .m-zoom-type2').addClass('active');
				});
				$('.btn-zoom-close').on('click', function() {
					$('.m-zoom-type1.active, .m-zoom-type2.active').removeClass('active');
				});

			});
		}(window, jQuery));

    </script>
<?else:?>
    <script>
        ;
        (function(window, $, undefined) {
            $(function() {
    
                App.brand.init();
                App.brand.section.init('.Eq900 .section');
                // App.brand.navigator.init({
                // 	section: '.section > article'
                // });
                //
                $(window).on('hashchange', function() {
                    $('.sec-indicator li').eq(7).find('a').click();
                });
                if(document.location.hash === '#g90l'){
                    setTimeout(function() {
                        $('.sec-indicator li').eq(7).find('a').click();
                    }, 700);
                };
    
            });
        }(window, jQuery));
    
    </script>
<?endif?>
