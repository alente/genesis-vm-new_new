<?
$this->title = "Performance - Genesis ".$this->params['modelName'];

$mobilePrefix = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?'m-':null;
?>
<?if(!$mobilePrefix):?><div id="container" class="Eq900">
	<nav class="sec-indicator">
		<div class="nav-inner">
			<div class="gnb-area">
				<ul>
					<li><a href="javascript:void(0);" title="" class="cur"><span>PERFORMACE</span></a></li>
					<li><a href="javascript:void(0);" title="" class=""><span>HTRAC, Electronic AWD</span></a></li>
					<li><a href="javascript:void(0);" title="" class=""><span>Genesis Adaptive <br>Control Suspension</span></a></li>
					<li><a href="javascript:void(0);" title="" class=""><span>Powertrain</span></a></li>
				</ul>
				<span class="line"></span>
			</div>
		</div>
	</nav>
	<div class="inner-container">
		<div class="btn-down-wrap visible">
			<a class="btn-back on up" href="<?=Yii::$app->homeUrl?>/<?=$this->params['model']?>#design">BACK <i></i></a>
			<a class="btn-down" href="#"><i></i> CLICK FOR MORE</a>
		</div>
		<!-- performance -->
		<section class="section module-skin1 title-type1 color-type2">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">ПРОИЗВОДИТЕЛЬНОСТЬ</h2>
					<p class="desc">ДИНАМИКА <br>И МОЩЬ</p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/performance/eq900-performance_intro.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- // performance -->
		<!-- htrac -->
		<section class="section module-skin2 title-type5 color-type1 interval-type">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">ПОЛНЫЙ ПРИВОД HTRAC</h2>
					<p class="desc">Интеллектуальная система полного привода HTRAC помогает адаптироваться к различным дорожным условиям. Она варьирует крутящий момент на передних и задних колёсах, что обеспечивает безопасность при движении по извилистой и скользкой дороге.</p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img class="step1" src="<?=Yii::$app->homeUrl?>/images/desktop/performance/eq900-htrac_content01.jpg" alt="">
						<img class="step2" src="<?=Yii::$app->homeUrl?>/images/desktop/performance/eq900-htrac_content02.jpg" alt="">
						<img class="step3" src="<?=Yii::$app->homeUrl?>/images/desktop/performance/eq900-htrac_content03.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- // htrac -->
		<!-- control suspension -->
		<section class="section module-skin2 title-type4 color-type2">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">АДАПТИВНАЯ ПОДВЕСКА GENESIS</h2>
					<p class="desc">Сочетание интегрированной системы управления шасси с электронно-управляемой адаптивной подвеской Genesis обеспечивает стабильность вождения автомобиля. Система автоматически распределяет демпфирующие усилия передних и задних амортизаторов в неблагоприятных дорожных условиях.</p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/performance/eq900-control-suspension_bg.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- // control suspension -->
		<!-- power train -->
		<section class="section layer-type motion-type7">
			<article class="feature">
				<div class="tab-wrap">
					<div class="tab-content on">
						<figure>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/performance/eq900-power-tranin_content01.jpg" alt=""></span>
							<figcaption class="spec-desc">
								<strong>Бензиновый <br> 5.0 V8 GDi</strong>
								<span> <i>Максимальная мощность</i> 413 л.с. <br>(при 6 000 об./мин.) </span>
								<span> <i>Крутящий момент</i> 505 Нм <br>(при 5 000 об./мин.) </span>
							</figcaption>
						</figure>
					</div>
					<div class="tab-content">
						<figure>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/performance/eq900-power-tranin_content02.jpg" alt=""></span>
							<figcaption class="spec-desc">
								<strong>Бензиновый <br> 3.3 V6 T-GDi</strong>
								<span> <i>Максимальная мощность</i> 370 л.с. <br>(при 6 000 об./мин.) </span>
								<span> <i>Крутящий момент</i> 510 Нм <br>(при 1 300 - 4 500 об./мин.) </span>
							</figcaption>
						</figure>
					</div>
					<div class="tab-content">
						<figure>
							<span><img src="<?=Yii::$app->homeUrl?>/images/desktop/performance/eq900-power-tranin_content03.jpg" alt=""></span>
							<figcaption class="spec-desc">
								<strong>Бензиновый <br> 3.8 V6 GDi</strong>
								<span> <i>Максимальная мощность</i> 309 л.с. <br>(при 6 000 об./мин.) </span>
								<span> <i>Крутящий момент</i> 391 Нм <br>(при 5 000 об./мин.) </span>
							</figcaption>
						</figure>
					</div>
					<span class="tab-indicator">
							<a class="on" href="#">tab1</a>
							<a href="#">tab2</a>
							<a href="#">tab3</a>
						</span>
					<div class="brand-header">
						<h2 class="title">ДВИГАТЕЛИ</h2>
						<p class="desc">Вся линейка из 3-х двигателей, включая турбированный силовой агрегат объемом 3.3 литра, обеспечивает мощное ускорение и эффективный расход топлива.</p>
					</div>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/performance/eq900-control-suspension_bg.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- // power train -->
		<!-- banner -->
		<section class="eq-banner">
			<a href="<?=Yii::$app->homeUrl?>/<?=$this->params['model']?>/gallery">
					<span class="description">
						<span>NEXT</span>
						<strong>GALLERY</strong>

						<i>SCROLL</i>
					</span>
				<img src="<?=Yii::$app->homeUrl?>/images/desktop/performance/eq900-performance_banner.jpg" alt="">
			</a>
		</section>
		<!-- // banner -->
	</div>
</div>
<?else:?>
	<div id="container">
		<div class="inner-container">
			<!-- <a class="btn-back" href="#" title="">BACK <i></i></a> -->
			<!-- kv -->
			<section class="section m-module-skin1 m-title-type3 m-color-type2">
				<article class="feature">
					<div class="brand-header">
						<h2 class="title">ПРОИЗВОДИТЕЛЬНОСТЬ</h2>
						<p class="desc">ДИНАМИКА <br>И МОЩЬ</p>
					</div>
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/performance/eq900-performance_intro.jpg" alt="">
						</figure>
					</div>
				</article>
			</section>
			<!-- // kv -->
			<!-- htrac -->
			<section class="section m-module-skin2 m-text-type2 m-color-type1 m-interval-type">
				<article class="feature">
					<div class="brand-content">
						<figure class="feature">
							<img class="step1" src="<?=Yii::$app->homeUrl?>/images/mobile/performance/eq900-htrac_content01.jpg" alt="">
							<img class="step2" src="<?=Yii::$app->homeUrl?>/images/mobile/performance/eq900-htrac_content02.jpg" alt="">
							<img class="step3" src="<?=Yii::$app->homeUrl?>/images/mobile/performance/eq900-htrac_content03.jpg" alt="">
						</figure>
					</div>
					<div class="brand-header">
						<h2 class="title">ПОЛНЫЙ ПРИВОД HTRAC</h2>
						<p class="desc">Интеллектуальная система полного привода HTRAC помогает адаптироваться к различным дорожным условиям. Она варьирует крутящий момент на передних и задних колёсах, что обеспечивает безопасность при движении по извилистой и скользкой дороге.</p>
					</div>
				</article>
			</section>
			<!-- // htrac -->
			<!-- control suspension -->
			<section class="section m-module-skin2 m-text-type2 m-color-type1">
				<article class="feature">
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/performance/eq900-control-suspension_bg.jpg" alt="">
						</figure>
					</div>
					<div class="brand-header">
						<h2 class="title">АДАПТИВНАЯ ПОДВЕСКА GENESIS</h2>
						<p class="desc">Сочетание интегрированной системы управления шасси с электронно-управляемой адаптивной подвеской Genesis обеспечивает стабильность вождения автомобиля. Система автоматически распределяет демпфирующие усилия передних и задних амортизаторов в неблагоприятных дорожных условиях.</p>
					</div>
				</article>
			</section>
			<!-- // control suspension -->
			<!-- power train -->
			<section class="section m-module-skin5 m-color-type1 m-motion-type7">
				<article class="feature">
					<div class="swipe-wrap">
						<div class="slide-wrap">
							<ul>
								<li>
									<figure class="engine-con">
										<span><img src="<?=Yii::$app->homeUrl?>/images/mobile/performance/genesis-eq900-features-performance-powertrain01.jpg" alt=""></span>
										<figcaption class="spec-desc">
											<strong>Бензиновый 5.0 V8 GDi</strong>
											<span> <i>Максимальная мощность</i> 413 л.с. <br>(при 6 000 об./мин.) </span>
											<span> <i>Крутящий момент</i> 505 Нм <br>(при 5 000 об./мин.) </span>
										</figcaption>
									</figure>
								</li>
								<li>
									<figure class="engine-con">
										<span><img src="<?=Yii::$app->homeUrl?>/images/mobile/performance/genesis-eq900-features-performance-powertrain02.jpg" alt=""></span>
										<figcaption class="spec-desc">
											<strong>Бензиновый 3.3 V6 T-GDi</strong>
											<span> <i>Максимальная мощность</i> 370 л.с. <br>(при 6 000 об./мин.) </span>
											<span> <i>Крутящий момент</i> 510 Нм <br>(при 1 300 - 4 500 об./мин.) </span>
										</figcaption>
									</figure>
								</li>
								<li>
									<figure class="engine-con">
										<span><img src="<?=Yii::$app->homeUrl?>/images/mobile/performance/genesis-eq900-features-performance-powertrain03.jpg" alt=""></span>
										<figcaption class="spec-desc">
											<strong>Бензиновый 3.8 V6 GDi</strong>
											<span> <i>Максимальная мощность</i> 309 л.с. <br>(при 6 000 об./мин.) </span>
											<span> <i>Крутящий момент</i> 391 Нм <br>(при 5 000 об./мин.) </span>
										</figcaption>
									</figure>
								</li>
							</ul>
						</div>
					</div>
					<div class="brand-header">
						<h2 class="title">ДВИГАТЕЛИ</h2>
						<p class="desc">Вся линейка из 3-х двигателей, включая турбированный силовой агрегат объемом 3.3 литра, обеспечивает мощное ускорение и эффективный расход топлива.</p>
					</div>
				</article>
			</section>
			<!-- // power train -->
			<!-- banner -->
			<section class="eq-banner">
				<a href="g90-gallery-mobile.html">
					<span class="description">
						<span>NEXT</span>
						<strong>GALLERY</strong>
					</span>
					<img src="<?=Yii::$app->homeUrl?>/images/mobile/performance/img_banner.jpg" alt="">
				</a>
			</section>
			<!-- // banner -->
		</div>
	</div>
<?endif?>

<?php echo $this->render('/partials/footer'); ?>
<?php echo $this->render('/model/scripts'); ?>

<?if(!$mobilePrefix):?>
<script>
	;
	(function(window, $, undefined) {
		$(function() {
			App.brand.init();
			App.brand.section.init('.Eq900 .section');

		});
	}(window, jQuery));

</script>
<?endif?>