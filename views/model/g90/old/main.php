<?
$this->title = "Genesis ".$this->params['modelName'];

$mobilePrefix = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?'m-':null;
?>
<?if(!$mobilePrefix):?><div id="container" class="Eq900">
	<nav class="sec-indicator">
		<div class="nav-inner">
			<div class="gnb-area">
				<ul>
					<li><a href="#Intro" title="" class="cur"><span>INTRO</span></a></li>
					<li><a href="#Design" title=""><span>DESIGN</span></a></li>
					<li><a href="#Comfort" title=""><span>COMFORT</span></a></li>
					<li><a href="#Safety" title=""><span>SAFETY</span></a></li>
					<li><a href="#Performance" title=""><span>PERFORMANCE</span></a></li>
					<li><a href="#360 view" title=""><span>360 VIEW</span></a></li>
				</ul>
				<span class="line"></span>
			</div>
		</div>
	</nav>
	<div class="inner-container">
		<div class="btn-down-wrap visible">
			<a class="btn-scroll" href="#">
				<span class="next">scroll</span>
				<strong class="title"></strong>
				<i></i>
			</a>
		</div>
		<section class="section module-intro-mov">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title">GENESIS G90</h2>
					<!-- <p class="desc">LUXURY EVOLVED</p> -->
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/genesis-eq900-highlights-kv.jpg" alt="">
					</figure>
					<div class="btn-qta">
						<a href="#"><span>КОНФИГУРАТОР</span></a>
						<a href="#"><span>НАЙТИ ДИЛЕРА</span></a>
						<a href="#"><span>ТЕСТ-ДРАЙВ</span></a>
					</div>
					<span class="copper-line"></span>
					<!-- <video playsinline="" muted="" preload="metadata">
						<source src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/genesis-eq900-hero_720p.mp4" type="video/mp4">
						<source src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/genesis-eq900-hero_720p.ogv" type="video/ogg">
						<source src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/genesis-eq900-hero_720p.webm" type="video/webm">
					</video> -->
				</div>
			</article>
		</section>
		<!-- design -->
		<section class="section module-skin3 title-type1 color-type2" data-hash="design">
			<article class="feature">
				<div class="brand-header">
					<h3 class="title">ДИЗАЙН</h3>
					<p class="desc">ГАРМОНИЯ СИЛЫ<br>И ИЗЯЩЕСТВА</p>
					<p class="btn-area">
						<a href="g90-design.html" title="" class="btn-more">
							<span class="def">LEARN MORE</span>
							<span class="over">GO TO DESIGN</span> <i></i>
						</a>
						<span class="over-box">
							<a href="#vr360" onclick="App.brand.section.sethash('#vr360');return false;">360 VIEW</a>
							<a href="g90-specs.html">SPECS</a>
						</span>
					</p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-highlight-design.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- //design -->
		<!-- comfort -->
		<section class="section module-skin3 title-type1 color-type2" data-hash="comport">
			<article class="feature">
				<div class="brand-header">
					<h3 class="title">КОМФОРТ</h3>
					<p class="desc">ПЕРЕДОВЫЕ ТЕХНОЛОГИИ<br>ДЛЯ БЕСКОМПРОМИССНОГО<br>КОМФОРТА</p>
					<p class="btn-area">
						<a href="g90-comfort.html" title="" class="btn-more">
							<span class="def">LEARN MORE</span>
							<span class="over">GO TO COMFORT</span> <i></i>
						</a>
						<span class="over-box">
								<a href="#vr360" onclick="App.brand.section.sethash('#vr360');return false;">360 VIEW</a>
								<a href="g90-specs.html">SPECS</a>
							</span>
					</p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-highlight-comfort.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- //comfort -->
		<!-- safety -->
		<section class="section module-skin3 title-type2 color-type2" data-hash="safety">
			<article class="feature">
				<div class="brand-header">
					<h3 class="title">БЕЗОПАСНОСТЬ</h3>
					<p class="desc">ЭВОЛЮЦИЯ<br>ФИЛОСОФИИ<br> БЕЗОПАСНОСТИ</p>
					<p class="btn-area">
						<a href="g90-safety.html" title="" class="btn-more">
							<span class="def">LEARN MORE</span>
							<span class="over">GO TO SAFETY</span> <i></i>
						</a>
						<span class="over-box">
								<a href="#vr360" onclick="App.brand.section.sethash('#vr360');return false;">360 VIEW</a>
								<a href="g90-specs.html">SPECS</a>
							</span>
					</p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-highlight-safety.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- //safety -->
		<!-- performance -->
		<section class="section module-skin3 title-type1 color-type2" data-hash="performance">
			<article class="feature">
				<div class="brand-header">
					<h3 class="title">УПРАВЛЯЕМОСТЬ</h3>
					<p class="desc">ГАРМОНИЯ СТАБИЛЬНОГО<br> УПРАВЛЕНИЯ И ИДЕАЛЬНОГО<br> ЕЗДОВОГО КОМФОРТА</p>
					<p class="btn-area">
						<a href="g90-performance.html" title="" class="btn-more">
							<span class="def">LEARN MORE</span>
							<span class="over">GO TO PERFORMANCE</span> <i></i>
						</a>
						<span class="over-box">
								<a href="#vr360" onclick="App.brand.section.sethash('#vr360');return false;">360 VIEW</a>
								<a href="g90-specs.html">SPECS</a>
							</span>
					</p>
				</div>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-highlight-performance.jpg" alt="">
					</figure>
				</div>
			</article>
		</section>
		<!-- //performance -->
		<section id="vr360" class="section module-vr360" data-hash="vr360">
			<article class="feature">
				<div class="brand-header">
					<h2 class="title blind">360 VIEW</h2>
					<span class="select-box">
						<label for=""><a href="#a">3.8Luxury</a></label>
						<span class="select-lst">
							<span class="lst-item"><a href="#a">3.8Luxury</a></span>
					<span class="lst-item"><a href="#a">3.8Premium Luxury</a></span>
					<span class="lst-item"><a href="#a">3.8Prestige</a></span>
					<span class="lst-item"><a href="#a">3.3TLuxury</a></span>
					<span class="lst-item"><a href="#a">3.3TPremium Luxury</a></span>
					<span class="lst-item"><a href="#a">3.3TPrestige</a></span>
					<span class="lst-item"><a href="#a">5.0Prestige</a></span>
					<span class="lst-item"><a href="#a">5.0Limousine Prestige</a></span>
					</span>
					</span>
					<span class="btn-spec"><a href="#">VIEW SPECS <span class="plus"></span></a>
					</span>
				</div>
				<div class="brand-content">
					<!-- EXTERIOR -->
					<div class="exterior-area cur">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-highlight_360view.jpg" alt="">
						</figure>
						<div class="info-desc">
							<p>*Above image may differ from actual spec and color. Visiting nearest dealer and checking actual vehicle are recommended.</p>
							<p>*Above image is based on representative trim and maybe updated without prior notice.</p>
							<p>* 3.8 Premium Luxury Signature Design Selection images will be updated.</p>
						</div>
						<div class="vr-ctrl">
							<button type="button"><span>VR 360</span></button>
						</div>
						<div id="exteriorCanvas" class="canvas"></div>
					</div>
					<!-- //EXTERIOR -->
					<!-- INTERIOR -->
					<div class="interior-area">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-highlight-interior-visual.jpg" alt="">
						</figure>
						<div class="info-desc">
							<p>*Above image may differ from actual spec and color. Visiting nearest dealer and checking actual vehicle are recommended.</p>
							<p>*Above image is based on representative trim and maybe updated without prior notice.</p>
							<p>* 3.8 Premium Luxury Signature Design Selection images will be updated.</p>
						</div>
						<div id="interiorCanvas" class="canvas"></div>
						<div class="vr-int-ctrl">
							<a href="#a" class="zoom-in">zoom in</a>
							<a href="#a" class="zoom-out">zoom out</a>
						</div>
					</div>
					<!-- //INTERIOR -->
					<div class="brand-info">
						<button type="button" class="open"><span>open</span></button>
						<div class="over-box">
							<p>*Above image may differ from actual spec and color. Visiting nearest dealer and checking actual vehicle are recommended.</p>
							<p>*Above image is based on representative trim and maybe updated without prior notice.</p>
							<p>* 3.8 Premium Luxury Signature Design Selection images will be updated.</p>
							<button type="button" class="close"><span>close</span></button>
						</div>
					</div>
				</div>
				<div class="brand-footer brand-color">
					<div class="select-background">
						<h3 class="tit-type1">BACKGROUND</h3>
						<div class="background-content">
							<ul>
								<li><a href="#a" data-background-color="sea" class="vr-info cur">
										<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/img_select_bg_thum1.jpg" alt=""></span>
										<span class="select-check"></span>
									</a></li>
								<li><a href="#a" data-background-color="forest" class="vr-info">
										<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/img_select_bg_thum2.jpg" alt=""></span>
										<span class="select-check"></span>
									</a></li>
							</ul>
						</div>
					</div>
					<div class="select-colors">
						<div class="colors-tab">
							<h3 class="exterior">
								<a href="#selectExterior" class="tit-type1 cur">EXTERIOR COLOR</a>
							</h3>
							<h3 class="interior">
								<a href="#selectInterior" class="tit-type1">INTERIOR COLOR</a>
							</h3>
						</div>
						<div class="colors-cont">
							<div id="selectExterior" class="exterior-content cur">
								<ul>
									<li>
										<a href="#a" data-color="red" class="cur vr-info">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-ext-color1.jpg" alt=""></span>
													<span class="txt">Marble White</span>
													<span class="select-check"></span>
												</span>
										</a>
									</li>
									<li>
										<a href="#a" data-color="blue" class="vr-info">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-ext-color2.jpg" alt=""></span>
													<span class="txt">Platinum Silver</span>
													<span class="select-check"></span>
												</span>
										</a>
									</li>
									<li>
										<a href="#a" class="vr-info">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-ext-color3.jpg" alt=""></span>
													<span class="txt">Graceful Gray</span>
													<span class="select-check"></span>
												</span>
										</a>
									</li>
									<li>
										<a href="#a" class="vr-info">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-ext-color4.jpg" alt=""></span>
													<span class="txt">Royal Blue</span>
													<span class="select-check"></span>
												</span>
										</a>
									</li>
									<li>
										<a href="#a" class="vr-info">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-ext-color5.jpg" alt=""></span>
													<span class="txt">Titanium Black</span>
													<span class="select-check"></span>
												</span>
										</a>
									</li>
								</ul>
							</div>
							<div id="selectInterior" class="interior-content">
								<ul>
									<li>
										<a href="#a" class="cur">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-int-color1.jpg" alt=""></span>
													<span class="txt">채스트넛 브라운/월넛</span>
													<span class="select-check"></span>
												</span>
										</a>
									</li>
									<li>
										<a href="#a">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-int-color2.jpg" alt=""></span>
													<span class="txt">채스트넛 브라운/바버나</span>
													<span class="select-check"></span>
												</span>
										</a>
									</li>
									<li>
										<a href="#a">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-int-color3.jpg" alt=""></span>
													<span class="txt">채스트넛 브라운/버취</span>
													<span class="select-check"></span>
												</span>
										</a>
									</li>
									<li>
										<a href="#a">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-int-color4.jpg" alt=""></span>
													<span class="txt">채스트넛 브라운/그레이 애쉬</span>
													<span class="select-check"></span>
												</span>
										</a>
									</li>
									<li>
										<a href="#a">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-int-color5.jpg" alt=""></span>
													<span class="txt">채스트넛 브라운/블랙 애쉬</span>
													<span class="select-check"></span>
												</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="btn-result">
						<a href="#a" class="result"><strong>next</strong> <i class="i-next"></i></a>
					</div>
				</div>
				<div class="brand-footer brand-last">
					<div class="btn-reset">
						<a href="#a"><i class="i-prev"></i> <strong>BACK</strong></a>
					</div>
					<div class="select-result">
						<div class="result-cont">
							<div class="result-cost col">
								<h3><a href="#" class="tit-type1 cur">GENESIS g90</a></h3>
								<span class="model">3.8 Luxury</span>
								<strong>KWR<em>75,000,000</em></strong>
								<a href="#" class="btn-cost">BUILD & PRICE</a>
							</div>
							<div class="size-area">
								<div class="result-place col cur">
									<h3><a href="#" class="tit-type1">YOUR NEAREST DEALER</a></h3>
									<div class="text">
										<strong class="result-title">서울 강남센트럴 지점<span>14.2km</span></strong>
										<!-- <p>서울특별시 강남구 강남대로 350 역삼 서울특별시 강남구 강남대로 350 역삼</p>
										<a href="#" class="view-detail">자세히 보기</a> -->
									</div>
								</div>
								<div class="result-event col">
									<h3><a href="#" class="tit-type1">CONSULTATION</a></h3>
									<strong class="result-title">PURCHASE CONSULTATION</strong>
									<!-- <p>당신의 특별한 날을 위한 제네시스만의 당신의 특별한 날을 위한 제네시스만의</p>
									<a href="#" class="view-detail">자세히 보기</a> -->
								</div>
							</div>
							<span class="place-arrow">
									<a href="#none" class="prev">prev</a>
									<a href="#none" class="next">next</a>
								</span>
						</div>
					</div>
					<div class="btn-result btn-more">
						<a href="<?=Yii::$app->homeUrl?>/<?=$this->params['model']?>/design">
							<strong>
								<em>g90</em><br>
								<span>SEE DETAILS</span>
							</strong>
							<i class="i-next"></i>
						</a>
					</div>
				</div>
			</article>
		</section>
	</div>
</div>
<?else:?>
	<div id="container">
		<div class="inner-container">
			<!-- <a class="btn-back" href="#" title="">BACK <i></i></a> -->
			<!-- kv type -->
			<section class="section m-module-intro-mov">
				<article class="feature">
					<div class="brand-header">
						<h2 class="title">GENESIS g90</h2>
						<p class="desc">LUXURY EVOLVED</p>
					</div>
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/img_feature_intro.jpg" alt="">
						</figure>
					</div>
					<div class="btn-qta">
						<a href="#"><span>КОНФИГУРАТОР</span></a>
						<a href="#"><span>НАЙТИ ДИЛЕРА</span></a>
						<a href="#"><span>ТЕСТ-ДРАЙВ</span></a>
					</div>
				</article>
			</section>
			<!-- // kv type -->
			<!-- desing -->
			<section class="section m-module-skin3 m-title-type1 color-type2" data-hash="design">
				<article class="feature">
					<div class="brand-header">
						<h2 class="title">ДИЗАЙН</h2>
						<p class="desc">ГАРМОНИЯ СИЛЫ<br>И ИЗЯЩЕСТВА</p>
						<p class="btn-area">
							<a href="g90-design-mobile.html" title="" class="btn-more">
								<span class="def">LEARN MORE</span>
								<span class="over">GO TO DESIGN</span> <i></i>
							</a>
							<span class="over-box">
								<a href="#vr360" onclick="goto_section('#vr360');return false;">360 VIEW</a>
								<a href="g90-specs-mobile.html">SPECS</a>
							</span>
						</p>
					</div>
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/img_feature_design.jpg" alt="">
						</figure>
					</div>
				</article>
			</section>
			<!-- //desing -->
			<!-- comfort -->
			<section class="section m-module-skin3 m-title-type2 color-type2" data-hash="comfort">
				<article class="feature">
					<div class="brand-header">
						<h2 class="title">КОМФОРТ</h2>
						<p class="desc">ПЕРЕДОВЫЕ ТЕХНОЛОГИИ<br>ДЛЯ БЕСКОМПРОМИССНОГО<br>КОМФОРТА</p>
						<p class="btn-area">
							<a href="g90-comfort-mobile.html" title="" class="btn-more">
								<span class="def">LEARN MORE</span>
								<span class="over">GO TO DESIGN</span> <i></i>
							</a>
							<span class="over-box">
								<a href="#vr360" onclick="goto_section('#vr360');return false;">360 VIEW</a>
								<a href="g90-specs-mobile.html">SPECS</a>
							</span>
						</p>
					</div>
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/img_feature_comfort.jpg" alt="">
						</figure>
					</div>
				</article>
			</section>
			<!-- //comfort -->
			<!-- safety -->
			<section class="section m-module-skin3 m-title-type2 color-type2" data-hash="safety">
				<article class="feature">
					<div class="brand-header">
						<h2 class="title">БЕЗОПАСНОСТЬ</h2>
						<p class="desc">ЭВОЛЮЦИЯ<br>ФИЛОСОФИИ<br> БЕЗОПАСНОСТИ</p>
						<p class="btn-area">
							<a href="g90-safety-mobile.html" title="" class="btn-more">
								<span class="def">LEARN MORE</span>
								<span class="over">GO TO DESIGN</span> <i></i>
							</a>
							<span class="over-box">
								<a href="#vr360" onclick="goto_section('#vr360');return false;">360 VIEW</a>
								<a href="g90-specs-mobile.html">SPECS</a>
							</span>
						</p>
					</div>
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/img_feature_safety.jpg" alt="">
						</figure>
					</div>
				</article>
			</section>
			<!-- //safety -->
			<!-- performance -->
			<section class="section m-module-skin3 m-title-type1 color-type2" data-hash="performance">
				<article class="feature">
					<div class="brand-header">
						<h2 class="title">УПРАВЛЯЕМОСТЬ</h2>
						<p class="desc">ГАРМОНИЯ СТАБИЛЬНОГО<br> УПРАВЛЕНИЯ И ИДЕАЛЬНОГО<br> ЕЗДОВОГО КОМФОРТА</p>
						<p class="btn-area">
							<a href="g90-performance-mobile.html" title="" class="btn-more">
								<span class="def">LEARN MORE</span>
								<span class="over">GO TO DESIGN</span> <i></i>
							</a>
							<span class="over-box">
								<a href="#vr360" onclick="goto_section('#vr360');return false;">360 VIEW</a>
								<a href="g90-specs-mobile.html">SPECS</a>
							</span>
						</p>
					</div>
					<div class="brand-content">
						<figure class="feature">
							<img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/img_feature_performance.jpg" alt="">
						</figure>
					</div>
				</article>
			</section>
			<!-- //performance -->
			<!-- vr360 -->
			<section class="section m-module-vr360 select-item" id="vr360" data-hash="vr360">
				<article class="feature">
					<div class="brand-header">
						<h2 class="title blind">360 VIEW</h2>
						<span class="select-box">
							<label for=""><a href="#a">3.8Luxury</a></label>
							<select>
								<option>3.8Luxury</option>
								<option>3.8Premium Luxury</option>
								<option>3.8Prestige</option>
								<option>3.3TLuxury</option>
								<option>3.3TPremium Luxury</option>
								<option>3.3TPrestige</option>
								<option>5.0Prestige</option>
								<option>5.0Limousine Prestige</option>
							</select>
						</span>
						<span class="btn-spec">
							<a href="#">VIEW SPECS <span class="plus"></span></a>
					</span>
					</div>
					<div class="brand-content">
						<!-- EXTERIOR -->
						<div class="exterior-area cur">
							<figure class="feature">
								<img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/eq900-highlight_360view.jpg" alt="">
							</figure>
							<div class="vr-ctrl">
								<button type="button"><span>VR 360</span></button>
							</div>
							<div id="exteriorCanvas" class="canvas"></div>
						</div>
						<!-- //EXTERIOR -->
						<!-- INTERIOR -->
						<div class="interior-area">
							<figure class="feature">
								<img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/eq900-highlight-interior-visual.jpg" alt="">
							</figure>
							<div class="vr-ctrl">
								<button type="button"><span>VR 360</span></button>
							</div>
							<div id="interiorCanvas" class="canvas"></div>
							<div class="vr-int-ctrl">
								<a href="#a" class="zoom-in">zoom in</a>
								<a href="#a" class="zoom-out">zoom out</a>
								<a href="#a" class="return"><i></i><span>돌아가기</span></a>
							</div>
						</div>
						<!-- //INTERIOR -->
						<!-- active : "brand-info on" -->
						<div class="brand-info">
							<button type="button" class="open"><span>open</span></button>
							<div class="over-box">
								<p>*Above image may differ from actual spec and color. Visiting nearest dealer and checking actual vehicle are recommended.</p>
								<p>*Above image is based on representative trim and maybe updated without prior notice.</p>
								<p>* 3.8 Premium Luxury Signature Design Selection images will be updated.</p>
								<button type="button" class="close"><span>close</span></button>
							</div>
						</div>
					</div>
					<div class="brand-footer">
						<div class="select-background">
							<h3 class="tit-type1">BACKGROUND</h3>
							<div class="background-content">
								<ul>
									<li><a href="#a" data-background-color="sea" class="vr-info cur">
											<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/img_select_bg_thum1.jpg" alt=""></span>
											<span class="select-check"></span>
										</a></li>
									<li><a href="#a" data-background-color="forest" class="vr-info">
											<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/img_select_bg_thum2.jpg" alt=""></span>
											<span class="select-check"></span>
										</a></li>
								</ul>
							</div>
						</div>
						<div class="select-colors">
							<div class="colors-tab">
								<h3 class="exterior">
									<a href="#selectExterior" class="tit-type1 cur">EXTERIOR COLOR</a>
								</h3>
								<h3 class="interior">
									<a href="#selectInterior" class="tit-type1">INTERIOR COLOR</a>
								</h3>
							</div>
							<div class="colors-cont">
								<div id="selectExterior" class="exterior-content cur">
									<ul>
										<li>
											<a href="#a" data-color="red" class=" vr-info">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/eq900-ext-color1.jpg" alt=""></span>
													<span class="txt">Marble White</span>
													<span class="select-check"></span>
												</span>
											</a>
										</li>
										<li>
											<a href="#a" data-color="blue" class="vr-info">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/eq900-ext-color2.jpg" alt=""></span>
													<span class="txt">Platinum Silver</span>
													<span class="select-check"></span>
												</span>
											</a>
										</li>
										<li>
											<a href="#a" class="cur vr-info">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/eq900-ext-color3.jpg" alt=""></span>
													<span class="txt">Graceful Gray</span>
													<span class="select-check"></span>
												</span>
											</a>
										</li>
										<li>
											<a href="#a" class="vr-info">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/eq900-ext-color4.jpg" alt=""></span>
													<span class="txt">Royal Blue</span>
													<span class="select-check"></span>
												</span>
											</a>
										</li>
										<li>
											<a href="#a" class="vr-info">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/eq900-ext-color5.jpg" alt=""></span>
													<span class="txt">Titanium Black</span>
													<span class="select-check"></span>
												</span>
											</a>
										</li>
									</ul>
								</div>
								<div id="selectInterior" class="interior-content">
									<ul>
										<li>
											<a href="#a" class="cur">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/eq900-int-color1.jpg" alt=""></span>
													<span class="txt">채스트넛 브라운/월넛</span>
													<span class="select-check"></span>
												</span>
											</a>
										</li>
										<li>
											<a href="#a">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/eq900-int-color2.jpg" alt=""></span>
													<span class="txt">채스트넛 브라운/바버나</span>
													<span class="select-check"></span>
												</span>
											</a>
										</li>
										<li>
											<a href="#a">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/eq900-int-color3.jpg" alt=""></span>
													<span class="txt">채스트넛 브라운/버취</span>
													<span class="select-check"></span>
												</span>
											</a>
										</li>
										<li>
											<a href="#a">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/eq900-int-color4.jpg" alt=""></span>
													<span class="txt">채스트넛 브라운/그레이 애쉬</span>
													<span class="select-check"></span>
												</span>
											</a>
										</li>
										<li>
											<a href="#a">
												<span class="area">
													<span class="img"><img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/eq900-int-color5.jpg" alt=""></span>
													<span class="txt">채스트넛 브라운/블랙 애쉬</span>
													<span class="select-check"></span>
												</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="btn-result disabled">
							<a href="#a" class="result"><strong>next</strong></a>
						</div>
					</div>
					<!-- //색상 선택 -->
				</article>
			</section>
			<section class="section m-module-vr360 result-item">
			<span class="btn-select-back">
					<a href="#"><i></i> 다시 선택</a>
				</span>
				<div class="brand-content">
					<figure class="feature">
						<img src="<?=Yii::$app->homeUrl?>/images/mobile/highlight/eq900-highlight_360view.jpg" alt="">
					</figure>
					<div class="brand-info">
						<button type="button" class="open"><span>open</span></button>
						<div class="over-box">
							<p>*상기 이미지는 실제 차량과 사양 및 컬러가 다를 수 있으므로 전시장 방문 및 실차 확인을 권장합니다.</p>
							<p>*일부 이미지는 대표 등급 기준으로 연출되었으며 추후 사전예고 없이 변경이나 업데이트가 될 수 있습니다.</p>
							<button type="button" class="close"><span>close</span></button>
						</div>
					</div>
				</div>
				<div class="brand-footer">
					<div class="select-result">
						<div class="result-cont">
							<div class="result-cost">
								<div>
									<h3 class="tit-type2">GENESIS g90</h3>
									<span class="model">3.8 Luxury</span>
								</div>
								<span>
									<strong>KWR<em>75,000,000</em></strong>
									<a href="#" class="btn-cost">BUILD & PRICE</a>
								</span>
							</div>
							<div class="result-bottom slide-wrap">
								<div class="result-slide">
									<div class="result-place">
										<h3 class="tit-type1">YOUR NEAREST DEALER</h3>
										<div class="text">
											<strong class="result-title">서울 강남센트럴 지점<span>14.2km</span></strong>
											<p>서울특별시 강남구 강남대로 350 역삼 서울특별시 강남구 강남대로 350 역삼</p>
											<!-- <a href="#" class="view-detail">자세히 보기</a> -->
										</div>
									</div>
									<div class="result-place">
										<h3 class="tit-type1">CONSULTATION</h3>
										<strong class="result-title">PURCHASE CONSULTATION</strong>
										<p>당신의 특별한 날을 위한 제네시스만의 당신의 특별한 날을 위한 제네시스만의</p>
										<!-- <a href="#" class="view-detail">자세히 보기</a> -->
									</div>
								</div>
								<a href="#" class="btn-prev">prev</a>
								<a href="#" class="btn-next">next</a>
							</div>
						</div>
					</div>
					<div class="btn-result">
						<a href="#a"><strong class="ico-none">g90 SEE DETAILS</strong></a>
					</div>
				</div>
			</section>
		</div>
	</div>
<?endif?>

<?php echo $this->render('/partials/footer'); ?>
<?php echo $this->render('/model/scripts'); ?>

<script src="<?=Yii::$app->homeUrl?>/scripts/App.vr360.js"></script>

<?if($mobilePrefix):?>
<script>
	window.redPath = './images/mobile/highlight/blazing_red/';
	window.redCarData = [
		redPath + 'blazing_red_01.jpg',
		redPath + 'blazing_red_02.jpg',
		redPath + 'blazing_red_03.jpg',
		redPath + 'blazing_red_04.jpg',
		redPath + 'blazing_red_05.jpg',
		redPath + 'blazing_red_06.jpg',
		redPath + 'blazing_red_07.jpg',
		redPath + 'blazing_red_08.jpg',
		redPath + 'blazing_red_09.jpg',
		redPath + 'blazing_red_10.jpg',
		redPath + 'blazing_red_11.jpg',
		redPath + 'blazing_red_12.jpg',
		redPath + 'blazing_red_13.jpg',
		redPath + 'blazing_red_14.jpg',
		redPath + 'blazing_red_15.jpg',
		redPath + 'blazing_red_16.jpg',
		redPath + 'blazing_red_17.jpg',
		redPath + 'blazing_red_18.jpg',
		redPath + 'blazing_red_19.jpg',
		redPath + 'blazing_red_20.jpg',
		redPath + 'blazing_red_21.jpg',
		redPath + 'blazing_red_22.jpg',
		redPath + 'blazing_red_23.jpg',
		redPath + 'blazing_red_24.jpg',
		redPath + 'blazing_red_25.jpg',
		redPath + 'blazing_red_26.jpg',
		redPath + 'blazing_red_27.jpg',
		redPath + 'blazing_red_28.jpg',
		redPath + 'blazing_red_29.jpg',
		redPath + 'blazing_red_30.jpg',
		redPath + 'blazing_red_31.jpg',
		redPath + 'blazing_red_32.jpg',
		redPath + 'blazing_red_33.jpg',
		redPath + 'blazing_red_34.jpg',
		redPath + 'blazing_red_35.jpg',
		redPath + 'blazing_red_36.jpg'
	];
	window.bluePath = './images/mobile/highlight/G80/';
	window.blueCarData = [
		bluePath + '19464_Light_Off_00000.jpg',
		bluePath + '19464_Light_Off_00001.jpg',
		bluePath + '19464_Light_Off_00002.jpg',
		bluePath + '19464_Light_Off_00003.jpg',
		bluePath + '19464_Light_Off_00004.jpg',
		bluePath + '19464_Light_Off_00005.jpg',
		bluePath + '19464_Light_Off_00006.jpg',
		bluePath + '19464_Light_Off_00007.jpg',
		bluePath + '19464_Light_Off_00008.jpg',
		bluePath + '19464_Light_Off_00009.jpg',
		bluePath + '19464_Light_Off_00010.jpg',
		bluePath + '19464_Light_Off_00011.jpg',
		bluePath + '19464_Light_Off_00012.jpg',
		bluePath + '19464_Light_Off_00013.jpg',
		bluePath + '19464_Light_Off_00014.jpg',
		bluePath + '19464_Light_Off_00015.jpg',
		bluePath + '19464_Light_Off_00016.jpg',
		bluePath + '19464_Light_Off_00017.jpg',
		bluePath + '19464_Light_Off_00018.jpg',
		bluePath + '19464_Light_Off_00019.jpg',
		bluePath + '19464_Light_Off_00020.jpg',
		bluePath + '19464_Light_Off_00021.jpg',
		bluePath + '19464_Light_Off_00022.jpg',
		bluePath + '19464_Light_Off_00023.jpg',
		bluePath + '19464_Light_Off_00024.jpg',
		bluePath + '19464_Light_Off_00025.jpg',
		bluePath + '19464_Light_Off_00026.jpg',
		bluePath + '19464_Light_Off_00027.jpg',
		bluePath + '19464_Light_Off_00028.jpg',
		bluePath + '19464_Light_Off_00029.jpg',
		bluePath + '19464_Light_Off_00030.jpg',
		bluePath + '19464_Light_Off_00031.jpg',
		bluePath + '19464_Light_Off_00032.jpg',
		bluePath + '19464_Light_Off_00033.jpg',
		bluePath + '19464_Light_Off_00034.jpg',
		bluePath + '19464_Light_Off_00035.jpg',
	];
	var panoramaXML = '<xml version="1.0" encoding="UTF-8">';
	panoramaXML += '<panorama appversion="5.2 beta4" id="node1" apprev="15957">';
	panoramaXML += '  <input levelingpitch="0" levelbias="0.400" leveltilesize="510" levelbiashidpi="0.400" overlap="1" width="1620" leveltileurl="./xml/tiles/node1/cf_%c/l_%l/c_%x/tile_%y.jpg" levelingroll="0" height="1620">';
	panoramaXML += '    <level predecode="0" width="1620" height="1620" preload="0"/>';
	panoramaXML += '    <level predecode="0" width="810" height="810" preload="0"/>';
	panoramaXML += '    <level predecode="1" width="405" height="405" preload="1"/>';
	panoramaXML += '    <preview strip="1" color="0x808080"/>';
	panoramaXML += '  </input>';
	panoramaXML += '  <view pannorth="0" fovmode="0">';
	panoramaXML += '    <start fov="70" pan="0" tilt="0" projection="4"/>';
	panoramaXML += '    <flyin fov="170" pan="0" tilt="-90" projection="9"/>';
	panoramaXML += '    <min fov="50" pan="0" tilt="-90"/>';
	panoramaXML += '    <max fov="70" scaletofit="1" fovfisheye="360" pan="360" fovstereographic="270" tilt="90"/>';
	panoramaXML += '  </view>';
	panoramaXML += '  <userdata title="" description="" copyright="" customnodeid="" tags="Desktop" author="" datetime="2017-03-14 &#50724;&#51204; 10:22" info="" latitude="" longitude="" comment="" source=""/>';
	panoramaXML += '  <hotspots width="180" height="20" wordwrap="1">';
	panoramaXML += '    <label background="1" bordercolor="0x000000" textalpha="1" backgroundcolor="0xffffff" borderalpha="1" width="180" textcolor="0x000000" height="20" backgroundalpha="1" border="1" borderradius="1" wordwrap="1" enabled="1"/>';
	panoramaXML += '    <polystyle bordercolor="0x0000ff" backgroundcolor="0x0000ff" borderalpha="1" mode="0" handcursor="1" backgroundalpha="0.2509803921568627"/>';
	panoramaXML += '  </hotspots>';
	panoramaXML += '  <media/>';
	panoramaXML += '  <transition zoomoutpause="1" type="crossdissolve" zoomout="0" softedge="0" zoomspeed="2" zoomfov="20" blendtime="1" zoomin="0" enabled="0" blendcolor="0x000000"/>';
	panoramaXML += '  <control lockedwheel="0" invertwheel="0" invertcontrol="1" lockedkeyboard="0" simulatemass="1" rubberband="0" lockedmouse="0" dblclickfullscreen="0" contextprojections="0" hideabout="0" speedwheel="1" contextfullscreen="0" lockedkeyboardzoom="0" sensitivity="8"/>';
	panoramaXML += '</panorama>';
	panoramaXML += '';

	;
	(function(window, $) {
		$(function() {
			// vr 360.
			var $exteriorCanvas = $('#exteriorCanvas');
			var $exteriorController = $('.exterior-area .vr-ctrl');
			var $interiorController = $('.interior-area .vr-ctrl');
			var $interiorControlBtns = $('.interior-area .vr-int-ctrl');

			App.vr360 = {};
			App.vr360.interior = {}; //new pano2vrPlayer("interiorCanvas");
			App.vr360.interior.viewer = false;

			/*!
			 * 참고.
			 * exterior 360 호출하는 부분인 "App.brand.vr360"을 제외한 나머지 부분은
			 * 임의로 넣어놓은 부분입니다. 개발시 수정하여 사용하시면 됩니다.
			 */
			App.vr360.exterior = {
				viewer: false,
				color: 'red',
				background: 'sea',
				target: $exteriorCanvas,
				controller: $exteriorController,
				view: function() {
					var _this = this;
					var carModel = _this.color + '-' + _this.background;
					var carData;
					var stillcut = './images/mobile/highlight/eq900-highlight_360view.jpg';

					_this.controller.show();
					if (carModel == 'red-sea') {
						carData = redCarData;
					} else {
						carData = blueCarData;
					}

					/*!
					 * data {Object}
					 * data.name {String} - 고유의 데이터명.
					 * data.images {Array} - 이미지SRC 배열 값.
					 * data.stillcut {String} - 이미지SRC 배열 값.
					 * data.callback {Function} - 이미지로드 후 callback함수.
					 */
					App.brand.vr360.setup({
						name: carModel,
						images: carData, // 이미지 배열
						stillcut: stillcut, // 360이미지 로드전 보여질 스틸 이미지
						callback: function() {
							// console.log('---------------------------');
							// console.log( App.brand.vr360.getData() );
							_this.controller.hide();
							$exteriorCanvas.addClass('on');
							$('.exterior-area .feature > img').css('opacity', 0);
							goto_section('#vr360');
						}
					});
				},
				change: function(vrExColor, vrExBackground) {
					this.color = vrExColor;
					this.background = vrExBackground;
					if (App.vr360.exterior.viewer) {
						this.view();
					} else {
						var stillcut = './images/mobile/highlight/eq900-highlight_360view.jpg';
						$('.exterior-area .feature > img').css('opacity', 1);
						$('.exterior-area .feature > img').attr('src', stillcut);
					}
				}
			};

			$exteriorController.on('click', function() {
				App.vr360.exterior.viewer = true;
				App.vr360.exterior.view();
			});


			App.vr360.interior.on = function() {

				if (!App.vr360.interior.viewer) {
					App.vr360.interior = new pano2vrPlayer('interiorCanvas');
					App.vr360.interior.readConfigString(panoramaXML);
					$interiorControlBtns.addClass('on');
					$interiorController.hide();
					App.vr360.interior.viewer = true;
				}
				return false;
			}
			App.vr360.interior.off = function() {
				if (App.vr360.interior.viewer) {
					App.vr360.interior.removeHotspot('interiorCanvas');
					App.vr360.interior = {};
					$('#interiorCanvas').html('');
					$interiorControlBtns.removeClass('on');
					$interiorController.show();
					App.vr360.interior.viewer = false;
				}
				return false;
			}
			$interiorController.on('click', App.vr360.interior.on);
			$interiorControlBtns.find('a.return').on('click', App.vr360.interior.off);

			// 배경 및 외관색상 선택
			$('.brand-footer a.vr-info').on('click', function() {
				var $this = $(this);
				if (!$this.hasClass('.cur')) {

					var curIndex = $this.parent().index();
					var $links = $this.closest('ul').find('a');

					$links.removeClass('cur');
					$this.addClass('cur');

					var vrExColor = $this.data('color') || App.vr360.exterior.color;
					var vrExBackground = $this.data('background-color') || App.vr360.exterior.background;
					App.vr360.exterior.change(vrExColor, vrExBackground);

				}
				return false;
			});

			// 내장색상 선택
			$('.brand-footer .select-colors .interior-content a').on('click', function(e) {
				var _thisIndex = $(this).parent().index();
				var $elements = $('.brand-footer .select-colors .interior-content a');
				$elements.removeClass('cur').eq(_thisIndex).addClass('cur');
				e.preventDefault();
			});

			var eventPreventDefault = function(e) {
				e.preventDefault();
			}

			$('.select-item .btn-result').addClass('disabled').on('click.disabled', eventPreventDefault);

			var isResultFirst = false;
			var resultItemOpen = function() {
				$('.select-item').hide();
				$('.result-item').show();
				if (!isResultFirst) {
					isResultFirst = true;
					var oSlide = new Swipe($('.result-item .slide-wrap')[0]);
					$('.slide-wrap .btn-prev').on('click', function(e) {
						oSlide.prev();
						e.preventDefault();
					});
					$('.slide-wrap .btn-next').on('click', function(e) {
						oSlide.next();
						e.preventDefault();
					});
				}
				goto_section('#vr360');
				return false;
			}

			var resultItemClose = function() {
				$('.select-item').show();
				$('.result-item').hide();
				return false;
			}
			$('.btn-select-back>a').on('click', resultItemClose);


			// 내장 & 외장 색상 선택.
			$('.select-colors h3>a.tit-type1').on('click', function() {
				var $this = $(this);
				var $tabs = $('.select-colors h3>a.tit-type1');
				var $cont = $('.select-item .brand-content>div');
				var $tabCont = $('.select-colors .colors-cont>div');
				var index = $tabs.index(this);

				$('.select-item .btn-result').removeClass('disabled')
				.off('click.disabled').on('click', resultItemOpen);

				if (!$this.hasClass('cur')) {
					$tabs.removeClass('cur').eq(index).addClass('cur');
					$cont.removeClass('cur').eq(index).addClass('cur');
					$tabCont.removeClass('cur').eq(index).addClass('cur');

					// if ( $this.parent().hasClass('interior') ) {
					// 	App.vr360.interior.readConfigUrlAsync("./xml/pano.xml");
					// 	App.vr360.interior.readConfigString(panoramaXML);
					// }
					goto_section('#vr360');
				}

				return false;
			});

			// interrior vr360 +,-
			$('.interior-area .zoom-in').on('click', function() {
				var _this = App.vr360.interior;
				_this.moveTo(_this.getPan(), _this.getTilt(), _this.getFov() - 6);
				// App.vr360.interior.setFov( App.vr360.interior.getFov()-5 );
				return false;
			});
			$('.interior-area .zoom-out').on('click', function() {
				var _this = App.vr360.interior;
				_this.moveTo(_this.getPan(), _this.getTilt(), _this.getFov() + 6);
				// App.vr360.interior.setFov( App.vr360.interior.getFov()+5 );
				return false;
			});

			// 스펙보기 레이어 열기
			$('.btn-spec a').on('click', function(e) {
				toApp_showPopup('.popup-spec-view');
				return false;
			});
			// 스펙보기 레이어 닫기
			$('.popup-spec-view button.close').on('click', function() {
				toApp_hidePopup('.popup-spec-view');
				return false;
			});

			// 360 view 안내메세지
			$('.brand-info button').on('click', function() {
				if ($(this).hasClass('open')) {
					$('.brand-info').addClass('on');
				} else {
					$('.brand-info').removeClass('on');
				}
			});

			// Learn more 버튼클릭시
			$('.m-module-skin3 .btn-more').on('click', function() {
				var $area = $(this).parent();
				if (!$area.hasClass('on')) {
					$area.addClass('on');
					return false;
				}
			});

			// setInterval(function() {
			// 	var h = $('.m-module-vr360:visible .brand-footer').outerHeight()+20;
			// 	$('.brand-info').css('bottom', h);
			// }, 150);

			// var vrInfoSetPosition = function() {

			// };
			// vrInfoSetPosition();

		});
	}(window, jQuery));

</script>
<?else:?>
<script>
	/*! sample data */
	window.redPath = '<?=Yii::$app->homeUrl?>/images/desktop/highlight/blazing_red/';
	window.redCarData = [
		redPath + 'blazing_red_01.jpg',
		redPath + 'blazing_red_02.jpg',
		redPath + 'blazing_red_03.jpg',
		redPath + 'blazing_red_04.jpg',
		redPath + 'blazing_red_05.jpg',
		redPath + 'blazing_red_06.jpg',
		redPath + 'blazing_red_07.jpg',
		redPath + 'blazing_red_08.jpg',
		redPath + 'blazing_red_09.jpg',
		redPath + 'blazing_red_10.jpg',
		redPath + 'blazing_red_11.jpg',
		redPath + 'blazing_red_12.jpg',
		redPath + 'blazing_red_13.jpg',
		redPath + 'blazing_red_14.jpg',
		redPath + 'blazing_red_15.jpg',
		redPath + 'blazing_red_16.jpg',
		redPath + 'blazing_red_17.jpg',
		redPath + 'blazing_red_18.jpg',
		redPath + 'blazing_red_19.jpg',
		redPath + 'blazing_red_20.jpg',
		redPath + 'blazing_red_21.jpg',
		redPath + 'blazing_red_22.jpg',
		redPath + 'blazing_red_23.jpg',
		redPath + 'blazing_red_24.jpg',
		redPath + 'blazing_red_25.jpg',
		redPath + 'blazing_red_26.jpg',
		redPath + 'blazing_red_27.jpg',
		redPath + 'blazing_red_28.jpg',
		redPath + 'blazing_red_29.jpg',
		redPath + 'blazing_red_30.jpg',
		redPath + 'blazing_red_31.jpg',
		redPath + 'blazing_red_32.jpg',
		redPath + 'blazing_red_33.jpg',
		redPath + 'blazing_red_34.jpg',
		redPath + 'blazing_red_35.jpg',
		redPath + 'blazing_red_36.jpg'
	];
	window.bluePath = '<?=Yii::$app->homeUrl?>/images/desktop/highlight/lapis_blue/';
	window.blueCarData = [
		bluePath + 'lapis_blue_01.jpg',
		bluePath + 'lapis_blue_02.jpg',
		bluePath + 'lapis_blue_03.jpg',
		bluePath + 'lapis_blue_04.jpg',
		bluePath + 'lapis_blue_05.jpg',
		bluePath + 'lapis_blue_06.jpg',
		bluePath + 'lapis_blue_07.jpg',
		bluePath + 'lapis_blue_08.jpg',
		bluePath + 'lapis_blue_09.jpg',
		bluePath + 'lapis_blue_10.jpg',
		bluePath + 'lapis_blue_11.jpg',
		bluePath + 'lapis_blue_12.jpg',
		bluePath + 'lapis_blue_13.jpg',
		bluePath + 'lapis_blue_14.jpg',
		bluePath + 'lapis_blue_15.jpg',
		bluePath + 'lapis_blue_16.jpg',
		bluePath + 'lapis_blue_17.jpg',
		bluePath + 'lapis_blue_18.jpg',
		bluePath + 'lapis_blue_19.jpg',
		bluePath + 'lapis_blue_20.jpg',
		bluePath + 'lapis_blue_21.jpg',
		bluePath + 'lapis_blue_22.jpg',
		bluePath + 'lapis_blue_23.jpg',
		bluePath + 'lapis_blue_24.jpg',
		bluePath + 'lapis_blue_25.jpg',
		bluePath + 'lapis_blue_26.jpg',
		bluePath + 'lapis_blue_27.jpg',
		bluePath + 'lapis_blue_28.jpg',
		bluePath + 'lapis_blue_29.jpg',
		bluePath + 'lapis_blue_30.jpg',
		bluePath + 'lapis_blue_31.jpg',
		bluePath + 'lapis_blue_32.jpg',
		bluePath + 'lapis_blue_33.jpg',
		bluePath + 'lapis_blue_34.jpg',
		bluePath + 'lapis_blue_35.jpg',
		bluePath + 'lapis_blue_36.jpg'
	]

	;
	(function(window, $, undefined) {
		$(function() {
			App.brand.init();
			App.brand.section.init('.section');
			// App.brand.navigator.init();

			// vr 360.
			var $vrContainer = $('#vr360');
			var $exteriorCanvas = $('#exteriorCanvas');
			var $exteriorController = $('.exterior-area .vr-ctrl');

			// subnav ??.
			$vrContainer.on({
				inView: function() {
					var $subNavi = $('.sub-navi nav.pages li');
					$subNavi.removeClass('curr').eq(1).addClass('curr');
				},
				outView: function() {
					var $subNavi = $('.sub-navi nav.pages li');
					$subNavi.removeClass('curr').eq(0).addClass('curr');
				}
			})

			App.vr360 = {};
			App.vr360.interior = new pano2vrPlayer("interiorCanvas");
			App.vr360.interior.viewer = false;

			/*!
			 * ??.
			 * exterior 360 ???? ??? "App.brand.vr360"? ??? ??? ???
			 * ??? ???? ?????. ??? ???? ????? ???.
			 */
			App.vr360.exterior = {
				viewer: false,
				color: 'red',
				background: 'sea',
				target: $exteriorCanvas,
				controller: $exteriorController,
				view: function() {
					var _this = this;
					var carModel = _this.color + '-' + _this.background;
					var carData;
					var stillcut = '<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-highlight_360view.jpg';

					_this.controller.show();
					if (carModel == 'red-sea') {
						carData = redCarData;
					} else {
						carData = blueCarData;
					}

					/*!
					 * data {Object}
					 * data.name {String} - ??? ????.
					 * data.images {Array} - ???SRC ?? ?.
					 * data.stillcut {String} - ???SRC ?? ?.
					 * data.callback {Function} - ????? ? callback??.
					 */
					App.brand.vr360.setup({
						name: carModel,
						images: carData, // ??? ??
						stillcut: stillcut, // 360??? ??? ??? ?? ???
						callback: function() {
							// console.log('---------------------------');
							// console.log( App.brand.vr360.getData() );
							_this.controller.hide();
							$exteriorCanvas.addClass('on')
						}
					});
				},
				change: function(vrExColor, vrExBackground) {
					this.color = vrExColor;
					this.background = vrExBackground;
					if (App.vr360.exterior.viewer) {
						this.view();
					} else {
						var stillcut = '<?=Yii::$app->homeUrl?>/images/desktop/highlight/eq900-highlight_360view.jpg';
						$('.exterior-area .feature > img').attr('src', stillcut);
					}
				}
			};

			$exteriorController.on('click', function() {
				App.vr360.exterior.viewer = true;
				App.vr360.exterior.view();
			});

			// ?? ? ???? ??
			$('.brand-footer a.vr-info').on('click', function() {
				var $this = $(this);
				if (!$this.hasClass('.cur')) {

					var curIndex = $this.parent().index();
					var $links = $this.closest('ul').find('a');

					$links.removeClass('cur');
					$this.addClass('cur');

					var vrExColor = $this.data('color') || App.vr360.exterior.color;
					var vrExBackground = $this.data('background-color') || App.vr360.exterior.background;
					App.vr360.exterior.change(vrExColor, vrExBackground);

				}
				return false;
			});

			// ???? ??
			$('.brand-footer .select-colors .interior-content a').on('click', function(e) {
				var _thisIndex = $(this).parent().index();
				var $elements = $('.brand-footer .select-colors .interior-content a');
				$elements.removeClass('cur').eq(_thisIndex).addClass('cur');
				e.preventDefault();
			})


			// ?? & ?? ?? ??.
			$('.select-colors h3>a.tit-type1').on('click', function() {
				var $this = $(this);
				var $tabs = $('.select-colors h3>a.tit-type1');
				var $cont = $('#vr360 .brand-content>div');
				var $tabCont = $('.select-colors .colors-cont>div');
				var index = $tabs.index(this);
				if (!$this.hasClass('cur')) {
					$tabs.removeClass('cur').eq(index).addClass('cur');
					$cont.removeClass('cur').eq(index).addClass('cur');
					$tabCont.removeClass('cur').eq(index).addClass('cur');

					if ($this.parent().hasClass('interior')) {
						App.vr360.interior.readConfigUrlAsync("./xml/pano.xml");
					}
				}
				return false;
			});

			// interrior vr360 +,-
			$('.interior-area .zoom-in').on('click', function() {
				var _this = App.vr360.interior;
				_this.moveTo(_this.getPan(), _this.getTilt(), _this.getFov() - 6);
				// App.vr360.interior.setFov( App.vr360.interior.getFov()-5 );
				return false;
			});
			$('.interior-area .zoom-out').on('click', function() {
				var _this = App.vr360.interior;
				_this.moveTo(_this.getPan(), _this.getTilt(), _this.getFov() + 6);
				// App.vr360.interior.setFov( App.vr360.interior.getFov()+5 );
				return false;
			});

			// ???? ?? ??? ??? ??
			$('.btn-spec a').on('click', function(e) {
				toApp_showPopup('.popup-spec-view');

				// ??? ?? ??
				App.brand.pageMoveLock();
				return false;
			});

			// ??? ??
			$('.popup-spec-view button.close').on('click', function() {

				// ??? ?? ??
				App.brand.pageMoveUnLock();

				toApp_hidePopup('.popup-spec-view');
			});

			$('.result').on('click', function() {
				if (!$('.brand-last').is(':visible')) {
					$(this).parent().parent().fadeOut();
					$('.brand-last').fadeIn();
				}
			});

			$('.btn-reset a').on('click', function() {
				if (!$('.brand-color').is(':visible')) {
					$(this).parent().parent().fadeOut();
					$('.brand-color').fadeIn();
				}
			});

			$('.place-arrow a').on('click', function() {
				$('.select-result .size-area > div').toggleClass('cur');
			});

			var toggleIdx;
			var prevSizeMode;
			$toggleList = [];
			$(window).on('resize', function() {
				var winWidth = $(window).width();
				var sizeMode; //large or middle
				toggleIdx = 0;

				if (winWidth > 1440) {
					sizeMode = 'large';
					$toggleList = $('.col2');
				} else {
					sizeMode = 'middle';
					$toggleList = $('.col3');
				}

				if (prevSizeMode != sizeMode) {
					prevSizeMode = sizeMode;

					$toggleList.removeClass('on').eq(0).addClass('on');
				}
			}).trigger('resize');

			$('.place-arrow a').on('click', function(e) {
				if ($(this).hasClass('prev')) {
					(toggleIdx > 0) ? toggleIdx -= 1: toggleIdx = $toggleList.length - 1;
				} else {
					(toggleIdx < $toggleList.length - 1) ? toggleIdx += 1: toggleIdx = 0;
				}
				$toggleList.removeClass('on').eq(toggleIdx).addClass('on');

				e.preventDefault();
			});
		});
	}(window, jQuery));

</script>
<?endif?>