<?
$this->title = "Комфорт - GENESIS ".$this->params['modelName'];
$isMobile = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?true:false;
$mobilePrefix = $isMobile?'m-':null;


if($isMobile){
	include "../static/models/source/html/g90-comfort-mobile.html";
}else{
	include "../static/models/source/html/g90-comfort.html";
}
echo $this->render('/partials/footer');
echo $this->render('/model/scripts');
if($isMobile){
	//include "../static/models/source/html/inc/scripts-mobile.html";
}else{
	include "../static/models/source/html/inc/g90-scripts-comfort.html";
}