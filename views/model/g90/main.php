<?
$this->title = "Обзор - GENESIS ".$this->params['modelName'];

$isMobile = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?true:false;
$mobilePrefix = $isMobile?'m-':null;


if($isMobile){
	include "../static/models/source/html/g90-main-mobile.html";
}else{
	include "../static/models/source/html/g90-main.html";
}
echo $this->render('/partials/footer');
if($isMobile){
	include "../static/models/source/html/inc/popup-mobile.html";
}else{
	include "../static/models/source/html/inc/popup.html";
}

echo $this->render('/model/scripts');
if($isMobile){
	include "../static/models/source/html/inc/g90-scripts-main-mobile.html";
}else{
	include "../static/models/source/html/inc/g90-scripts-main.html";
}
?>
