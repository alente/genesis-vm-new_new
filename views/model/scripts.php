<?php if(\Yii::$app->devicedetect->isMobile()):?>
	<script src="<?=Yii::$app->homeUrl?>scripts/models-mobile.js"></script>
<?php else:?>
	<script src="<?=Yii::$app->homeUrl?>scripts/models-desktop.js"></script>
<?php endif?>