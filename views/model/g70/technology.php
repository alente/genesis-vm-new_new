<?
$this->title = "Technology - Genesis ".$this->params['modelName'];
$isMobile = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?true:false;
$mobilePrefix = $isMobile?'m-':null;


if($isMobile){
	include "../static/models/source/html/g70-technology-mobile.html";
}else{
	include "../static/models/source/html/g70-technology.html";
}
echo $this->render('/partials/footer');
echo $this->render('/model/scripts');
if($isMobile){
	include "../static/models/source/html/inc/g70-scripts-technology-mobile.html";
}else{
	include "../static/models/source/html/inc/g70-scripts-technology.html";
}