<?
$this->title = "Genesis G70";

$isMobile = (\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y')?true:false;
?>
<!-- browser upgrade and skip Navigation -->
<!--[if lt IE 8]>
<p class="browser-upgrade">
    You are using an outdated browser.
    <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.
</p>
<![endif]-->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="/js/three.min.js"></script>

<?if($isMobile):?><?endif?>

<section class="teaser <?if($isMobile):?>teaser-mobile<?endif?>">

	<?if($isMobile):?>
		<video src="/images/video/G70-m.mp4" playsinline></video>
		<style media="screen">
		.cssload-container {
			opacity: 0;
		}
		</style>
	<?else:?>
		<video src="/images/video/G70.mp4" autoplay preload="auto"></video>
		<style media="screen">
		.cssload-container {
			opacity: 1;
		}
		</style>
	<?endif?>


	<div class="cssload-container">
		<div class="cssload-speeding-wheel"></div>
	</div>

	<script type="text/javascript" defer>
		setTimeout(function () {

			var video = document.querySelector('.teaser video');
			var preloader = document.querySelector('.cssload-container');
			video.addEventListener( 'canplaythrough', function(event) {
				preloader.style.opacity = 0;
				setTimeout(function () {
					preloader.style.display = 'none';
				}, 0);
			});
		}, 10);
	</script>
	<style media="screen">
		.cssload-container {
			opacity: 0;
			text-align: center;
			position: absolute;
			z-index: 4;
			top: 0;
			left: 0;
			height: 100%;
			width: 100%;
			background: black;
			display: flex;
			align-items: center;
			justify-content: center;
			transition: all .5s ease;
		}

		.cssload-speeding-wheel {
			width: 49px;
			height: 49px;
			margin: 0 auto;
			border: 3px solid rgb(163,107,79);
			border-radius: 50%;
			border-left-color: transparent;
			border-right-color: transparent;
			animation: cssload-spin 875ms infinite linear;
				-o-animation: cssload-spin 875ms infinite linear;
				-ms-animation: cssload-spin 875ms infinite linear;
				-webkit-animation: cssload-spin 875ms infinite linear;
				-moz-animation: cssload-spin 875ms infinite linear;
			}



			@keyframes cssload-spin {
			100%{ transform: rotate(360deg); transform: rotate(360deg); }
			}

			@-o-keyframes cssload-spin {
			100%{ -o-transform: rotate(360deg); transform: rotate(360deg); }
			}

			@-ms-keyframes cssload-spin {
			100%{ -ms-transform: rotate(360deg); transform: rotate(360deg); }
			}

			@-webkit-keyframes cssload-spin {
			100%{ -webkit-transform: rotate(360deg); transform: rotate(360deg); }
			}

			@-moz-keyframes cssload-spin {
			100%{ -moz-transform: rotate(360deg); transform: rotate(360deg); }
			}
	</style>


  <button class="btn skipVideo">Пропустить</button>

	<?if($isMobile):?>
		<div class="fl play__container">
			<button class="btn play">
				Смотреть видео
				<svg height="512px" style="enable-background:new 0 0 512 512;" version="1.1" viewBox="0 0 512 512" width="512px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M144,124.9L353.8,256L144,387.1V124.9 M128,96v320l256-160L128,96L128,96z"/></g></svg>
			</button>
		</div>
	<?endif?>

  <div class="car fl">
    <div class="car__headlight fl">
      <div class="car__light fl"></div>
    </div>
  </div>
  <div class="content">
    <h1 class="headText">Новый Genesis g70</h1>
    <div class="headSubText">12 апреля. Испытайте его первым</div>
    <div class="textAlign-center testDrive__container">
      <a class="btn testDrive" href="#popup">
        Записаться на тест-драйв
      </a>
    </div>
  </div>

<div class="container__form">
    <div class="teaser-wrap">
        <div class="teaser__form form js-testDriveForm">
            <!-- include g70-teaser-success.html -->
            <div class="form__header">
                <a href="javascript:void(0);" class="teaser__close"></a>
                <div class="form__title">предварительная запись на&nbsp;тест&#8209;драйв</div>
                <div class="form__subtitle">genesis g70</div>
            </div>
            <div class="form__content">
                <form action="javascript:void(0);">
                    <div class="form__field">
                        <div class="form__element">

                            <!-- когда какой-то пункт выбран вешается класс .hasValue -->
                            <div class="inputSelect js-city" data-style="inputSelect">
                                <!-- визуальный инпут -->
                                <input class="inputSelect__input--hidden" name="city" type="hidden" value="">
                                <div class="inputSelect__input" data-value=""></div>
                                <span class="inputSelect__placeholder">Выберите город</span>
                                <span class="inputSelect__iconDown">
								<!-- место для иконки, когда список открывается на inputSelect вешается .opened -->
							</span>
                                <svg class="graphic graphic--madoka" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                                    <path d="m0,0l404,0l0,77l-404,0l0,-77z"></path>
                                </svg>
                                <div class="form__error">Выберите город</div>
                                <!-- Лист с опциями -->
                                <div class="inputSelect__list">
                                    <!-- опции -->

                                </div>
                            </div>
                            <!--  -->
                        </div>
                    </div>
                    <div class="form__field">
                        <div class="form__element">


                            <!-- когда какой-то пункт выбран вешается класс .hasValue -->
                            <div class="inputSelect js-dealer" data-style="inputSelect">
                                <!-- визуальный инпут -->
                                <input class="inputSelect__input--hidden" name="dealer" type="hidden" value="">
                                <div class="inputSelect__input" data-value=""></div>
                                <span class="inputSelect__placeholder">Выберите дилера</span>
                                <span class="inputSelect__iconDown">
								<!-- место для иконки, когда список открывается на inputSelect вешается .opened -->
							</span>
                                <svg class="graphic graphic--madoka" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                                    <path d="m0,0l404,0l0,77l-404,0l0,-77z"></path>
                                </svg>
                                <div class="form__error">Выберите дилера</div>
                                <!-- Лист с опциями -->
                                <div class="inputSelect__list"></div>

                            </div>
                            <!--  -->
                        </div>
                    </div>
                    <div class="form__field">
                        <div class="form__element js-name">
                            <input type="text" name="name" placeholder="Имя" class="form-control">
                            <svg class="graphic graphic--madoka" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                                <path d="m0,0l404,0l0,77l-404,0l0,-77z"></path>
                            </svg>
                            <div class="form__error">Введите имя</div>
                        </div>
                    </div>
                    <div class="form__field">
                        <div class="form__element js-secondName">
                            <input type="text" name="surname" placeholder="Фамилия" class="form-control">
                            <svg class="graphic graphic--madoka" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                                <path d="m0,0l404,0l0,77l-404,0l0,-77z"></path>
                            </svg>
                            <div class="form__error">Введите фамилию</div>
                        </div>
                    </div>
                    <div class="form__field">
                        <div class="form__element js-phone">
                            <input type="tel" name="phone" placeholder="+7(___)__-__-__" class="form-control">
                            <svg class="graphic graphic--madoka" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                                <path d="m0,0l404,0l0,77l-404,0l0,-77z"></path>
                            </svg>
                            <div class="form__error">Введите телефон</div>
                        </div>
                    </div>
                    <div class="form__field">
                        <div class="form__element js-mail">
                            <input type="email" name="email" placeholder="Email" class="form-control">
                            <svg class="graphic graphic--madoka" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                                <path d="m0,0l404,0l0,77l-404,0l0,-77z"></path>
                            </svg>
                            <div class="form__error">Введите Email</div>
                        </div>
                    </div>
                    <div class="form__field teaser__agreement">
                        <div class="form__element">
                            <label class="label label--checkbox js-agree">
                                <input type="checkbox">
                                <span class="icon--checkbox"></span>
                                <div class="label__text">Я даю согласие на обработку моих
                                    <a href="javascript:void(0);" class="label__link" onclick="document.querySelector('.overlayed').style.display = 'block'">персональных данных</a></div>
                                <div class="form__error">Для продолжения нужно согласие с условиями</div>
                            </label>
                        </div>
                    </div>
                    <div class="form__field">
                        <div class="form__element">
                            <button class="button js-button">отправить заявку</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


  <div class="form__submitMessage fl">
    <div class="teaser__success">
    	<a href="javascript:void(0);" class="teaser__close js-last-message-close"></a>
    	<div class="teaser__title">Ваша заявка принята</div>
    	<div class="teaser__subtitle">Менеджер выбранного диллекского центра<br/>свяжется с вами после 12 апреля</div>
    </div>

    <div style="display:none">
      <div>Ваша заявка принята</div>
      <div style="font-size:.5em">Менеджер выбранного дилерского центра</div>
      <div style="font-size:.5em">свяжется с вами после 12 апреля</div>
    </div>
  </div>
  <div class="overlayed legal-wrap" style="display:none;">
  	<div class="test-drive-popup test-drive-popup__legal">
  		<div class="closeIt">×</div>
  		<h2 class="skyblue">Правила обработки<br>персональных данных</h2>
  		<div class="clearfix legal_info_">
  			<p class="offers__text fleft">
  				Настоящим Я, в соответствии с требованиями Федерального закона от 27.07.09 №152-ФЗ «О персональных данных» даю свое согласие лично, своей волей и в своем интересе
  				на обработку (сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование, распространение, передачу (включая трансграничную передачу), обезличивание, блокирование и уничтожение) моих персональных данных, в том числе с использованием средств автоматизации.
  				<br><br>
  				Такое согласие мною даётся в отношении следующих персональных данных: фамилии, имя, отчество; контактный адрес электронной почты (e-mail); контактный телефон;
  				для определения потребностей в производственной мощности, мониторинга исполнения сервисными центрами гарантийной политики; ведения истории обращения в сервисные центры; проведения дилерами, дистрибьюторами, контрагентами маркетинговых исследований в области продаж, сервиса и послепродажного обслуживания;
  				для рекламных, исследовательских, информационных,
  			</p>
  			<p class="offers__text fleft">
  				а также иных целей, в том числе, путем осуществления
  				со мной прямых контактов по различным средствам связи.
  				<br><br>
  				Согласие дается Hyundai Motor Company (Хёндэ Мотор Компани, 231 Янгджи-Донг, Сеочо-Гу, Сеул, 137-938, Республика Корея), ООО «Хендэ Мотор СНГ» (г. Москва, ул.Тестовская, д.10), ООО «Хендэ Мотор МануфактурингPус» (197706, Санкт-Петербург, г. Сестрорецк, Левашовское ш.,
  				д. 20, литер А).
  				<br><br>
  				Я даю свое согласие передавать мои персональные данные для обработки исследовательским агентствам: ООО «Международный институт маркетинговых и социальных исследований «ГФК-Русь» (г. Москва, 9-я Парковая улица,
  				д. 48, корп. 4), ЗАО «Бизнес Аналитика МК» (г. Москва,
  				ул. Новослободская, д. 31, стр. 2); ЗАО «АвтоАссистанс»
  				(г. Москва, 2-й Южнопортовый проезд, д.18 корп. 2),
  				ООО «Ипсос» (г. Москва, Гамсоновскийпереулок, д. 5),
  				а также любым другим третьим лицам, для целей, указанных в настоящем согласии.
  			</p>
  		</div>
  		<!--<div class="closeIt">&times;</div>-->
  	</div>
  </div>
  <script type="text/javascript">
    document.querySelector('.overlayed').addEventListener( 'click', function(event) {
      if (event.target === this) this.style.display = 'none';
    });
    document.querySelector('.closeIt').addEventListener( 'click', function(event) {
      document.querySelector('.overlayed').style.display = 'none';
    });
  </script>

<?/*if(!$isMobile):*/?>
  <style>
  .overlayed {
    display: none;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.8);
    position: fixed;
    z-index: 200;
  }
  .test-drive-popup {
    background: #fff;
    position: absolute;
    width: 940px;
    height: 550px;
    top: 50%;
    left: 0;
    right: 0;
    margin: -275px auto 0;
    z-index: 150;
    box-sizing: border-box;
}.test-drive-popup__legal {
    padding: 45px 55px;
}
.closeIt {
    background: #9a9a9a none repeat scroll 0 0;
    box-sizing: border-box;
    color: #fff;
    cursor: pointer;
    font-size: 32px;
    height: 48px;
    padding: 0 0 0 15px;
    position: absolute;
    right: 0;
    top: 0;
    width: 48px;
    z-index: 155;
    transition: background 200ms;
    -webkit-transition: background 200ms;
}
.closeIt:hover {
    background: #9A5341;
}

.show-legal {
    bottom: 8px;
    display: block;
    font-size: 14px;
    left: 57px;
    position: relative;
    text-decoration: none !important;
    width: 124px;
    white-space: nowrap;
}
.legal-info {
    position: relative;
    margin-bottom: 75px;
}
.agree_rules:not(checked) {
    position: absolute;
    opacity: 0;
}
.agree_rules:not(checked) + label {
    color: #fff;
    display: inline-block;
    /*font-family: Arial !important;*/
    font-size: 14px;
    font-weight: normal;
    line-height: 30px;
    margin-right: 50px;
    padding: 0 0 0 58px;
    position: relative;
    cursor: pointer;
}
.agree_rules:not(checked) + label::before {
    background: transparent;
    border: 1px solid #505050;
    box-sizing: border-box;
    content: "";
    height: 40px;
    left: 0;
    position: absolute;
    top: 5px;
    transition: border-color 200ms ease 0s;
    width: 40px;
}
.agree_rules:not(checked) + label::after {
    background: transparent url("/images/icons/checked.png") no-repeat scroll 15px 18px;
    content: "";
    height: 40px;
    left: 0;
    opacity: 0;
    position: absolute;
    top: 0;
    transition: opacity 200ms ease 0s;
    width: 40px;
}
.agree_rules:checked + label::after {
    opacity: 1;
}
.agree_rules:hover + label::before {
    border: 1px solid #fff;
    background: #202020;
}
.controls__wrap {
    margin-bottom: 17px;
}
.send-req {
    background: #fff;
    color: #9A5341;
    cursor: pointer;
    /*font-family: "Arial";*/
    font-size: 12px;
    font-weight: 900;
    padding: 18px 0 14px;
    text-align: center;
    text-transform: uppercase;
    transition: background 200ms ease 0s;
    width: 290px;
}
.send-req:hover {
    background: #9A5341;
    color: #fff;
}
/*
    POP UP
    */
.gm-style .gm-style-iw__testdrive {
    width: 250px !important;
    padding: 30px !important;
    box-sizing: border-box;
}
.pop_text__test-drive > span {
    color: #959595;
}
/*
    MAP and LEGAL POPUP
    */
.test-drive-popup {
    background: #fff;
    position: absolute;
    width: 940px;
    height: 550px;
    top: 50%;
    left: 0;
    right: 0;
    margin: -275px auto 0;
    z-index: 150;
    box-sizing: border-box;
}
.test-drive-popup__map #map_canvas {
    height: 550px;
}
.test-drive-popup__legal {
    padding: 45px 55px;
}
.test-drive-popup__legal h2 {
    /*font-family: "ModernHBold";*/
    font-size: 30px;
    line-height: 1;
    margin-bottom: 25px;
}
.test-drive-popup__legal .offers__text{
    font-size: 13px;
    /*font-family: "Arial", sans-serif;*/
    width: 380px;
    color: #505050;
    line-height: 1;
    line-height: 1.385;
    text-align: left;
}
.offers__text {
  margin-top: 20px;
}
.test-drive-popup__legal .offers__text strong{
    font-weight: 800;
}
.test-drive-popup__legal .offers__text:last-child{
    margin-left: 70px;
}
.closeIt {
    background: #9a9a9a none repeat scroll 0 0;
    box-sizing: border-box;
    color: #fff;
    cursor: pointer;
    font-size: 32px;
    height: 48px;
    padding: 0 0 0 15px;
    position: absolute;
    right: 0;
    top: 0;
    width: 48px;
    z-index: 155;
    transition: background 200ms;
    -webkit-transition: background 200ms;
}
.closeIt:hover {
    background: #9A5341;
}
#test-drive-response {
    background: url('images/icons/check.jpg') no-repeat center 45px #fff;
    bottom: 0;
    box-sizing: border-box;
    height: 260px;
    left: 0;
    margin: auto;
    padding: 80px 0 40px;
    position: absolute;
    right: 0;
    top: 0;
    width: 465px;
    z-index: 150;
}
#test-drive-response h2 {
    font-family: "ModernH";
    font-size: 25px;
    font-weight: bold;
    line-height: 40px;
    margin-bottom: 5px;
    text-align: center;
}
#test-drive-response .text {
    color: #7e7e7e;
    line-height: 24px;
    text-align: center;
    display: none;
}
.overlayed {
    display: none;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,0.8);
    position: fixed;
    z-index: 200;
}
/*
    .overlayed.map-wrap {
      left: -999999px;
      display: block;
    }
    */

.layout-black {
    background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
    border: 0 none;
    display: none;
    height: 100%;
    left: 0;
    position: fixed;
    right: 0;
    top: 0;
    width: 100%;
    z-index: 1000;
}
.progress-ring-wrap {
    left: 50%;
    margin-left: -100px;
    margin-top: -100px;
    opacity: 1;
    top: 50%;
    z-index: 10000;
}
.legal-info::before {
    padding: 0 !important;
}
.progress-ring {
    animation: 1.5s linear 0s normal none infinite running spin360;
}
@keyframes spin360 {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}
@keyframes spin360 {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}
@keyframes spin360 {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}
.info-gather__col__car {
    display: none;
}
.dealer__location-dropdown,
    /*.selectMr__wrap,*/
.controls__wrap > input {
    width: 100%;
}
.info-gather__col {
    width:48% !important;
}
.dropdown-item,
.first {
    width: 100%;
}
/**{
          font-family: Arial !important;
        }*/
.legal-info {
    float:left;
}
.test_drive_car {
    display: block;
    margin: 20px auto;
    width:50%;
}
.test-drive-main {
    background-image: url("/images/test_drive_back.jpg");
    background-size: cover;
}
@media screen and (max-width: 573px){
    .test-drive-main {
        padding-top: 50px;
    }
}
@media only screen and (min-width : 574px) and (max-width : 769px) {
    .test-drive-main {
        padding-top: 50px;
    }
}
@media only screen and (max-width : 769px) {
	.test-drive-popup.test-drive-popup__legal {
		width: auto;
		margin: 0 20px;
		top:20px;
		bottom: 20px;
		height: auto;
		padding: 20px;
		display: flex;
		flex-direction: column;
	}
	.p.offers__text.fleft {
		width: auto;
	}
	.legal_info_ {
		overflow: scroll;
	}

	body .test-drive-popup__legal .offers__text:last-child {
		margin: 20px 0;
	}
}
@media screen and (min-width: 1270px){
    .test-drive-content {
        padding-top: 0px !important;
    }
}
.clearfix:after, .clearfix:before {
    content: " ";
    display: table;
    clear: both;
}


.button--link-inverted {
    border: none !important;
}
h2.skyblue {
    margin: 0 !important;
}
.test-drive-popup__legal .offers__text{
    font-size: 13px;
    /*font-family: "Arial", sans-serif;*/
    width: 380px;
    color: #505050;
    line-height: 1;
    line-height: 1.385;
    text-align: left;
}.test-drive-popup__legal .offers__text{
    font-size: 13px;
    /*font-family: "Arial", sans-serif;*/
    width: 380px;
    color: #505050;
    line-height: 1;
    line-height: 1.385;
    text-align: left;
}
.test-drive-popup__legal .offers__text strong{
    font-weight: 800;
}
.test-drive-popup__legal .offers__text:last-child{
    margin-left: 70px;
}.fleft {
    float: left;
}
.fright {
    float: right;
}h2, h3, h4 {
    text-transform: uppercase;
    font-family: 'uFontKitUi';
    line-height: 100%;
    font-family: "GenesisSansHead";
}.skyblue{
    color: #9A5341 !important;
}
</style>
<?/*endif*/?>


</section>



<?if($isMobile):?>
<?/*
    <style>
  .wrapper.fixed {
    z-index: 12;
  }
  .teaser {
    width: 100vw;
    height: 100%;
  }
  .fl {
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
  }
  .textAlign-center {
    text-align: center;
  }
  .content {
    font-family: 'HyundaiSansHead-Light';
    color: #fff;
    position: absolute;
    top:0;
    left:0;
    padding: 0 10px;
    padding-top: 50px;
    width: 100vw;
    height: calc(100% - 50px);
    box-sizing: border-box;
  }
  .testDrive__container {
    position: absolute;
    left: 0;
    bottom: 0;
    width: 100%;
  }
  .btn {
    z-index: 4;
    position: relative;
    text-transform: uppercase;
    display: inline-block;
    height: 40px;
    padding: 0 20px;
    background: rgba(0, 0, 0, .75);
    line-height: 40px;
    letter-spacing: 0.112em;
    text-align: center;
    font-size: 13px;
    color: #fff !important;
    font-family: 'HyundaiSansHead-Light';
    font-weight: 100;
    border: 1px solid #a36b4f;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    opacity: 0;
    transition: .3s ease;
  }
  .play {
    padding: 0 50px;
    z-index: 6;
  }
  .play__container {
    width: 100vw;
    height: 100%;
  }
  .play svg {
    fill: #a36b4f;
    width: 30px;
    height:30px;
    position: absolute;
    top:0;
    bottom:0;
    right: 10px;
    margin: auto;
  }
  .testDrive {
    transform: translateY(-5vh);
  }
  .headText {
    font-size: 6vmin;
    font-weight: 100;
    position: relative;
    z-index: 4;
    margin: 10vh 0 10px;
    letter-spacing: .2em;
    text-align: center;
    text-transform: uppercase;
  }
  .headSubText {
    font-size: 4vmin;
    position: relative;
    z-index: 4;
    text-align: center;
    text-transform: uppercase;
  }

  .testDrive:hover {
  background: rgba(0, 0, 0, .95);
  }
  body,html {
    height: 100%;
    margin: 0;
    background: #000;
    overflow: hidden;
  }
  .wrapper {
    height: 100%;
  }
  .car {
    background: #000 url(images/all/teaser1-m.png) center center no-repeat;
    background-size: contain ;
    width: 100vw;
    height: 100%;
    top: 0;
    left: 0;
    position: absolute;
    opacity: 0;
    transition:opacity 2s ease;
    z-index: 1;
  }
  .car__headlight,
  .car__light {
    height: 100%;
    width: 100%;
    opacity: 0;
  }
  .car__headlight {
    background: url(images/all/teaser2-m.png) center center no-repeat;
    transition: opacity .5s ease 1s;
  }
  .car__light {
    background: url(images/all/teaser3-m.png) center center no-repeat;
    transition: opacity .5s ease 2s;
  }
  .opa .car__headlight,
  .opa .car__light {
    opacity: 1;
    background-size: contain ;
  }

  section {
    background: inherit;
  }

  video {
    width: 100vw;
    height: calc(100% - 50px);
    top: 0;
    left: 0;
    margin-top: 50px;
    object-fit: cover;
    background: #000;
    position: absolute;
    z-index: 5;
  }

  canvas {
    position:absolute;
    top:0;
    left:0;
    opacity:0;
    transition:opacity 3s ease;
    opacity: 0;
    z-index: 3;
  }
  .show {
    opacity: .4;
  }

  .skipVideo {
    position: absolute;
    z-index: 6;
    bottom: 50px;
    right: 10px;
    border: none;
    text-decoration: underline;
    color: #a36b4f !important;
  }

</style>
*/?>
<?else:?>
<style>


body {
  height: 100vh;
  margin: 0;
  background: #000;
  position: fixed;
}

video {
  width: 100vw;
  height: 100vh;
  top: 0;
  left: 0;
  object-fit: cover;
  background: #000;
  position: absolute;
  z-index: 5;
}

canvas {
  position:absolute;
  top:0;
  left:0;
  opacity:0;
  transition:opacity 3s ease;
  opacity: 0;
  z-index: 3;
}
</style>
<?endif?>

<?if($isMobile):?>
<script type="text/javascript">

const play = document.querySelector('.play');
const skip = document.querySelector('.skipVideo');
const video = document.querySelector('video');

  setTimeout(function () {
    play.style.opacity = 1;
    setTimeout(function () {
      skip.style.opacity = 1;
    }, 300);
  }, 100);

  skip.addEventListener( 'click', event => {
    play.style.opacity = 0;
    setTimeout(function () {
      play.style.display = 'none';
    }, 400);
		var preloader = document.querySelector('.cssload-container');
		preloader.style.opacity = 0;
		preloader.style.display = 'none';

    skip.style.transition = 'all .5s ease';
    requestAnimationFrame( () => {
       requestAnimationFrame( () => {
         skip.style.opacity = 0;
       });
    });

    video.style.transition = 'all .5s ease';
    requestAnimationFrame( () => {
       requestAnimationFrame( () => {
         video.style.opacity = 0;
       });
    });
    video.addEventListener( 'transitionend', event => {
      video.style.display = 'none';
      endedFunc();
    });

  });

  let click;
  play.addEventListener( 'click', event => {
    if (click) return;
    click = true;
    play.style.opacity = 0;
		var preloader = document.querySelector('.cssload-container');
		preloader.style.opacity = 1;
    setTimeout(function () {
      play.style.display = 'none';
    }, 0);

    video.play().then(()=>{
      // video.currentTime = 21
    });
  });

  let endedFunc = event => {
    play.style.opacity = 0;
    setTimeout(function () {
      play.style.display = 'none';
    }, 400);

    skip.style.transition = 'all .5s ease';
    requestAnimationFrame( () => {
       requestAnimationFrame( () => {
         skip.style.opacity = 0;
       });
    });

    video.style.transition = 'all .5s ease';
    video.style.opacity = 0;
    video.addEventListener( 'transitionend', event => {
      video.style.display = 'none';
    });

    try {
      // smoke();
    } catch (e) {

    }


    setTimeout( async function() {

      document.querySelector('.car').style.opacity = 1;
      document.querySelector('.testDrive').style.opacity = 1;

      await new Promise( ( resolve, reject ) => {
         setTimeout(function () {
           resolve();

         }, 1500);
      });
      document.querySelector('canvas') && document.querySelector('canvas').classList.add('show');
      document.body.classList.add('opa');

    }, 0);

  };

  video.addEventListener( 'ended', endedFunc);

  function smoke() {
    var camera, scene, renderer, geometry, material, mesh;

    init();
    animate();

    function init() {

        clock = new THREE.Clock();

        renderer = new THREE.WebGLRenderer({ alpha: true });
        renderer.setSize( window.innerWidth, window.innerHeight );

        scene = new THREE.Scene();

        camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 10000 );
        camera.position.z = 1000;
        scene.add( camera );

        geometry = new THREE.CubeGeometry( 200, 200, 200 );
        material = new THREE.MeshLambertMaterial( { wireframe: false, transparent: true } );
        mesh = new THREE.Mesh( geometry, material );
        //scene.add( mesh );
        cubeSineDriver = 0;

        textGeo = new THREE.PlaneGeometry(300,300);
        THREE.ImageUtils.crossOrigin = ''; //Need this to pull in crossdomain images from AWS
        textTexture = THREE.ImageUtils.loadTexture('https://s3-us-west-2.amazonaws.com/s.cdpn.io/95637/quickText.png');
        textMaterial = new THREE.MeshLambertMaterial({color: 0x00ffff, opacity: 1, map: textTexture, transparent: true, blending: THREE.AdditiveBlending})
        text = new THREE.Mesh(textGeo,textMaterial);
        text.position.z = 800;
        //scene.add(text);

        light = new THREE.DirectionalLight(0xffffff,0.5);
        light.position.set(-1,0,1);
        scene.add(light);

        smokeTexture = THREE.ImageUtils.loadTexture('images/all/Smoke-Element.png');
        smokeMaterial = new THREE.MeshLambertMaterial({color: 0xffffff, map: smokeTexture, transparent: true});
        smokeGeo = new THREE.PlaneGeometry(300,300);
        smokeParticles = [];


        for (p = 0; p < 150; p++) {
            var particle = new THREE.Mesh(smokeGeo,smokeMaterial);
            particle.position.set(Math.random()*500-250,Math.random()*500-250,Math.random()*1000-100);
            particle.rotation.z = Math.random() * 360;
            scene.add(particle);
            smokeParticles.push(particle);
        }

        document.body.appendChild( renderer.domElement );

    }

    function animate() {

        // note: three.js includes requestAnimationFrame shim

        delta = clock.getDelta();
        requestAnimationFrame( animate );
        evolveSmoke();
        render();

    }

    function evolveSmoke() {
        var sp = smokeParticles.length;
        while(sp--) {
            smokeParticles[sp].rotation.z += (delta * 0.2);
        }
    }

    function render() {

        mesh.rotation.x += 0.005;
        mesh.rotation.y += 0.01;
        cubeSineDriver += .01;
        mesh.position.z = 100 + (Math.sin(cubeSineDriver) * 500);
        renderer.render( scene, camera );

    }
  }
</script>
<?else:?>
<script type="text/javascript">
	var img = new Image();
	img.src = "/images/all/Smoke-Element.png";

	var smokeParticles;
	var video = document.querySelector('video');
	var skip = document.querySelector('.skipVideo');
  skip.addEventListener( 'click', function(event){
    skip.style.transition = 'all .5s ease';
    requestAnimationFrame(function(){
       requestAnimationFrame(function(){
         skip.style.opacity = 0;
       });
    });

    video.style.transition = 'all .5s ease';
    requestAnimationFrame(function(){
       requestAnimationFrame( function(){
         video.style.opacity = 0;
       });
    });
    video.addEventListener( 'transitionend', function(event){
      video.currentTime = video.duration;
      video.currentTime = video.duration;
      video.currentTime = video.duration;
      requestAnimationFrame(function(){
         requestAnimationFrame(function(){
            video.style.display = 'none';
            skip.style.display = 'none';
         });
      });
    });

  });

  let click;
  document.body.addEventListener( 'click', function(event){
    if (click) return;
    click = true;

    video.play()/*.then(function(){
      // video.currentTime = 21
    })*/;
  });


  video.addEventListener( 'ended', function(event){
    skip.style.transition = 'all .5s ease';
    requestAnimationFrame(function(){
       requestAnimationFrame(function(){
         skip.style.opacity = 0;
       });
    });

    video.style.transition = 'all .5s ease';
    video.style.opacity = 0;
    video.addEventListener( 'transitionend', function(event){
      video.style.display = 'none';
    });


    try {
      //smoke();
    } catch (e) {

    } finally {

    }


    //setTimeout( async function() {

      document.querySelector('.car').style.opacity = 1;
      document.querySelector('.testDrive').style.opacity = 1;

		setTimeout(function(){
			document.querySelector('canvas') && document.querySelector('canvas').classList.add('show');
			document.body.classList.add('opa');
		}, 1500);

      /*await new Promise( ( resolve, reject ) => {
         setTimeout(function () {
           resolve();
         }, 1500);
      });
      document.querySelector('canvas') && document.querySelector('canvas').classList.add('show');
      document.body.classList.add('opa');*/

    //}, 0);

  });
  function smoke(){
    var camera, scene, renderer, geometry, material, mesh;

	var smokeLoaded, img = new Image();

	//img.onload = function() {
		init();
		animate();
		//loaded = true;
	//};
	//img.src = '/images/all/Smoke-Element.png';

    function init() {

        clock = new THREE.Clock();

        renderer = new THREE.WebGLRenderer({ alpha: true });
        renderer.setSize( window.innerWidth, window.innerHeight );

        scene = new THREE.Scene();

        camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 10000 );
        camera.position.z = 1000;
        scene.add( camera );

        geometry = new THREE.BoxGeometry( 200, 200, 200 );
        material = new THREE.MeshLambertMaterial( { wireframe: false, transparent: true } );
        mesh = new THREE.Mesh( geometry, material );
        //scene.add( mesh );
        cubeSineDriver = 0;

        textGeo = new THREE.PlaneBufferGeometry(300,300);
        THREE.ImageUtils.crossOrigin = ''; //Need this to pull in crossdomain images from AWS
        textTexture = THREE.ImageUtils.loadTexture('https://s3-us-west-2.amazonaws.com/s.cdpn.io/95637/quickText.png');
        textMaterial = new THREE.MeshLambertMaterial({color: 0x00ffff, opacity: 1, map: textTexture, transparent: true, blending: THREE.AdditiveBlending})
        text = new THREE.Mesh(textGeo,textMaterial);
        text.position.z = 800;
        //scene.add(text);

        light = new THREE.DirectionalLight(0xffffff,0.5);
        light.position.set(-1,0,1);
        scene.add(light);

        smokeTexture = THREE.ImageUtils.loadTexture('/images/all/Smoke-Element.png');
        smokeMaterial = new THREE.MeshLambertMaterial({color: 0xffffff, map: smokeTexture, transparent: true});
        smokeGeo = new THREE.PlaneBufferGeometry(300,300);
		smokeMaterial.map.minFilter = THREE.LinearFilter;
        smokeParticles = [];


        for (p = 0; p < 150; p++) {
            var particle = new THREE.Mesh(smokeGeo,smokeMaterial);
            particle.position.set(Math.random()*500-250,Math.random()*500-250,Math.random()*1000-100);
            particle.rotation.z = Math.random() * 360;
            scene.add(particle);
            smokeParticles.push(particle);
        }

        document.body.appendChild( renderer.domElement );

    }

    function animate() {

        // note: three.js includes requestAnimationFrame shim

        delta = clock.getDelta();
        requestAnimationFrame( animate );
        evolveSmoke();
        render();

    }

    function evolveSmoke() {
        var sp = smokeParticles.length;

		//console.log(123);

        while(sp--) {

            smokeParticles[sp].rotation.z += (delta * 0.2);
        }
    }

    function render() {

        mesh.rotation.x += 0.005;
        mesh.rotation.y += 0.01;
        cubeSineDriver += .01;
        mesh.position.z = 100 + (Math.sin(cubeSineDriver) * 500);
        renderer.render( scene, camera );

    }
  }
</script>
<?endif?>
<!-- Layer -->
<div class="popup popup-spec-view hide">
	<div class="ly-head">
		<h2 class="ly-title">3.8 럭셔리</h2>
	</div>
	<div class="ly-content">
		<table class="tbl-spec">
			<caption>3.8럭셔리 스펙정보</caption>
			<tbody>
				<tr>
					<td>
						<div class="td-area">
							<i class="icon1"></i>
							<dl class="info">
								<dt>엔진형식</dt>
								<dd>3.8 GDI <span>V6람다</span></dd>
							</dl>
						</div>
					</td>
					<td>
						<div class="td-area">
							<i class="icon2"></i>
							<dl class="info">
								<dt>구동방식</dt>
								<dd>2WD/AWD</dd>
							</dl>
						</div>
					</td>
					<td>
						<div class="td-area">
							<i class="icon2"></i>
							<dl class="info">
								<dt>변속기</dt>
								<dd>8단 <span>자동변속기</span></dd>
							</dl>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="td-area">
							<i class="icon4"></i>
							<dl class="info">
								<dt>최대토크</dt>
								<dd>40.5 <span>kg.m /5000rpm</span></dd>
							</dl>
						</div>
					</td>
					<td>
						<div class="td-area">
							<i class="icon5"></i>
							<dl class="info">
								<dt>엔진형식</dt>
								<dd>315 <span>PS/6000 rpm</span></dd>
							</dl>
						</div>
					</td>
					<td>
						<div class="td-area">
							<i class="icon6"></i>
							<dl class="info">
								<dt>엔진형식</dt>
								<dd>245/50R18 <span>미쉐린</span></dd>
							</dl>
						</div>
					</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="3" class="payment">
						<strong>75,000,000</strong>
						<span>원</span>
					</td>
				</tr>
			</tfoot>
		</table>
		<div class="detail-info">
			<p class="tit"><strong>KEY SPEC</strong></p>
			<ul>
				<li>- HID 헤드램프(다이나믹 벤딩 기능), 245/50R18 미쉐린 타이어 &amp; 휠</li>
				<li>- 7인치 TFT LCD 클러스터, 전자식 변속레버(SBW)</li>
				<li>- 9 에어백 시스템(운전석 &amp; 동승석 어드밴스드 에어백, 운전석 무릎 에어백, 앞 &amp; 뒤 사이드 에어백, 전복 대응 커튼 에어백)</li>
				<li>- 앞좌석 스마트폰 무선 충전 시스템, 버튼시동 &amp; 스마트키 시스템, 카드타입 스마트키, 터치타입 아웃사이드 도어 핸들, 세이프티 언락, 스마트 전동식 트렁크</li>
				<li>- 운전석 전동시트(럼버써포트, 쿠션 익스텐션 포함), 동승석 전동시트(워크인 스위치, 높이조절 포함), 전동식 뒷면 유리커튼</li>
				<li>- DIS(블루링크2.0, 12.3인치 파노라믹 디스플레이, 블루투스 핸즈프리), 렉시콘 프리미엄 사운드(14스피커, 퀀텀 로직 서라운드), DVD 플레이어</li>
			</ul>
		</div>
	</div>
	<button class="close"><span class="blind">close</span></button>
</div>
<div id="overlay" class="hide"></div>


<?if($isMobile):?>
	<script src="/scripts/models-mobile.js"></script>
<?else:?>
	<script src="/scripts/models-desktop.js"></script>
<?endif?>
