<?
use \app\models\Contacts;
$this->title = 'Контакты - Восток Моторс';
	$params = require("../config/params-dealer.php");
?>
    <link type="text/css" rel="stylesheet" href="css/build.css"/>
    <?/*<link type="text/css" rel="stylesheet" href="styles/header/header.css">*/?>
    <script>
        var dealerData = <?=json_encode($params['dealer'])?>;
    </script>
<div class="layout__content">
        <div class="dealer-near-you">
            <div class="dealer-near-you__heading">Контакты</div>
            <div class="dealer-near-you__content">

        <div class="section">
          <div class="layout__wrapper">
            <div class="page__content">
			
              <div class="warranty-cont">
                <div class="warranty-cont-in" style="padding-top:0">
                  <h2><?=Contacts::getParam('contactsName')?></h2>
                  <p class="warranty-p01">
                    <?=Contacts::getParam('contactsAddresses')?>
                  </p>
                </div>
                <div class="warranty-cont-in" style="padding-bottom:0">
                  <h2>Телефон</h2>
                  <p class="warranty-p01">
                    <a href="tel:<?=Contacts::getParam('contactsPhoneLink')?>"><span class="callibri_phone"><?=Contacts::getParam('contactsPhone')?></span></a>
                  </p>
                </div>
				
                <div class="warranty-cont-in" style="padding-bottom:0">
                  <h2>Часы работы</h2>
                  <p class="warranty-p01">
                    <?=Contacts::getParam('contactsHours')?>
                  </p>
                </div>
				
				
              </div>

			</div>
		  </div>
		</div>
		
		
		
		
			<link type="text/css" rel="stylesheet" href="css/workers.css"/>
			<?php /*<div class="section section--workers-x-cont">
				<div class="section__content workers-x-cont is-current" id="workers-sales">

								<div class="workers-item-content">
									<div class="worker-avatar"><img src="images/workers/grigoriev.jpg" class="worker-avatar__img js-ofit" alt="Григорьев"></div>									<div class="worker-name">Сергей<br>Григорьев</div>
									<style>.worker-post {margin-bottom:25px} .worker-post:after {display:none}</style>
									<div class="worker-post">Менеджер отдела продаж</div>
									<div class="worker-contacts">
										<a href="mailto:sergey.grigoryev@hyundai-vostokmotors.ru" class="worker-mail">sergey.grigoryev@hyundai-vostokmotors.ru</a>										<a href="tel:83452521070" class="worker-phone"><span class="callibri_phone">8 (3452) 521-070</span></a>									</div>
								</div>

					<div class="workers-feedback-single"></div>
				</div>
			</div>*/ ?>
		
		
			

               <div id="map"></div>

            </div>
        </div>
    </div>
<div class="top-btn">
    Наверх
</div>
<div class="mobileDetector" id="mobileDetector"></div>
<script src="js/build.js" charset="utf-8"></script>
<!-- Add scripts here if needed custom for a page as-->
<script src="https://maps.googleapis.com/maps/api/js?key=<?=$params['dealer']['apiKey']?>"></script>
<script type="text/javascript" src="js/map.js"></script>