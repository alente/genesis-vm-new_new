
<?
use app\models\Products;
use app\models\ShopCategories;
?>


<?
use app\components\helpers\AdminTools;
?>

<div class="form">
    <? $form = \yii\bootstrap\ActiveForm::begin([
        "layout" => "horizontal",
        "enableAjaxValidation" => true,
        "enableClientValidation" => false,
        "fieldConfig" => [
            "horizontalCssClasses" => [
                "label" => "col-sm-2",
                "offset" => "col-sm-offset-4",
                "wrapper" => "col-sm-8",
                "error" => "",
                "hint" => "",
            ],
        ],
    ]); ?>

    <p class="note"><span class="required">*</span> Обязательно для заполнения.</p>

	<?=$form->field($model, "title")?>
    <?=$form->field($model, "on_category_page")->checkbox([], false)?>
	<?=$form->field($model, "hit")->checkbox([], false)?>
	<?=$form->field($model, "new")->checkbox([], false)?>
	<?=$form->field($model, "promo")->checkbox([], false)?>
	<?=$form->field($model, "popular")->checkbox([], false)?>
	<?=AdminTools::generateImageInputs($model, $form, "image")?>
    <?=$form->field($model, "xml_id")?>
    <?=$form->field($model, "price")?>
	<?=$form->field($model, "old_price")?>
    <?=$form->field($model, "price_fixed")->checkbox([], false)?>
	<?=AdminTools::generateRedactor($model, $form, "short_desc")?>
	<?=AdminTools::generateRedactor($model, $form, "full_desc")?>
	<?=AdminTools::generateSelect2($model, $form, "categories", ShopCategories::getDropDownList(), true)?>
	<?=AdminTools::generateSelect2($model, $form, "related_products", Products::getDropDownListSelf($model->id, false, true), true)?>
    <?=AdminTools::generateSelect2($model, $form, "colors", \app\models\ProductColor::getDropDownList(), true)?>
	<?=$form->field($model, "handle")?>
	<?=$form->field($model, "seo_title")?>
	<?=$form->field($model, "seo_description")->textarea()?>
	<?=$form->field($model, "seo_keywords")?>
	<?=$form->field($model, "public")->checkbox([], false)?>


    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=$form->errorSummary($model)?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=\yii\bootstrap\Html::submitButton("Сохранить", ["class" => "btn btn-primary", "name"=>"save"])?>
            <?=\yii\bootstrap\Html::submitButton("Применить", ["class" => "btn btn-primary", "name"=>"apply"])?>
            <?=\yii\bootstrap\Html::submitButton("Отмена", ["class" => "btn btn-default", "name"=>"cancel"])?>
        </div>
    </div>

    <? $form->end(); ?>

</div>
