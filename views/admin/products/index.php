<?
use app\components\helpers\BImages;
use yii\helpers\Html;
use app\models\Products;

$fields = [
    "id",
	[
        "attribute"  => "public",
        "content" => function ($data) {
            if ($data->public == 1) return "+";
            else if ($data->public == 0) return "-";
            else return "импорт";

        },
        "filter" => [1 => "Активные", 0 => "Неактивные", 2 => "Импорт"],
    ],
	'title',
	[
        "attribute"  => "image",
        "content" => function ($data) {
            return Html::img(BImages::doProp($data->image, 100, 100));
        },
        "filter" => false,
    ],
	'price',
    [
        "attribute"  => "promo",
        "content" => function ($data) {
            return $data->promo>0?"+":"-";
        },
        "filter" => [1 => "Активные", 0 => "Неактивные"],
    ],
    [
        "attribute"  => "on_category_page",
        "content" => function ($data) {
            return $data->on_category_page>0?"+":"-";
        },
        "filter" => [1 => "Активные", 0 => "Неактивные"],
    ],
    [
        "attribute"  => "categories",
        "content" => function ($data) {
            $result = '';
            foreach ($data->categories as $cat){
                $result = $result . $cat->title;
                if ($cat !== end($data->categories)) {
                    $result = $result . ' / ';
                }
            }
            return $result;
        },
        "filter" => false,
    ],
];
?>
<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item, 'searcher' => true])?>