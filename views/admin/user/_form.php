<?
    use app\components\helpers\AdminTools;
use app\models\base\User;
use app\models\Tags;
?>

<div class="form">
    <? $form = \yii\bootstrap\ActiveForm::begin([
        'layout' => 'horizontal',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            //'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <p class="note"><span class="required">*</span> Обязательно для заполнения.</p>

    <?=$form->field($model, 'username')?>
    <?=$form->field($model, 'name')?>
    <?=$form->field($model, 'email')?>
    <?=$form->field($model, 'newPassword')?>
    <?=AdminTools::generateImageInputs($model, $form, 'image')?>
    <?/* if (!$model->isNewRecord) { ?>
        <?=AdminTools::generateSelect2($model, $form, 'roles', User::getRolesArray(), true)?>
    <? } */?>
    <?=$form->field($model, 'public')->checkbox([], false)?>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=$form->errorSummary($model)?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=\yii\bootstrap\Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name'=>'save'])?>
            <?=\yii\bootstrap\Html::submitButton('Применить', ['class' => 'btn btn-primary', 'name'=>'apply'])?>
            <?=\yii\bootstrap\Html::submitButton('Отмена', ['class' => 'btn btn-default', 'name'=>'cancel'])?>
        </div>
    </div>

    <? $form->end(); ?>

</div><!-- form -->