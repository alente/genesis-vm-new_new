<?php
use app\components\helpers\BImages;
use yii\helpers\Html;
use app\models\City;

$fields = [
    "id",
	[
        "attribute"  => "public",
        "content" => function ($data) {
            return $data->public>0?"+":"-";
        },
        "filter" => [1 => "Активные", 0 => "Неактивные"],
    ],
	'title',
	'title_2',
	'title_3',
	'title_4',
	'title_5',
	'title_6',
	[
        "attribute"  => "is_default",
        "content" => function ($data) {
            return $data->is_default>0?"+":"-";
        },
        "filter" => [1 => "Вкл", 0 => "Выкл"],
    ],
	'handle',
	'weight',
];
?>
<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>