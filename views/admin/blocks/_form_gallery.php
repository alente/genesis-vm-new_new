<?
use app\components\helpers\AdminTools;
use app\models\Gallery;

$urlParams = [];
if ($this->context->pback) $urlParams['pback'] = $this->context->pback;
if ($this->context->partial) $urlParams['partial'] = 1;
?>

<div class="form">
    <? $form = \yii\bootstrap\ActiveForm::begin([
        'action' => ['admin/'.$this->context->mainmenuHandle.'/blocksave', 'id'=>$model->parent_id]+$urlParams,
        'layout' => 'horizontal',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            //'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?=AdminTools::generateSelect2($model, $form, 'gallery_id', Gallery::getDropDownList(false, true, 'Не выбрано', 'weight'))?>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=$form->errorSummary($model)?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=\yii\bootstrap\Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name'=>'save'])?>
            <?=\yii\bootstrap\Html::submitButton('Применить', ['class' => 'btn btn-primary', 'name'=>'apply'])?>
        </div>
    </div>

    <? $form->end(); ?>

</div><!-- form -->