<?

use app\models\base\Blocks;

$fields = [
    'id',
    [
        'attribute'  => 'public',
        'content' => function ($data) {
            return $data->public>0?'+':'-';
        },
        'filter' => [1 => 'Активные', 0 => 'Неактивные'],
    ],
    'title',
    [
        'attribute' => 'template_id',
        'content' => function($data) {
            return isset($data->template_id)?Blocks::$types[$data->template_id]:'';
        }
    ],
    'weight',
];
?>

<?=$this->render('//admin/base/index', ['fields' => $fields, 'dataProvider' => $dataProvider, 'item' => $item])?>