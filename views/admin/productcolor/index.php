<?
use app\components\helpers\BImages;
use yii\helpers\Html;
use app\models\ProductColor;

$fields = [
    "id",
	'title',
	[
        "attribute"  => "image",
        "content" => function ($data) {
            return Html::img(BImages::doProp($data->image, 100, 100));
        },
        "filter" => false,
    ],
	[
            "attribute" => "color_type_id",
            "content" => function($data) {
                if ($data->colorType) {
                    return $data->colorType->getTemplateValue("[title]");
                }
                return "(empty)";
            },
        ],
	'weight',
];
?>
<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>