<?
use app\components\helpers\BImages;
use yii\helpers\Html;
use app\models\CatalogPrice;

$fields = [
    "id",
	'price',
	[
            "attribute" => "price_id",
            "content" => function($data) {
                if ($data->pricetype) {
                    return $data->pricetype->getTemplateValue("[title]");
                }
                return "(empty)";
            },
        ],
];
?>
<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>