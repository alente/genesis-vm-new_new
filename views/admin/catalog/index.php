<?
use app\components\helpers\BImages;
use yii\helpers\Html;
use app\models\Catalog;

$fields = [
    "id",
	[
        "attribute"  => "public",
        "content" => function ($data) {
            return $data->public>0?"+":"-";
        },
        "filter" => [1 => "Активные", 0 => "Неактивные"],
    ],
	'title',
	'xml_id',
	[
            "attribute" => "category_id",
            "content" => function($data) {
                if ($data->category) {
                    return $data->category->getTemplateValue("[title]");
                }
                return "(empty)";
            },
        ],
	'handle',
	'weight',
];
?>
<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item, 'searcher' => true])?>