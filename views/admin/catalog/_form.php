
<?
use app\models\Catalog;
use app\models\Category;
?>


<?
use app\components\helpers\AdminTools;
?>

<div class="form">
    <? $form = \yii\bootstrap\ActiveForm::begin([
        "layout" => "horizontal",
        "enableAjaxValidation" => true,
        "enableClientValidation" => false,
        "fieldConfig" => [
            "horizontalCssClasses" => [
                "label" => "col-sm-2",
                "offset" => "col-sm-offset-4",
                "wrapper" => "col-sm-8",
                "error" => "",
                "hint" => "",
            ],
        ],
    ]); ?>

    <p class="note"><span class="required">*</span> Обязательно для заполнения.</p>

	<?=$form->field($model, "title")?>
	<?=AdminTools::generateRedactor($model, $form, "text")?>
	<?=$form->field($model, "xml_id")?>
	<?=$form->field($model, "xml_priceunit")?>
	<?=$form->field($model, "is_not_changable")->checkbox([], false)?>
	<?=AdminTools::generateSelect2($model, $form, "category_id", Category::find()->getDropDownList("[title]", "Не выбрано"))?>
	<?=AdminTools::generateSelect2($model, $form, "same", Catalog::getDropDownList(), true)?>
	<?=AdminTools::generateSelect2($model, $form, "related", Catalog::getDropDownList(), true)?>
	<?=AdminTools::generateSelect2($model, $form, "similar", Catalog::getDropDownList(), true)?>
	<?=AdminTools::generateSelect2($model, $form, "related_category", Category::getDropDownList(), true)?>
	<?=AdminTools::generateSelect2($model, $form, "similar_category", Category::getDropDownList(), true)?>
	<?=$form->field($model, "handle")?>
	<?=$form->field($model, "seo_title")?>
	<?=$form->field($model, "seo_description")->textarea()?>
	<?=$form->field($model, "seo_keywords")?>
	<?=$form->field($model, "weight")?>
	<?=$form->field($model, "public")->checkbox([], false)?>


    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=$form->errorSummary($model)?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=\yii\bootstrap\Html::submitButton("Сохранить", ["class" => "btn btn-primary", "name"=>"save"])?>
            <?=\yii\bootstrap\Html::submitButton("Применить", ["class" => "btn btn-primary", "name"=>"apply"])?>
            <?=\yii\bootstrap\Html::submitButton("Отмена", ["class" => "btn btn-default", "name"=>"cancel"])?>
        </div>
    </div>

    <? $form->end(); ?>

</div>
