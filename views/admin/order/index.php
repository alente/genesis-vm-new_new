<?
use app\components\helpers\BImages;
use yii\helpers\Html;
use app\models\Order;

$fields = [
    "id",
	'name',
	'phone',
	'comment',
	'count',
	'price',
	[
        "attribute" => "status",
        "content" => function($data) {
            return Order::$statusList[$data->status];
        },
    ],
	[
        "attribute" => "catalog_id",
        "content" => function($data) {
            if ($data->catalog) {
                return $data->catalog->getTemplateValue("[title]");
            }
            return "(empty)";
        },
    ],
    [
        "attribute" => "created_at",
        "content" => function($data) {
            return date('d.m.Y H:i:s', $data->created_at);
        },
    ],
];
?>
<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>