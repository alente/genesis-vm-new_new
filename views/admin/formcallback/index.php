<?
use app\components\helpers\BImages;
use yii\helpers\Html;
use app\models\Formcallback;

$fields = [
    "id",
	'title',
	'phone',
	'email',
	[
		"attribute" => "created_at",
		"content" => function($data) {
			return date('d.m.Y H:i:s', $data->created_at);
		},
	],
];
?>
<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>