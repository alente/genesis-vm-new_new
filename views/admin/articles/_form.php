<?
    use app\components\helpers\AdminTools;
?>

<div class="form">
    <? $form = \yii\bootstrap\ActiveForm::begin([
        'layout' => 'horizontal',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <p class="note"><span class="required">*</span> Обязательно для заполнения.</p>

    <?=$form->field($model, 'title')?>
    <?=$form->field($model, 'handle')?>
    <?=AdminTools::generateDatepicker($model, $form, 'date')?>
    <?=AdminTools::generateImageInputs($model, $form, 'image')?>
    <?=AdminTools::generateImageInputs($model, $form, 'image_preview')?>
    <?=$form->field($model, 'image_alt')?>
    <?=AdminTools::generateRedactor($model, $form, 'description')?>
    <?=AdminTools::generateRedactor($model, $form, 'text')?>
    <?=$form->field($model, 'title_seo')?>
    <?=$form->field($model, 'keywords_seo')?>
    <?=$form->field($model, 'description_seo')?>
	
    <?=$form->field($model, 'weight')?>
    <?=$form->field($model, 'public')->checkbox([], false)?>
    <?=$form->field($model, 'public_on_main')->checkbox([], false)?>
    
    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=$form->errorSummary($model)?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=\yii\bootstrap\Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name'=>'save'])?>
            <?=\yii\bootstrap\Html::submitButton('Применить', ['class' => 'btn btn-primary', 'name'=>'apply'])?>
            <?=\yii\bootstrap\Html::submitButton('Отмена', ['class' => 'btn btn-default', 'name'=>'cancel'])?>
        </div>
    </div>

    <? $form->end(); ?>

</div><!-- form -->