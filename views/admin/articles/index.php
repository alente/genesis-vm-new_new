<?
use app\components\helpers\BImages;
use yii\helpers\Html;

$fields = [
    'id',
    [
        'attribute'  => 'public',
        'content' => function ($data) {
            return $data->public>0?'+':'-';
        },
        'filter' => [1 => 'Активные', 0 => 'Неактивные'],
    ],
    [
        'attribute'  => 'public_on_main',
        'content' => function ($data) {
            return $data->public_on_main>0?'+':'-';
        },
        'filter' => [1 => 'Активные', 0 => 'Неактивные'],
    ],
    'title',
    'handle',
    [
        'attribute'  => 'image_preview',
        'content' => function ($data) {
            return Html::img(BImages::doProp($data->image_preview, 100, 100));
        },
        'filter' => false,
    ],
    'weight',
];
?>

<?=$this->render('//admin/base/index', ['fields' => $fields, 'dataProvider' => $dataProvider, 'item' => $item])?>
