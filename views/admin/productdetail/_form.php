
<?
use app\models\ProductDetail;
use app\models\ ProductAttribute;
?>


<?
use app\components\helpers\AdminTools;
?>

<div class="form">
    <? $form = \yii\bootstrap\ActiveForm::begin([
        "layout" => "horizontal",
        "enableAjaxValidation" => true,
        "enableClientValidation" => false,
        "fieldConfig" => [
            "horizontalCssClasses" => [
                "label" => "col-sm-2",
                "offset" => "col-sm-offset-4",
                "wrapper" => "col-sm-8",
                "error" => "",
                "hint" => "",
            ],
        ],
    ]); ?>

    <p class="note"><span class="required">*</span> Обязательно для заполнения.</p>

	<?=$form->field($model, "value")?>
    <?=AdminTools::generateSelect2($model, $form, "parent_id",  \app\models\Products::find()->getDropDownList("[title]", "Не выбрано"))?>
	<?=AdminTools::generateSelect2($model, $form, "attribute_id",  ProductAttribute::find()->getDropDownList("[title] - [ProductAttributeTitle]", "Не выбрано"))?>


    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=$form->errorSummary($model)?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=\yii\bootstrap\Html::submitButton("Сохранить", ["class" => "btn btn-primary", "name"=>"save"])?>
            <?=\yii\bootstrap\Html::submitButton("Применить", ["class" => "btn btn-primary", "name"=>"apply"])?>
            <?=\yii\bootstrap\Html::submitButton("Отмена", ["class" => "btn btn-default", "name"=>"cancel"])?>
        </div>
    </div>

    <? $form->end(); ?>

</div>
