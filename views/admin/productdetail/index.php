<?
use app\components\helpers\BImages;
use yii\helpers\Html;
use app\models\ProductDetail;

$fields = [
    "id",
	'value',
	'productAttributeTitle',
	'ProductTitle',
];
?>
<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>