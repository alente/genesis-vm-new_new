<?
    $fields = [
        'id',
        'code',
        'title',
        'value',
        'weight',
    ];
?>

<?=$this->render('//admin/base/index', ['fields' => $fields, 'dataProvider' => $dataProvider, 'item' => $item])?>
