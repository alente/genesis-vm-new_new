
<?
use app\models\Category;
?>


<?
use app\components\helpers\AdminTools;
?>

<div class="form">
    <? $form = \yii\bootstrap\ActiveForm::begin([
        "layout" => "horizontal",
        "enableAjaxValidation" => true,
        "enableClientValidation" => false,
        "fieldConfig" => [
            "horizontalCssClasses" => [
                "label" => "col-sm-2",
                "offset" => "col-sm-offset-4",
                "wrapper" => "col-sm-8",
                "error" => "",
                "hint" => "",
            ],
        ],
    ]); ?>

    <p class="note"><span class="required">*</span> Обязательно для заполнения.</p>

	<?=$form->field($model, "title")?>
	<?=AdminTools::generateImageInputs($model, $form, "image")?>
	<?=$form->field($model, "title2")?>
	<?=AdminTools::generateRedactor($model, $form, "text")?>
	<?=$form->field($model, "in_footer")->checkbox([], false)?>
	<?=$form->field($model, "is_not_changable")->checkbox([], false)?>
	<?=$form->field($model, "real_handle")?>
	<?=$form->field($model, "tree_handle")->checkbox([], false)?>
	<?=$form->field($model, "seo_title")?>
	<?=$form->field($model, "seo_description")->textarea()?>
	<?=$form->field($model, "seo_keywords")?>
	<?=$form->field($model, "public")->checkbox([], false)?>
	<? if($this->context->action->id == 'create') { ?>		<? $mn = $this->context->fullModelName; ?>		<?=$form->field($model, 'tree_parent_id')->dropDownList($mn::getDropDownList())?>	<? } ?>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=$form->errorSummary($model)?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=\yii\bootstrap\Html::submitButton("Сохранить", ["class" => "btn btn-primary", "name"=>"save"])?>
            <?=\yii\bootstrap\Html::submitButton("Применить", ["class" => "btn btn-primary", "name"=>"apply"])?>
            <?=\yii\bootstrap\Html::submitButton("Отмена", ["class" => "btn btn-default", "name"=>"cancel"])?>
        </div>
    </div>

    <? $form->end(); ?>

</div>
