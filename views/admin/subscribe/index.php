<?
use app\components\helpers\BImages;
use yii\helpers\Html;
use app\models\Subscribe;

$fields = [
    "id",
	[
        "attribute"  => "public",
        "content" => function ($data) {
            return $data->public>0?"+":"-";
        },
        "filter" => [1 => "Активные", 0 => "Неактивные"],
    ],
	'email',
];
?>
<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>