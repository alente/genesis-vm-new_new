<?
use app\components\helpers\BImages;
use yii\helpers\Html;
use app\models\Vacancy;

$fields = [
    "id",
	[
        "attribute"  => "public",
        "content" => function ($data) {
            return $data->public>0?"+":"-";
        },
        "filter" => [1 => "Активные", 0 => "Неактивные"],
    ],
	'title',
	[
            "attribute" => "city_id",
            "content" => function($data) {
                if ($data->city) {
                    return $data->city->getTemplateValue("[title]");
                }
                return "(empty)";
            },
        ],
	'weight',
];
?>
<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>