<?
use app\components\helpers\BImages;
use yii\helpers\Html;
use app\models\Property;

$fields = [
    "id",
	[
        "attribute"  => "public",
        "content" => function ($data) {
            return $data->public>0?"+":"-";
        },
        "filter" => [1 => "Активные", 0 => "Неактивные"],
    ],
	'title',
	[
        "attribute"  => "is_filter",
        "content" => function ($data) {
            return $data->is_filter>0?"+":"-";
        },
        "filter" => [1 => "Вкл", 0 => "Выкл"],
    ],
	[
        "attribute"  => "is_show",
        "content" => function ($data) {
            return $data->is_show>0?"+":"-";
        },
        "filter" => [1 => "Вкл", 0 => "Выкл"],
    ],
	'handle',
	'weight',
];
?>
<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item, 'searcher' => true])?>