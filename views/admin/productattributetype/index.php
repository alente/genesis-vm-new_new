<?
use app\components\helpers\BImages;
use yii\helpers\Html;
use app\models\ProductAttributeType;

$fields = [
    "id",
	'title',
	'code',
];
?>
<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>