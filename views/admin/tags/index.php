<?
use app\models\base\Tags;

$fields = [
    'id',
    [
        'attribute'  => 'public',
        'content' => function ($data) {
            return $data->public>0?'+':'-';
        },
        'filter' => [1 => 'Активные', 0 => 'Неактивные'],
    ],
    'title',
    'handle',
    [
        'attribute'  => 'type',
        'content' => function ($data) {
            return Tags::$tagTypes[$data->type];
        },
        'filter' => false,
    ],
    'weight',
];
?>

<?=$this->render('//admin/base/index', ['fields' => $fields, 'dataProvider' => $dataProvider, 'item' => $item])?>
