<?
use app\models\Blocks;

$fields = [
    'id',
    [
        'attribute'  => 'public',
        'content' => function ($data) {
            return $data->public>0?'+':'-';
        },
        'filter' => [1 => 'Активные', 0 => 'Неактивные'],
    ],
    'title',
    [
        'attribute'  => 'real_handle',
        'content' => function ($data) {
            return $data->real_handle . ' (<b>/' . $data->handle . '</b>)';
        },
    ],
    'description',
    'seo_title',
    'seo_description',
    'seo_keywords',
];
?>

<?=$this->render('//admin/base/index', ['fields' => $fields, 'dataProvider' => $dataProvider, 'item' => $item, 'tabId' => 'section'])?>
