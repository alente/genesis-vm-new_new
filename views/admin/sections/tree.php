<?
    $lastLvl = 1;
?>

<h1><?=$this->context->modelTitles[0]?>: Дерево</h1>

<ul class="content-tree">
    <? foreach ($tree as $treeElement) { ?>
        <? if($lastLvl < $treeElement->depth) { echo str_repeat('<ul>', 1); } ?>
        <? if($lastLvl > $treeElement->depth) { echo str_repeat('</ul>', $lastLvl - $treeElement->depth); } ?>
        <li class="<?=($treeElement->public==1)?'':'disabled'?>"><a href="<?=Yii::$app->urlManager->createUrl(['admin/'.$this->context->mainmenuHandle.'/update', 'id' => $treeElement->id])?>"><?=$treeElement->title?> [<?=$treeElement->handle?>]</a></li>
    <? $lastLvl = $treeElement->depth; } ?>

    <? if($lastLvl > 1) { echo str_repeat('</ul>', $lastLvl - 1); } ?>
</ul>