<div class="form">
    <? $form = \yii\bootstrap\ActiveForm::begin([
        'action' => ['admin/'.$this->context->mainmenuHandle.'/treeform', 'id'=>$model->id],
        'layout' => 'horizontal',
        'fieldConfig' => [
            //'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?=$form->field($treeForm, 'operation')->dropDownList($treeForm->operationList)?>

    <? $mn = $this->context->fullModelName; ?>
    <?=$form->field($treeForm, 'target_id')->dropDownList($mn::getDropDownList())?>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=$form->errorSummary($treeForm)?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=\yii\bootstrap\Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name'=>'save'])?>
            <?=\yii\bootstrap\Html::submitButton('Применить', ['class' => 'btn btn-primary', 'name'=>'apply'])?>
        </div>
    </div>

    <? $form->end(); ?>

</div><!-- form -->