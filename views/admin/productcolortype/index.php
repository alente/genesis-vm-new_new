<?
use app\components\helpers\BImages;
use yii\helpers\Html;
use app\models\ProductColorType;

$fields = [
    "id",
	'title',
	'code',
];
?>
<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>