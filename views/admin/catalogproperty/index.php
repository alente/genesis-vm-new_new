<?
use app\components\helpers\BImages;
use yii\helpers\Html;
use app\models\CatalogProperty;

$fields = [
    "id",
	[
        "attribute" => "property_id",
        "content" => function($data) {
            if ($data->property) {
                return $data->property->getTemplateValue("[title]");
            }
            return "(empty)";
        },
    ],
    'value',
	'weight',
];
?>
<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>