<h1><?=$this->context->modelTitles[0]?>: редактирование <?=isset($model->title)?$model->title:$model->id; ?></h1>

<? if (count($tabs) > 1) { ?>
    <ul class="nav nav-tabs">
        <? foreach($tabs as $tab) { ?>
            <li class="<?=$tab['active']?'active':''?>">
                <a href="<?=$tab['link']?>"><?=$tab['label']?></a>
            </li>
        <? } ?>
    </ul>
<? } ?>

<?=$content?>