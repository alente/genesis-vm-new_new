<?
use app\components\helpers\BImages;
use yii\helpers\Html;
use app\models\ShopCategories;

$fields = [
    "id",
	[
        "attribute"  => "public",
        "content" => function ($data) {
            return $data->public>0?"+":"-";
        },
        "filter" => [1 => "Активные", 0 => "Неактивные"],
    ],
	'title',
	[
        "attribute"  => "image",
        "content" => function ($data) {
            return Html::img(BImages::doProp($data->image, 100, 100));
        },
        "filter" => false,
    ],
	'handle',
	'weight',
];
?>
<?=$this->render("//admin/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>