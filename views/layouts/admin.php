<?php

use app\assets\AdminAsset;
use yii\helpers\Html;
use yii\helpers\Url;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
    <head>
        <?=Html::csrfMetaTags()?>
        <?php $this->head() ?>
    </head>

    <body>
        <?php $this->beginBody() ?>

        <header>
            <div class="row">
                <div class="col-sm-1"><a class="logo" href="<?=Url::home()?>"><img src="/images/common/logo_wite.png" alt="Genesis"></a></div>
                <div class="col-sm-8">
                    <ul class="topsections">
                        <li><a href="<?=Yii::$app->urlManager->createUrl(['admin/news/index'])?>">Новости и мероприятия</a></li> 
                        <li><a href="<?=Yii::$app->urlManager->createUrl(['admin/articles/index'])?>">Статьи</a></li> 
                        <li><a href="<?=Yii::$app->urlManager->createUrl(['admin/contacts/index'])?>">Контакты</a></li>
                        <li><a href="<?=Yii::$app->urlManager->createUrl(['admin/workers/index'])?>">Наша команда</a></li>                       
                        <li><a href="<?=Yii::$app->urlManager->createUrl(['admin/settings/index'])?>">Настройки</a></li>
                        <li><a href="<?=Yii::$app->urlManager->createUrl(['admin/settings/import'])?>" target="_blank">Запустить импорт авто в наличии</a></li>

                    </ul>
                </div>
                <div class="col-sm-3">
                    <ul class="topmenu">
                        <li><a href="<?=Yii::$app->urlManager->createUrl(['site/logout'])?>">Выйти</a></li>
                    </ul>
                </div>
            </div>
        </header>

        <div class="pageContainer">
            <div class="row">
                <? if (count($this->context->leftMenu) > 0) { ?>
                    <div class="col-sm-2 sidebar">
                        <ul class="nav nav-sidebar">
                            <? foreach($this->context->leftMenu as $lm) { ?>
                                <? if(isset($lm['confirm'])) { ?>
                                    <li>
                                        <?=Html::a($lm['title'], $lm['link'], ['data-confirm' => Yii::t('yii', $lm['confirm'])])?>
                                    </li>
                                <? } else { ?>
                                    <li><a href="<?=$lm['link']?>" onclick="<?=$lm['onclick']?>"><?=$lm['title']?></a></li>
                                <? } ?>
                            <? } ?>
                        </ul>
                    </div>
                <? } ?>
                <div class="col-sm-<?=((count($this->context->leftMenu) > 0)?'10':'12')?>"><?=$content?></div>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
