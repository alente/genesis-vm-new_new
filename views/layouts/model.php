<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\widgets\Menu;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$this->beginPage();

$v=3;
?><!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <link rel="shortcut icon" type="image/x-icon" href="<?=Yii::$app->homeUrl?>images/favicon/favicon.ico"/>
    
    <title><?= Html::encode($this->title) ?></title>
	
	
	<?php if(\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y'):?>
        <link rel="stylesheet" href="<?=Yii::$app->homeUrl?>styles/models-mobile.css?v=<?=$v?>">
	<?php else:?>
        <link rel="stylesheet" href="<?=Yii::$app->homeUrl?>styles/models-desktop.css?v=<?=$v?>">
	<?php endif?>
	<link type="text/css" rel="stylesheet" href="<?=Yii::$app->homeUrl?>styles/header/header-model.css">
	
	
</head>

<body>

<div class="wrapper">
	<?php if(\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y'):?>
    <?else:?>
    <!-- header -->
    <!-- browser upgrade and skip Navigation -->
    <!--[if lt IE 8]>
    <p class="browser-upgrade">
        You are using an outdated browser.
        <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.
    </p>
    <![endif]-->
    <div id="skip-navi">
        <a href="#container"><span>Закрыть</span></a>
        <a href="#gnb-menu-open-btn"><span>Меню</span></a>
    </div>
    <?endif?>
    <?php /*<header class="header_2017">
        <span class="header-background"></span>
	    <?php if(\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y'):?>
		    <?php echo $this->render('/partials/menuMob'); ?>
	    <?php else:?>
            <?php echo $this->render('/partials/menu'); ?>
	    <?php endif?>
    </header>*/ ?>

        <span class="header-background"></span>
	    <?php if(\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y'):?>
			<header class="header_2017--mob">
				<?php echo $this->render('/partials/menuMob'); ?>
			</header>
	    <?php else:?>
			<header class="header_2017">
				<?php echo $this->render('/partials/menu'); ?>
			</header>
	    <?php endif?>
    
    <!-- //header -->

    <?/*if($this->params['model'] != 'g70'):*/?>
    <!-- sub navigator -->
    <div class="sub-navi">
        <div class="nav-inner">
	        <?
	        $menuItems = [];
	        $menuItems[] = [
		        'label' => 'ОБЗОР',
		        'url' => ['/'.$this->params['model'].'.html'],
		        'active' => $this->context->actionParams['page']=='main'
	        ];
	        if($this->params['model']=='g70'/* || $this->params['model']=='g90'*/){
		        $item = [
			        'label' => '360°',
			        'url' => ['/'.$this->params['model'].'.html#vr360'],
		
		        ];
		        if($this->context->actionParams['page'] == 'main'/* && !\Yii::$app->devicedetect->isMobile() && $_GET['mobile'] != 'y'*/){
			        $item['template'] = '<a href="{url}" onclick="App.brand.section.sethash(\'#vr360\');return false;">{label}</a>';
		        }
		        $menuItems[] = $item;
	        }
	        $menuItems[] = [
		        'label' => 'ДИЗАЙН',
		        'url' => ['/'.$this->params['model'].'-design.html'],
		        'active' => $this->context->actionParams['page']=='design'
	        ];
	        /*if($this->params['model']=='g90'){
		        $menuItems[] = [
			        'label' => 'G90 L',
			        'url' => ['/'.$this->params['model'].'l.html'],
			        'active' => $this->context->actionParams['page']=='l'
		        ];
	        }*/
	        if($this->params['model']=='g90'){
		        $menuItems[] = [
			        'label' => 'КОМФОРТ',
			        'url' => ['/'.$this->params['model'].'-comfort.html'],
			        'active' => $this->context->actionParams['page']=='comfort'
		        ];
	        }
	        $menuItems[] = [
		        'label' => 'БЕЗОПАСНОСТЬ',
		        'url' => ['/'.$this->params['model'].'-safety.html'],
		        'active' => $this->context->actionParams['page']=='safety'
	        ];
	        $menuItems[] = [
		        'label' => 'ПРОИЗВОДИТЕЛЬНОСТЬ',
		        'url' => ['/'.$this->params['model'].'-performance.html'],
		        'active' => $this->context->actionParams['page']=='performance'
	        ];
	        if($this->params['model']=='g80'){
		        $menuItems[] = [
			        'label' => 'ИННОВАЦИИ',
			        'url' => ['/'.$this->params['model'].'-innovation.html'],
			        'active' => $this->context->actionParams['page']=='innovation'
		        ];
	        }
	
	        if($this->params['model']=='g70'){
		        $menuItems[] = [
			        'label' => 'ТЕХНОЛОГИИ',
			        'url' => ['/'.$this->params['model'].'-technology.html'],
			        'active' => $this->context->actionParams['page']=='technology'
		        ];
	        }
	        $menuItems[] = [
		        'label' => 'ФОТО',
		        'url' => ['/'.$this->params['model'].'-gallery.html'],
		        'active' => $this->context->actionParams['page']=='gallery'
	        ];
	        /*$menuItems[] = [
				'label' => 'ХАРАКТЕРИСТИКИ',
				'url' => ['/'.$this->params['model'].'-specs.html'],
				'active' => $this->context->actionParams['page']=='specs'
			];*/
	
	        $activeLabel = "GENESIS ".strtoupper($this->params['model']);
	        if($isMobile){
		        foreach($menuItems as $menuItem){
			        if($menuItem['active']==true){
				        $activeLabel = $menuItem['label'];
				        break;
			        }
		        }
	        }
	        ?>
			<div class="nav on">
				<span class="gnb-ctrl"><button type="button">МЕНЮ</button></span>
				<a href="javascript:void(0);"><?=$activeLabel?></a>
			</div>
			<nav class="pages">
		        <?
		        echo Menu::widget([
			        'items' => $menuItems,
			        'activeCssClass'=>'curr',
		        ]);
		        ?>
	
			</nav>
	
			
            <div class="cta-area">
                <button class="btn"><span>ДОПОЛНИТЕЛЬНО</span></button>
	            <?php if(\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y'):?>
                    <div class="lst-wrap">
						<ul class="lst">
							<li><a href="/configurator"><span>КОНФИГУРАТОР</span></a></li>
							<li><a href="/test-drive"><span>ТЕСТ-ДРАЙВ</span></a></li>
								<?php  if($this->params['model']=='g70'){ ?>
						<li><a href="/dl/G70.pdf" target="_blank"><span>ПРАЙС-ЛИСТ</span></a></li>
						<?php } else  if($this->params['model']=='g90'){ ?>
						<li><a href="/dl/G90_fl.pdf" target="_blank"><span>ПРАЙС-ЛИСТ</span></a></li>
						<?php } ?>
						</ul>
					</div>
				<?else:?>
					<ul class="lst">
						<li><a href="/configurator">КОНФИГУРАТОР</a></li>
						<li><a href="/test-drive">ТЕСТ-ДРАЙВ</a></li>
							<?php  if($this->params['model']=='g70'){ ?>
						<li><a href="/dl/G70.pdf" target="_blank">ПРАЙС-ЛИСТ</a></li>
						<?php } else  if($this->params['model']=='g90'){ ?>
						<li><a href="/dl/G90_fl.pdf" target="_blank">ПРАЙС-ЛИСТ</a></li>
						<?php } ?>
					</ul>
                <?endif?>
            </div>
        </div>
    </div>
    <!-- //sub navigator -->
    <?/*endif*/?>
    
	
	<?= $content ?>
	
    
    <div class="btn-top">
        <a href="#"> <span class="btn-txt">TOP</span> <span class="vert-line-mask"> <span class="vert-line"></span> </span> </a> </div>
    <div id="overlay" class="hide"></div>
</div>
<!-- //wrapper -->
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="/js/jquery.nanoscroller.min.js"></script>
<script type="text/javascript" src="/js/forma-call.js"></script>

</body>
</html>