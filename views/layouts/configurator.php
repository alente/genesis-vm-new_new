<?php
use app\assets\AppAsset;
use alexandernst\devicedetect\DeviceDetect;
use yii\helpers\Html;

$device = 'desktop';
if(\Yii::$app->devicedetect->isMobile()){
	$device = 'mobile';
}
if(\Yii::$app->devicedetect->isTablet()){
	$device = 'tablet';
}
$version = 2;
?>
<?php $this->beginPage() ?>
<!doctype html>
    <html lang="ru" class="<?=$device?>">
    <head>
        <meta charset="utf-8">
		<?if($device == 'desktop'):?>
			<link name="viewport" content="width=device-width, initial-scale=3, user-scalable = no">
		<?else:?>
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0 ">
		<?endif?>
        <link http-equiv="X-UA-Compatible" content="IE=edge">
        
        <link rel="shortcut icon" type="image/x-icon" href="<?=Yii::$app->homeUrl?>images/favicon/favicon.ico"/>
        <script src="<?=Yii::$app->homeUrl?>scripts/htmlFontSize.js"></script>
        <link name="msapplication-TileColor" content="#ffffff"/>

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

        <link rel="stylesheet" href="<?=Yii::$app->homeUrl?>styles/vendor.css?<?=$version?>">
		<link rel="stylesheet" href="<?=Yii::$app->homeUrl?>scripts/libs/threesixty-slider/src/styles/threesixty.css?<?=$version?>">
        <link rel="stylesheet" href="<?=Yii::$app->homeUrl?>styles/main.css?<?=$version?>">
    </head>

    <body
		    <?/*data-boreas-modules="topmenu slides"
		    data-json-base="<?=Yii::$app->homeUrl?>/configurator/json-data"
		    data-json-finance="<?=Yii::$app->homeUrl?>/configurator/json-data-program-credit"
		    data-json-cities="<?=Yii::$app->homeUrl?>/ajax/dealer-cities.json"
		    data-json-dealers="<?=Yii::$app->homeUrl?>/ajax/dealer-list.json"
		    data-pdf-action="<?=Yii::$app->homeUrl?>/download/pdf/"
		    data-pdf-path="<?=Yii::$app->homeUrl?>/pdf"
		    data-form-action="<?=Yii::$app->homeUrl?>/crm/send-pdf-to-crm/"*/?>
    >

        <?/*php $this->beginBody() */?>
        <!--[if lt IE 10]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
            your browser</a> to improve your experience.</p>
        <![endif]-->

        <?if(\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y'):?>
            <header class="header_2017--mob">
	            <? echo $this->render('/partials/menuMob'); ?>
            </header>
        <?else:?>
            <header class="header_2017">
	            <? echo $this->render('/partials/menu'); ?>
            </header>
        <?endif?>
 
        <?= $content ?>


        <script src="js/build.js" charset="utf-8"></script>
        <script src="scripts/header/common.js"></script>
        <script src="scripts/header/App.js"></script>
        <script src="scripts/header/App.navigation.js"></script>
        <script src="scripts/header/App.navigation-mob.js"></script>
		
		<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="/js/jquery.nanoscroller.min.js"></script>
<script type="text/javascript" src="/js/forma-call.js"></script>
        
        <?/*
        <script src="<?=Yii::$app->homeUrl?>/scripts/libs/browser-detect.js"></script>
        <script src="<?=Yii::$app->homeUrl?>/scripts/vendor.js?<?=$version?>"></script>
        <script src="<?=Yii::$app->homeUrl?>/scripts/views.js?<?=$version?>"></script>
        
        <script src="<?=Yii::$app->homeUrl?>/scripts/libs/jquery.inputmask.bundle.min.js?<?=$version?>"></script>
        <script src="<?=Yii::$app->homeUrl?>/scripts/libs/threesixty-slider/src/threesixty.js?<?=$version?>"></script>
        <script src="<?=Yii::$app->homeUrl?>/scripts/main.js?<?=$version?>"></script>
        */?>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-WQ9SQMC');</script>
		<!-- End Google Tag Manager -->
    </body>
</html>
<?php $this->endPage() ?>