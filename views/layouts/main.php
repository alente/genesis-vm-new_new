<?php
use yii\helpers\Html;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="no-js" lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <link rel="shortcut icon" type="image/x-icon" href="<?=Yii::$app->homeUrl?>images/favicon/favicon.ico"/>
    
    <?/*= Html::csrfMetaTags() */?>
    <title><?= Html::encode($this->title) ?></title>
	<?/*php $this->head() */?>
	<?if(\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y'):?>
        <link rel="stylesheet" href="<?=Yii::$app->homeUrl?>styles/mob/style.css">
	<?else:?>
        <link rel="stylesheet" href="<?=Yii::$app->homeUrl?>styles/desk/style.css">
	<?endif?>
</head>

<body>
<?php $this->beginBody() ?>
<div class="wrapper">
    <!-- Skip Nav -->
    <div id="skip-navi">
        <a href="#container"><span>Закрыть</span></a>
        <a href="#gnb-menu-open-btn"><span>Меню</span></a>
    </div>
    
    <header class="header_2017">
        <?if(\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y'):?>
            <? echo $this->render('/partials/menuMob'); ?>
        <?else:?>
	        <? echo $this->render('/partials/menu'); ?>
        <?endif?>
    </header>
    
    <?= $content ?>
	
	<?if(\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y'):?>
		<? echo $this->render('/partials/footerMobile'); ?>
    <?else:?>
		<? echo $this->render('/partials/footer'); ?>
	<?endif?>
    <div id="overlay" class="hide"></div>

</div><?/* .wrapper */?>

<div class="top-btn">
    Наверх
</div>
<a class="mobile-fb-btn toggleForm">
	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 480.56 480.56" style="enable-background:new 0 0 480.56 480.56;" xml:space="preserve">
        <path d="M365.354,317.9c-15.7-15.5-35.3-15.5-50.9,0c-11.9,11.8-23.8,23.6-35.5,35.6c-3.2,3.3-5.9,4-9.8,1.8 c-7.7-4.2-15.9-7.6-23.3-12.2c-34.5-21.7-63.4-49.6-89-81c-12.7-15.6-24-32.3-31.9-51.1c-1.6-3.8-1.3-6.3,1.8-9.4 c11.9-11.5,23.5-23.3,35.2-35.1c16.3-16.4,16.3-35.6-0.1-52.1c-9.3-9.4-18.6-18.6-27.9-28c-9.6-9.6-19.1-19.3-28.8-28.8 c-15.7-15.3-35.3-15.3-50.9,0.1c-12,11.8-23.5,23.9-35.7,35.5c-11.3,10.7-17,23.8-18.2,39.1c-1.9,24.9,4.2,48.4,12.8,71.3 c17.6,47.4,44.4,89.5,76.9,128.1c43.9,52.2,96.3,93.5,157.6,123.3c27.6,13.4,56.2,23.7,87.3,25.4c21.4,1.2,40-4.2,54.9-20.9 c10.2-11.4,21.7-21.8,32.5-32.7c16-16.2,16.1-35.8,0.2-51.8C403.554,355.9,384.454,336.9,365.354,317.9z" style="fill: #fff;"/>
        <path d="M346.254,238.2l36.9-6.3c-5.8-33.9-21.8-64.6-46.1-89c-25.7-25.7-58.2-41.9-94-46.9l-5.2,37.1 c27.7,3.9,52.9,16.4,72.8,36.3C329.454,188.2,341.754,212,346.254,238.2z" style="fill: #fff;"/>
        <path d="M403.954,77.8c-42.6-42.6-96.5-69.5-156-77.8l-5.2,37.1c51.4,7.2,98,30.5,134.8,67.2c34.9,34.9,57.8,79,66.1,127.5 l36.9-6.3C470.854,169.3,444.354,118.3,403.954,77.8z" style="fill: #fff;"/>
    </svg>
</a>
<style>
.mobile-fb-btn{
    display: none;
}
@media only screen and (max-device-width: 991px){
    .mobile-fb-btn,
    .mobile-fb-btn *{
        box-sizing: border-box;
    }
	.mobile-fb-btn{
        width: 35px;
        height: 35px;
        padding: 7px;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        -ms-border-radius: 50%;
        -o-border-radius: 50%;
        border-radius: 50%;
        position: fixed;
        bottom: 10px;
        left: 10px;
		background-color: #9a4835;
        display: block;
	}
}
</style>
<?/*php $this->endBody() */?>
<?if(\Yii::$app->devicedetect->isMobile()):?>
    <script src="<?=Yii::$app->homeUrl?>scripts/mob.js"></script>
<?else:?>
    <script src="<?=Yii::$app->homeUrl?>scripts/desktop.js"></script>
<?endif?>

<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="/js/jquery.nanoscroller.min.js"></script>
<script type="text/javascript" src="/js/forma-call.js"></script>

</body>
</html>
<?php $this->endPage() ?>