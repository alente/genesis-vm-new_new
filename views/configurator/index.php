<?
$this->title = 'Конфигуратор - Genesis';
?>
<style>
    html, body {
        width:100%;
        height:100%;
        overflow:hidden;
    }
    
    iframe#configurator {
        padding:0;
        margin:0;
        height: 100%;
        width:100%;
        border:none;
    }
</style>
<iframe
    id="configurator"
    src="//genesis-motors.ru/ru/ru/configurator?view=dealer&city_id=<?=Yii::$app->params['dealer']['city_id']?>&code=<?=Yii::$app->params['dealer']['code']?>"
></iframe>
<? include ("../views/partials/footer.php")?>