<?php
use app\components\helpers\BImages;
use app\components\helpers\TextHelper;
$this->title = 'Автомобили Genesis в наличии';

?>    
    <!--<link type="text/css" rel="stylesheet" href="/styles/header/webfonts.css"> -->
      
    <link type="text/css" rel="stylesheet" href="/css/build.css"/>
	<?/*
    <link type="text/css" rel="stylesheet" href="/styles/header/header.css">
	*/?>
	<style>
		.layout__content .section .feature-list{
			padding-top: 50px;
		}
		/*.layout__content .section .car-view-item{
			padding-top: 0px;
		}*/
		.feature-list .stock-cont p.krupno + hr{
		margin-bottom: 0px;
		}
	</style>
   <div class="layout__content">

   
   
   
   
<div class="section">
<div class="layout__wrapper">
<!--  TODO: replace icons-->
<div class="feature-list">
<div class="stock-cont">
<h1 class="stock-title">Автомобили в наличии</h1>


<?php foreach($cars as $k=>$car) { ?>

	<div class="car-view-item">
	<div class="car-view-item__img">
	<img src="<?= $car->getCarImage() ?>">
	</div>
	<div class="car-view-item__content">
	<h4 class="car-view-item__name">GENESIS <?= $car->model ?></h4>
	<div class="car-view-item__comment"><?= $car->getComplName() ?></div>
	<?php if ($car->getComplTech()){ ?><div class="car-view-item__comment2"><?= $car->getComplTech() ?></div><?php } ?>
	<div class="car-view-item__chars">
	<!--
	<div class="car-view-item__color-model" style="background-color: #0f0e14"></div>
	<div class="car-view-item__color-model" style="background-color: #efefef"></div>
	<div class="car-view-item__color-model" style="background-color: #dad9d9"></div>
	<div class="car-view-item__color-model" style="background-color: #3d3d3d"></div>
	<div class="car-view-item__color-model" style="background-color: #1d283e"></div>
	<div class="car-view-item__color-model" style="background-color: #635c56"></div>
	<div class="car-view-item__color-model" style="background-color: #173967"></div>
	<div class="car-view-item__color-model" style="background-color: #4a403e"></div>
	-->
	<div class="car-view-item__char">Цвет: <?= $car->getColor() ?></div>
	<div class="car-view-item__char">Год: <?= $car->year ?></div>
	<?php if(!strstr($car->vin, 'test')){ ?><div class="car-view-item__char">VIN: <?= $car->vin ?></div><?php } ?>
	</div>
	</div>
	<div class="car-view-item__right">
	<div class="car-view-item__cost"><?= number_format($car->price, 0, '', ' ' ) ?> р.</div>
	<a href="#callkeeper" data-model="GENESIS <?= $car->model ?>" data-vin="<?= $car->vin ?>" class="toggleForm car-view-item__btn callback-opener">Получить предложение</a>
	</div>
	</div>			

<?php } ?>

</div>

</div>
</div>
</div>


<style>
	
	.stock-title{
		font-size: 30px;
		color: #333;
		text-transform: none;
		font-family: GenesisSansTextKROTFRegular, Arial, serif;
		padding-bottom: 15px;
		margin-bottom: 10px;
	}
	.car-view-item {
	    padding: 50px 0 32px;
	    border-bottom: 1px solid #BFBFBF;
	    zoom: 1;
	}
	.car-view-item::after {
	    content: "";
	    display: table;
	    clear: both;
	}
	.car-view-item__img, .car-view-item__content, .car-view-item__right {
	    float: left;
	}
	.car-view-item__img {
	    width: 256px;
	    display: inline-block;
	    vertical-align: middle;
	    margin-right: 40px;
	}
	.car-view-item__img img {
	    max-width: 100%;
	}
	.car-view-item__content {
	    width: 310px;
	    margin-right: 36px;
	}
	.car-view-item__name {
	    color: #9D6456;
	    text-transform: none;
	    font-size: 28px;
	    font-family: GenesisSansTextKROTFRegular, Arial, serif;
	    padding-bottom: 5px;
	}
	.car-view-item__comment {
	    font-size: 16.4px;
	    color: #000000;
	    padding-bottom: 15px;
	}
	.car-view-item__comment2 {
	    font-size: 14px;
	    color: #000000;
	    padding-bottom: 15px;
	}
	.car-view-item__chars {
	    color: #666;
	    font-size: 14.8px;
	}
	.car-view-item__color-model {
	    display: inline-block;
	    -webkit-border-radius: 10em;
	    border-radius: 10em;
	    width: 20px;
	    height: 20px;
	}
	.car-view-item__right {
	    width: 257px;
	}
	.car-view-item__cost {
	    font-size: 24.65px;
	    color: #000;
	    text-align: center;
	    margin: 20px 0 20px;
	}
	.car-view-item__btn {
	    background: none;
	    color: #000;
	    display: block;
	    text-align: center;
	    text-decoration: none;
	    width: 100%;
	    line-height: 55px;
	    font-size: 13px;
	    font-family: GenesisSansTextKROTFRegular, Arial, serif;
	    text-transform: uppercase;
	    border: 1px solid #000;
	    -webkit-transition: background .1s ease, color .1s ease;
	    -o-transition: background .1s ease, color .1s ease;
	    transition: background .1s ease, color .1s ease;
	    cursor: pointer;
	}
	.car-view-item__btn:hover{
		text-decoration: none;
	}
	
</style>




   
   
    </div>
<div class="top-btn">
    Наверх
</div>
<div class="mobileDetector" id="mobileDetector"></div>
<script src="/js/build.js" charset="utf-8"></script>