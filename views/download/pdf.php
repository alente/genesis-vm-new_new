<?php
function showNumber($n, $top = false) {
    $n = explode('<br/>', $n);
    $sum = 0;
    foreach($n as $num)
        $sum += $num;

    if($top)
        return empty($sum) ? '' : number_format(intval(trim($sum)), 0, '.', ' ') . ' <img src="./images/pdf/rub_top.png" height="16" style="vertical-align: top; margin-top: 3px;">';
    else
        return empty($sum) ? '' : number_format(intval(trim($sum)), 0, '.', ' ') . ' <img src="./images/pdf/rub_final.png" height="16" style="vertical-align: top; margin-top: 16px;">';
}

function getSum($n)
{
    $n = explode('<br/>', $n);
    $sum = 0;
    foreach($n as $num)
        $sum += $num;

    return $sum;
}
?>
<style type="text/css">
    .for_print_block_inner table td {
        text-align: left;
        vertical-align: top;
    }

    /* Шрифты */


    @font-face {
        font-family: 'GenesisSansHeadKROTFLight';
        src: url("./fonts/GenesisSansHeadKROTFLight.ttf") format('truetype');
        font-weight: normal;
        font-style: normal;
    }
    @font-face {
        font-family: 'GenesisSansTextKROTFRegular';
        src: url("./fonts/GenesisSansTextKROTFRegular.ttf") format('truetype');
        font-weight: normal;
        font-style: normal;
    }


</style>
<div class="for_print_block_inner" style="font-family: DejaVu Sans, sans-serif; font-size: 18px; text-align: left;<?php if ($isForPDF) echo 'padding-right: 0px;' ?>">

    <table style="width: 100%; border-collapse: collapse; background: #000; border: 0px;">
        <tr>
            <td style="text-align:left; width: 70%; height: 90px; font-size: 48px; color:#e5e5e5; font-weight:normal; vertical-align: middle; padding-left: 20px; border: 0px; font-family: GenesisSansHeadKROTFLight;background: #000;">
                <?=$model_name?>
            </td>
            <td style="text-align:right; width: 30%; background: #000; height: 90px; vertical-align: middle; padding-right: 20px; border: 0px;background: #000;  border-right: 1px solid #fff;">
                <img src="<?= $logoImage ?>" style="width: 120px; height: 50px;"/>
            </td>
        </tr>
    </table>
    <table style="width: 100%; border-collapse: collapse; border: 0px;">
        <tr>
            <td style="text-align:right; width: 100%; vertical-align: bottom; border: 0px; color: #fff; font-size: 80%; height: 276px;">
                &nbsp;
            </td>
        </tr>
    </table>

    <div style="position: absolute; left: 0px; top: 90px; color: #fff; font-size: 80%; text-align: right; font-family: GenesisSansTextKROTFRegular; width: 100%; height: 276px;">
        <img src="<?= $bannerImage ?>" style="width: 100%; vertical-align: top; <?php if ($isForPDF) echo 'min-width: 700px;' ?>"/>
    </div>

    <div style="position: absolute; right: 25px; top: 305px; color: #fff; font-size: 80%; text-align: right; font-family: GenesisSansTextKROTFRegular;">
        Рекомендованная цена:<br/>
        <div style="font-size: 220%; line-height: 160%; padding-top: 10px;">
            <?php
            $sum = getSum($optionPrice) + $trimPrice;
            echo showNumber($sum, true);
            ?>
        </div>
    </div>

    <table style="width: 100%; border-collapse: collapse; margin-top: 0px;">
        <tr>
            <td colspan="3" style="border-bottom: 2px solid #804033; ; padding-top: 1em;">
                <div style="margin: 0.3em 0; font-size: 150%; font-weight: 300; font-family: GenesisSansHeadKROTFLight;">КОМПЛЕКТАЦИЯ</div>
            </td>
        </tr>

        <tr>
            <td colspan="3" style="color: #804033; ; padding-top: 1em;">
                <div style="margin: 0.3em 0; font-size: 100%; font-family: GenesisSansTextKROTFRegular;"><?=$trimModel; ?>&nbsp;<?= $trimCaption ?></div>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="padding-top: 1em; font-size: 90%;font-family: GenesisSansTextKROTFRegular;">
                КЛЮЧЕВЫЕ ХАРАКТЕРИСТИКИ
            </td>
        </tr>
        <!-- А тут надо добавить вывод характеристик по конкретной комплектации -->
        <?php
        foreach($specifications as $s) :?>
            <tr>
                <td colspan="3" style="border-bottom: 1px solid #858585; padding: 17px 0 5px; color: #5d5e5d; font-size: 90%;">
                    <?=$s?>
                </td>
            </tr>
        <?php endforeach;?>

        <tr>
            <td colspan="3" style="color: #804033; ; padding-top: 0.5em;">
                &nbsp;
            </td>
        </tr>

        <tr valign="middle">
            <td colspan="2" style="font-size: 120%; vertical-align: middle; font-family: GenesisSansTextKROTFRegular;">
                Стоимость комплектации:
            </td>
            <td style="font-size: 170%; height: 67px; padding-right: 5%; text-align: right; background-image: url('./images/price_bg.png'); background-position: right center; background-repeat: no-repeat; vertical-align: middle;">
                <?= number_format($trimPrice, 0, '', ' '); ?> <img src="./images/pdf/rub_bottom.png" height="16" style="vertical-align: top; margin-top: 16px;">
            </td>
        </tr>

        <tr>
            <td colspan="3">
                <table style="width: 100%;">
                    <tr>
                        <td style="border-bottom: 3px solid #804033; padding-top: 1em; width: 49%;">
                            <div style="margin: 0.3em 0; font-size: 150%;">ЭКСТЕРЬЕР</div>
                        </td>
                        <td style="width:10px;"></td>
                        <td style="border-bottom: 3px solid #804033; padding-top: 1em; width: 49%">
                            <div style="margin: 0.3em 0; font-size: 150%;">ИНТЕРЬЕР</div>
                        </td>
                    </tr>
                </table>
                <table style="width: 100%;">
                    <tr>

                        <?php if( !empty($colorName) ) { ?>

                            <td width="49%">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 75px; padding-top: .5em;"><div style="width: 55px; height: 55px; background-color: <?= $colorImage ?>" ></div></td>
                                        <td style="padding-top: .5em; width: auto;">
                                            <span><?= $colorName ?></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                        <?php } ?>

                        <td style="width:10px;"></td>

                        <?php if( !empty($interiorName) ) { ?>

                            <td width="49%">
                                <table width="100%">
                                    <tr>
                                        <td style="padding-top: .5em;">
                                            <img src="<?= $interiorImage ?>" style="width: 55px; height: 55px; margin-bottom: 10px;" /><br/><img src="<?= $interiorLeatherImage ?>" style="width: 55px; height: 55px;" />
                                        </td>
                                        <td style="padding-top: .5em;">
                                            <span><?= $interiorName ?></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                        <?php } ?>
                    </tr>
                </table>
            </td>
        </tr>

        <?php if( !empty($packet) ) :?>

            <tr>
                <td colspan="3" style="border-bottom: 2px solid #804033; ; padding-top: 1em;">
                    <div style="margin: 0.3em 0; font-size: 120%; font-weight: 300; font-family: GenesisSansHeadKROTFLight;">ОПЦИОНАЛЬНЫЕ ПАКЕТЫ</div>
                </td>
            </tr>

            <tr>
                <td colspan="3" style="color: #804033; ; padding-top: 1em;">
                    <div style="margin: 0.3em 0; font-size: 100%; font-family: GenesisSansTextKROTFRegular;"><?= $optionCaption ?></div>
                </td>
            </tr>
            <!-- А тут надо добавить вывод характеристик по конкретной комплектации -->
            <?php
            foreach($packet['specs'] as $s) :?>
                <tr>
                    <td colspan="3" style="border-bottom: 1px solid #858585; padding: 18px 0 6px; color: #5d5e5d; font-size: 90%;">
                        <?=$s?>
                    </td>
                </tr>
            <?php endforeach;?>

            <tr>
                <td colspan="3" style="color: #804033; ; padding-top: 1em;">
                    &nbsp;
                </td>
            </tr>

            <tr valign="middle">
                <td colspan="2" style="font-size: 120%; vertical-align: middle; font-family: GenesisSansTextKROTFRegular;">
                    Стоимость пакета:
                </td>
                <td style="font-size: 170%; height: 67px; padding-right: 5%; text-align: right; background-image: url('./images/price_bg.png'); background-position: right center; background-repeat: no-repeat; vertical-align: middle;">
                    <?= number_format($optionPrice, 0, '', ' '); ?> <img src="./images/pdf/rub_bottom.png" height="16" style="vertical-align: top; margin-top: 16px;">
                </td>
            </tr>

        <?php endif; ?>

    </table>

    <table style="width: 100%; border-collapse: collapse; margin-top: 50px;">
        <tr valign="bottom">
            <td style="font-size: 150%; color: #9a6b4d; border-bottom: 5px solid #9a6b4d; padding-bottom: 13px; font-family: GenesisSansTextKROTFRegular;">
                Итоговая стоимость автомобиля
            </td>
            <td style="font-size: 160%;  color: #9a6b4d; border-bottom: 5px solid #9a6b4d; padding-bottom: 10px; text-align: right;">
                <?php
                $sum = getSum($optionPrice) + $trimPrice;
                echo showNumber($sum);
                ?>
            </td>
        </tr>
        <tr valign="bottom">
            <td colspan="2" style="font-size: 50%; color: #8e8990; padding-top: 25px;">
                Дата формироваания комплектации: <?=date('d.m.Y');?>
            </td>
        </tr>
    </table>
</div>