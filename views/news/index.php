<?php
use app\components\helpers\BImages;
use app\components\helpers\TextHelper;
$this->title = 'Новости Genesis';

?>    
    <!--<link type="text/css" rel="stylesheet" href="/styles/header/webfonts.css"> -->
      
    <link type="text/css" rel="stylesheet" href="/css/build.css"/>
    <?/*<link type="text/css" rel="stylesheet" href="/styles/header/header.css">*/?>
   <div class="layout__content">
        <div class="news_announcement-top">
            <div class="news_announcement__item al-right">
                <a class="news_announcement__item-half news-an-img" href="<?=Yii::$app->urlManager->createUrl(['news/detail', 'handle' => $news[0]->handle])?>"><img alt="Новости Genesis"  src="<?=BImages::doCrop((($news[0]->image_preview!='')?$news[0]->image_preview:$news[0]->image), 675, 449)?>"></a>
                <div class="news_announcement__item-half news-an-text"><span class="news_announcement_date"><?=date('d.m.Y',$news[0]->getDateSrc('date'))?></span>
                    <h2 class="news_announcement_title"><a class="news_announcement_title-link" href="<?=Yii::$app->urlManager->createUrl(['news/detail', 'handle' => $news[0]->handle])?>"><?=$news[0]->title?></a></h2>
                    <p class="news_announcement_desc"><?=$news[0]->description?></p>
                    <div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="<?=Yii::$app->urlManager->createUrl(['news/detail', 'handle' => $news[0]->handle])?>">читать далее</a></div>
                </div>
            </div>
        </div>
        <div class="layout__wrapper news_announcement-content">
        <? foreach($news as $k => $newsItem) {
          if ($k == 0) continue; 
          if($k%2 == 0) {
          $side = "right";
          } else $side = "left";
        ?>
        
            <div class="news_announcement__item al-<?=$side?>">
                <a class="news_announcement__item-half news-an-img" href="<?=Yii::$app->urlManager->createUrl(['news/detail', 'handle' => $newsItem->handle])?>"><img alt="Новости Genesis"  src="<?=BImages::doCrop((($newsItem->image_preview!='')?$newsItem->image_preview:$newsItem->image), 482, 321)?>"></a>
                <div class="news_announcement__item-half news-an-text"><span class="news_announcement_date"><?=date('d.m.Y',$newsItem->getDateSrc('date'))?></span>
                    <h2 class="news_announcement_title"><a class="news_announcement_title-link" href="<?=Yii::$app->urlManager->createUrl(['news/detail', 'handle' => $newsItem->handle])?>"><?=$newsItem->title?></a></h2>
                    <p class="news_announcement_desc"><?=$newsItem->description?></p>
                    <div class="news_announcement_bottom"><a class="news_announcement_bottom-link" href="<?=Yii::$app->urlManager->createUrl(['news/detail', 'handle' => $newsItem->handle])?>">читать далее</a></div>
                </div>
            </div>
        <? } ?>    
        </div>
    </div>
<div class="top-btn">
    Наверх
</div>
<div class="mobileDetector" id="mobileDetector"></div>
<script src="/js/build.js" charset="utf-8"></script>