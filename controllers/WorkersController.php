<?php

namespace app\controllers;

use app\components\PublicController;
use app\models\Workers;
use Yii;
use yii\web\NotFoundHttpException;

class WorkersController extends PublicController
{
    public function actionIndex($page = 1)
    {
        $this->tempInit('', 'workers');

        $criteria = Workers::find()->published();
        $criteria->orderBy('t.date DESC, t.weight ASC');

        $countQuery = clone $criteria;

        $workers = $criteria->limit($pages->limit)->all();

        return $this->render('index', ['workers' => $workers]);
    }
}
