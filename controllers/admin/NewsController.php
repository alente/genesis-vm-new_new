<?php

namespace app\controllers\admin;

class NewsController extends \app\components\AdminController
{
    public $mainmenuHandle = 'news';
    public $modelName='News';
    public $modelTitles = ['Новость', 'Новости', 'Новостей'];
    public $images = [
        'image' => '@webroot/up/news',
        'image_preview' => '@webroot/up/news',
    ];
}