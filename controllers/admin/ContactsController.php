<?php

namespace app\controllers\admin;

class ContactsController extends \app\components\AdminController
{
    public $mainmenuHandle = 'contacts';
    public $modelName='Contacts';
    public $modelTitles = ['Контакты', 'Параметр'];
    public $images = [];
}