<?php

namespace app\controllers\admin;

class WorkersController extends \app\components\AdminController
{
    public $mainmenuHandle = 'workers';
    public $modelName='Workers';
    public $modelTitles = ['Сотрудники', 'Сотрудники', 'Сотрудники'];
    public $images = [
        'image' => '@webroot/up/workers',
    ];
}