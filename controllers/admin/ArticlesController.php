<?php

namespace app\controllers\admin;

class ArticlesController extends \app\components\AdminController
{
    public $mainmenuHandle = 'articles';
    public $modelName='Articles';
    public $modelTitles = ['Статья', 'Статьи', 'Статей'];
    public $images = [
        'image' => '@webroot/up/articles',
        'image_preview' => '@webroot/up/articles',
    ];
}