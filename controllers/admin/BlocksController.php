<?php

namespace app\controllers\admin;
use app\components\helpers\SystemTools;
use yii;
use yii\db\Query;
use yii\web\Response;
use yii\bootstrap\ActiveForm;

class BlocksController extends \app\components\AdminController
{
    public $mainmenuHandle = 'blocks';
    public $modelName='Blocks';
    public $modelTitles = ['Блоки', 'Блок'];
    public $images = [];
    public $children = [];
    public $parents = [
        'sections' => 'Назад к разделу',
    ];

    public $child_links = [];
    public $block_images = [
        'image' => '@webroot/up/block',
    ];

    public function beforeUpdate(&$model, &$content)
    {
        $block = $this->childBlock($model);
        if (method_exists($block, 'find')) {
            $content .= '<br>' . \Yii::$app->controller->renderPartial('_form_' . strtolower($model->template()), ['model' => $block]);
        }
    }

    public function childBlock($model) {
        if ($model->block) {
            $block = $model->block;
        } else {
            $blockClass = $model->blockClass;
            if($blockClass) {
                $block = new $blockClass;
                $block->parent_id = $model->id;
            } else {
                $block = false;
            }
        }

        return $block;
    }

    public function actionBlocksave($id, $pback = false, $partial = false) {
        $parentModel = $this->loadModel($id);
        $model = $this->childBlock($parentModel);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (isset($_POST['cancel'])) return [];
            return ActiveForm::validate($model);
        }

        $shortClassName = end(explode('\\', $model->className()));

        if (isset($_POST[$shortClassName])) {
            $model->load(\Yii::$app->request->post());
            $model->parent_id = $parentModel->id;

            if ($model->validate()) {
                foreach ($this->block_images as $imageVar => $imageWay) {
                    if (in_array($imageVar, array_keys($model->getAttributes()))) {
                        if (!empty($_POST[end(explode('\\', $parentModel->blockClass))][$imageVar . 'deleting'])) {
                            $model->$imageVar = '';
                        } else {
                            SystemTools::loadImage($model, $imageVar, $imageWay);
                        }
                    }
                }
            }

            if ($model->save()) {
                if (!empty($this->child_links[$shortClassName])) {
                    foreach ($this->child_links[$shortClassName] as $link => $linkVars) {
                        if (isset($_POST[$shortClassName][$link])) {
                            $query = new Query;
                            $command = $query->createCommand();
                            $command->sql = 'DELETE FROM `'.$linkVars[0].'` WHERE `'.$linkVars[1].'`="'.$model->id.'";';
                            $command->execute();

                            $values = [];
                            if (is_array($_POST[$shortClassName][$link])) {
                                foreach ($_POST[$shortClassName][$link] as $val) {
                                    $values[] = '("' . $model->id . '", "' . $val . '")';
                                }
                            }

                            if(count($values)) {
                                $command->sql = 'INSERT INTO `'.$linkVars[0].'` (`'.$linkVars[1].'`, `'.$linkVars[2].'`) VALUES ' . implode(', ', $values);
                                $command->execute();
                            }
                        }
                    }
                }

                if (isset($_POST['save'])) {
                    $params = [];
                    if ($pback !== false)  $params['page'] = $pback;
                    if ($this->parents && $partial !== false) {
                        return $this->redirect(['admin/'.current(array_keys($this->parents)).'/update', 'id' => $parentModel->parent_id, 'tab' => strtolower($this->modelName)] + $params);
                    } else {
                        return $this->redirect(['index'] + $params);
                    }
                } else {
                    $params = [];
                    if ($pback !== false) $params['pback'] = $pback;
                    if ($partial !== false) $params['partial'] = $partial;

                    return $this->redirect(['update', 'id' => $parentModel->id] + $params);
                }
            }
        }
    }
}
