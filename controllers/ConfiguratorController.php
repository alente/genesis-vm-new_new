<?php

namespace app\controllers;

use app\models\Arr;
use app\models\CarComplectationItem;
use app\models\CarComplectationItemType;
use app\models\CarComplectationPacket;
use app\models\CarModificationItem;
use app\models\CarModificationItemType;
use app\models\CarType;
use app\models\City;
use app\models\Dealer;
use app\models\Programs;
use app\models\ProgramTerms;
use app\models\Color;
use app\models\ColorInterior;
use Yii;
use yii\web\Response;



class ConfiguratorController extends ConfiguratorDefaultController
{

    public function beforeAction($action)
    {
        // ...set `$this->enableCsrfValidation` here based on some conditions...
        // call parent method that will check CSRF if such property is true.

        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }


    public function actionIndex()
    {
        $this->layout = 'configurator';

       // $cars = CarType::find()->all();
	    $viewType = filter_input(INPUT_GET, 'view');
        
        switch($viewType){
	        case 'dealer': break;
	        default: $viewType = 'default';
        }
        return $this->render('index', [ 'cars' => $cars, 'viewType' => $viewType ]);

    }
/*
    public function actionJsonDataProgramCredit()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

		$programs['creditProgram'] = Programs::find()->asArray()->orderBy(['rate' => 'ASC'])->all();
		foreach($programs['creditProgram'] as $k => $program){
			$terms = \app\models\ProgramTerms::find()->asArray()->where(['program_id' =>$program['id']])->all();
			$programs['creditProgram'][$k]['terms'] = $terms;
		}
       return $programs;
    }

    public function actionJsonCity()
    {

        $returnValue = null;

        Yii::$app->response->format = Response::FORMAT_JSON;

        $returnValue = Yii::$app->cache->get('jsonCity');

        if ( $returnValue === false ) {

            $cities = City::find()->asArray()->all();

            foreach($cities as $key => $city) {
                list($cities[$key]['latitude'], $cities[$key]['longitude']) = explode(',', $city['coordinates']);
            }

            $returnValue = $cities;

            Yii::$app->cache->set('jsonCity', $returnValue, 1800);

        }

        return $returnValue;

    }

    public function actionJsonDealer()
    {

        $returnValue = null;

        Yii::$app->response->format = Response::FORMAT_JSON;

        $returnValue = Yii::$app->cache->get('jsonDealer');

        if ( $returnValue === false ) {

            $dealers = Dealer::find()->asArray()->all();

            foreach($dealers as $key => $dealer) {
                list($dealers[$key]['latitude'], $dealers[$key]['longitude']) = explode(',', $dealer['coordinates']);
            }

            $returnValue = $dealers;

            Yii::$app->cache->set('jsonDealer', $returnValue, 1800);

        }

        return $returnValue;

    }

    public function actionJsonData()
    {

        $returnValue = Yii::$app->cache->get('jsonData');

        Yii::$app->response->format = Response::FORMAT_JSON;

        Yii::$app->controller->enableCsrfValidation = false;

        if ($returnValue === false) {

            $returnValue = [
                'model'                 => [],
                'specificationCategory' => [],
                'featureCategory'       => [],
                'package'               => [],
                'colorExterior'         => [],
                'colorInterior'         => [],
            ];

            $carModificationsItem = CarModificationItemType::find()->orderBy(['order'=>SORT_ASC])->where(['!=', 'id', 12])->all();

            foreach ($carModificationsItem as $carModificationItem) {
                $returnValue['specificationCategory'][] = [
                    'id'   => $carModificationItem->id,
                    'name' => $carModificationItem->name,
                ];
            }

            $сarComplectationItemTypes = CarComplectationItemType::find()->orderBy(['order'=>SORT_ASC])->all();

            foreach ($сarComplectationItemTypes as $сarComplectationItemType) {
                $returnValue['featureCategory'][] = [
                    'id'   => $сarComplectationItemType->id,
                    'name' => $сarComplectationItemType->name,
                ];
            }

            $colors = Color::find()->all();

            foreach ($colors as $color) {

                $returnValue['colorExterior'][$color->id] = [
                    'name'  => $color->name,
                    'type'  => $color->color_type_id,
                    'hex'   => $color->color_text,
                    'image' => $color->image,
                ];

            }

            $colorsInterior = ColorInterior::find()->all();

            foreach ($colorsInterior as $colorInterior) {

                $returnValue['colorInterior'][$colorInterior->id] = [
                    'description' => $colorInterior->name,
                    'icon1'       => $colorInterior->icon1,
                    'icon2'       => $colorInterior->icon2,
                    'photo'       => $colorInterior->photo
                ];

            }

            $сarComplectationPackets = CarComplectationPacket::find()->all();

            foreach ($сarComplectationPackets as $сarComplectationPacket) {

                $returnValue['package'][$сarComplectationPacket->id] = [
                    'id'     => $сarComplectationPacket->id,
                    'name'   => $сarComplectationPacket->name,
                    'price'  => $сarComplectationPacket->price,
                    'values' => []
                ];

                $сarComplectationPacketValues = $сarComplectationPacket->carComplectationPacketValues;

                foreach ($сarComplectationPacketValues as $сarComplectationPacketValue) {
                    if ( !empty($сarComplectationPacketValue->carComplectationItem) ) {
                        $returnValue['package'][$сarComplectationPacket->id]['values'][] = [
                            'name' => $сarComplectationPacketValue->carComplectationItem->name,
                            'value' => $сarComplectationPacketValue->value,
                        ];
                    }
                }

            }

            $cars = CarType::find()->all();

            foreach ($cars as $car) {

                $returnValue['model'][mb_strtolower($car->name)] = [
                    'code'          => mb_strtolower($car->name),
                    'name'          => $car->name,
                    'brochure'      => $car->carBrochure->brochure_url,
                    'maxPower'      => $car->max_power,
                    'drive'         => $car->drive,
                    'transmission'  => $car->transmission,
                    'complectation' => [],
                ];

                $complectations = $car->carComplectation;

                foreach ($complectations as $complectation) {

                    $modification = $complectation->carModification;

                    $returnValue['model'][mb_strtolower($car->name)]['complectation'][$complectation->id] = [
                        'id'                 => $complectation->id,
                        'name'               => $complectation->name,
                        'modificationName'   => $modification->name,
                        'price'              => $complectation->price,
                        'cssClass'           => $complectation->css_class,
                        'specificationBasic' => [],
                        'specification'      => [],
                        'feature'            => [],
                        'featureBasic'       => [],
                        'colorExterior'      => [],
                        'package'            => [],
                        'colorInterior'      => [],
                    ];

                    $carComplectationItems = $complectation->carComplectationItem;

                    foreach ($carComplectationItems as $carComplectationItem) {

                        if ( !empty($carComplectationItem->carComplectationItem) ) {

                            if ($carComplectationItem->carComplectationItem->car_complectation_item_type_id != 8) {
                                $returnValue['model'][mb_strtolower($car->name)]['complectation'][$complectation->id]['feature'][] = [
                                    'name'       => $carComplectationItem->carComplectationItem->name,
                                    'categoryId' => $carComplectationItem->carComplectationItem->car_complectation_item_type_id,
                                    'value'      => 'null',
                                    'sort'       => $carComplectationItem->carComplectationItem->carComplectationItemType->order
                                ];
                            } else {
                                $returnValue['model'][mb_strtolower($car->name)]['complectation'][$complectation->id]['featureBasic'][] = [
                                    'name'       => $carComplectationItem->carComplectationItem->name,
                                    'categoryId' => $carComplectationItem->carComplectationItem->car_complectation_item_type_id,
                                    'value'      => 'null',
                                    'sort'       => $carComplectationItem->carComplectationItem->carComplectationItemType->order
                                ];
                            }
                        }
                    }

                    $carComplCarModificationItemValues = $modification->carComplCarModificationItemValue;

                    foreach ($carComplCarModificationItemValues as $carComplCarModificationItemValue) {

                        if ($carComplCarModificationItemValue->carModificationItem->car_modification_item_type_id != 12) {
                            if ( !empty($carComplCarModificationItemValue->carModificationItem) ) {
                                $returnValue['model'][mb_strtolower($car->name)]['complectation'][$complectation->id]['specification'][] = [
                                    'name'       => $carComplCarModificationItemValue->carModificationItem->name,
                                    'value'      => $carComplCarModificationItemValue->text,
                                    'categoryId' => $carComplCarModificationItemValue->carModificationItem->car_modification_item_type_id,
                                    'sort'       => $carComplCarModificationItemValue->carModificationItem->car_modification_item_type_order
                                ];
                            }
                        } else {
                            if ( !empty($carComplCarModificationItemValue->carModificationItem) ) {
                                $returnValue['model'][mb_strtolower($car->name)]['complectation'][$complectation->id]['specificationBasic'][] = [
                                    'name'       => $carComplCarModificationItemValue->carModificationItem->name,
                                    'value'      => $carComplCarModificationItemValue->text,
                                    'categoryId' => 0,
                                    'sort'       => $carComplCarModificationItemValue->carModificationItem->car_modification_item_type_order
                                ];
                            }
                        }

                    }

                    usort( $returnValue['model'][mb_strtolower($car->name)]['complectation'][$complectation->id]['specificationBasic'], ['app\controllers\ConfiguratorController', 'cmp']);
                    usort( $returnValue['model'][mb_strtolower($car->name)]['complectation'][$complectation->id]['specification'], ['app\controllers\ConfiguratorController', 'cmp']);
                    usort( $returnValue['model'][mb_strtolower($car->name)]['complectation'][$complectation->id]['featureBasic'], ['app\controllers\ConfiguratorController', 'cmp']);
                    usort( $returnValue['model'][mb_strtolower($car->name)]['complectation'][$complectation->id]['feature'], ['app\controllers\ConfiguratorController','cmp']);

                    $colorsComplectation = $complectation->colorComplectation;

                    foreach ($colorsComplectation as $colorComplectation) {

                        $returnValue['model'][mb_strtolower($car->name)]['complectation'][$complectation->id]['colorExterior'][] = [
                            'colorExteriorId' => $colorComplectation->color_id,
                            'car360'          => [
                                'imagePath'   => $colorComplectation->image_path,
                                'filePrefix'  => $colorComplectation->file_prefix,
                                'ext'         => $colorComplectation->ext,
                                'image'       => $colorComplectation->image,
                            ]
                        ];
                    }

                    $colorsInteriorComplectation = $complectation->colorInteriorComplectation;

                    foreach ($colorsInteriorComplectation as $colorInteriorComplectation) {

                        $returnValue['model'][mb_strtolower($car->name)]['complectation'][$complectation->id]['colorInterior'][] = $colorInteriorComplectation->color_interior_id;
                    }

                    $carComplectationPackets = $complectation->carComplectationPacket;

                    foreach ($carComplectationPackets as $carComplectationPacket) {

                        $returnValue['model'][mb_strtolower($car->name)]['complectation'][$complectation->id]['package'][] = $carComplectationPacket->car_complectation_packet_id;
                    }


                }
            }

            Yii::$app->cache->set('jsonData', $returnValue, 1800);

        }

       return $returnValue;

    }
*/

    public function cmp($a, $b) {

        if ($a['sort'] == $b['sort']) {
            return 0;
        }

        return ($a['sort'] < $b['sort']) ? -1 : 1;

    }

}