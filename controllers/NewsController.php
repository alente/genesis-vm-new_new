<?php

namespace app\controllers;

use app\components\PublicController;
use app\models\News;
use app\models\Settings;
use Yii;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class NewsController extends PublicController
{
    public function actionIndex($page = 1)
    {
        $this->tempInit('', 'news');

        $criteria = News::find()->published();
        $criteria->orderBy('t.date DESC, t.weight ASC');

        $countQuery = clone $criteria;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = Settings::getParam('newsOnPage');
        $pages->route = 'news/index';
        $pages->page = $page - 1;

        $news = $criteria->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('index', ['news' => $news, 'pages' => $pages]);
    }

    public function actionDetail($handle) {
        $news = News::find()->where(['handle'=>$handle, 'public'=>1])->one();
        if (!$news) throw new NotFoundHttpException();

        $this->tempInit($news->title, 'news');
        $this->addBreadCrumb('', $news->title);

        return $this->render('detail', ['news' => $news]);
    }
}
