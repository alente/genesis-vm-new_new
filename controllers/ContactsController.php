<?
namespace app\controllers;

use app\components\PublicController;
use Yii;

class ContactsController extends PublicController
{
    public function actionIndex()
    {
        $this->tempInit('', 'contacts');
        return $this->render('index', []);
    }
}
