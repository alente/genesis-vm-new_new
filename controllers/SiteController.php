<?php

namespace app\controllers;

use app\models\Arr;
use Yii;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Settings;
use app\models\News;

class SiteController extends Controller
{
    /*public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }*/

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
		//return $this->redirect(['/genesis.html']);
	    
	    return $this->actionGenesis();
    }
	
    /*public function actionGenesis ()
    {
    	global $_GET;
	
	    $models = new ActiveDataProvider([
		    'query' =>  News::find()
			    ->where(['active' =>1])
			    ->OrderBy(['date'=>SORT_DESC]),
		    'sort' => false,
		    'pagination' => [
			    'pageSize' => 15,
		    ]
	    ]);
    	
    	$viewName = 'index';
		if(\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y'){
			$viewName = 'indexMobile';
		}
	
	    return $this->render($viewName, [
		    'news' => $models->getModels()
	    ]);
    }*/
	
	public function actionSitemap ()
    {
    	global $_GET;
    	
    	$viewName = 'sitemap';
		$news = News::find()->where(['public'=>1, 'public_on_main'=>1])->orderBy('date DESC')->all();
      
    	return $this->render($viewName, ['news' => $news]);
    }
	
	public function actionGenesis ()
    {
    	global $_GET;
    	
    	$viewName = 'index';
		if(\Yii::$app->devicedetect->isMobile() || $_GET['mobile'] == 'y'){
			$viewName = 'indexMobile';
		}
	    $news = News::find()->where(['public'=>1, 'public_on_main'=>1])->orderBy('date DESC')->all();
      
    	return $this->render($viewName, ['news' => $news]);
    }
	
	public function actionSearch ()
	{
		$query = filter_input(INPUT_GET, 'q');
		$page = filter_input(INPUT_GET, 'page' , FILTER_VALIDATE_INT);
		if(!$page){ $page = 1; }
		$onPage = 10;
		$startIndex = $onPage * $page - $onPage + 1;
		
		$apiKey = Yii::$app->params['dealer']['apiKey'];
		$searchId = Yii::$app->params['dealer']['searchId'];
		$apiUrl = 'https://www.googleapis.com/customsearch/v1?key='. $apiKey
			.'&cx='. $searchId
			.'&q='. urlencode($query)
			.'&num='. $onPage
			.'&start='. $startIndex;
		
		$error = null;
		$result = null;
		$pages = 1;
		
		$arResponse = $this->request($apiUrl, null, [
			CURLOPT_SSL_VERIFYPEER => false
		]);
		
		if($arResponse['status'] == 200){
			$result = json_decode($arResponse['response'], true);
			$total = $result['searchInformation']['totalResults'];
			$pages = intval(ceil($total / $onPage));
		}else{
			$error = "Не удалось получить результаты поиска. Попробуйте повторить позже.";
		}
		
		//echo "<pre>".var_export('<br><br><br><br><br>'.$apiUrl, true) . "</pre>";
		
		return $this->render('search', [
			'query' => $query,
			'result' => $result,
			'page' => $page,
			'pages' => $pages,
			'error' => $error,
		]);
	}
	
	/**
	 * Отправка POST запроса с использованием cURL
	 *
	 * @param string $url Адрес запроса
	 * @param array $post Массив переменных для отправки POST-запросом
	 * @param array $options Дополнительные параметры cURL
	 * @return array Массив, содержащий статус запроса и ответ сервера
	 * @throws \RuntimeException
	 */
	public function request($url, array $post = NULL, array $options = array())
	{
		$defaults = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_TIMEOUT => 4,
			//CURLOPT_HEADER => 0,
			//CURLOPT_FRESH_CONNECT => 1,
			//CURLOPT_FORBID_REUSE => 1,
			//CURLOPT_POSTFIELDS => http_build_query($post)
		);
		
		if(!empty($post)){
			$defaults[CURLOPT_POSTFIELDS] = http_build_query($post);
		}
		
		$ch = curl_init();
		curl_setopt_array($ch, ($options + $defaults));
		
		$response = curl_exec($ch);
		$error    = curl_error($ch);
		$errno    = curl_errno($ch);
		$status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		
		if (is_resource($ch)) {
			curl_close($ch);
		}
		
		if (0 !== $errno) {
			throw new \RuntimeException($error, $errno);
		}
		
		return array(
			'status' => $status,
			'response' => $response,
		);
	}
	
    public function actionLogin()
    {
	   $this->layout = 'system';
	   
	   if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack(Yii::$app->session->get('auth_come_back'));
        } else {

            //$come_back_url = Arr::get($_REQUEST,'come_back',null); var_dump($come_back_url);
            if($come_back_url)
            {
                Yii::$app->session->set('auth_come_back',$come_back_url);
            }
            else
            {
                Yii::$app->session->set('auth_come_back',null);
            }

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        //$this->layout = 'system';
		Yii::$app->user->logout();

        return $this->goHome();
    }
	
	public function actionError()
    {        
        $this->pageTitle = 'Страница не найдена';
        $this->layout = false;
        return $this->render('404');
    }

}
