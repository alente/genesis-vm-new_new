<?php

namespace app\controllers;

use app\components\PublicController;
use app\models\Settings;
use Yii;
use yii\web\NotFoundHttpException;

class EmailsController extends PublicController
{
	public function beforeAction($action) 
	{ 
		$this->enableCsrfValidation = false; 
		return parent::beforeAction($action); 
	}
	
    public function actionIndex()
    {
		$this->layout = 'empty';
		
		if (empty($_REQUEST)) exit();
		
		$to = Settings::find()->where(['code'=>'emailFormsAll'])->one()->value;
	//	$to = 'info@genesis-vm.ru, alentemail@yandex.ru';
		//$to = 'Info@hyundai-vm.ru, snozdrya@hyundai-vm.ru, nataliya.malenkih@hyundai-vm.ru, alentemail@yandex.ru';
		$to = explode(',', $to);
		$cc = '';

		foreach ($to as $k=>$e) {
			if (!$k) $to = $e;
			if ($k) $cc .= "\r\n".'Cc: '.$e;
		}

		$post = $_REQUEST;


		$subject = "Genesis Запрос обратного звонка";
		$from = "Content-type: text/html; charset=utf-8\r\nFrom: " . 'info@genesis-vm.ru';
		if ($cc) $from = $from.$cc;

		$currentDate = date('Y-m-d, g:i a');
		$message = $currentDate . "<br/>";
		//$message .= "<br/>Модель: " . $post['model'];
		//$message .= "<br/>Обращение: " . $post['salutation'];
		$message .= "<br/>Имя: " . $post['name'];
		$message .= "<br/>Фамилия: " . $post['surname'];
		$message .= "<br/>Телефон: " . $post['phone'];
		if($post['vin']) $message .= "<br/>Интересуются авто с vin-номером: " . $post['vin'];
		//$message .= "<br/>E-Mail: " . $post['email'];
				//echo $from; exit;
		if( mail($to, $subject, $message, $from) )
			$result = 'Y';
		else
			$result = 'N';
		
		
        return $this->render('index', ['result' => $result]);
    }
}
