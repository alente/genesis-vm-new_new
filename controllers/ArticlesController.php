<?php

namespace app\controllers;

use app\components\PublicController;
use app\models\Articles;
use app\models\Settings;
use Yii;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class ArticlesController extends PublicController
{
    public function actionIndex($page = 1)
    {
        $this->tempInit('', 'articles');

        $criteria = Articles::find()->published();
        $criteria->orderBy('t.date DESC, t.weight ASC');

        $countQuery = clone $criteria;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = Settings::getParam('articlesOnPage');
        $pages->route = 'articles/index';
        $pages->page = $page - 1;

        $articles = $criteria->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('index', ['articles' => $articles, 'pages' => $pages]);
    }

    public function actionDetail($handle) {
        $articles = Articles::find()->where(['handle'=>$handle, 'public'=>1])->one();
        if (!$articles) throw new NotFoundHttpException();

        $this->tempInit($articles->title, 'articles');
        $this->addBreadCrumb('', $articles->title);

        return $this->render('detail', ['articles' => $articles]);
    }
}
