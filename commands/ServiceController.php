<?php
/**
 * Genesis Production
 *
 * @author Anton Desin anton.desin@gmail.com
 * @copyright (c) Anton Desin
 * @link https://desin.name
 */

namespace app\commands;

use yii\console\Controller,
	Yii;

class ServiceController extends Controller
{
	function actionIndex () {
		echo "Сервисная команда не найдена";
	}
	
	function actionPress () {
		Yii::$app->pressReleaseLoader->process();
	}
}