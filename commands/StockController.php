<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii;
use yii\db\Query;
use yii\console\Controller;
use app\models\Stock;


class StockController extends Controller
{
    public function actionIndex()
    {
		$objPHPExcel = \PHPExcel_IOFactory::load('import/tmn_genesis_stock.xls');
		
		$new_file = '';
		foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
			$worksheetTitle     = $worksheet->getTitle();
			$highestRow         = $worksheet->getHighestRow(); // e.g. 10
			$highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
			$highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn);
			$nrColumns = ord($highestColumn) - 64;
			//$new_file .= "<br>The worksheet ".$worksheetTitle." has ";
			//$new_file .= $nrColumns . ' columns (A-' . $highestColumn . ') ';
			//$new_file .= ' and ' . $highestRow . ' row.';
			//$new_file .= '<br>Data: <table border="1"><tr>';
			for ($row = 1; $row <= $highestRow; ++ $row) {
				for ($col = 0; $col < $highestColumnIndex; ++ $col) {
					$cell = $worksheet->getCellByColumnAndRow($col, $row);
					$val = $cell->getValue();
					$dataType = \PHPExcel_Cell_DataType::dataTypeForValue($val);
					$new_file .= $val.'|';
				}
				$new_file .= "\n";
			}
			//$new_file .= '</table>';
		}
		if(file_put_contents('import/cars.txt', $new_file))
			echo 'XLS convert completed'."\n";
		else {
			echo 'XLS convert error'."\n";
			exit();
		}
		
		$cars = explode("\n", $new_file);
		
		Yii::$app->db->createCommand('UPDATE stock SET public=0 WHERE public=1')->execute();
		foreach ($cars as $car) {
			$car = explode('|', $car);
			if ($car[0] == 'Модель' || !strstr($car[0], 'G') || !$car) continue;
			
			/*
			0 - Модель - GENESIS G70
			1 - Код комплектации - G9S4L8G1KHH173
			2 - VIN-номер - KMTG541ADKU013291
			3 - Код цвета - T5K
			4 - Суффикс - 9S
				5 - Опции - 
				6 - Код модели - 
			7 - Модельный год - 2018
			8 - Дата производства - 01.01.2018 0:00:00
				9 - Модификация - 
			10 - Код салона - NNB
			11 - Статус авто - Склад
			12 - Цена - 2349000
			13 - Минимальная цена на автомобиль - 2349000
			14 - дальше пустые все
			*/
			
			$car_in_base = Stock::find()->where(['vin'=>$car[0]])->one();
			if (!$car_in_base) {
				$car_in_base = new Stock;
				$car_in_base->vin = $car[2];
			}
			
			$car_in_base->model = str_replace('GENESIS ', '', $car[0]);
			$car_in_base->complectation = $car[1];
			$car_in_base->color = $car[3];
			$car_in_base->suffix = $car[4];
			$car_in_base->year = $car[7];
			$car_in_base->location = $car[10];
			$car_in_base->status = $car[11];
			$car_in_base->price = $car[12];
			$car_in_base->public = 1;
				
			$car_in_base->save();
			echo '+1'."\n";
			
		}
		Yii::$app->db->createCommand('DELETE FROM stock WHERE public=0')->execute();
		echo 'Completed'."\n";
		
    }
}
