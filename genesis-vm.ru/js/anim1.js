var isPage1 = $("#luxury").length > 0;

if (isPage1) {
	var anims = [];

	anims.push(new ParallaxAnim(
		'.line-item--101',
		[
			{
				selector: '.line-item__image',
				props: [
					{
						name: 'opacity',
						vals: [0, 0.8]
					}
				],
				end: 30
			}
		],
		'inWindow',
		'half'
	));

	anims.push(new ParallaxAnim(
		'#security',
		[
			{
				selector: '.line-item__image',
				props: [
					{
						name: 'translateX',
						vals: [15, 1]
					}
				],
				start: 'inWindow',
				end: 'outWindow'
			}
		]
	));

	anims.push(new ParallaxAnim(
		'#connectivity',
		[
			{
				selector: '.line-item__image img',
				props: [
					{
						name: 'scale',
						vals: [1.1, 1]
					}
				]
			}
		],
		25,
		'half'
	));

	anims.push(new ParallaxAnim(
		'#dynamics',
		[
			{
				selector: '.line-item__image',
				props: [
					{
						name: 'opacity',
						vals: [0, 0.48]
					}
				],
				end: 30
			}
		],
		'inWindow',
		'half'
	));

	anims.push(new ParallaxAnim(
		'.line-item--205',
		[
			{
				selector: '.line-item__image img',
				props: [
					{
						name: 'scale',
						vals: [1.1, 1]
					}
				]
			}
		],
		20,
		50
	));

	anims.push(new ParallaxAnim(
		'.line-item--209',
		[
			{
				selector: '.line-item__image',
				props: [
					{
						name: 'opacity',
						vals: [0, 1]
					}
				],
				end: 30
			}
		],
		'inWindow',
		'half'
	));

	anims.push(new ParallaxAnim(
		'.line-item--301',
		[{
			selector: '.line-item__image',
			props: [
				{
					name: 'opacity',
					vals: [0, 0.8]
				}
			],
			end: 30
		}],
		'inWindow',
		'half'
	));

	anims.push(new ParallaxAnim(
		'.line-item--302',
		[
			{
				selector: '.line-item__image',
				props: [
					{
						name: 'opacity',
						vals: [0, 1]
					}
				],
				end: 30
			}
		],
		'inWindow',
		'half'
	));

	anims.push(new ParallaxAnim(
		'.line-item--305',
		[
			{
				selector: '.line-item__image img',
				props: [
					{
						name: 'scale',
						vals: [1.1, 1]
					}
				]
			}
		],
		20,
		50
	));
}
