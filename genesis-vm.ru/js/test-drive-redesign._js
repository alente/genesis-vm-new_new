$(document).ready(function () {
	var dropdownCarlisting = $('#dropdown-carlisting'),
		dropdownDealercitylisting = $('#dropdown-dealercitylisting'),
		dropdownDealerlisting = $('#dropdown-dealerlisting');


	function showResult(isSuccess) {
		var $dialog = $('#test-drive-response'),
		$dialogCaption = $dialog.find('h2'),
		$dialogText = $dialog.find('.text');

		if (isSuccess) {
			$dialogCaption.text("Запрос успешно отправлен");
			$dialogText.show();
			$('#name').val('');
			$('#surname').val('');
			$('#phone').val('');
			$('#email').val('');
			$('#agree_rules').prop('checked', false);
		} else {
			$dialogCaption.text("Извините, сервис временно недоступен");
			$dialogText.hide();
		}

		$('.response-wrap').fadeIn();
		setTimeout(function () {
			$('.response-wrap').fadeOut();
		}, 10000);
	}

	function closeDroppers() {
		$('.drop-it-down').each( function () {
			$(this).removeClass('is-dropped');
		});
	}

	function attachNano(selector) {
		$(selector).nanoScroller({
			alwaysVisible: true,
			sliderMinHeight: 45,
			sliderMaxHeight: 45,
			preventPageScrolling: true
		});
	}

	function processDealer(dealerId) {
		var dealer = DealerMapStructure.getDealerByDealerId(dealerId);
		$('.selected-dealer__wrap #selected-dealer').html(dealer[3] + '<br />' + dealer[4]); // address, phone
	}

	function onDropDownItemClick(event) {
		event.preventDefault();
		var $this = $(this).parents('.drop-it-down');
		if (! $(event.target).is('.nano-slider')) {
			if ( $this.hasClass('is-dropped') ) {
				$this.removeClass('is-dropped');
			} else { // close droppers
				closeDroppers();
				$this.addClass('is-dropped');
			}
		}
	}

	function clearDealer() {
		dropdownDealerlisting.find('.dropdown-item.first').addClass('first-active').text('Выберите дилера');
		$('.dealerlistdropdown').removeClass('incorrect');
		$('.selected-dealer__wrap').removeClass('active');
		$('#selected-dealer').html('');
	}

	function onBodyMouseDown(event) {
		var target = $( event.target ),
		dropDownMenuTarget = $('.is-dropped').find('*');
		if ( !target.is($(dropDownMenuTarget)) ) {
			closeDroppers();
		}
	}

	function updateCityCaption() {
		var currentCity = dropdownDealercitylisting.find('.dropdown-item.active').text();
		dropdownDealercitylisting.find('.dropdown-item.first').text(currentCity);
	}

	function updateCityDealers() {
		var cityId = $('#dropdown-dealercitylisting').find('.dropdown-item.active').data('value');
		var findedList = DealerMapStructure.getDealersByCityId(cityId);
		var dealerListTemp = [];
		for (var dealerIndex = 0; dealerIndex < findedList.length; dealerIndex++) {
			var dealer = findedList[dealerIndex];
			dealerListTemp.push('<li class="dropdown-item" data-id="' + dealer[dealer.length - 1] + '" data-code="' + dealer[6] + '">' + dealer[0] +'</li>');
		}
		$('#dealers').html(dealerListTemp.join(''));
		attachNano('#dealersNano');
	}

	function onCityClick(e) {
		e.preventDefault();

		$(this).parent().find('.active').removeClass('active');
		$(this).addClass('active');

		clearDealer();
		updateCityCaption();
		updateCityDealers();
	}

	function onCarClick(e) {
		e.preventDefault();

		$(this).parent().find('.active').removeClass('active');
		$(this).addClass('active');

		var currentCar = dropdownCarlisting.find('.dropdown-item.active').text();
		var currentCarId = dropdownCarlisting.find('.dropdown-item.active').data('id');
		dropdownCarlisting.find('.dropdown-item.first').text(currentCar).attr('data-id', currentCarId);
	}

	function updateDealerCaption() {
		var currentDealer = dropdownDealerlisting.find('.dropdown-item.active').text();
		var currentDealerId = dropdownDealerlisting.find('.dropdown-item.active').data('id');
		dropdownDealerlisting.find('.dropdown-item.first').text(currentDealer).attr('data-id', currentDealerId);
	}

	function onDealerClick(e) {
		e.preventDefault();

		$('.selected-dealer__wrap').addClass('active');
		$(this).parent().find('.active').removeClass('active');
		$(this).addClass('active');
		$('.incorrect').removeClass('incorrect');

		updateDealerCaption();
		processDealer($(this).data('id'));
	}

	function formHasErrors() {
		var hasErrors = false;
		$('.incorrect').removeClass('incorrect');

		var currentDealerId = dropdownDealerlisting.find('.dropdown-item.active').data('id');
		if (typeof currentDealerId == 'undefined') {
			$('.dealerlistdropdown').addClass('incorrect');
			return true;
		}

		$('#name,#surname,#phone,#email').each(function () {
			if ($(this).val() == '') {
					$(this).parent().addClass('incorrect');
					$(this).attr('placeholder','Пожалуйста, введите корректные данные.');
				hasErrors = true;
				return false;
			}
		});

		if (hasErrors) return true;

		var $email = $('#email');
		var vEmail = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}/;
		var Email = $email.val();
		var firstName = vEmail.exec(Email);
		if (!firstName)
		{
			$email.parent().addClass('incorrect');
			document.getElementById('err').style.display = 'block';
			return true;
		}

		if ($('#agree_rules').is(':not(:checked)')) {
			$('.legal-info').addClass('incorrect');
			return true;
		}

		return false;
	}
	$("#name").on('keyup', function(e) {
			var val = $(this).val();
		 if (val.match(/[^A-Za-zА-Яа-яЁё]/g)) {
				 $(this).val(val.replace(/[^A-Za-zА-Яа-яЁё]/g, ''));
		 }
	});
	$("#surname").on('keyup', function(e) {
			var val = $(this).val();
		 if (val.match(/[^A-Za-zА-Яа-яЁё-]/g)) {
				 $(this).val(val.replace(/[^A-Za-zА-Яа-яЁё-]/g, ''));
		 }
	});
	$("#email").on('keyup change', function(e) {
		var val = $(this).val().toLowerCase();
		if (val.match(/[А-Яа-яЁё-]/g)) {
			$(this).val(val.replace(/[А-Яа-яЁё-]/g, ''));
		} else {
			$(this).val(val);
		}
	});
	function sendForm() {
		if (!formHasErrors()) {
			var dealerItem = dropdownDealerlisting.find('.dropdown-item.active');
			var currentDealerCode = dealerItem.data('code');
//			$.post('sendcrm.html',
			$.post('/testdrive',
			{
				name: $('#name').val(),
				surname: $('#surname').val(),
				salutation: $('input[name=mrOrms]:checked').val(),
				city: dropdownDealercitylisting.find('.dropdown-item.active').text(),
				phone: $('#phone').val(),
				email: $('#email').val(),
				model: dropdownCarlisting.find('.dropdown-item.active').attr('data-model'),
				dealer: currentDealerCode
			},
			/*function (response) {
				if (typeof response == 'string') {
					try {
						var json = $.parseJSON(response);
						if (json['answer'] == 'Y') {
							showResult(true);
						} else {
							console.log('Error:', response);
							showResult(false);
						}
					} catch (x) {}
				} else {
					console.log('Error:', response);
					showResult(false);
				}
			}*/
			);
		showResult(true);
		}else{
			console.log('has errors!');
		}
		}
	}

	function initCities() {
		var cities = DealerMapStructure.getAllCities();
		var citiesItems = [];
		for (var index = 0; index < cities.length; index++) {
			var city = cities[index];
			citiesItems.push('<li data-value="' + city[3] +'" class="dropdown-item' + (index == 0 ? ' active' : '') + '">' + city[0] + '</li>');
		}
		$('#cities').html(citiesItems.join(''));
		attachNano('#citiesNano');
		updateCityCaption();
		clearDealer();
		updateCityDealers();
	}

	function initTestDrive() {
		$('#phone').mask("+7 (999)999-99-99");

		initCities();

		$(document).on('click', '.dropdown-item', onDropDownItemClick);
		$(document).on('click', '.arrow_select', onDropDownItemClick);
		$(document).on('mousedown', 'body', onBodyMouseDown);
		$(document).on('click', '#dropdown-dealercitylisting .dropdown-item:not(.first,.divider)', onCityClick);
		$(document).on('click', '#dropdown-dealerlisting .dropdown-item:not(.first,.divider)', onDealerClick);
		$(document).on('click', '#dropdown-carlisting .dropdown-item:not(.first,.divider)', onCarClick);
		$(document).on('click', '.send-req', function (e) {
			e.preventDefault();
			sendForm();
		});
		$('.controls__wrap input').focus(function () {
			$(this).parent().removeClass('incorrect');
		});
		$('#agree_rules').change(function () {
			$('.legal-info').removeClass('incorrect');
		});
		$(document).on('click', '.show-legal', function (e) {
			e.preventDefault();
			$('.legal-wrap').fadeIn();
		});
		$(document).on('click', '.closeIt', function (e) {
			e.preventDefault();
			$('.overlayed').fadeOut();
		});
		$(document).keydown('click', function (e) {
			if (e.keyCode == 27) {
				$('.overlayed').fadeOut();
			}
		});
	}
	initTestDrive();
});
