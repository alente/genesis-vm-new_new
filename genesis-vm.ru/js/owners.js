$(document).ready(function(){

	function formHasErrors() {
		var hasErrors = false;
		$('.incorrect').removeClass('incorrect');

		$('#fName,#lName,#cNumber,#email').each(function () {
			if ($(this).val() == '') {
					$(this).parent().addClass('incorrect');
					$(this).attr('placeholder','Пожалуйста, введите корректные данные.');
				hasErrors = true;
				return false;
			}
		});

		if (hasErrors) return true;

		var $email = $('#email');
		var vEmail = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}/;
		var Email = $email.val();
		var firstName = vEmail.exec(Email);
		if (!firstName)
		{
			$email.parent().addClass('incorrect');
			return true;
		}

		return false;
	}
	$("#fName").on('keyup', function(e) {
			var val = $(this).val();
		 if (val.match(/[^A-Za-zА-Яа-яЁё]/g)) {
				 $(this).val(val.replace(/[^A-Za-zА-Яа-яЁё]/g, ''));
		 }
	});
	$("#lName").on('keyup', function(e) {
			var val = $(this).val();
		 if (val.match(/[^A-Za-zА-Яа-яЁё-]/g)) {
				 $(this).val(val.replace(/[^A-Za-zА-Яа-яЁё-]/g, ''));
		 }
	});
	$("#email").on('keyup change', function(e) {
		var val = $(this).val().toLowerCase();
		if (val.match(/[А-Яа-яЁё-]/g)) {
			$(this).val(val.replace(/[А-Яа-яЁё-]/g, ''));
		} else {
			$(this).val(val);
		}
	});
	function sendForm() {
		if (!formHasErrors()) {
			$.post(
				'sendquestion.html',
				{
					name: $('#fName').val(),
					surname: $('#lName').val(),
					phone: $('#cNumber').val(),
					email: $('#email').val(),
					msg: $('#msg').val()
				},
				function (response) {
					if (response == 'Y') {
						showResult(true);
					} else {
						console.log('Error:', response);
						showResult(false);
					}
				}
			);
		}
	}


	function showResult(res)
	{
		$('.send-question__form').slideUp();
		$('.send-question__submit').slideUp();
		//$('.send-question__text').slideUp();
		$('.send-question__success').slideDown();
	}


	function showForm(res)
	{
		$('.send-question__success').slideUp();
		$('.send-question__form').slideDown();
		$('.send-question__submit').slideDown();
		//$('.send-question__text').slideDown();
	}

	$('#showForm').on('click', function(e){
		e.preventDefault();
		showForm(true);
	});


	$(document).on('submit', '#questionform', function (e) {
			e.preventDefault();
			sendForm();

			//console.log('submit');

			return false;
		});

	$('#questionform input').focus(function () {
			$(this).parent().removeClass('incorrect');
		});

	$('#cNumber').mask("+7 (999)999-99-99");

});