var isPage1 = $("#luxury").length > 0;
var isPage2 = $("#view360").length > 0;

if (isPage1) {
	function simpleAnimate(number, direction) {
		var line = $('.line-item--' + number);
		if (line.length > 0) {
			line.find('.line-item__info').attr('data-aos', 'fade-' + direction);
		}
	}

	simpleAnimate(101, 'up');
	simpleAnimate(102, 'right');
	simpleAnimate(103, 'left');
	simpleAnimate(104, 'right');
	simpleAnimate(201, 'up');
	simpleAnimate(202, 'right');
	simpleAnimate(203, 'left');
	simpleAnimate(204, 'right');
	simpleAnimate(205, 'left');
	simpleAnimate(207, 'right');
	simpleAnimate(208, 'left');
	simpleAnimate(209, 'up');
	simpleAnimate(301, 'down');
	simpleAnimate(302, 'up');
	simpleAnimate(303, 'right');
	simpleAnimate(304, 'left');
	simpleAnimate(305, 'right');
	simpleAnimate(306, 'up');
} else if (isPage2) {
	var columnBlocks = $(".layout__wrapper .collumns .collumns__items");
	for (var columnIndex = 0; columnIndex < columnBlocks.length; columnIndex++) {
		var column = columnBlocks[columnIndex];
		var offset = 0;
		var columnItems = $(column).find(".collumns__item");
		if (columnBlocks.length > 0) {
			for (var columnItemIndex = 0; columnItemIndex < columnItems.length; columnItemIndex++) {
				var columnItem = columnItems[columnItemIndex];
				$(columnItem).attr('data-aos', 'fade-up').attr("data-aos-delay", offset);
				offset += 200;
			}
		}
	}
}

if ($("[data-aos]").length > 0) {
	AOS.init({duration: 1200});
}
