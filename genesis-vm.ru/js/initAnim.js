$(document).ready(function () {
	$('.intro__image.faded').removeClass('faded');
	//no mobiles
	if ($('#mobileDetector').is(':visible')) return;

	if (typeof anims !== 'undefined') {
		window.scene = new AnimScene(anims);
	}
});