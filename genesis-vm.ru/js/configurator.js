$(function(){
	$(document).on('change', 'input[name=trim]', function(){
		console.log($(this));
		console.log($(this).attr('data-price'));
	});
});

var ConfiguratorController = {
	maxSteps: 5,
	$wizard: undefined,
	$priceForm: undefined,
	$steps: undefined,
	$prev: undefined,
	$next: undefined,
	rubSymbol: ' <i class="fa fa-rub" aria-hidden="true"></i>',
	_changeStepDot: function(newIndex) {
		ConfiguratorController.$steps.find('.on').removeClass('on');
		ConfiguratorController.$steps.find(':nth-child(' + (newIndex + 1) + ')').addClass('on');
	},
	getCurrentStepIndex: function() {
		return ConfiguratorController.$wizard.steps("getCurrentIndex");
	},
	onChangeWizardStep: function (currentIndex, priorIndex) {
		ConfiguratorController._changeStepDot(currentIndex);
		// other
		var currentStep = ConfiguratorController.getCurrentStepIndex();
		ConfiguratorController.$prev.toggle(currentStep != 0);
		ConfiguratorController.$prev.text(ConfiguratorController.$wizard.steps("getStep", Math.max(currentStep - 1, 0)).title);
		ConfiguratorController.$next.text(ConfiguratorController.$wizard.steps("getStep", Math.min(currentStep + 1, ConfiguratorController.maxSteps - 1)).title);
	},
	onWizardStepChanged: function(e, currentIndex, priorIndex) {
		ConfiguratorController.onChangeWizardStep(currentIndex, priorIndex);
		$('.actions ul li .nextstep, .actions ul li .prevstep').text( function(i,txt) {return txt.replace(/\d+./,''); });
	},
	gotoStep: function(stepIndex) {
		var curIndex = ConfiguratorController.getCurrentStepIndex(), i;
		if (curIndex > stepIndex) {
			for (i = 0; i < (curIndex - stepIndex); i++) ConfiguratorController.$prev.click();
		} else if (curIndex < stepIndex) {
			for (i = 0; i < (stepIndex - curIndex); i++) ConfiguratorController.$next.click();
		}
	},
	_updateModelLabel: function() {
		var $actualModel = $('.pack.checked label input').attr('data-label');
		$('.view-360__view-actual span').html($actualModel);
	},
	initDefaults: function() {
		ConfiguratorController.$priceForm.find('ul.trim_holder input:radio:first').click();
		ConfiguratorController.$priceForm.find('ul.color_holder input:radio:first').click();
		ConfiguratorController.$priceForm.find('ul.options_holder input:radio:first').click();
		ConfiguratorController.$priceForm.find('ul.interior_holder input:radio:first').click();
		ConfiguratorController._updatePacketsVisibility();
	},
	_updatePacketsVisibility: function() {
		if ($('.pack1 input').prop("checked")) {
			$('.forpack4 input, .forpack5 input').prop("checked", false);
			$('.forpack2, .forpack3').show();
			$('.forpack4, .forpack5').hide();
		} else if ($('.pack2 input').prop("checked") || $('.pack3 input').prop("checked")) {
			$('.forpack2 input, .forpack3 input').prop("checked", false);
			$('.forpack2, .forpack3').hide();
			$('.forpack4, .forpack5').show();
		} else if ($('.pack4 input').prop("checked") || $('.pack5 input').prop("checked") || $('.pack6 input').prop("checked")) {
			$('.forpack2 input, .forpack3 input, .forpack4 input, .forpack5 input').prop("checked", false);
			$('.forpack2, .forpack3, .forpack4, .forpack5').hide();
		}
	},
	init: function () {


		ConfiguratorController.$priceForm = $('.price_form');
		ConfiguratorController.$priceForm.addClass('show');

		// Jquery steps
		ConfiguratorController.$wizard = ConfiguratorController.$priceForm;
		ConfiguratorController.$wizard.steps({
			headerTag: "div",
			bodyTag: "ul",
			transitionEffect: "slideLeft",
			titleTemplate: "#title#",
			onStepChanged: ConfiguratorController.onWizardStepChanged,
			labels: {
				cancel: "Cancel",
				current: "current step:",
				pagination: "Pagination",
				finish: "<span class='finishstep'>Оплата</span>",
				next: "<span class='nextstep'>2. Экстерьер</span>",
				previous: "<span class='prevstep'>Другая модель</span>",
				loading: "Loading ..."
			}
		});

		ConfiguratorController.$steps = ConfiguratorController.$priceForm.find('span.step');
		ConfiguratorController.$steps.prependTo(ConfiguratorController.$priceForm);

		ConfiguratorController.$prev = ConfiguratorController.$wizard.find('.prevstep');
		ConfiguratorController.$next = ConfiguratorController.$wizard.find('.nextstep');

		$('.finishstep').parent().hide();

		$('.finish_list > span > i, span.step span').unbind("click").on("click", function (x) {
			var btn = $(this);
			var stepToGo = parseInt(btn.data("step"), 10);
			ConfiguratorController.gotoStep(stepToGo);
			ConfiguratorController.onChangeWizardStep(stepToGo);
			$('.actions ul li .nextstep, .actions ul li .prevstep').text( function(i,txt) {return txt.replace(/\d+./,''); });
		});

		ConfiguratorController.onChangeWizardStep(0);

		function formatPrice(price) {
			return price.reverse().replace(/((?:\d{2})\d)/g, '$1 ').reverse();
		}

		String.prototype.reverse = function () {
			return this.split('').reverse().join('');
		};

		if ($('.pack1 input').prop("checked")) {
			$('.forpack2, .forpack3').show();
			$(this).parent().find('.need_show').show();
		}

		var values = {};
		// When we change checked radio button
		ConfiguratorController.$priceForm.find('input:radio').change(function () {
			ConfiguratorController._updatePacketsVisibility();
			var name = $(this).attr('name');
			values[name] = parseFloat($(this).data('price'));
			var total = 0.0;
			$.each(values, function (i, e) {
				total += e;
			});
			var $formated_price = formatPrice(total.toFixed(0));
			ConfiguratorController.$priceForm.find('#total').text($formated_price).append(ConfiguratorController.rubSymbol);
			$('.view-flexbox').find('#left-total').text($formated_price).append(ConfiguratorController.rubSymbol);
		});

		ConfiguratorController.$priceForm.find('input:checkbox').change(function () {
			ConfiguratorController._updatePacketsVisibility();
			var name = $(this).attr('name');

			var packs = 0;
			$.each($(this).parents('ul').find('.checked').find('input'), function (i, e) {
				packs += parseFloat($(e).data('price'));
			});

			values[name] = parseFloat(packs);
			var total = 0.0;
			$.each(values, function (i, e) {
				total += e;
			});
			var $formated_price = formatPrice(total.toFixed(0));
			ConfiguratorController.$priceForm.find('#total').text($formated_price).append(ConfiguratorController.rubSymbol);
			console.log('2 - ' + $formated_price);
			$('.view-flexbox').find('#left-total').text($formated_price).append(ConfiguratorController.rubSymbol);
		});

		ConfiguratorController.$priceForm.find('ul.trim_holder input:radio').change(function () {
			$('.finish_trim-holder').children('li').remove();
			$(this).parents('li').clone().appendTo('.finish_trim-holder');
			$('.finish_trim-holder .need_show').attr('style', '').show().css('margin', 0).find(':not(.show_summary)').hide();
		});

		ConfiguratorController.$priceForm.find('ul.color_holder input:radio').change(function () {
			$('.finish_color-holder').children('li').remove();
			$(this).parents('li').clone().appendTo('.finish_color-holder');
		});

		ConfiguratorController.$priceForm.find('ul.options_holder input:radio').change(function () {
			$('.finish_options-holder').children('li').remove();
			$(this).parents('li').clone().appendTo('.finish_options-holder');
		});

		ConfiguratorController.$priceForm.find('ul.interior_holder input:radio').change(function () {
			$('.finish_interior-holder').children('li').remove();
			$(this).parents('li').clone().appendTo('.finish_interior-holder');
		});

		// spec on trim_holder price
		ConfiguratorController.$priceForm.find('.trim_holder').find('input:radio').change(function () {
			var trimvalues = {};
			var name = $(this).attr('name');
			trimvalues[name] = parseFloat($(this).data('price'));
			var total = 0.0;
			$.each(trimvalues, function (i, e) {
				total += e;
			});
			var $formated_price = formatPrice(total.toFixed(0));
			ConfiguratorController.$priceForm.find('#pricetotal').text($formated_price).append(ConfiguratorController.rubSymbol);

			ConfiguratorController._updateModelLabel();
		});

		// spec on options_holder price
		ConfiguratorController.$priceForm.find('.options_holder').find('input:radio').change(function () {
			var optionsvalues = {};
			var name = $(this).attr('name');
			optionsvalues[name] = parseFloat($(this).data('price'));
			var total = 0.0;
			$.each(optionsvalues, function (i, e) {
				total += e;
			});
			var $formated_price = formatPrice(total.toFixed(0));
			ConfiguratorController.$priceForm.find('#optionstotal').text($formated_price).append(ConfiguratorController.rubSymbol);
		});

		ConfiguratorController.$priceForm.find('input').click(function () {
			if ($(this).closest('.pack').hasClass('checked')) {
				var liElement = $(this).closest('.pack');
				liElement.find('.need_show').slideToggle(300);
				$(this).closest('.pack').toggleClass("pre");
			} else {
/*
				if($(this).parents('ul').hasClass('options_holder')){
					if($(this).parents('li').hasClass('forpack1')){
						$(this).parents('ul').children('li').removeClass('pre').removeClass('checked').find('.need_show').hide("100");
						$(this).parents('li').addClass('checked').find('.need_show').slideDown("100");

						$('.finish_options-holder').children('li').remove();
					} else {
						if($(this).parents('li').hasClass('checked')){
							if($(this).parents('ul').find('.checked').length <= 1){
								$(this).parents('ul').find('.forpack1').addClass('pre').addClass('checked');

								$('.finish_options-holder').children('li').remove();
								$(this).parents('ul').find('.forpack1').clone().appendTo('.finish_options-holder');
							} else {
								$('.finish_options-holder').find('.' + $(this).parents('li').attr('class').split(' ')[0]).remove();
							}

							$(this).parents('li').removeClass('pre').removeClass('checked').find('.need_show').hide("100");
						} else {
							$(this).parents('ul').find('.forpack1').removeClass('pre').removeClass('checked');
							$(this).parents('li').addClass('checked').find('.need_show').slideDown("100");
						}
					}
				} else {
*/
					$(this).parents('ul').children('li').removeClass('pre').removeClass('checked').find('.need_show').hide("100");
					$(this).parents('li').addClass('checked').find('.need_show').slideDown("100");
//				}
			}

			var liel = $(this).closest('li.exterior-interior__color');
			if (liel.length > 0) {
				if (liel.hasClass("color__item") && typeof liel.data("color") !== 'undefined') {
					if (typeof View360 !== 'undefined') {
						View360.setColor(liel.data("color"));
					}
				}
			}
		});



		// scroll to pack
		$('.pack').click(function() {
			var container = $('.content'),
		    scrollTo = $(this),
		    doc = $(document);
		    if ($(window).width() > 768) {
				setTimeout(function() {
					container.animate({
						scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
					});
				}, 400);
		    } if ($(window).width() < 768) {
		    	setTimeout(function() {
					$('body').animate({
						scrollTop: scrollTo.offset().top - 100
					});
				}, 400);
		    }
		});


		// When page load
		ConfiguratorController.$priceForm.find('input:radio').each(function () {
			if ($(this).is(':checked')) {
				// $(this).parents('ul').children('li').removeClass('checked').find('.need_show').hide("100");
				// $(this).parents('li').addClass('checked').find('.need_show').show("100");
				if ($(this).parents('ul').is('.trim_holder')) {
					$(this).parents('ul').children('li').removeClass('checked').find('.need_show').hide("100");
					$(this).parents('li').addClass('checked').find('.need_show').show("100");
					$(this).parents('li').clone().appendTo('.finish_trim-holder');
					$('.finish_trim-holder .need_show').attr('style', '').show().css('margin', 0).find(':not(.show_summary)').hide();
				}
				if ($(this).parents('ul').is('.color_holder')) {
					$(this).parents('ul').children('li').removeClass('checked').find('.need_show').hide("100");
					$(this).parents('li').addClass('checked').find('.need_show').show("100");
					$(this).parents('li').clone().appendTo('.finish_color-holder');
				}
				if ($(this).parents('ul').is('.options_holder')) {
					$(this).parents('li').addClass('checked').find('.need_show').show("100");
					$(this).parents('li').clone().appendTo('.finish_options-holder');
				}
			}
		});

		// spec on trim_holder price
		ConfiguratorController.$priceForm.find('input[name=trim]:checked').each(function () {
			var name = $(this).attr('name');
			values[name] = parseFloat($(this).data('price'));
			var total = 0.0;
			$.each(values, function (i, e) {
				total += e;
			});
			var $formated_price = formatPrice(total.toFixed(0));
			console.log('3 - ' + $formated_price);
			$('.view-flexbox').find('#left-total').text($formated_price).append(ConfiguratorController.rubSymbol);
			ConfiguratorController.$priceForm.find('#pricetotal').text($formated_price).append(ConfiguratorController.rubSymbol);
			ConfiguratorController.$priceForm.find('#total').text($formated_price).append(ConfiguratorController.rubSymbol);
		});

		ConfiguratorController.initDefaults();
	}
};

$(document).ready(function () {
	ConfiguratorController.init();
	$('.actions ul li .nextstep').text( function(i,txt) {return txt.replace(/\d+./,''); });
	$('.pack').find('.need_show').hide();
	$('.pack1').addClass('pre');
	$('.price_form .actions .nextstep, .price_form .actions .prevstep').on("click", function(){
			$('.price_form .content').scrollTop(0);
	});

});