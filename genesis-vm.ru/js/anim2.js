var isPage2 = $("#view360").length > 0;

if (isPage2) {
	var anims = [];

	anims.push(new ParallaxAnim(
		'.line-item--803',
		[
			{
				selector: '.line-item__image',
				props: [
					{
						name: 'translateX',
						vals: [-100, 0]
					}
				]
			}
		],
		20,
		50
	));
}