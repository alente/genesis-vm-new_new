<?php

/**
 * Developer: Andrew Karpich
 * Date: 12.09.2016 15:16
 */
class Curl {

	public $debug = false;

	public $timeout = 30;

	protected $ch;

	protected $countRequests;

	protected $_requestHeaders;

	protected $_responseHeaders;

	public function __construct($headers = null, $debug = false){
		$this->debug = $debug;

		$this->ch = curl_init() or die("NO CURL!");

		curl_setopt($this->ch, CURLOPT_HEADER, 1);
		curl_setopt($this->ch, CURLOPT_ENCODING, 'deflate');
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);
		curl_setopt($this->ch, CURLOPT_TIMEOUT, $this->timeout);
		curl_setopt($this->ch, CURLINFO_HEADER_OUT, true);

		$this->setHeaders($headers);
	}

	function post($url, $data = false){
		curl_setopt($this->ch, CURLOPT_URL, $url);
		curl_setopt($this->ch, CURLOPT_POST, 1);
		curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');

		if($data){
			$data = (is_array($data)) ? http_build_query($data) : $data;

			$this->_requestHeaders['Content-Length'] = 'Content-Length: ' . strlen($data);

			curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
		} else {

			$this->_requestHeaders['Content-Length'] = 'Content-Length: 0';

		}

		if($this->debug){

			//SystemLog::get()->addLog('Request URL', $url);

			/*if($data){
				if(is_array($data))
					SystemLog::get()->addLog('Request body', implode('; ', $data));
				else
					SystemLog::get()->addLog('Request body', $data);
			}*/
		}

		return $this->exec();
	}

	function get($url){
		curl_setopt($this->ch, CURLOPT_URL, $url);
		curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($this->ch, CURLOPT_HTTPGET, 1);

		if($this->debug){
			//SystemLog::get()->addLog('Request URL', $url);
		}

		return $this->exec();
	}

	function exec(){

		if(!empty($this->_requestHeaders)){

			curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->_requestHeaders);

			//if ($this->debug) SystemLog::get()->addLog('Request headers', implode("; ", $this->_requestHeaders));
		}

		$res = curl_exec($this->ch);

		$i = curl_getinfo($this->ch);

		//$err = curl_error($this->ch);
		//$errNo = curl_errno($this->ch);

		$rawHeaders = substr($res, 0, $i['header_size']);
		$body = substr($res, $i['header_size']);

		foreach(explode("\r\n", $rawHeaders) as $i => $rawHeader){
			$rawHeader = trim($rawHeader);
			if($rawHeader){
				if($i === 0) $this->_responseHeaders['code'] = $rawHeader;
				else {
					$rawHeaderChunks = explode(': ', $rawHeader);
					if(!isset($rawHeaderChunks[1])) $rawHeaderChunks[1] = '';
					$this->_responseHeaders[ $rawHeaderChunks[0] ] = $rawHeaderChunks[1];
				}
			}
		}

		/*if($this->debug){
			SystemLog::get()->addLog('Curl Error', $err);
			SystemLog::get()->addLog('Curl Error No', $errNo);
			SystemLog::get()->addLog('Response headers', $rawHeaders);
			SystemLog::get()->addLog('Response body', $body);
		}*/

		$this->countRequests++;

		return $body;
	}

	public function setHeaders($headers){
		$this->_requestHeaders = $headers;
	}

	public function getResponseHeaders(){
		return $this->_responseHeaders;
	}

	public function getRequestCount(){
		return $this->countRequests;
	}
}