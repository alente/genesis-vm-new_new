<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');
//defined('SITE_DIR') or define('SITE_DIR', '/ru/ru');

error_reporting(E_ALL & ~E_DEPRECATED & ~E_WARNING & ~E_NOTICE);

//ini_set('memory_limit', '196M');

require(realpath(__DIR__ . '/..') . '/vendor/autoload.php');
require(realpath(__DIR__ . '/..') . '/vendor/yiisoft/yii2/Yii.php');

$config = require(realpath(__DIR__ . '/..') . '/config/web.php');

(new yii\web\Application($config))->run();
